      program refarm
*
*     implicit integer (a-z)
      implicit none
      character*80 title
      character*128 fname
      character*20 name(20), rname
      real*8 atry,e,etarg,etargr,d0,sce,xsn,fl,xsnl
      integer i,nelc,nz,starg,ltarg,iind,jind,ichl,lchl,matr,
     1        mati,ncol,icases,itype,ntarg,nspn2,npty2,lrgl2,
     1        ich,numen,nchan,ie,nop2,nopen,iout,ion,ij,j,ix,
     1        ixx,nx,nx1,nx2,il,nl,nloop,ilp,jup,nxsn,nume,iin,
     1        maxst,maxne,maxch,maxnl,nc2,mxnxsn
*
*    read a binary FARM file and print
*
      parameter (maxst=10,maxne=10000,maxch=50,maxnl=20)
      parameter (nc2=(maxch*(maxch+1))/2)
      parameter (mxnxsn=(maxst*(maxst+1))/2)
      parameter (atry=2.0d0)
      common /io/ iin,iout
      COMMON /XXX/ etarg(maxst),starg(maxst),ltarg(maxst),sce(maxne),
     1 xsn(mxnxsn,maxne), iind(mxnxsn),jind(mxnxsn),
     1 ichl(maxch),lchl(maxch),matr(nc2),mati(nc2),
     1 fl(maxnl),xsnl(maxnl,maxne,mxnxsn)
*
c     dimension etarg(maxst),starg(maxst),ltarg(maxst)
c     dimension sce(maxne), xsn(mxnxsn,maxne)
c     dimension iind(mxnxsn),jind(mxnxsn)
c     dimension ichl(maxch),lchl(maxch),matr(nc2),mati(nc2)
c     dimension fl(maxnl),xsnl(maxnl,maxne,mxnxsn)
      data name(1),name(3),name(4),name(5),name(6),name(7)
     x  /' K-MATRIX',' T-MATRIX',' X-SECTION ',' OMEGA ',
     x   ' X-SECT-FOR-TOPUP',' OMEGA-FOR-TOPUP '/
*
      ncol = 5
      read(5,231)fname
  231 format(A128)
      iout = 6
      open (7,file = fname,access = 'sequential',
     x      status = 'old',form = 'unformatted')
      read (7) rname, title
      write(iout,1000) title, rname, fname
      do 10 i = 1,10
         if (rname .eq. name(i)) itype = i
   10 continue
      read (7) nelc,nz,ntarg,(etarg(i),i=1,ntarg),
     x         (ltarg(i),i=1,ntarg),(starg(i),i=1,ntarg)
      if (maxst .lt. ntarg) then
         write(iout,'('' **** dimension overflow'')')
         write(iout,'('' maxst must be .gt. '',i3)') ntarg
         stop
      end if
      write(iout,1005)
      d0 = etarg(1)
      do 15  i=1,ntarg
         etargr = (etarg(i)-d0) * atry
         write (iout,1010) i,ltarg(i),starg(i),etargr
 15   continue
*
*  read K- or T-matrices
*
      if (itype .eq. 1 .or. itype .eq. 3) then
         do 16 icases = 1,100000
            read (unit=7,end=50) nspn2,lrgl2,npty2,numen,nchan,
     x                        (ichl(ich),lchl(ich),ich=1,nchan)
            if (maxch .lt. nchan) then
               write(iout,'('' **** dimension overflow'')')
               write(iout,'('' maxch must be .gt. '',i4)') nchan
               stop
            end if
            write (iout, 1011) nspn2,lrgl2,npty2
            write (iout, 1012)
            write (iout, 1013) (i,ichl(i),lchl(i),i=1,nchan)
            if (itype .eq. 1) then
               do 20 ie = 1,numen
                  read(7) e,nopen,nop2,(matr(i),i=1,nop2)
                  write(iout,1020) e,nopen
                  write (iout,1021)
                  call wrttmt (matr,nopen,ncol)
   20          continue
            else
               do 30 ie = 1,numen
                  read(7) e,nopen,nop2,(matr(i),mati(i),i=1,nop2)
                  write(iout,1020) e,nopen
                  write (iout,1031)
                  call wrttmt (matr,nopen,ncol)
                  write (iout,1032)
                  call wrttmt (mati,nopen,ncol)
   30          continue
            end if
   16    continue
   50    continue
*
*  read cross-sections or collision strengths
*
      else if (itype .le. 7) then
*
*  calculate initial and final target index for each x-section
*
         ion = nz - nelc
         ij = 0   
         do 60 i = 1,ntarg
            jup = i
            if (ion .gt. 0) jup = i-1
            do 60 j = 1,jup
               ij = ij + 1
               iind(ij) = i
               jind(ij) = j
   60       continue
*
         if (itype .eq. 4) write(iout,1045)
         if (itype .eq. 5) write(iout,1046) 
         if (itype .le. 5) then
            read(7) nxsn,nume
            if (maxne .lt. nume) then
               write(iout,'('' **** dimension overflow'')')
               write(iout,'('' maxne must be .gt. '',i4)') nume
               stop
            end if
            read(7) (sce(ie),ie=1,nume)
            read(7) ((xsn(ix,ie),ix=1,nxsn),ie=1,nume)
*
*   print all transitions 
*
           do 65  ix=1,nxsn,ncol
              ixx = min(ix+ncol-1,nxsn)
              write(6,1050)(jind(ij),iind(ij),ij=ix,ixx)
              do 70  ie=1,nume
                 write(6,1051) ie,sce(ie),(xsn(nx,ie),nx=ix,ixx)
   70         continue
   65      continue
*
         else if (itype .le. 7) then
            if (itype .eq. 6) write(iout,1055)
            if (itype .eq. 7) write(iout,1056)
            read(7)nxsn,nume,nl
            if (maxne .lt. nume) then
               write(iout,'('' **** dimension overflow'')')
               write(iout,'('' maxne must be .gt. '',i4)') nume
               stop
            end if
            if (maxnl .lt. nl) then
               write(iout,'('' **** dimension overflow'')')
               write(iout,'('' maxnl must be .gt. '',i4)') nl
               stop
            end if
            read(7)(sce(ie),ie = 1,nume)
            read(7)(fl(il),il = 1,nl)
            read(7)(((xsnl(il,ie,ix), ix = 1,nxsn),ie = 1,nume),
     x              il = 1,nl)
*
            do 80 ie = 1, nume
               write(iout,1057) ie,sce(ie)
               nloop = (nxsn + 4) / 5
               nx2 = 0
               do 85 ilp = 1, nloop
                  nx1 = nx2 + 1
                  nx2 = min (nx1 + 4, nxsn)
                  write(iout,1058) (jind(ix),iind(ix),ix = nx1,nx2)
                  do 90 il = 1,nl
                     write(iout,1059)fl(il),
     x                     (xsnl(il,ie,ix), ix = nx1,nx2)
   90             continue
   85           continue
   80        continue
         end if
      else 
         write(iout,1080) itype
      end if
      stop
 999  format(' dimension failure - nxsn =',i4,' nume =',
     x       i5,/' corresponding dimensions are',i4,i5)
 1000 format(1x,a80//' Filetype ',a15,' from file ',a128)
 1005 format(20x, 'target states'/
     x       20x, '*************',//
     x       10x,'index',5x,'total l',3x,'(2*s+1)',8x,'energy'/
     x       43x,'scaled ryd')
 1010 format(3x,3i10,7x,f12.6)
 1011 format (/,15x,'****************',/,15x,'s l p =',3i3,/
     x           15x,'****************', /)
 1012 format (12x,'channel',2x,'target',4x,'small'/
     x        12x,'index ',3x,'index  ',5x,'l'/)
 1013 format (7x,i8,i9,i10)
 1020 format (1x,'e = ',f9.5,' scaled ryd.  no. of open chans. = ',i4)
 1021 format (/,' k-matrix (lower half):')
 1031 format (/,' real part of t-matrix (lower half)')
 1032 format (/,' imag part of t-matrix (lower half)')
 1045 format (//,15x,'TOTAL CROSS-SECTIONS in units of a0**2'/,
     x           15x,'**************************************',/)
 1046 format (//,15x,'TOTAL COLLISION STRENGTHS'/,
     x           15x,'*************************',/)
 1050 format (//,4x,'i',3x,'e(scld ryd )',2x,6(i3,'-',i3,5x))
 1051 format (1x,i4,2x,d15.8,6d15.8)
 1055 format (//,15x,'CROSS-SECTIONS in units of a0**2'/,
     x           15x,'********************************',/)
 1056 format (//,15x,'COLLISION STRENGTHS'/,
     x           15x,'*******************',/)
 1057 format(/1x,i3,' energy =',d13.5)
 1058 format(4x,'L',3x,5(i3,'-',i3,5x))
 1059 format(1x,f7.1,5d15.8)
 1080 format (' ******** cannot read this file -  itype = ',I5)
*
      end
      subroutine wrttmt (a,n,ncol)
*
* $Id: wrttmt.f,v 1.1 93/04/22 14:25:25 vmb Exp Locker: vmb $
*
      implicit integer (a-z)
      real*8 a
      common /io/ iin,iout
      dimension a(*)
*
*     wrttmt : prints an n*n matrix stored in a lower triangular array
*
      nf = 0
      do 40 k = 1, n
         ntim = k / ncol
         if ( ntim .gt. 0 ) then
            do 20 i = 1, ntim
               ni = nf + 1
               nf = nf + ncol
               write (iout,1000) (a(kk),kk = ni,nf)
   20       continue
         endif
         ires = mod(k,ncol)
         if ( ires .ge. 1 ) then
            ni = nf + 1
            nf = nf + ires
            write (iout,1000) (a(kk),kk = ni,nf)
         endif
         write (iout,1010)
   40 continue
      return
*
 1000 format (7d13.5)
 1010 format (/)
      end
