      program hfread
      implicit integer (a-z)
      character*128 filh,filhf
      real*8 x,bbloch,rmatr,cf,eig,wmat,etarg, cfbut
      parameter (ntar=50,lra=100,nch=100,lam=14,nst=14000)
      dimension etarg(ntar),ltarg(ntar),starg(ntar),cfbut(3,lra)
      dimension nltarg(ntar),lchl(nch),ichl(nch),
     x          cf(nch,nch,lam),eig(nst),wmat(nch,nst)
*
      data filh/'H'/,filhf/'HF'/
      data nfth/80/,nfthf/81/
*
*  read first record on  HF 
*
      open (nfthf, file = filhf, access = 'sequential', 
     x      status ='old', form = 'formatted')
      read (nfthf,1010) nelc,nz,lrang2,lamax,ntarg,rmatr,bbloch
      write(6,*) nelc,nz,lrang2,lamax,ntarg,rmatr,bbloch
*
      if (ntarg .gt. ntar .or. lrang2 .gt. lra) then
         write(6,3000)
         stop
      end if
*
      read (nfthf,1020) (etarg(i),i=1,ntarg)
      print *,'A',etarg(ntarg)
      read (nfthf,1030) (ltarg(i),i=1,ntarg)
      print *,'B'
      read (nfthf,1030) (starg(i),i=1,ntarg)
      print *,'C'
      read (nfthf,1020) ((cfbut(i,j),i=1,3),j=1,lrang2)
      print *,'D'

*
*    open  H and write target part
*
      open (nfth, file = filh, access = 'sequential', 
     x      status ='new', form = 'unformatted')
*
      write (nfth) nelc,nz,lrang2,lamax,ntarg,rmatr,bbloch
      write (nfth) (etarg(i),i=1,ntarg)
      write (nfth) (ltarg(i),i=1,ntarg)
      write (nfth) (starg(i),i=1,ntarg)
      write (nfth) ((cfbut(i,j),i=1,3),j=1,lrang2)
*
 10   read (nfthf,1030, end=90) lrgl2,nspn2,npty2,nchan,nstat,more2
      print *,'E'
*
      write (6, 2000) nspn2,lrgl2,npty2
      write (6,*)nchan,nstat,more2
*
      if (nchan .gt. nch  .or.  nstat .gt. nst) then
         write(6,3000)
         stop
      end if
*
*  read channel information for current SLPI case 
*
      read (nfthf,1030) (nltarg(i),i=1,ntarg)
      print *,'F'
      read (nfthf,1030) (lchl(i),i=1,nchan)
      print *,'G'
      read (nfthf,1020) (((cf(i,j,k),i=1,nchan),j=1,nchan),k=1,lamax)
      print *,'H'
      read (nfthf,1020) (eig(i),i=1,nstat)
      print *,'I'
      read (nfthf,1020) ((wmat(j,i),j=1,nchan),i=1,nstat)
      print *,'J'
*
      write (nfth) lrgl2,nspn2,npty2,nchan,nstat,more2
*
*    write arrays for current case
*
      write (nfth) (nltarg(i),i=1,ntarg)
      write (nfth) (lchl(i),i=1,nchan)
      write (nfth) (((cf(i,j,k),i=1,nchan),j=1,nchan),k=1,lamax)
      write (nfth) (eig(i),i=1,nstat)
      write (nfth) ((wmat(j,i),j=1,nchan),i=1,nstat)
      if (more2 .ge. 1) go to 10
*
 90   stop
*
 1010 format(5i3,2d21.13)
 1020 format(5d21.13)
 1030 format(20i5)
 2000 format (/,15x,'****************',/,15x,'s l p =',3i3,/
     x           15x,'****************', /)
 3000 format(' ** dimension overflow **')
       end
