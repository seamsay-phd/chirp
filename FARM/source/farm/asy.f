      program asy
*
* $Id: asy.f,v 2.1 94/09/29 18:47:35 vmb Exp Locker: vmb $
*
*
*     main program for FARM
*
      implicit integer (a-z)
      real*8 x
      parameter (icore = 10000000, iread = 5)
      dimension x(icore), ix(icore)
      common /io/ iin,iout
      equivalence (x(1),ix(1))
*
      iin = iread
      call cntrl (x,ix,icore)
      stop
*
      end
      function iadtwp(i)
*
* $Id: iadtwp.f,v 2.1 94/09/29 18:46:25 vmb Exp Locker: vmb $
*
      implicit integer (a-z)
      iadtwp = i/2+1
c     iadtwp = i
      return
      end

      integer function wpadti (i)
*
* $Id: wpadti.f,v 2.1 94/09/29 18:46:26 vmb Exp Locker: vmb $
*
      implicit integer (a-z)
*     wpadti = i
      wpadti = i + i - 1
      return
      end
