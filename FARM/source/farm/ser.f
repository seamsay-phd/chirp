      subroutine compc(lrgl2,nspn2,npty2,lrgl,lrgs,lrgpi,ncases,iflg)
*
* $Id: compc.f,v 2.1 94/09/29 18:45:07 vmb Exp Locker: vmb $
*
      dimension lrgl(ncases),lrgs(ncases),lrgpi(ncases)
*
*    determine whether current block of data corresponds to a required case
*
      do 10  i=1,ncases
         if (lrgl(i) .eq. lrgl2 .and. lrgs(i) .eq. nspn2 .and. 
     &       lrgpi(i) .eq. npty2) then
            lrgl(i) = -1
            iflg = 0
            go to 20
         end if
 10   continue
      iflg = -1
 20   return
      end
      subroutine fincas(maxcas,lrgs,lrgl,lrgpi,ncases,ix,mcore)
*
* $Id: fincas.f,v 2.1 94/09/29 18:44:59 vmb Exp Locker: vmb $
*
      implicit integer (a-z)
c
c     dummy routine for use with serial H file
c
      dimension ix(mcore)
      return
      end
      subroutine rdch (nfth,nltarg,lchl,ichl,cf,eig,wmat,ntarg,nstat,
     &                nchan,n2,lamax,rmatr,ion,iprint,x,ix,mcor1)
*
* $Id: rdch.f,v 2.1 94/09/29 18:45:00 vmb Exp Locker: vmb $
*
      implicit integer (a-z)
      real*8 cf,eig,wmat,rmatr,fac,flion,x,one,two,dfloat
      common /io/ iin,iout
      dimension nltarg(ntarg),lchl(nchan),ichl(nchan),
     x          cf(n2,lamax),eig(nstat),wmat(nstat,nchan),
     x          ix(mcor1),x(mcor1)
      parameter ( one=1.0d0,two=2.0d0)
*
*    read arrays for current case
*
      read (nfth) (nltarg(i),i=1,ntarg)
      read (nfth) (lchl(i),i=1,nchan)
      read (nfth) ((cf(i,j),i=1,n2),j=1,lamax)
      read (nfth) (eig(i),i=1,nstat)
      read (nfth) ((wmat(i,j),j=1,nchan),i=1,nstat)
      print *,'INPUT DATA'
      print *,'nltarg',(nltarg(i),i=1,ntarg)
      print *,'lchl',(lchl(i),i=1,nchan)
      print *,'cf(i,j)',((cf(i,j),i=1,n2),j=1,lamax)
      print *,'eig ',(eig(i),i=1,nstat)
      print *,'wmat',((wmat(i,j),j=1,nchan),i=1,nstat)
*
*    divide wmat by sqrt 2*rmatr for consistency with molecular code
*    and  scale for positive ions
*
      if (ion .gt. 0) then
         flion = dfloat (ion)
         fac = one / sqrt (two * rmatr * flion)
      else
         fac = one / sqrt (two * rmatr) 
      end if
      do 5 j = 1,nchan
         do 5 i = 1,nstat
            wmat(i,j) = fac * wmat(i,j)
 5    continue
*
      i = 0
      do 10 it  = 1,ntarg
         nt = nltarg(it)
         do 20 n = 1,nt
            i = i+1
            ichl(i) = it
	    print *,i,ichl(i),lchl(i)
 20      continue
 10   continue
      if (iprint .gt. 0) then
         write (iout, 1000)
         write (iout, 1010) (i,ichl(i),lchl(i),i=1,nchan)
      end if
c
      if (ion .gt. 0) then
         fac = one / (flion * flion)
         do 30  i = 1,nstat
            eig(i) = fac * eig(i)
 30      continue
         fac = one
         do 40  lam = 2,lamax
            fac = fac * flion
            do 50  i = 1,n2
                  cf(i,lam) = fac * cf(i,lam)
 50         continue
 40      continue
      end if
 1000 format (12x,'channel',2x,'target',4x,'small'/
     x        12x,'index ',3x,'index  ',5x,'l'/)
 1010 format (7x,i8,i9,i10)
      return
      end
      subroutine rdhed(nfth,filh,nelc,nz,lrang2,ismax,ntarg,rmatr,
     x                 bbloch)
*
* $Id: rdhed.f,v 2.1 94/09/29 18:44:56 vmb Exp Locker: vmb $
*
      implicit integer (a-z)
      real*8 rmatr,bbloch
      character*48 filh
c
c     open a serial H file and read the opening few constants
c
      open (nfth, file = filh, access = 'sequential', 
     x      status ='old', form = 'unformatted')
      read (nfth) nelc,nz,lrang2,ismax,ntarg,rmatr,bbloch
c
      return
      end
      subroutine rdhslp(nfth,ncases,icase,lrgl,lrgs,lrgpi,lrgl2,
     x                  nspn2,npty2,nchan,nstat,iflg,ix,mcore)
*
* $Id: rdhslp.f,v 2.1 94/09/29 18:44:57 vmb Exp Locker: vmb $
*
      implicit integer (a-z)
      dimension lrgl(ncases),lrgs(ncases),lrgpi(ncases),ix(*)
c
c     reads the first few constants for a particular lrgl2,nspn2
c     npty2 when ncases =0 and when ncases > 0
c      
 50   read (nfth, end=90) lrgl2,nspn2,npty2,nchan,nstat,more2
      if (ncases .ne. 0) then
         call compc (lrgl2,nspn2,npty2,lrgl,lrgs,lrgpi,
     x               ncases,iflg)
         if (iflg .lt. 0) then 
            read (nfth)
            read (nfth)
            read (nfth)
            read (nfth)
            read (nfth)
            go to 50
         end if
      end if
      return
c
 90   iflg=-1
      return
c
      end
      subroutine rdtar(nfth,etarg,ltarg,starg,cfbut,ntarg,lrang2,ion,
     x                 nbutx,maxcas,itlast,ix,mcor1)
*
* $Id: rdtar.f,v 2.1 94/09/29 18:45:04 vmb Exp Locker: vmb $
*
      implicit integer (a-z)
      real*8 etarg, cfbut, atry, etargr, d0, one, flion2, scale,dfloat
      parameter ( one=1.0d0, atry=2.0d0 )
      common /io/ iin,iout
      dimension etarg(ntarg),ltarg(ntarg),starg(ntarg),cfbut(3,lrang2),
     x          ix(mcor1)
*
      maxcas=0
*
*     maxcas is the total number of cases on a direct access H file
*     the value 0 is used to distinguish serial from direct access
*
      read (nfth) (etarg(i),i=1,ntarg)
      read (nfth) (ltarg(i),i=1,ntarg)
      read (nfth) (starg(i),i=1,ntarg)
      read (nfth) ((cfbut(i,j),i=1,3),j=1,lrang2)
      write (iout,1000)
      if (ion .gt. 0) then
         flion2 = dfloat (ion * ion)
         scale = one / flion2 
         do 5  i=1,ntarg
            etarg(i) = scale * etarg(i)
 5       continue
      end if
      d0 = etarg(1)
      do 10  i=1,ntarg
         etargr = atry * (etarg(i) - d0)
         write (iout,1010) i,ltarg(i),starg(i),etargr
 10   continue
      nbutx = 0
      do 20  i=1,lrang2
         nbutx = max( nbutx, -int(cfbut(3,i))/10000)
 20   continue
      if (ion .gt. 0 .and. nbutx .eq. 0) then
         scale = one
         do 25  m = 2,3
            scale = scale * flion2
            do 30  l = 1,lrang2
               cfbut(m,l) = scale * cfbut(m,l)
 30         continue
 25      continue
      end if
 1000 format(20x, 'target states'/20x,'*************',//
     x       10x,'index',5x,'total l',3x,'(2*s+1)',8x,'energy'/
     x       43x,'scaled ryd')
 1010 format(3x,3i10,7x,f12.6)
      return
      end
