      subroutine lwint (nmax,mcore,pt0)
*
* $Id: lwint.f,v 2.1 94/09/29 18:44:00 vmb Exp Locker: vmb $
*
      implicit integer (a-z)
      common /io/ iin,iout
      common /lw/ vmat,enrg,tc,tev,tp,tsave,pdiag,r4,space,tpev,b1,
     x		  rmtx,cupest,ipvt
*
      nm2 = nmax * nmax
*
      vmat = pt0
      enrg = vmat + (nm2+nmax)/2
      tc = enrg + nmax
      tev = tc + nm2
      tp = tev + nm2
      tsave = tp + nm2
      pdiag = tsave + nm2
      r4 = pdiag + 4 * nmax
      space = r4 + nm2
      tpev = space + 3 * nm2
      b1 = tpev + nm2
      rmtx = b1 + nm2
      cupest = rmtx + nm2
      ipvt = wpadti(cupest+nmax)
      last = iadtwp(ipvt+nmax) + 1
      if ( last .gt. mcore ) then
	 write (iout,1020) last,mcore
	 stop
      endif
      pt0 = last + 1
      return
*
 1020 format (' lwint : storage overflow, last =',i10,' mcore =',i10)
      end
      subroutine lwprop (enrg,tev,tpev,tsave,pdiag,r4,keep,nstp,
     x                  nmax,etot,prntr,debug,rstart,rfinal,
     x                  space,b1,rmtx,idiscr,ipvt,rmat)
*
* $Id: lwprop.f,v 2.1 94/09/29 18:44:02 vmb Exp Locker: vmb $
*
      implicit integer (a-z)
      real*8 enrg,tev,tpev,tsave,pdiag,r4,etot,rstart,
     x	               r,rfinal,space,zero,one,rmtx,dr,rl,b1,rr,
     x                 rmat
      logical prntr,debug
      parameter ( zero=0.0d0,one=1.0d0 )
      dimension enrg(nmax),tev(nmax,nmax),tsave(nmax,nmax),
     x          pdiag(nmax,2),r4(nmax,nmax),
     x          space(nmax,nmax,3),tpev(nmax,nmax),b1(nmax,nmax),
     x          rmtx(nmax,nmax),ipvt(nmax),rmat(*),keep(nstp)
*
*     lwprop    LW Propagator Driver
*
*     keep(nstep) decides how many channels are kept at each step.
      nstep = 1
      n = keep(nstep)
      k = 0
      do 20 j = 1, nmax
         do 10 i = 1, j
            k = k + 1
            r4(i,j) =  - rstart * rmat(k)
            r4(j,i) =  - rstart * rmat(k)
 10      continue
 20   continue
      if (prntr) call printm ('r4mt',r4,n,n,nmax)
      read (idiscr, rec = 1) r,dr,tev,tpev
      rr = r
      call dcopy (n*nmax,r4,1,rmtx,1)
      call dgemm ('n','n',n,n,n,one,rmtx,nmax,tev,nmax,zero,tsave,nmax)
      call dgemm ('n','n',n,n,n,one,tpev,nmax,tsave,nmax,zero,r4,nmax)
      do 50 nstep = 2, nstp
         n = keep(nstep)
         read (idiscr, rec = nstep) rl,dr,enrg,tsave,tev,tpev
         if (debug) call printm ('tev ',tev,n,n,nmax)
*     propagation code
         call rpropl (enrg,-dr,etot,r4,pdiag,tsave,n,nmax,space,ipvt)
         if (debug) call printm ('r4mt',r4,n,n,nmax)
         rr = rl + dr
         if ( rr .ge. rfinal ) go to 60
         rl = rr
   50 continue
   60 rfinal = rr
      if (prntr) call printm ('rfin',r4,n,n,nmax)
*     apply boundary conditions
      call dgemm ('n','n',n,n,n,one,r4,nmax,tpev,nmax,zero,b1,nmax)
      call dgemm ('n','n',n,n,n,one,tev,nmax,b1,nmax,zero,r4,nmax)
      k = 0
      do 140 j = 1, n
	 do 130 i = 1, j
            k = k + 1
	    rmat(k) = - r4(i,j) / rfinal
  130	 continue
  140 continue
      return
      end
      subroutine mprint ( name, a, n, m, nmax )
*
* $Id: mprint.f,v 2.1 94/09/29 18:44:04 vmb Exp Locker: vmb $
*
      implicit integer (a-z)
      real*8 a
      character*4 name
      dimension a(nmax,m)
      common /io/ iin,iout
*
*     mprint -- prints a real matrix a(n,m)
*		name	  -- 4 character identification
*		a(nmax,m) -- array to be printed
*		n	  -- number of rows to be printed
*		m	  -- number of columns to be printed ( .le. 10 )
*		nmax	  -- row dimension of a
*
      write (iout,1000) name, (a(1,i), i = 1, m )
      if ( n .gt. 1 ) then
	 do 10 i = 2, n
	    write (iout,1010) (a(i,j), j = 1, m )
   10	 continue
      endif
      return
*
 1000 format ( '0', a4, 2x, 10g12.4 )
 1010 format ( 7x, 10g12.4 )
      end
      subroutine pot (r,nchan,v,nv,nchx,ismax,ion,lchl,cf,ethr)
*
* $Id: pot.f,v 2.1 94/09/29 18:44:06 vmb Exp Locker: vmb $
*
      implicit integer (a-z)
      real*8 r,v,cf,ethr,zero,one,two,rk,elx,vc
      parameter ( zero=0.0d0,one=1.0d0,two=2.0d0 )
      dimension v(nv),lchl(nchx),ethr(nchx),cf(nchx,nchx,ismax)
*
*     potential at radius r
*
      do 10 i = 1, nv
         v(i) = zero
   10 continue
      vc = - two * dble(ion) / r
      do 50 k = 1, ismax
	 rk = one / r**(k+1)
         ij = 0
	 do 40 j = 1, nchan
	    do 30 i = 1, j
               ij = ij + 1
	       v(ij) = v(ij) + cf(i,j,k) * rk
   30	    continue
   40	 continue
   50 continue
      rk = one / ( r * r )
      do 60 i = 1, nchan
	 elx = dble( lchl(i) * ( lchl(i) + 1 ) )
         ij = (i*(i+1))/2
	 v(ij) = v(ij) + elx * rk + ethr(i) + vc
   60 continue
      return
      end
      subroutine potp (r,nchan,v,nv,nchx,ismax,ion,lchl,cf)
*
* $Id: potp.f,v 2.1 94/09/29 18:44:07 vmb Exp Locker: vmb $
*
      implicit integer (a-z)
      real*8 r,v,cf,zero,two,rk,elx,vc
      parameter ( zero=0.0d0,two=2.0d0 )
      dimension v(nv),lchl(nchx),cf(nchx,nchx,ismax)
*
*     potential gradient at radius r
*
      do 10 i = 1, nv
         v(i) = zero
   10 continue
      vc =  two * dble (ion) / ( r * r )
      do 50 k = 1, ismax
	 rk = dble (k + 1) / r**(k+2)
         ij = 0
	 do 40 j = 1, nchan
	    do 30 i = 1, j
               ij = ij + 1
	       v(ij) = v(ij) - cf(i,j,k) * rk  
   30	    continue
   40	 continue
   50 continue
      rk = two / r**3
      do 60 i = 1, nchan
         ij = (i*(i+1))/2
	 elx = dble( lchl(i) * ( lchl(i) + 1 ) )
	 v(ij) = v(ij) - elx * rk + vc
   60 continue
c     print *,(v(ij),ij=1,nv)
      return
      end
      subroutine printm (name,array,n,m,nmax)
*
* $Id: printm.f,v 2.1 94/09/29 18:44:09 vmb Exp Locker: vmb $
*
      implicit integer (a-z)
      real*8 array
      character*4 name,namec
      dimension array(*)
*
*     printm -- driver for mprint
*
      namec(1:4) = name(1:4)
      nh = 1
      mp = 6
      i = 0
      iz = ichar('0')
      do 10 nl = m, 1, -mp
         nc = min( nl, mp )
         call mprint (namec,array(nh),n,nc,nmax)
         i = i + 1
         namec(4:4) = char(iz+mod(i,10)) 
         nh = nh + nmax * nc
   10 continue
      end
      subroutine printv (name,n,nv,vmat)
*
* $Id: printv.f,v 2.1 94/09/29 18:44:11 vmb Exp Locker: vmb $
*
      implicit integer (a-z)
      real*8 vmat
      character*4 name,namec
      parameter ( ncol=9 )
      dimension vmat(nv)
      common /io/ iin,iout
c
      namec(1:4) = name(1:4)
      iz = ichar('0')
      do 10 ncf = 1, n, ncol
         ncl = min(ncf+ncol-1,n)
         write (iout,1010) namec
         do 20 ir = ncf, n
            nc = (ir*(ir-1))/2
            nc1 = nc + ncf
            nc2 = nc + min(ir,ncl)
            write (iout,1000) (vmat(j),j=nc1,nc2)
   20    continue
         write (iout,*)
         namec(4:4) = char(iz+mod(mod(ncf,ncol)+1,10))
   10 continue
      return
 1000 format (9f8.4)
 1010 format (2x,a4)
      end
      subroutine rpropl (en,du,e,r4,dchan,tot,n,nmax,space,ipvt)
*
* $Id: rpropl.f,v 2.1 94/09/29 18:44:12 vmb Exp Locker: vmb $
*
      implicit integer (a-z)
      real*8 r4,dchan,tot,space,zero,one,en,du,e,ek,
     x                 hek,s,sh,ch
      parameter ( zero=0.0d0,one=1.0d0 )
      common /io/ iin,iout
      dimension r4(nmax*nmax),tot(nmax,n),dchan(nmax,2),en(nmax),
     x          space(nmax,nmax,3),ipvt(nmax)
*
*     space(1,1,1) = z inverse ; space(1,1,2) = sector r(2)
*     space(1,1,3) = sector r(3)
*
      do 10 i = 1, n
	 ek = e - en(i)
	 if ( ek .ge. zero ) then
	    ek = sqrt(ek)
	    hek = du * ek
	    s = - one
	    sh = sin(hek)
	    ch = cos(hek)
	 else
	    s = one
	    ek = sqrt(-ek)
	    hek = du * ek
	    sh = sinh(hek)
	    ch = cosh(hek)
	 endif
	 dchan(i,1) = one / ( s * sh * ek )
	 dchan(i,2) = ch * dchan(i,1)
   10 continue
*     build sector r(2) and start building sector r(1)
      n2 = n * nmax
      do 45 ij = 1, n2
         j = (ij-1)/nmax + 1
         i = ij - (j-1) * nmax
         space(i,j,1) = r4(ij)
         space(i,j,2) = tot(i,j) * dchan(j,1)
	 space(i,j,3) = tot(i,j) * dchan(j,2)
 45   continue
*     finish sector r(1) and build z inverse
      call dgemm ('n','t',n,n,n,one,space(1,1,3),nmax,tot,nmax,
     x            one,space,nmax)
*     save sector r(3) and invert z inverse
      call transp (space(1,1,2),space(1,1,3),n,nmax)
      call dgetrf (n,n,space,nmax,ipvt,ier)
      if ( ier .ne. 0 ) then
	 write (iout,1000) ier
	 stop
      endif
      call dgetrs('n',n,n,space,nmax,ipvt,space(1,1,2),nmax,ier)
      if ( ier .ne. 0 ) then
	 write (iout,1010) ier
	 stop
      endif
*     z*r(2) computed in space(1,1,2) ; r(3)*zr(2) + old r4
      call dgemm ('n','n',n,n,n,-one,space(1,1,3),nmax,space(1,1,2),
     x            nmax,zero,r4,nmax)
      ii = 1
      n1 = nmax + 1
      do 150 i = 1, n
	 r4(ii) = dchan(i,2) + r4(ii)
         ii = ii + n1 
  150 continue
      return
 1000 format (' rpropl : error return from dgetrf, info = ',i6)
 1010 format (' rpropl : error return from dgetrs, info = ',i6)
      end
      subroutine slwpot (nstart,vmat,enrg,tc,tp,tsave,n,nmax,
     x			prntv,debug,rstart,rfinal,lwsect,lwkeep,
     x                  lwran,beta,cuplim,space,tev,tpev,ismax,ion,
     x                  lchl,cf,ethr,idiscr,cupest)
*
* $Id: slwpot.f,v 2.1 94/09/29 18:44:14 vmb Exp Locker: vmb $
*
      implicit integer (a-z)
      real*8 vmat,enrg,tc,tp,tsave,rstart,cf,ethr,rlim,stp,
     x	               rfinal,drstrt,stpmin,stpmax,beta,space,tev,tpev,
     x                 expt,zero,one,half,dr,rl,step,r,e1,couple,vprime,
     x                 or,rr,oe1,cupest,cuplim,lwsect
      logical prntv,debug
      parameter ( zero=0.0d0,one=1.0d0,half=0.5d0,expt=1.0d0/6.0d0 )
      dimension vmat(nmax,nmax),enrg(nmax),tc(nmax,nmax),tev(nmax,nmax),
     x		tp(nmax,nmax),tsave(nmax,nmax),space(nmax,nmax,3),
     x		tpev(nmax,nmax),lchl(nmax),cf(nmax,nmax,ismax),
     x          ethr(nmax),cupest(nmax),rlim(2),stp(2),
     x          lwsect(lwran),lwkeep(lwran)
      common /io/ iin,iout
*
*     slwpot -  diagonalises the potential in each sector
*               nstart = 1 at first entry. On exit,
*               nstart = number of steps taken (nstart can be used to
*               restart slwpot ready to propagate further)
*
*     test whether sectors are required
*
      print *,'SLWPOT entered with nstart ',nstart
      if (nstart .eq. lwran) then
         write(iout,1060) nstart
         go to 520
      end if 
*     calculate the first step length using the gradient of the
*     potential at rstart and the max using rfinal
*
      n2 = n * n
      nv = ( n2 + n ) / 2
      ik = 1
      if (nstart .gt. 1) ik = 2
      rlim(1) = rstart
      rlim(2) = rfinal
c     print *,((vmat(j,i),j=1,n),i=1,n)
      do 10 k = ik, 2
         call potp (rlim(k),n,vmat,nv,nmax,ismax,ion,lchl,cf)
c print *,(vmat(i,1),i=1,n),(vmat(i,2),i=1,n)
         call vdiag (vmat,enrg,tc,n,space,nv,nmax)
c print *,(vmat(i,1),i=1,n),(vmat(i,2),i=1,n)
         e1 = zero
         do 20 i = 1,n
	    print *,enrg(i)
            e1 = e1 + enrg(i)
   20    continue
         vprime = (e1/n) ** 2  
         stp(k) = (beta / vprime) ** expt
	 print *,beta,vprime,expt,e1,n
   10 continue
c     stop
      stpmax = stp(2)
      if (nstart .eq. 1) then
         lwkeep(1) = n
         stpmin = stp(1)
         drstrt = stpmin
         if (prntv .or. debug)
     x       write (iout,1000) rstart,rfinal,stpmin,stpmax,beta
         call pot (rstart,n,vmat,nv,nmax,ismax,ion,lchl,cf,ethr)
         if (prntv) then
            write (iout,1010) rstart
            call printv ('vcup',n,nv,vmat)
         endif
         call vdiag (vmat,enrg,tc,n,space,nv,nmax)
         do 35 ik = 1, n2
            i = (ik-1)/n + 1
            k = ik - (i-1) * n
            tev(i,k) = tc(i,k)
            tpev(k,i) = tc(i,k)
            tp(k,i) = tc(i,k)
   35    continue
         if (debug) write (iout,1020)
         write (idiscr,rec=nstart) rstart,drstrt,tev,tpev
         or = rstart
      else
         read (idiscr,rec = nstart+1) or,rstart,drstrt,(enrg(i),i=1,n),
     x                                ((tp(i,k),i=1,n),k=1,n)
      endif
      if (rfinal .le. rstart) then
         write(iout,1070) nstart,rstart,rfinal
         go to 520
      endif
      olim = lwkeep(nstart)
      lwsect(nstart) = rstart
      rl = rstart
      dr = drstrt
      stpmin = drstrt
      e1 = zero
      do 50 i = 1, n
         e1 = e1 + enrg(i)
   50 continue
      e1 = e1 / n
      do 500 nstep = nstart+1, lwran
         r = rl + half * dr
         rr = rl + dr
         lwsect(nstep) = rr
         call pot (r,n,vmat,nv,nmax,ismax,ion,lchl,cf,ethr)
         if (prntv) call printv ('vcup',n,nv,vmat)
         call vdiag (vmat, enrg, tc, n, space, nv, nmax)
         if (debug) call printm ('tcc ',tc,n,n,nmax)
         do 95 ixy = 1, n2
            ix = (ixy-1)/n + 1
            iy = ixy - (ix-1) * n
            tev(ix,iy) = tc(ix,iy)
            tpev(iy,ix) = tc(ix,iy)
   95    continue
         oe1 = e1
         e1 = zero
         do 110 i = 1, n
            e1 = e1 +  enrg(i) 
  110    continue
         e1 = e1 / n
*     construct adiabatic overlap coupling matrix
         call dgemm ('n','n',n,n,n,one,tp,nmax,tc,nmax,zero,tsave,nmax)
         if (debug) call printm ('tsve', tsave,n,n,nmax)
         call dcopy (n*nmax,tpev,1,tp,1)
         write (idiscr,rec = nstep) rl, dr, enrg, tsave , tev , tpev
*
*     estimate the coupling between each channel with higher channels
         lim = 0
         j2 = 0
         do 100  j = 2, n
            j2 = j2 + 2
            cupest(j) = zero
            do 105  k = 1, j - 1
               cupest(j) = cupest(j) + tsave(j,k) * tsave(j,k)  +  
     x                     tsave(k,j) * tsave(k,j)
  105       continue
            cupest(j) = sqrt(cupest(j)) / (dble(j2) * dr)
            if (cupest(j) .gt. cuplim) lim = j
  100    continue
         if (lim .lt. olim) then
            lim = olim - 1
         elseif (lim .gt. olim) then
            do 115 i = nstep-1, 1, -1
               lwkeep(i) = max (lwkeep(i),lim)
  115       continue
         endif
         lwkeep(nstep) = lim
         olim = lim
         nprnt = min(n,2)
         if (debug) then
            couple = zero
            do 120 i = 1, n
               couple = couple + abs( tsave(i,i) )
  120       continue
            couple = one - couple / dble(n)
            write (iout,1040) nstep, rl, r, rr, dr, couple,
     x                        (enrg(i),i=1,nprnt)
         endif
*     next step size code
         vprime = ( ( oe1 - e1 ) / ( or - r ) )**2
         step = ( beta / vprime )**expt
         step = max( step, stpmin )
*     to force propogation to end at rfinal, uncomment next statement
*        step = min( step, rfinal-rr )
         or = r
         rl = rr
         if (rr .ge. rfinal) then
            nstart = nstep
            go to 510
         endif
         step = min( step, stpmax )
         dr = step
  500 continue
      nstep = lwran
      nstart = lwran
  510 write (idiscr,rec = nstep+1) or,rl,dr,enrg,tpev
      if (prntv .or. debug) write (iout,1050) nstep,step
  520 continue
      return
*
 1000 format (' R-Matrix Propagation : Light-Walker Method',/,
     x	      ' Initial radius =',f10.5,5x,'; Final radius   =',f10.5,/,
     x	      ' Min. step size =',e15.5,   '; Max. step size =',e15.5,/,
     x	      ' Beta =',e15.5)
 1010 format (' Potential at r =',f10.5)
 1020 format ( '1Light-Walker Propagation',/,' nstep ',
     x	       ' r(left)  r(cntr)  r(rght)     step coupling',
     x	       ' Lowest potl evalues:..' )
 1040 format ( i6, 3f9.3, f9.4, f9.4, 1p2e11.3 )
 1050 format (' nstep =',i5,5x,'step =',f15.6)
 1060 format (' subroutine slwpot',/,
     x        ' nstart =',i4,/,
     x        ' nstart = lwran - no more sectors available',/,
     x        ' increase parameter lwran')
 1070 format (' subroutine slwpot',/,
     x        ' nstart =',i4,' rstart =', f9.4,/,
     x        ' rfinal =', f9.4,'no sectors required') 
      end
      subroutine transp ( a, b, n, nmax )
*
* $Id: transp.f,v 2.1 94/09/29 18:44:18 vmb Exp Locker: vmb $
*
      implicit integer (a-z)
      real*8   a, b
      dimension a(nmax,n), b(nmax,n)
*
*     transp -- copies the transpose of a(nmax,n) to b(nmax,n)
*
      n2 = n * n
      do 10 ij = 1, n2
         i = (ij-1)/n + 1
         j = ij - (i-1)*n
         b(j,i) = a(i,j)
   10 continue
      return
      end
      subroutine vdiag (v,e,t,n,work,nv,nmax)
*
* $Id: vdiag.f,v 2.1 94/09/29 18:44:20 vmb Exp Locker: vmb $
*
      integer n,nmax,info,iin,iout
      real*8 v,e,t,work
      common /io/ iin,iout
      dimension e(n), t(nmax,n), work(3*n), v(nv)
*
*     v(nv)     real symmetric matrix to be diagonalized
*     e(n)	eigenvalues of v
*     t(nmax,n) eigenvectors of v
*
      call DSPEV ( 'V', 'U', N, v, e, t, nmax, WORK, INFO )
      if ( info .ne. 0 ) then
         write (iout,1000) info
         stop
      endif
      return
*
 1000 format (' vdiag : error return by dspev, info =', i3)
      end
