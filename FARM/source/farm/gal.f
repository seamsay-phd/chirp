      subroutine asywfn (nchan,nmax,iasy,f,fp,fc,fcp,gc,gcp,eps,ion,
     x                   nfc,ipflg,iasx,xg,xc,xd,j,lj,rafin,
     x                   skj,etj,etaj,jopen,ga1,ga2,xcp,xdp,ierfg)
*
* $Id: asywfn.f,v 2.1 94/09/29 18:43:26 vmb Exp Locker: vmb $
*
      implicit integer (a-z)
      logical jopen
      real*8 f,fp,fc,fcp,gc,gcp,eps,xg,xc,xd,rafin,skj,etj,etaj,ga1,
     x	     ga2,rscalx,zero,rootk,rho,flj,f1,g1,fp1,gp1,xi,del1,del2,
     x	     del3,del4,del,delp,dr1,dpr1,dr2,dpr2,aerr1,aerr2,aepr1,
     x	     aepr2,zz,u,uder,xcp,xdp,fp2,gp2,fdd,one,two,dfloat,dr1p,
     x       dr2p,dpr1p,dpr2p,scalx
      parameter ( zero=0.0d0,one=1.0d0,two=2.0d0 )
      dimension fc(nfc),gc(nfc),fcp(nfc),gcp(nfc),ga1(nmax,0:iasy),
     x		f(nchan,nchan,2),fp(nchan,nchan,2),ga2(nmax,0:iasy),
     x		xg(0:iasy),xc(0:iasx),xd(0:iasx),xcp(0:iasx),
     x		xdp(0:iasx)
      common /io/ iin,iout
      common /mach/ scalx,rscalx
*
*     asywfn : asymptotic scattering functions, f, and derivatives, fp.
*
      rootk = sqrt(skj)
      rho = rafin * skj
      flj = dfloat(lj)
      llj = lj * ( lj + 1 )
      if ( jopen ) then
	 call coulfg (rho,etaj,flj,flj,fc,gc,fcp,gcp,1,0,ifail)
         if (ifail .ne. 0) then
	    write (iout,1000) ifail,lj,rho,etaj
         end if
	 f1 = fc(lj+1) / rootk
	 g1 = gc(lj+1) / rootk
	 fp1 = fcp(lj+1) / rootk
	 gp1 = gcp(lj+1) / rootk
	 fdd = ( dfloat(llj) / rho + two * etaj ) / rho - one
         fp2 = fdd * f1
         gp2 = fdd * g1
	 if ( ipflg .eq. 1 ) then
	    write (iout,1000) ifail,lj,rho,etaj
	    write (iout,1010) rafin,skj,f1,g1,fp1,gp1
	 endif
      else
	 zz = dfloat(ion)
	 if ( ion .ne. 0 ) then
	    call coul (lj,zz,etj,rafin,u,uder,iasy,xg,xc,xd,xcp,xdp,
     x		       ipflg,ierfg)
	 else
	    call decay (etj,lj,rafin,u,uder)
	 endif
	 f1 = u / rootk
	 fp1 = uder / rootk
	 fp2 = ((dfloat(llj) / rho + two * etaj) / rho + one) * skj *
     x         skj * f1
	 if ( ipflg .eq. 1 ) write (iout,1020) lj,ion,rafin,etj,skj,f1,
     x					       fp1
      endif
*     evaluate asymptotic series
      xi = one / rafin
      do 320 i = 1, nchan
	 ily = 0
	 ilz = 0
	 do 220 p = 0, iasy
	    if ( abs(ga1(i,p)) .le. scalx * 1.0d-6) ily = p
	    if ( abs(ga2(i,p)) .le. scalx * 1.0d-6) ilz = p
  220	 continue
	 ily1 = ily + 1
         ilym = ily - 1
	 if ( ily .eq. iasy ) then
	    dr1 = zero
	    dpr1 = zero
            aerr1 = zero
            aepr1 = zero
	 elseif ( ily .lt. iasy ) then
	    ncfct = iasy - ily1
	    if ( ipflg .ne. 0 ) write (iout,1030) ily1,iasy
	    do 240 ip1 = ily1, iasy
	       xg(ip1-ily1) = ga1(i,ip1)
  240	    continue
	    np = ( ncfct + 1 ) / 2
	    call rrfr (ncfct,np,xg,xc,xcp,xd,xdp)
	    call rrfvl (np,ily1,xi,xc,xd,xcp,xdp,dr1,dpr1,aerr1,aepr1,
     x			ierfg)
	 endif
	 ilz1 = ilz + 1
         ilzm = ilz - 1
	 if ( ilz .eq. iasy ) then
	    dr2 = zero
	    dpr2 = zero
            aerr2 = zero
            aepr2 = zero
	 elseif ( ilz .lt. iasy ) then
	    ncfct = iasy - ilz1
	    if ( ipflg .ne. 0 ) write (iout,1030) ilz1,iasy
	    do 245 ip1 = ilz1, iasy
	       xg(ip1-ilz1) = ga2(i,ip1)
  245	    continue
	    np = ( ncfct + 1 ) / 2
	    call rrfr (ncfct,np,xg,xc,xcp,xd,xdp)
	    call rrfvl (np,ilz1,xi,xc,xd,xcp,xdp,dr2,dpr2,aerr2,aepr2,
     x			ierfg)
	 endif
	 if ( ily .ge. 1 ) then
	    call rpolq (ga1,xi,dr1p,dpr1p,ilym,nmax,i)
	    dr1 = dr1 + dr1p
	    dpr1 = dpr1 + dpr1p
	 endif
	 if ( ilz .ge. 1 ) then
	    call rpolq (ga2,xi,dr2p,dpr2p,ilzm,nmax,i)
	    dr2 = dr2 + dr2p
	    dpr2 = dpr2 + dpr2p
	 endif
         dr1 = rscalx * dr1
	 dpr1 = rscalx * dpr1
	 dr2 = rscalx * dr2
	 dpr2 = rscalx * dpr2
*     asymptotic functions and errors
	 if ( jopen ) then
	    f(i,j,1) =  f1 * dr1 + fp1 * dr2 
	    fp(i,j,1) =  skj * fp1 * dr1 + f1 * dpr1 + skj *
     x			fp2 * dr2 + fp1 * dpr2
	    f(i,j,2) =  g1 * dr1 + gp1 * dr2
	    fp(i,j,2) =  skj * gp1 * dr1 + g1 * dpr1 + skj *
     x			gp2 * dr2 + gp1 * dpr2
	    if ( ierfg .ne. 0 ) then
               aerr1 = rscalx * aerr1
               aerr2 = rscalx * aerr2
               aepr1 = rscalx * aepr1
               aepr2 = rscalx * aepr2
	       del1 = abs( f1 * aerr1 + fp1 * aerr2 )
	       del2 = abs( skj * fp1 * aerr1 + f1 * aepr1 +
     x		      skj * fp2 * aerr2 + fp1 * aepr2 )
	       del = max( del1, del2 )
	       if ( del .gt. eps .or. ipflg .ne. 0 ) then
		  call galerr (i,j,f(i,j,1),del1,fp(i,j,1),del2)
	       endif
	       del3 = abs( g1 * aerr1 + gp1 * aerr2 )
	       del4 = abs( skj * gp1 * aerr1 + g1 * aepr1 +
     x		      skj * gp2 * aerr2 + gp1 * aepr2 )
	       delp = max( del3, del4 )
	       if ( delp .gt. eps .or. ipflg .ne. 0 ) then
		  call galerr (i,j,f(i,j,2),del3,fp(i,j,2),del4)
	       endif
	    endif
	 else
	    f(i,j,1) = f1 * dr1 - fp1 * dr2 / skj 
	    fp(i,j,1) = fp1 * dr1 + f1 * dpr1 - ( fp2 *
     x			dr2 + fp1 * dpr2 ) / skj 
	    if ( ierfg .ne. 0 ) then
               aerr1 = rscalx * aerr1
               aerr2 = rscalx * aerr2
               aepr1 = rscalx * aepr1
               aepr2 = rscalx * aepr2
	       del1 = abs( f1 * aerr1 - fp1 * aerr2 / skj )
	       del2 = abs( fp1 * aerr1 + f1 * aepr1 - ( fp2 *
     x		      aerr2 + fp1 * aepr2 ) / skj )
	       del = max( del1, del2 )
	       if ( del .gt. eps .or. ipflg .ne. 0 ) then
		  call galerr (i,j,f(i,j,1),del1,fp(i,j,1),del2)
	       endif
	    endif
	 endif
  320 continue
      return
*
 1000 format (' coulfg : ifail =',i4,' lj =',i4,' rho =',d16.8,
     x        ' etaj =',d16.8)
 1010 format (' rafin =',f12.5,' skj =',d16.8,' f =',d16.8,' g =',d16.8,
     x	      /,' fp =',d16.8,' gp =',d16.8)
 1020 format (' coul/decay : lj =',i3,' ion =',i3,' rafin =',d16.8,
     x	      ' etj =',d16.8,' skj =',f12.5,/,14x,' f1 =',d16.8,
     x	      ' dp1 =',d16.8)
 1030 format (' coefficients',i3,' to',i3,' extrapolated ')
      end
      subroutine couerr (fx,efx,eps,fxp,efxp)
*
* $Id: couerr.f,v 2.1 94/09/29 18:43:28 vmb Exp Locker: vmb $
*
      implicit integer (a-z)
      real*8 fx,efx,eps,fxp,efxp,escat
      common /ie/ ierr,escat
*
      if ( ierr .ne. 0 ) then
         write (ierr,1000) escat,fx,efx,eps,fxp,efxp
      endif
      return
 1000 format (' coul : e =',f12.5,' fx  =',d16.8,' efx  =',d16.8,
     x	      ' eps =',d16.8,/,'         fxp =',d30.17,' efxp =',d16.8)
      end
      subroutine coul (l,z,e,r,fx,fxp,nterm,xg,xc,xd,xcp,xdp,
     x                 ipflg,ierr)
*
* $Id: coul.f,v 2.1 94/09/29 18:43:29 vmb Exp Locker: vmb $
*
      implicit integer (a-z)
      real*8 z,e,r,fx,fxp,xg,xc,xd,one,half,two,eps,ak,c,x,cx,dfloat,
     x       fl,fl1,c1,q,f1,f2,f3,an,efx,fac,f,ef,epf,efxp,xcp,xdp,fp,
     x       xi
      parameter ( one=1.0d0,half=0.5d0,two=2.0d0,eps=1.0d-08 )
      dimension xg(0:nterm),xc(*),xd(*),xcp(*),xdp(*)
      common /io/ iin,iout
*
*     coul : whittaker function and derivative using the asymptotic
*	     expansion converted to a continued fraction
*
      ak = sqrt(-e)
      c = ak / z
      x = z * r
      cx = ak * r
      fl = dfloat(l)
      fl1 = dfloat(l+1)
      c1 = one / c
      q = one
      xg(0) = q
      f1 = fl - c1
      f2 = - fl1 - c1
      f3 = - half * c1
      do 10 n = 1, nterm
	 an = dfloat(n)
	 q = ( f1 + an ) * ( f2 + an ) * f3 * q / an
	 xg(n) = q
   10 continue
      if ( ipflg .ne. 0 ) write (iout,1000) nterm,(xg(i),i = 0,nterm)
*
*     continued fraction
*
      np = ( nterm + 1 ) / 2
      call rrfr (nterm,np,xg,xc,xcp,xd,xdp)
      xi = one / x
      call rrfvl (np,0,xi,xc,xd,xcp,xdp,f,fp,ef,epf,ierr)
      fac = exp(-cx) * ( two * cx ) ** c1
      fx = fac * f
      fxp = z * ( ( one / cx - c ) * fx + fac * f p)
      if ( ipflg .ne. 0 ) then
         write (iout,1010) fx,fxp
      end if
      if ( ierr .ne. 0 ) then
         efx = abs(ef*fx)
         efxp = abs(epf*fxp)
         if ( efx .gt. eps .or. efxp .gt. eps ) then
             write (iout,1020) e,r,fx,efx,eps,fxp,efxp
c            call couerr (fx,efx,eps,fxp,efxp)
         endif
      endif
      return
*
 1000 format (' nterm =',i6,/,(4d16.8))
 1010 format (' coul : fx  =',d16.8,'fxp =',d16.8)
 1020 format (' coul : e =',f12.5,'  r =', f12.5,/,
     x        '        fx  =',d16.8,' efx  =',d16.8,' eps =',d16.8,/,
     x        '        fxp =',d30.17,' efxp =',d16.8)
      end
      subroutine coulfg (xx,eta1,xlmin,xlmax, fc,gc,fcp,gcp,mode1,kfn,
     x			 ifail)
*
* $Id: coulfg.f,v 2.1 94/09/29 18:43:31 vmb Exp Locker: vmb $
*
*
************************************************************************
*								       *
*     coulomb wavefunction program using steed's method                *
*								       *
*  a. r. barnett	   manchester  march   1981		       *
*								       *
*  original program 'rcwfn'      in    cpc  8 (1974) 377-395           *
*		  + 'rcwff'      in    cpc 11 (1976) 141-142           *
*  full description of algorithm in    cpc 21 (1981) 297-314	       *
*  this version written up	 in    cpc 27 (1982) 147-166	       *
*								       *
*  coulfg returns f,g,f',g', for real xx.gt.0,real eta1 (including 0), *
*   and real lambda(xlmin) .gt. -1 for integer-spaced lambda values    *
*   thus giving positive-energy solutions to the coulomb schrodinger   *
*   equation,to the klein-gordon equation and to suitable forms of     *
*   the dirac equation ,also spherical & cylindrical bessel equations  *
*								       *
*  for a range of lambda values (xlmax - xlmin) must be an integer,    *
*  starting array element is m1 = max0(idint(xlmin+accur),0) + 1       *
*      see text for modifications for integer l-values		       *
*								       *
*  if 'mode' = 1  get f,g,f',g'   for integer-spaced lambda values     *
*	     = 2      f,g      unused arrays must be dimensioned in    *
*	     = 3      f 	      call to at least length (1)      *
*  if 'kfn'  = 0 real        coulomb functions are returned            *
*	     = 1 spherical   bessel	 '      '     '                *
*	     = 2 cylindrical bessel	 '      '     '                *
*  the use of 'mode' and 'kfn' is independent                          *
*								       *
*  precision:  results to within 2-3 decimals of 'machine accuracy'    *
*   in oscillating region x .ge. eta1 + sqrt(eta1**2 + xlm(xlm+1))     *
*   coulfg is coded for real*8 on ibm or equivalent  accur = 10**-16   *
*   use autodbl + extended precision on hx compiler  accur = 10**-33   *
*   for mantissas of 56 & 112 bits. for single precision cdc (48 bits) *
*   reassign dsqrt=sqrt etc.  see text for complex arithmetic version  *
************************************************************************
*
      implicit real*8 (a-h,o-z)
      dimension    fc(1),gc(1),fcp(1),gcp(1)
      logical	   etane0,xlturn
      parameter ( zero=0.0d0,one=1.0d0,two=2.0d0,ten2=1.0d2,abort=8.0d4,
     x		  half=0.5d0,tm30=1.0d-30,ten=1.0d1,
     x		  rt2dpi=0.79788 45608 02865 35587 98921 19868 76373 d0)
      common /io/ iin,iout
*
*     constant is  dsqrt(two/pi)
*
      accur = 1.0d-16
*
      mode  = 1
      if ( mode1 .eq. 2 .or. mode1 .eq. 3 ) mode = mode1
      ifail = 0
      iexp  = 1
      eta   = eta1
      gjwkb = zero
      paccq = one
      if ( kfn .ne. 0 ) eta = zero
      etane0  = eta .ne. zero
      acc   = accur * ten
      acc4  = acc*ten2*ten2
      acch  = sqrt(acc)
*
*	 test range of xx, exit if .le. sqrt(accur) or if negative
*
      if ( xx .le. acch ) then
	 ifail = - 1
	 write (iout,1000) xx,acch
	 return
      endif
      x     = xx
      xlm   = xlmin
      if ( kfn .eq. 2 ) xlm = xlm - half
      if ( xlm .le. -one .or. xlmax .lt. xlmin ) then
	 ifail = - 2
	 write (iout,1010) xlmax,xlmin,xlm
	 return
      endif
      e2mm1 = eta * eta + xlm * xlm + xlm
      xlturn = x * ( x - two * eta ) .lt. xlm * xlm + xlm
      dell  = xlmax - xlmin + acc
      if ( abs(mod(dell,one)) .gt. acc ) write (iout,1050) xlmax,xlmin,
     x							    dell
      lxtra = int(dell)
      xll   = xlm + dfloat(lxtra)
*
*      lxtra = number of additional lambda values to be computed
*      xll   = max lambda value, or 0.5 smaller for j,y bessels
*	       determine starting array element (m1) from xlmin
*
      m1  = max(int(xlmin + acc),0) + 1
      l1  = m1 + lxtra
*
*     evaluate cf1  =  f   =  fprime(xl,eta,x) / f(xl,eta,x)
*
      xi  = one / x
      fcl = one
      pk  = xll + one
      px  = pk	+ abort
      f   =  eta / pk + pk * xi
      if ( abs(f) .lt. tm30 ) f = tm30
      d = zero
      c = f
*
*     begin cf1 loop on pk = k = lambda + 1
*
   10 pk1   = pk + one
      ek  = eta / pk
      rk2 = one + ek * ek
      tk  = ( pk + pk1 ) * ( xi + ek / pk1 )
      d   =  tk - rk2 * d
      c   =  tk - rk2 / c
      if ( abs(c) .lt. tm30 ) c = tm30
      if ( abs(d) .lt. tm30 ) d = tm30
      d = one / d
      df = d * c
      f  = f * df
      if ( d .lt. zero ) fcl = - fcl
      pk = pk1
      if ( pk .gt. px ) then
	 ifail = 1
	 write (iout,1020) abort,f,df,pk,px,acc
	 return
      endif
      if ( abs(df-one) .ge. acc ) go to 10
      if ( lxtra .eq. 0 ) go to 30
*
*     downward recurrence to lambda = xlm. array gc,if present,stores rl
*
      fcl = fcl * tm30
      fpl = fcl * f
      if ( mode .eq. 1 ) fcp(l1) = fpl
      fc (l1) = fcl
      xl  = xll
      rl  = one
      el  = zero
      do 20  lp = 1, lxtra
	 if (etane0) el = eta / xl
	 if (etane0) rl = sqrt(one + el*el)
	 sl    =  el  + xl * xi
	 l     =  l1  - lp
	 fcl1  = ( fcl * sl + fpl ) / rl
	 fpl   =  fcl1 * sl - fcl * rl
	 fcl   =  fcl1
	 fc(l) =  fcl
	 if ( mode .eq. 1 ) fcp(l)  = fpl
	 if ( mode .ne. 3 .and. etane0 ) gc(l+1) = rl
	 xl = xl - one
   20 continue
      if ( fcl .eq. zero ) fcl = acc
      f  = fpl / fcl
*
*     now reached lambda = xlmin = xlm
*     evaluate cf2 = p + i.q  again using steed's algorithm
*
   30 if ( xlturn ) call jwkb (x,eta,max(xlm,zero),fjwkb,gjwkb,iexp)
      if ( iexp .gt. 1 .or. gjwkb .gt. one/(acch*ten2) )  go to 50
      xlturn = .false.
      ta =  two * abort
      pk =  zero
      wi =  eta + eta
      p  =  zero
      q  =  one - eta * xi
      ar = - e2mm1
      ai =  eta
      br =  two * ( x - eta )
      bi =  two
      dr =  br / ( br * br + bi * bi )
      di = - bi / ( br * br + bi * bi )
      dp = - xi * ( ar * di + ai * dr )
      dq =  xi * ( ar * dr - ai * di )
   40 p     = p  + dp
      q  = q  + dq
      pk = pk + two
      ar = ar + pk
      ai = ai + wi
      bi = bi + two
      d  = ar * dr - ai * di + br
      di = ai * dr + ar * di + bi
      c  = one / ( d * d + di * di )
      dr =  c * d
      di = - c * di
      a  = br * dr - bi * di - one
      b  = bi * dr + br * di
      c  = dp * a - dq * b
      dq = dp * b + dq * a
      dp = c
      if ( pk .gt. ta ) then
	 ifail = 2
	 write (iout,1030) abort,p,q,dp,dq,acc
	 return
      endif
      if ( abs(dp)+abs(dq) .ge. (abs(p)+abs(q))*acc )	go to 40
      paccq = half * acc / min( abs(q), one )
      if ( abs(p) .gt. abs(q) ) paccq = paccq * abs(p)
*
*     solve for fcm = f at lambda = xlm, then find norm factor w = w/fcm
*
      gam = ( f - p ) / q
      if ( q .le. acc4*abs(p) ) then
	 ifail = 3
	 write (iout,1040) p,q,acc,dell,lxtra,m1
	 return
      endif
      w   = one / sqrt((f - p)*gam + q)
      go to 60
*
*     here if g(xlm) .gt. 10**6 or iexp .gt. 70 and xlturn = .true.
*
   50 w   = fjwkb
      gam = gjwkb * w
      p   = f
      q   = one
*
*     normalise for spherical or cylindrical bessel functions
*
   60 alpha = zero
      if ( kfn	.eq. 1 ) alpha = xi
      if ( kfn	.eq. 2 ) alpha = xi * half
      beta  = one
      if ( kfn	.eq. 1 ) beta  = xi
      if ( kfn	.eq. 2 ) beta  = sqrt(xi) * rt2dpi
      fcm  = sign(w,fcl) * beta
      fc(m1)  = fcm
      if ( mode .eq. 3 ) go to 70
      if ( .not. xlturn ) gcl =  fcm * gam
      if ( xlturn ) gcl =  gjwkb * beta
      if ( kfn .ne. 0 ) gcl = - gcl
      gc(m1) = gcl
      gpl =  gcl * ( p - q / gam ) - alpha * gcl
      if ( mode .eq. 2 ) go to 70
      gcp(m1) = gpl
      fcp(m1) = fcm * ( f - alpha )
   70 if ( lxtra .eq. 0 ) return
*
*     upward recurrence from gc(m1),gcp(m1)  stored value is rl
*     renormalise fc,fcp at each lambda and correct regular derivative
*     xl   = xlm here and rl = one , el = zero for bessels
*
      w    = beta * w / abs(fcl)
      maxl = l1 - 1
      do 80 l = m1, maxl
	 if ( mode .eq. 3 ) go to 80
	 xl = xl + one
	 if (etane0) el = eta / xl
	 if (etane0) rl = gc(l+1)
	 sl = el + xl * xi
	 gcl1 = ( ( sl - alpha ) * gcl - gpl ) / rl
	 gpl = rl * gcl - ( sl + alpha ) * gcl1
	 gcl = gcl1
	 gc(l+1) = gcl1
	 if ( mode .eq. 2 ) go to 80
	 gcp(l+1) = gpl
	 fcp(l+1) = w * ( fcp(l+1) - alpha * fc(l+1) )
	 fc(l+1) = w * fc(l+1)
   80 continue
      return
*
 1000 format (' for xx = ',1pd12.3,' try small-x  solutions',
     x	      ' or x negative',/,' square root accuracy parameter =  ',
     x	      d12.3/)
 1010 format (/,' problem with input order values:xlmax,xlmin,xlm = ',
     x	      1p3d15.6,/)
 1020 format (' cf1 has failed to converge after ',f10.0,' iterations',
     x	      /,' f,df,pk,px,accur =  ',1p5d12.3,//)
 1030 format (' cf2 has failed to converge after ',f7.0,' iterations',
     x	      /,' p,q,dp,dq,accur =  ',1p4d17.7,d12.3//)
 1040 format (' final q.le.abs(p)*acc*10**4 , p,q,acc = ',1p3d12.3,4x,
     x	      ' dell,lxtra,m1 = ',d12.3,2i5,/)
 1050 format (' xlmax - xlmin = dell not an integer ',1p3d20.10,/)
      end
      subroutine decay (skj,l,r,u,uder)
*
* $Id: decay.f,v 2.1 94/09/29 18:43:33 vmb Exp Locker: vmb $
*
      implicit integer (a-z)
      real*8 skj,r,u,uder,zero,one,two,a,sk,rootk,twor,factor,xlp,sum1,
     x	     sum2,dfloat
      parameter ( zero=0.0d0,one=1.0d0,two=2.0d0,lmax=150 )
      dimension a(lmax+1)
      common /io/ iin,iout
*
*     decay : exponentially decaying solution at r of the equation
*		2     2    2
*	       d u /dr +(-k-l(l+1)/(r*r))u =0
*		  l			  l
*	      where k*k is real positive, l = angular momentum
*	      solution obtained by expanding,
*	      u = exp(-k*r) * ( a(1) + a(2)/r + a(3)/r**2 + ... )
*	      and using recursion relation
*	      a    = ( (l*l+l-n*n-n) / ( 2*k*(n+1) ) ) *a  with a =1
*	       n+1					 n	 0
*	      series terminates when n=l
*
      sk = - skj
      if ( sk .lt. zero ) then
	  write (iout,1000) sk
	  stop
      endif
      if ( l .gt. lmax ) then
	 write (iout,1010) l,lmax
	 stop
      endif
      rootk = sqrt(sk)
      twor = two * rootk
      factor = exp(-rootk*r)
      if ( l .gt. 0 ) then
	 a(1) = one
	 xlp = dfloat(l*(l+1))
	 n = l
	 do 10 j = 1, n
	    a(j+1) = a(j) * ( xlp - dfloat((j-1)*j)) / ( twor * 
     x               dfloat(j) )
   10	 continue
	 sum1 = one
	 sum2 = zero
	 do 20 jj = 1, n
	    sum1 = sum1 + a(jj+1) / ( r ** jj )
	    sum2 = sum2 + ( a(jj+1) * dfloat(jj) ) / ( r ** (jj+1) )
   20	 continue
	 u = factor * sum1
	 uder = - factor * sum2 - rootk * u
      else
	 u = factor
	 uder = - rootk * u
      endif
      return
*
 1000 format (' decay : input error, sk =',f12.5)
 1010 format (' decay : input error, l  =',i10,' lmax =',i10)
      end
      function flminx ()
*
* $Id: flminx.f,v 2.1 94/09/29 18:43:35 vmb Exp Locker: vmb $
*
      real*8 flminx,value
      parameter ( value=1.0d-200 )
      flminx = value
      return
      end
      subroutine gail2 (j,skj,etaj,et,l2p,nchan,cf,numax,jopen,iasy,
     x			degeny,ga,gb,gmax,fac1,fac2,fac3,fac4,
     x                  iac1,deg,sma,smb)
*
* $Id: gail2.f,v 2.1 94/09/29 18:43:36 vmb Exp Locker: vmb $
*
      implicit integer (a-z)
      logical jopen
      real*8 skj,etaj,et,cf,degeny,zero,etj,ga,gb,sma,smb,sgn,
     x       fac1,fac2,fac3,fac4,save,two,dfloat,dena,gmax,one,fp1,fp2,
     x       fp12,scalx,rscalx
      parameter ( zero=0.0d0,one=1.0d0,two=2.0d0 )
      dimension et(nchan),l2p(nchan),cf(nchan,nchan,numax),deg(nchan),
     x          ga(nchan,0:iasy+1),gb(nchan,0:iasy+1),fac1(nchan),
     x          fac2(nchan),fac3(nchan),fac4(nchan),iac1(nchan),
     x          sma(nchan),smb(nchan)
      common /io/ iin,iout
      common /mach/ scalx,rscalx
*
      iasy1 = iasy + 1 
      etj = et(j)
      lj = l2p(j)
      llj = lj * ( lj + 1 )
      sgn = one
      if (.not.jopen) sgn = - sgn
      do 800 i = 1, nchan
            fac2(i) = two * skj
            iac1(i) = llj - l2p(i) * ( l2p(i) + 1 ) 
            fac3(i) = two * etaj
            fac4(i) = two * dfloat( llj ) / skj
            ga(i,0) = zero
            ga(i,1) = zero
            gb(i,0) = zero
            gb(i,2) = zero    
         if ( abs(et(i)-etj) .gt. degeny ) then
            fac1(i) = one / ( et(i) - etj )
            deg(i) = 0
            ga(i,2) = scalx * fac1(i) * cf(i,j,1)
            gb(i,1) = zero
         else
            deg(i) = 1
            gb(i,1) = scalx * cf(i,j,1) / fac2(i)
         endif
  800 continue
      ga(j,0) = scalx
      do 500 p = 3, iasy1
         p1 = p - 1
         p2 = p - 2
         p12 = p1 * p2
         fp1 = dfloat(p1)
         fp2 = dfloat(p2)
         fp12 = dfloat(p1 + p2)
         do 300 m = 1, nchan
            sma(m) = zero
            smb(m) = zero
            save = dfloat( p12 + iac1(m) )
            gb(m,p) = - save * gb(m,p2)
            ga(m,p) =  - save * ga(m,p2) + fac3(m) * sgn *
     x                  fp12 * gb(m,p2) + sgn * fac4(m) * 
     x                  fp2 * gb(m,p-3)
  300    continue
         if (numax .ge. 1) then
            nux = min( numax, p1 )
            do 35 nu = 1,nux
               call dgemv('n',nchan,nchan,one,cf(1,1,nu),nchan,
     x                    ga(1,p1-nu),1,one,sma,1)
               call dgemv('n',nchan,nchan,one,cf(1,1,nu),nchan,
     x                    gb(1,p1-nu),1,one,smb,1)
 35         continue
         end if 
         do 45 m = 1,nchan
            if ( deg(m) .eq. 1 ) then
               dena = one / (fac2(m) * fp1)
               ga(m,p1) = -sgn * (gb(m,p) + smb(m)) * dena
               gb(m,p1) = (ga(m,p) + sma(m)) * dena 
            else
               dena = fac2(m) * fp1
               ga(m,p) = (ga(m,p) + sma(m) - dena * gb(m,p1))*fac1(m)
               gb(m,p) = (gb(m,p) + smb(m) + sgn * dena *
     x                   ga(m,p1)) * fac1(m) 
            end if     
             gmax = max(abs(ga(m,p1)),abs(gb(m,p1)),gmax)
 45      continue
         if ( gmax .gt. rscalx ) then
            p2 = (p2/2) * 2
            write (iout,1050) p2
            stop
         endif
  500 continue
      return
*
 1050 format (' gail2 : scaling overflow, set iasy =',i3)
      end
      subroutine gailco (nchan,ipflg,et,l2p,ion,cf,lamax,
     x                   iasy,degeny,gmax,x,ix)
*
* $Id: gailco.f,v 2.1 94/09/29 18:43:38 vmb Exp Locker: vmb $
*
      implicit integer (a-z)
      logical jopen
      real*8 et,cf,degeny,x,zero,etj,skj,
     x       etaj,dfloat,gmax
      parameter ( zero=0.0d0 )
      dimension et(nchan),ipflg(4),
     x          l2p(nchan),cf(nchan,nchan,lamax),x(*),ix(*)
      common /glpt/ ga1,ga2,fc,fcp,gc,gcp,xg,xc,xd,xcp,xdp,fac1,fac2,
     x              fac3,fac4,iac1,deg,sma,smb
*
      iasy1 = iasy - 1
      ncha = nchan * (iasy + 1)
      ga = ga1 - ncha
      gb = ga2 - ncha
      gmax = zero
      do 80 j = 1, nchan
         etj = et(j)
         skj = sqrt(abs(etj))
         etaj = - dfloat(ion) / skj
         jopen = etj .ge. zero
         ga = ga + ncha
         gb = gb + ncha
         call gail2 (j,skj,etaj,et,l2p,nchan,cf,lamax,jopen,iasy1,
     x	             degeny,x(ga),x(gb),gmax,x(fac1),x(fac2),
     x               x(fac3),x(fac4),ix(iac1),ix(deg),x(sma),x(smb))
   80 continue
*
*     print expansion coefficients if ipflg=1
*
      if (ipflg(1) .ne. 0) then
         ga = ga1 - ncha
         gb = ga2 - ncha
         do 90 j = 1, nchan
            ga = ga + ncha
            gb = gb + ncha
            call prgail (j,nchan,iasy1,x(ga),x(gb))
 90      continue
      end if
      return
      end
      subroutine gailit (f,fp,nchan,nmax,ipflg,rafin,et,l2p,ion,
     x                   iasy,eps,lmax,x)
*
* $Id: gailit.f,v 2.1 94/09/29 18:43:40 vmb Exp Locker: vmb $
*
      implicit integer (a-z)
      logical jopen
      real*8 f,fp,rafin,et,eps,x,zero,etj,skj,
     x       etaj,dfloat
      parameter ( zero=0.0d0 )
      dimension f(nchan,nchan,2),fp(nchan,nchan,2),et(nmax),ipflg(4),
     x          l2p(nmax),x(*)
      common /glpt/ ga1,ga2,fc,fcp,gc,gcp,xg,xc,xd,xcp,xdp,fac1,fac2,
     x              fac3,fac4,iac1,deg,sma,smb
*
      nfc = lmax + 10
      iasx = iasy / 2
      iasy1 = iasy - 1
      do 50 jdd = 1, nchan
         do 40 idd = 1, nchan
            f(idd,jdd,1) = zero
            fp(idd,jdd,1) = zero
            f(idd,jdd,2) = zero
            fp(idd,jdd,2) = zero
   40    continue
   50 continue
      ncha = nmax * (iasy + 1)
      ga = ga1 - ncha
      gb = ga2 - ncha
      do 80 j = 1, nchan
         jchan = j
         lj = l2p(j)
         etj = et(j)
         skj = sqrt(abs(etj))
         etaj = - dfloat(ion) / skj
         jopen = etj .ge. zero
         ga = ga + ncha
         gb = gb + ncha
         call asywfn (nchan,nmax,iasy1,f,fp,x(fc),x(fcp),x(gc),
     x                x(gcp),eps,ion,nfc,ipflg(3),iasx,x(xg),
     x                x(xc),x(xd),jchan,lj,rafin,skj,etj,etaj,jopen,
     x                x(ga),x(gb),x(xcp),x(xdp),ipflg(4))
   80 continue
      return
      end
      subroutine galerr (i,j,f1,d1,fp1,dp1)
*
* $Id: galerr.f,v 2.1 94/09/29 18:43:42 vmb Exp Locker: vmb $
*
      implicit integer (a-z)
      real*8 f1,fp1,d1,dp1,escat,e
      common /ie/ ierr,escat
*
      if ( ierr .ne. 0 ) then
         e = escat
         write (ierr,1000) e,i,j,f1,d1,fp1,dp1
      endif
      return
 1000 format (' galerr : e =',f12.5,' i =',i3,' j =',i3,' f =',f12.6,
     x        ' (',f12.6,') fp =',f12.6,' (',f12.6,')')
      end
      subroutine galptr (nchan,iasy,lmax,pt0,mcor)
*
* $Id: galptr.f,v 2.1 94/09/29 18:43:43 vmb Exp Locker: vmb $
*
      implicit integer (a-z)
      common /io/ iin,iout
      common /glpt/ ga1,ga2,fc,fcp,gc,gcp,xg,xc,xd,xcp,xdp,fac1,fac2,
     x              fac3,fac4,iac1,deg,sma,smb
*
      nfc = lmax + 10
      iasx = iasy / 2
      iasx1 = iasx + 1
      iasy1 = iasy + 1
      lega =iasy1  * nchan * nchan
*
      ga1 = pt0
      ga2 = ga1 + lega
      fc = ga2 + lega
      fcp = fc + nfc
      gc = fcp + nfc
      gcp = gc + nfc
      xg = gcp + nfc
      xc = xg + iasy1
      xd = xc + iasx1
      xcp = xd + iasy1
      xdp = xcp + iasx1
      fac1 = xdp + iasx1
      fac2 = fac1 + nchan
      fac3 = fac2 + nchan
      fac4 = fac3 + nchan
      sma = fac4 + nchan
      smb = sma + nchan
      iac1 = wpadti(smb + nchan + 1)
      deg = iac1 + nchan
      last = iadtwp(deg + nchan + 1) + 1
      if ( last .gt. mcor ) then
         write (iout,1000) last,mcor
         stop
      endif
      pt0 = last
      return
 1000 format (' storage overflow ( galptr ) : last =',i10,' core =',
     x        i10)
      end
      subroutine jwkb (xx,eta1,xl,fjwkb,gjwkb,iexp)
*
* $Id: jwkb.f,v 2.1 94/09/29 18:43:45 vmb Exp Locker: vmb $
*
      implicit integer (a-z)
      real*8 aloge,half,one,rl35,six,ten,zero,eta,eta1,fjwkb,gh,gh2,
     x       gjwkb,hl,hll,phi,phi10,rl2,sl,x,xl,xll1,xx,dfloat
      parameter ( zero=0.0d0,half=0.5d0,one=1.0d0,six=6.0d0,ten=1.0d1,
     x		  rl35=35.0d0,aloge=0.4342945d0 )
*
*     jwkb approximations to coulomb functions for xl.ge. 0
*	   biedenharn et al. phys rev 97 (1955) 542-554
*
      x     = xx
      eta   = eta1
      gh2   = x * ( eta + eta - x )
      xll1  = max(xl*xl + xl,zero)
      if ( gh2 + xll1 .le. zero ) return
      hll  = xll1 + six / rl35
      hl   = sqrt(hll)
      sl   = eta / hl + hl / x
      rl2  = one + eta * eta / hll
      gh   = sqrt(gh2 + hll) / x
      phi  = x * gh - half * ( hl * log((gh + sl)**2/rl2) - log(gh) )
      if ( eta .ne. zero ) phi = phi - eta * atan2(x*gh,x - eta)
      phi10 = - phi * aloge
      iexp  =  int(phi10)
      if ( iexp .gt. 70 ) gjwkb = ten ** ( phi10 - dfloat(iexp) )
      if ( iexp .le. 70 ) gjwkb = exp(-phi)
      if ( iexp .le. 70 ) iexp	= 0
      fjwkb = half / ( gh * gjwkb )
      return
      end
      subroutine prgail (j,nchan,iasy,ga,gb)
*
* $Id: prgail.f,v 2.1 94/09/29 18:43:55 vmb Exp Locker: vmb $
*
      implicit integer (a-z)
      real*8 ga,gb
      dimension ga(nchan,0:iasy+1),gb(nchan,0:iasy+1)
      common /io/ iin,iout
*
*     print expansion coefficients if ipflg=1
*
      write (iout,1010) j
      write (iout,1020)
      write (iout,1030)
      do 260 i = 0, iasy
	 write (iout,1000) (ga(jj,i),jj = 1,nchan)
  260 continue
      write (iout,1040)
      do 270 i = 0, iasy
	 write (iout,1000) (gb(jj,i),jj = 1,nchan)
  270 continue
*
 1000 format (1x,5d14.6)
 1010 format (' expansion coefficients with j= ',i2)
 1020 format (1x,33('-'))
 1030 format (' gailitis a-coefficients :')
 1040 format (' gailitis b-coefficients :')
      return
      end
      subroutine rpolp (cr,zr,sr,srp,n)
*
* $Id: rpolp.f,v 2.1 94/09/29 18:43:46 vmb Exp Locker: vmb $
*
      implicit integer (a-z)
      real*8 cr,zr,sr,srp,dfloat
      dimension cr(0:n)
*
*     evaluate power series and derivative, coefficients cr, 
*     argument 1/zr, results in sr and srp
*
      sr = cr(n)
      srp = dfloat(n) * sr
      do 10 i = n-1, 0, -1
	 sr = sr * zr + cr(i)
         srp = srp * zr + dfloat(i) * cr(i)
   10 continue
      srp = - srp * zr
      return
      end
      subroutine rpolpp (cr,zr,sr,srp,n,np)
*
* $Id: rpolpp.f,v 2.1 94/09/29 18:43:48 vmb Exp Locker: vmb $
*
      implicit integer (a-z)
      real*8 cr,zr,sr,srp,dfloat,srq
      dimension cr(0:n)
*
*     evaluate power series and derivative, coefficients cr, 
*     argument 1/zr, results in sr and srp
*     leading inverse power of np
*
      sr = cr(n)
      srp = dfloat(np+n) * sr
      do 10 i = n-1, 0, -1
	 sr = sr * zr + cr(i)
         srp = srp * zr + dfloat(np+i) * cr(i)
   10 continue
      srq = zr**np
      sr = srq * sr
      srp = - srq * srp * zr
      return
      end
      subroutine rpolq (cr,zr,sr,srp,n,m,m1)
*
* $Id: rpolq.f,v 2.1 94/09/29 18:43:49 vmb Exp Locker: vmb $
*
      implicit integer (a-z)
      real*8 cr,zr,sr,srp,dfloat
      dimension cr(m,0:n)
*
*     evaluate power series and derivative, coefficients cr, 
*     argument zr, results in sr and srp
*
      sr = cr(m1,n)
      srp = dfloat(n) * sr
      do 10 i = n-1, 0, -1
	 sr = sr * zr + cr(m1,i)
         srp = srp * zr + dfloat(i) * cr(m1,i)
   10 continue
      srp = - zr * srp
      return
      end
      subroutine rrfr (n,np,g,c,cp,d,dp)
*
* $Id: rrfr.f,v 2.1 94/09/29 18:43:51 vmb Exp Locker: vmb $
*
      implicit integer (a-z)
      real*8 one,zero,ak,c,ci,cp,ct,d,dd,di,dn,dp,dt,g
      parameter ( zero=0.0d0,one=1.0d0 )
      dimension g(0:n),c(0:np),cp(0:np),d(0:np),dp(0:np)
*
*     rrfr : converts polynomial g to a rational fraction
*	       g    = vector g(k), k=0,n
*	       c,cp = numerator polynomials, convergents n and n-1
*	       d,dp = denominator polynomials, convergents n and n-1
*
      do 10  i = 1, np
	 c(i) = zero
	 cp(i) = zero
	 d(i) = zero
	 dp(i) = zero
   10 continue
      c(0) = g(0)
      cp(0) = g(0)
      d(0) = one
      dp(0) = one
      ct = zero
      dt = one
      dn = g(0)
      do 40 k = 1, n
	 dd = dn
	 dn = zero
	 nq = ( k + 1 ) / 2
c$dir scalar
	 do 20 i = 0, nq
	    dn = dn + g(k-i) * d(i)
   20	 continue
	 ak = - dn / dd
	 if ( ak .eq. zero ) return
	 do 30 i = 1, nq
	    ci = c(i)
	    c(i) = ci + ak * ct
	    ct = cp(i)
	    cp(i) = ci
	    di = d(i)
	    d(i) = di + ak * dt
	    dt = dp(i)
	    dp(i) = di
   30	 continue
	 ct = g(0)
	 dt = one
   40 continue
      return
      end
      subroutine rrfvl (nt,ip,xi,xc,xd,xcp,xdp,dr,dpr,aerr,aepr,ierr)
*
* $Id: rrfvl.f,v 2.1 94/09/29 18:43:53 vmb Exp Locker: vmb $
*
      implicit integer (a-z)
      real*8 xi,xc,xd,xcp,xdp,dr,dpr,aerr,aepr,csr,cspr,dsr,dspr,
     x       dr1,dpr1
      dimension xc(0:nt),xd(0:nt),xcp(0:nt),xdp(0:nt)
*
*     rrfvl : evaluates real rational fraction
*
      call rpolpp (xc,xi,csr,cspr,nt,ip)
      call rpolp (xd,xi,dsr,dspr,nt)
      dr = csr / dsr
      dpr = ( cspr * dsr - csr * dspr ) / ( dsr * dsr )
*     error estimates
      if ( ierr .ne. 0 ) then
	 nt1 = nt - 1
         call rpolpp (xcp,xi,csr,cspr,nt1,ip)
         call rpolp (xdp,xi,dsr,dspr,nt1)
         dr1 = csr / dsr
         dpr1 = ( cspr * dsr - csr * dspr ) / ( dsr * dsr )
	 aerr = abs(dr-dr1)
	 aepr = abs(dpr-dpr1)
      endif
      return
      end
