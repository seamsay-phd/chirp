      subroutine asyms (ie,e,rmat,akmat,nchan,nschan,ispch,ichsp,
     x                  lchl,ion,lamax,cf,r,bbloch,
     x                  ethr,f,fp,et,nbasis,nampx,nranmx,rpflg,
     x                  prtflg,icol,apeps,degeny,iasy,lmax,
     x                  ewron,nopen,raf,raflw,asect,lwsect,lwkeep,
     x                  keep,nfinal,lwran,iapflg,diaflg,idiscr,
     x                  lwdb,beta,cuplim,deplim,ncrit,rfact,
     x                  gfact,maxtry,radius,rstats,rafix,x,ix)
*
* $Id: asyms.f,v 2.1 94/09/29 18:39:17 vmb Exp Locker: vmb $
*
      implicit integer (a-z)
      real*8 e,rmat,akmat,cf,bbloch,r,x,f,fp,et,ewron,apeps,degeny,
     x     rafin,asect,raflw,raf,lwsect,rfact,gfact,rafmax,ethr,
     x     r1,r2,beta,gmax,gmaxp,radius,rafix,rlwmax,cuplim,deplim
      logical fail,lwdb
      dimension lchl(nchan),cf(nchan,nchan,*),ix(*),
     x          nbasis(nranmx),rmat(*),akmat(*),x(*),
     x          prtflg(*),iapflg(4),rpflg(6),diaflg(20),
     x          f(nchan*nchan*2),fp(nchan*nchan*2),et(nchan),asect(*),
     x          raf(*),lwsect(*),radius(maxtry),ethr(nchan),
     x          rstats(maxtry),nschan(2),ispch(nchan),ichsp(nchan),
     x          lwkeep(lwran),keep(lwran),lwdb(2)
      common /io/ iin,iout
      common /pptr/ ampa,eigen,bigvec,wo
      common /kp/ bb,iwrk,bigvc
      common /lw/ vmat,enrg,tc,tev,tp,tsave,pdiag,r4,space,tpev,b1,
     x		  rmtx,cupest,ipvt
      save gmaxp,nopenp
*
*   keep channels if less deeply bound than deplim
*   but keep at least ncrit channels
*   further keep at each propagation sector at least the number of
*   channels decided by the strength of the coupling in the LW 
*   package
*
      do 40  i = nopen + 1,nchan
         if (et(i) .lt. -deplim) then
            nkeep = i - 1
            go to 45
         end if
 40   continue
      nkeep = nchan
 45   nkeep = min (nkeep + ncrit, nchan)
      do 50  i = 1,nfinal
         keep(i) = max(lwkeep(i), nkeep)
 50   continue
*
*  calculate gailitis coefficients for all channels as these are 
*  independent of radius
*
      call gailco (nchan,iapflg,et,lchl,ion,cf,lamax,iasy,
     x             degeny,gmax,x,ix)
*
*  on the basis of the magnitude of the maximum coefficient decide
*  whether to use at least the final radius calculated for the previous 
*  electron energy so long as a threshold has not been crossed
* 
      if ( ie .gt. 1  ) then
         if ( nopen .eq. nopenp  .and. gmax .ge. gmaxp * gfact ) then
            raf(ie) = max (raf(ie),raf(ie - 1))
         end if
      end if
      rafin = raf(ie)
      rafmax = lwsect(nfinal)
      rlwmax = rafix
      if (nfinal .eq. lwran) rlwmax = lwsect(lwran)
*
*  redefine the trial radius to lie on a sector end
*
      call secend (rafin,nrange,nranmx,asect,isect,nfinal,lwsect,lwran)
*
*  try at successive radii until obtain converged wavefunctions
*
      do 60 itry = 1,maxtry
         ntry = itry
         raf(ie) = rafin
	 print *,' isect = ', isect
         nc = keep(isect)
*
*  calculate wavefunctions at rafin for the retained channels
*
         call gailit (f,fp,nc,nchan,iapflg,rafin,et,lchl,ion,
     x                iasy,apeps,lmax,x)
         radius(itry) = rafin
*
*  test the multi-channel wronskian for convergence
*
         call wronsk (nc,nopen,f,fp,diaflg(5),diaflg(6),ewron,fail)
         if ( diaflg(4) .eq. 1 ) then
            k1 = 1  
            do 70 k = 1, 2
               write (iout,1110) rafin
               call wrtmat (f(k1),nc,nc,nc,icol)
               k1 = k1 + nc*nc 
   70       continue
            k1 = 1
            do 80 k = 1, 2
               write (iout,1120) rafin
               call wrtmat (fp(k1),nc,nc,nc,icol)
               k1 = k1 + nc*nc
   80       continue
         end if
*
*  if the test fails and rafin is not at its allowed maximum try
*  extending rafin by a factor rfact
*
         if ( fail .and. rafin .lt. rlwmax ) then
            gmaxp = gmax
            rafin = min(rfact * rafin, rlwmax)
*
*  if necessary extend the range over which the propagator is
*  initiated otherwise redefine the new trial radius at a sector
*  end
*
            if (rafin .gt. rafmax) then
               nrange = nranmx
               r1 = rafmax
               r2 = rafin 
               nf = nfinal
               call slwpot (nfinal,x(vmat),x(enrg),x(tc),x(tp),x(tsave),
     x                      nchan,nchan,lwdb(1),lwdb(2),r1,r2,lwsect,
     x                      lwkeep,
     x                      lwran,beta,cuplim,x(space),x(tev),x(tpev),
     x                      lamax,ion,lchl,cf,ethr,idiscr,x(cupest))
               rafin = lwsect(nfinal)
               if (nfinal .eq. lwran) rlwmax = lwsect(lwran)
               do 100 i = nf + 1,nfinal
                  keep(i) = max(lwkeep(i),nkeep)
 100           continue
               isect = nfinal       
               rafmax = rafin
            else
               call secend (rafin,nrange,nranmx,asect,isect,nfinal,
     x                      lwsect,lwran)
            end if   
         else
            go to 90
         end if
 60   continue
      write (iout,1130) ntry, rafin
      stop 
 90   if (fail) then
         if (nfinal .lt. lwran) write (iout,1140) rafin
         if (nfinal .eq. lwran) write (iout,1145) rafin, lwran
      end if
      nopenp = nopen
      rstats(ntry) = rstats(ntry) + 1
      if (ie .eq. 1) gmaxp = gmax
      if (diaflg(12) .eq. 1) then
         write (iout,1150)ie,e,(radius(i),i=1,ntry)
      end if
*
*     propagate r-matrix to radius rafin
*     use BBM propagator as far as raflw and LW to rafin
*
      if (raflw .ne. r ) then
         if ( diaflg(3) .ne. 0 ) then
            write (iout,*)'rmat before splitting spins'
            call wrttmt (rmat,nchan,icol)
         end if
         call splitr(nchan,rmat,x(bigvec),ichsp)
         if ( diaflg(3) .ne. 0 ) then
            write (iout,*)'rmat after splitting spins'
            call wrttmt (rmat,nchan,icol)
         end if
         call rprop2 (rmat,e,nschan,nbasis,x(ampa),x(eigen),x(bigvec),
     x                nranmx,nampx,rpflg(4),1,nrange,asect,0)
         call backr (nchan,rmat,x(bigvec),ispch)
         if ( diaflg(3) .ne. 0 ) then
            r1 = min (raflw,rafin)
            write (iout,1160) r1
            call wrttmt (rmat,nchan,icol)
         endif
      end if
      if (raflw .lt. rafin) then
         call lwprop (x(enrg),x(tev),x(tpev),x(tsave),x(pdiag),x(r4),
     x                keep,nfinal,nchan,e,lwdb(1),lwdb(2),raflw,
     x                rafin,x(space),x(b1),x(rmtx),idiscr,ix(ipvt),
     x                rmat)
         if ( diaflg(3) .ne. 0 ) then
            write (iout,1170) rafin
            call wrttmt (rmat,keep(nfinal),icol)
         endif
      end if
      call kmat (nc,rafin,bbloch,x(bigvc),x(bb),ix(iwrk),nopen,
     x           f,fp,rmat,akmat)
      if ( prtflg(1) .gt. 1 ) then
         write (iout,1180)
         call wrttmt (akmat,nopen,icol)
      endif
      return
*
 1110 format (/' solutions at rafin=',f9.3)
 1120 format (/' derivatives at rafin',f9.3)
 1130 format (/' failed to calculate accurate wave functions'/
     x        /' tried ',i3,' times.  final radius was ',f9.3)
 1140 format (/,' *****  warning - wronskian test failed  at ',f9.3,/
     x          ' the calculation continues at this radius',/,
     x          ' this radius is limited by rafix set in subr cntrl',
     x          ' *********' )
 1145 format (/,' *****  warning - wronskian test failed  at ',f9.3,/
     x          ' the calculation continues at this radius',/,
     x          ' this radius is at the end of the last possible',
     x          ' LW sector,lwran =',i4,/,
     x          ' lwran is set in subr cntrl',' *********' )
 1150 format (/12x,'energy    radii at which functions calculated',/
     x        ' ',i5,2x,f12.6,5(2x,f12.5),/,23x,5(f12.5,2x),
     x        /,23x,5(f12.5,2x),/,23x,5(f12.5,2x))
 1160 format (/,' r-matrix after propagation by BBM at radius =',f10.5)
 1170 format (/,' r-matrix after propagation by LW at radius =',f10.5,/)
 1180 format (/' k-matrix :')
      end
      subroutine backr(nchan,rmat,sprmat,ichsp)
*
* $Id: backr.f,v 2.1 94/09/29 18:40:25 vmb Exp Locker: vmb $
*
      implicit integer (a-z)
      real*8 rmat,sprmat
      dimension rmat(*),sprmat(nchan,nchan),ichsp(nchan)
*
*     split rmat into spins by rearranging channels
*
      kk = 0
      do 10 i2 = 1,nchan
         ich2 = ichsp(i2)
         do 10 i1 = 1,i2
            kk = kk + 1
            ich1 = ichsp(i1)
            sprmat(ich1,ich2) = rmat(kk)
            sprmat(ich2,ich1) = rmat(kk)
 10   continue
      kk = 0
      do 20 i2 = 1,nchan
         do 20 i1 = 1,i2
            kk = kk + 1
            rmat(kk) = sprmat(i1,i2)
 20   continue
      return
      end
      double precision function but0 (nbut,fkn,uk,u)
*
* $Id: but0.f,v 2.1 94/09/29 18:39:20 vmb Exp Locker: vmb $
*
      implicit real*8 (a-h,o-z)
      parameter (zero=0.0d0,one=1.0d0,two=2.0d0,pt4=0.4d0,
     x           sr=0.04d0,srm=-0.04d0,closep=0.3d0)
      logical pole
      dimension fkn(0:nbut),uk(0:nbut)
*
*     buttle correction
*
      s1 = one / dfloat(3)
      s2 = one / dfloat(45)
      s3 = two / dfloat(945)
      s4 = one / dfloat(4725)
      b = zero
*
*     u .gt. sr
*
      if ( u .gt. sr ) then
         fk = sqrt(u)
         pole = .false.
         do 10 n = 0, nbut
            if ( abs(fk-fkn(n)) .gt. closep ) then
               b = b + one / ( u - uk(n) )
            else
               pole = .true.
               d1 = fk - fkn(n)
            endif
   10    continue
         if (pole) then
            d2 = d1**2
            d = d1 * ( s1 + d2 * ( s2 + d2 * ( s3 + s4 * d2 ) ) )
            b = two * b + ( d + one / ( two * fk - d1 ) ) / fk
         else
            b = two * b + tan(fk) / fk
         endif
*
*     sum for u .lt. sr
*
      else
         do 20 n = 0, nbut
            b = b + one / ( u - uk(n) )
   20    continue
*
*    u .lt. sr and u .gt. srm=-sr
*
         if ( u .gt.  srm ) then
            b = two * b + one + s1 * u * ( one + pt4 * u )
*
*     u lt. -sr
*
         else
            fk = sqrt(-u)
            b = two * b + tanh(fk) / fk
         endif
      endif
      but0 = b
      return
      end
      subroutine butco (cf,cfbut1,lchl,nchan,lrang2)
*
* $Id: butco.f,v 2.1 94/09/29 18:40:08 vmb Exp Locker: vmb $
*
      implicit integer (a-z)
      real*8  cf,cfbut1
      dimension cf(3,lrang2),cfbut1(3,lrang2),lchl(nchan)
*
*     butco selects the required buttle coefficients
*
      do 10 i= 1,nchan
         l = lchl(i) + 1
         do 20 j = 1,3
            cf(j,i) = cfbut1(j,l)
 20      continue
 10   continue
      return
      end
      subroutine case(llo,lup,ldel,nlran,lrglx,ncases,mintsp,
     x                maxtsp,minlsp,maxlsp,lrgs,lrgl,lrgpi)
*
* $Id: case.f,v 2.1 94/09/29 18:40:55 vmb Exp Locker: vmb $
*
      implicit integer (a-z)
      common /io/ iin, iout
      dimension llo(nlran),lup(nlran),ldel(nlran),lrgs(ncases),
     x          lrgl(ncases),lrgpi(ncases)
*
*   construct the list of slpi cases 
*
      icase = 0
      do 30 i = 1,nlran
         ld = max(ldel(i),1)
         do 40 l = llo(i), lup(i), ld
            if (l .le. lrglx) then
               do 50 ipi = 0,1
                  do 60 ilsp =  minlsp, maxlsp, 2
                     icase = icase + 1
                     lrgl(icase) = l
                     lrgpi(icase) = ipi
                     lrgs(icase) = ilsp
 60               continue
 50            continue
            else
               do 70 ipi = 0,1
                  do 80 itsp = mintsp, maxtsp, 2
                     icase = icase + 1
                     lrgl(icase) = l
                     lrgpi(icase) = ipi
                     lrgs(icase) = -itsp
 80               continue
 70            continue
            end if
 40      continue
 30   continue
*
      if (icase .gt. ncases ) then
          write(iout,1000)icase, ncases
          stop
      end if
*
      return
*
 1000 format(' **** error in case - array overflow',/
     x        'icase, ncases',2i4)
      end

            
      
      subroutine cbut (fkn,uk,nbutx)
*
* $Id: cbut.f,v 2.1 94/09/29 18:40:07 vmb Exp Locker: vmb $
*
      implicit integer  (a-z)
      real*8 fkn,uk,g,pi,half
      parameter  (half = 0.5d0,pi = 3.14159265d0)
      dimension fkn(0:nbutx),uk(0:nbutx)
*
*     cbut sets up the arrays needed by but0
*
      g = half * pi
      g = -g
      do 10 i = 0,nbutx
         g = g + pi
         fkn(i) = g
         uk(i) = g * g
 10   continue
      return
      end

      subroutine cntrl(x,ix,mcore)
*
* $Id: cntrl.f,v 2.1 94/09/29 18:39:14 vmb Exp Locker: vmb $
*
      implicit integer (a-z)
      character*80 title
      character*128 fout,filh,fkmat,fpha,ftmat,fcros,fom,ftop,
     x              fsmat,flwp,frmp
      real*8 zero,x,dfloat,bbloch,rmatr, apeps, rafin, sw1,
     x       quad,esc1,abvthr,belthr,delta,
     x       ewron,rfirst,rlast,degeny, e0, 
     x       ebig, scalx, rscalx, flmin,dlamch,one, 
     x       facmin,rafest,rafmax,raflw,raflw1,beta,
     x       lwsect, flion, cuplim, cuplm, mingap, ming,
     x       deplim, rfact,gfact,rafix,radwm 
      logical jj,lwdb,finis,adwm
      parameter ( one=1.0d0,facmin=1.0d+15 )
      parameter ( nbmx=100,lwran=1000,ncase=80,nlran=10 )
      dimension nschan(2),nspin(2),idscbb(2),
     x          ne1(10),esc1(20),ls(ncase),ll(ncase),
     x          lpi(ncase),llo(nlran),lup(nlran),ldel(nlran),
     x          x(mcore),ix(*),
     x          nbasis(nbmx),rpflg(6),iapflg(4),
     x          diaflg(20),prtflg(7),nmt(7),lwsect(lwran),
     x          lwkeep(lwran),keep(lwran),lwdb(2),frmp(2)
      common /io/ iin, iout
      common /lw/ vmat,enrg,tc,tev,tp,tsave,pdiag,r4,space,tpev,b1,
     x		  rmtx,cupest,ipvt
      common /mach/ scalx,rscalx
      namelist /phzfil/fout,filh,fkmat,fpha,ftmat,fcros,fom,ftop,
     x                 fsmat,flwp,frmp,
     x                 iout,nfth,nmt,idiscr,idscbb
      namelist /phzin/ title,prtflg,diaflg,ne1,esc1,
     x                 ls,ll,lpi,jj,llo,lup,ldel,lrglx,lrglr,
     x                 abvthr,belthr,degeny,ewron,apeps,iasy,rafin,
     x                 rafix,rfact,gfact,maxtry,iapflg,
     x                 idbbw,rpflg,raflw,beta,cuplim,deplim,ncrit,
     x                 lwdb,icon,quad,ncol,ibuttl,lamax,radwm
*
*  phzin default values
*
      data prtflg/0,0,0,3,0,0,0/,diaflg/20*0/,ne1/10*0/,
     x     ll/80*-1/,jj/.false./,llo/10*0/,lup/10*-1/,ldel/10*1/,
     x     lrglx/12/,lrglr/1000/,
     x     abvthr/1.0d-3/,belthr/1.0d-3/,degeny/1.0d-3/,ewron/1.0d-4/,
     x     apeps/1.0d-4/,iasy/20/,
     x     rafix/1.0d4/,rfact/1.333d0/,gfact/0.5d0/,maxtry/33/,
     x     iapflg/4*0/,idbbw/0/,rpflg/6*0/,raflw/0.0d0/,
     x     beta/1.0d-6/,cuplim/1.0d-6/,deplim/1.0d-2/,ncrit/1/,
     x     lwdb/.false.,.false./,icon/1/,quad/0/,ncol/4/,
     x     lamax/20/,ibuttl/1/,radwm/2.0d0/
*
*  phzfil default values
*
      data fout/'OUT'/,filh/'H'/,
     x     fkmat/'KMATB'/,fpha/'PHASEF'/,ftmat/'TMATB'/,
     x     fcros/'CROSSB'/,fom/'OMEGAB'/,ftop/'TOPB'/,
     x     fsmat/'S00'/,
     x     flwp/'lwpscr'/,frmp/'bbscr1','bbscr2'/
      data nfth/10/,idscbb/11,12/,idiscr/13/,
     x     nmt/21,22,23,24,25,26,30/
*
*  topin default values for NO top up, prtflg(6)=0 (see subroutine topdat)
*
      data lrgle/999/,enum2/0/,nl/0/
*
      data zero/0.0d0/
*
*   data criteria
*   abvthr, belthr > 10**-8, sqrt(accur) in coulfg
*   rafix, max value of rafin, < abort/sqrt(max e), in coulfg
*   maxtry defined by rmatr*rfact**maxtry .ge. rafix
*
      iout = 3
      read (iin,phzfil,end=18,err=18)
      go to 19
 18   write (6,phzfil)
      stop
 19   open (iout,file = fout,
     x      access = 'sequential',status = 'unknown',
     x      form = 'formatted')
      write (iout, 1000)
*
*  calculate the safe minimum for the machine
*  calculate the scaling factor for the gailitis coefficients allowing
*  the factor facmin above the minimum to prevent underflow
*  calculate the reciprocal
*
c      flmin = dlamch('S')
      flmin = 1.0d-200
      scalx = facmin * flmin
      rscalx = one / scalx
      write(iout,*)'scalx,rscalx',scalx,rscalx
*
      do 10 i = 1,80
          title(i:i) = ' '
   10 continue
*
*  read first record on H file
*
      call rdhed(nfth,filh,nelc,nz,lrang2,ismax,ntarg,rmatr,
     x           bbloch)
      write (iout,2010) filh,nelc,nz,lrang2,ntarg,ismax,rmatr
      write (iout,2030)
      ion = nz - nelc
      zion = ion
*
*  for positive ions use z-scaling
*
      if (ion .gt. 0) then
         ion = 1
         flion = dfloat (zion)
         rmatr = flion * rmatr
      end if
*
*  set storage for target arrays
*
      etarg = 1
      ltarg = wpadti (etarg + ntarg+1)
      starg = ltarg + ntarg
      cfbut1 = iadtwp (starg + ntarg + 1)
      tlast = cfbut1 + 3 * lrang2 
      itlast = wpadti(tlast + 1)
      if (tlast .gt. mcore ) then
         write (iout, 1010) tlast,mcore
         stop
      end if
*
      it = itlast
      mcor1 = mcore -tlast +1
      call rdtar (nfth,x(etarg),ix(ltarg),ix(starg),x(cfbut1),ntarg,
     x            lrang2,zion,nbutx,maxcas,itlast,ix(it),mcor1)
      if ( zion .gt. 0) then
         nxsn = (ntarg * ( ntarg - 1 )) / 2
      else
         nxsn = (ntarg * ( ntarg + 1 )) / 2
      end if
      iind = itlast
      jind = iind + nxsn
      itlast = jind + nxsn
      tlast = iadtwp (itlast + 1)
      call xsnind(ntarg,nxsn,zion,ix(iind),ix(jind))
*
*  set storage for line strength and initialise
*
      sline = tlast
      tlast = sline + nxsn
      itlast = wpadti (tlast + 1)
      do 17 i = 1,nxsn
         x(sline + i - 1) = -one
 17   continue
*
*  set storage for buttle correction
*
      if (ibuttl .ne. 0 .and. nbutx .ne. 0) then
         fkn = tlast 
         uk = fkn +  nbutx + 1
         tlast = uk + nbutx + 1
         itlast = wpadti(tlast + 1)
         if (tlast .gt. mcore ) then
            write (iout, 1010) tlast,mcore
            stop
         end if
         call cbut (x(fkn),x(uk),nbutx)
      end if
      rafin = rmatr
      read (iin,phzin,end=20,err=20)
      go to 21
 20   write (iout,phzin)
      stop
*
*  final radius reset to the rmatrix radius if it had been wrongly input
*  to a value less than rmatr.   It is reset to rafix if it has been input
*  at a value too large for the coulomb wave function package.
*  raflw is the radius after which the LW propagator is used.   This must
*  also be initialised to at least the rmatrix radius and less than rafix.
*
 21    rafin = max(rafin,rmatr)
       rafin = min(rafin,rafix)
       raflw = max(raflw,rmatr)
       raflw = min(raflw,rafix)
*
*  allow the number of multipoles retained in the potential to be reduced
*  by the phzin namelist value of lamax.  If lamax = 0  the channel 
*  wavefunctions will be coulomb functions. If the input value of rafin 
*  causes propagation the long range potential is included in the propagation.
*  If rafin is defaulted there will be no propagation beyond rmatr and the 
*  long range potential will be neglected entirely.
*
      lamax = min(lamax,ismax)
      if (lamax .eq. 0) then
         iasy = 2
      else if (lamax .lt. ismax) then
         ismax = lamax
      end if
      if (ismax .eq. 0) then
         rafin = rmatr
         raflw = rmatr
      end if
*
*  ensure iasy is an even number gt 0
*
       iasy = (iasy/2) * 2
       iasy = max(2,iasy)
*
*  print basic input data or complete namelist phzin if diaflg(1) set
*
      write(iout,1020)title
      if ( diaflg(1) .gt. 0 ) then
         write (iout,phzin)
      else 
         call prtdat(prtflg,rafin,raflw,jj,lrglx,lrglr,radwm,
     x               lamax,iasy,abvthr,belthr,degeny,ewron,
     x               beta,cuplim,deplim,ncrit)
      end if
*
*  check prtflg 
*
      if (prtflg(4) .gt. 0  .and.  prtflg(5) .gt. 0) then 
          write(iout,1150)
          stop
      end if
      if (prtflg(6) .gt. 0  .and. 
     x    prtflg(4) .eq. 0  .and.  prtflg(5) .eq. 0) then 
         write(iout,1160)
         stop
      end if
      if (prtflg(6) .gt. 0  .and. prtflg(7) .ne. 0 ) then 
         write(iout,1165)
         stop
      end if
*      
*  open files required for output
*
      if ( prtflg(1) .eq. 1  .or.  prtflg(1) .eq. 3) then
         open (nmt(1),file = fkmat,access = 'sequential',
     x         status = 'unknown',form = 'unformatted')
         write(iout,2011) fkmat
      end if
      if ( prtflg(2) .eq. 1  .or.  prtflg(2) .eq. 3) then
         open (nmt(2),file = fpha,access = 'sequential',
     x         status = 'unknown',form = 'formatted')
         write(iout,2011) fpha
      end if
      if ( prtflg(3) .eq. 1  .or.  prtflg(3) .eq. 3) then
         open (nmt(3),file = ftmat,access = 'sequential',
     x         status = 'unknown',form = 'unformatted')
         write(iout,2011) ftmat
      end if
      if ( prtflg(4) .eq. 1  .or.  prtflg(4) .eq. 3) then
         open (nmt(4),file = fcros,access = 'sequential',
     x         status = 'unknown',form = 'unformatted')
         write(iout,2011) fcros
      end if
      if ( prtflg(5) .eq. 1  .or.  prtflg(5) .eq. 3) then
         open (nmt(5),file = fom,access = 'sequential',
     x         status = 'unknown',form = 'unformatted')
         write(iout,2011) fom
      end if
      if ( prtflg(6) .eq. 1  .or.  prtflg(6) .eq. 3) then
         open (nmt(6),file = ftop,access = 'sequential',
     x         status = 'unknown',form = 'unformatted')
         write(iout,2011) ftop
      end if
      if ( prtflg(7) .eq. 1  .or.  prtflg(7) .eq. 3) then
         open (nmt(7),file = fsmat,access = 'sequential',
     x         status = 'unknown',form = 'unformatted')
         write(iout,2011) fsmat
      end if
*
*  direct access files are opened for smatrices in prsmat.
*
*  make thresholds closer than degeny truly degenerate   
*  print the new energy levels if changed anf write headings to
*  output files for k-matrix and t-matrix
*
      call degtar (nelc,nz,ntarg,x(etarg),ix(ltarg),ix(starg),
     x             degeny,prtflg,nmt,title)
*
*  read ranges of L or J  
*
      call rdlran (jj,llo,lup,ldel,nlran,lrglx,ncases,ntarg,
     x             ix(starg),mintsp,maxtsp,minlsp,maxlsp)
*
*  if any ranges of L have been specified construct pointers for
*  arrays of S L Pi
*  else copy arrays of S L Pi from namelist
* 
      if (ncases .gt. 0) then
         lrgs = itlast
         lrgl = lrgs + ncases
         lrgpi = lrgl + ncases
         itlast = lrgpi + ncases
         tlast = iadtwp (itlast + 1)
         if (tlast .gt. mcore ) then
            write (iout, 1010) tlast,mcore
            stop
         end if
         call case(llo,lup,ldel,nlran,lrglx,ncases,mintsp,maxtsp,
     x             minlsp,maxlsp,ix(lrgs),ix(lrgl),ix(lrgpi))
      else
         lrgs = itlast
         lrgl = lrgs + ncase
         lrgpi = lrgl + ncase
         itlast = lrgpi + ncase
         tlast = iadtwp (itlast + 1)
         call rdcas (ls,ll,lpi,ix(lrgs),ix(lrgl),ix(lrgpi),ncase,
     x               ncases)
      end if
*
*  set storage for energy mesh
*
      nume1 = necnt(ne1)
      sce1 = tlast
      noch = wpadti (sce1 + nume1 + 1)
      itlast = noch + nume1
      tlast = iadtwp ( itlast + 1)
      scest = tlast
      norder = wpadti(scest + nume1 + 1)
*
*  store the energy mesh after pruning out energies too close to
*  threshold 
*
      call evals (ntarg,nume1,ne1,esc1,x(etarg),abvthr,belthr,x(sce1),
     x            enum1,ebig,x(scest),ix(norder))
      if (enum1 .lt. nume1) then
         write (iout,2020) enum1
      end if
*
*  read in data and set up arrays required for outputting a file for the
*  top up program
* 
      if (prtflg(6) .gt. 0) then
         call topdat(ntarg,x(etarg),abvthr,belthr,ebig,sce2,
     x               enum2,ncases,ix(lrgl),lrgle,nl,fl,lstart,lfin,
     x               nxsn,xsnl,tlast,x,ix)
         if (lstart .ge. lrgle) enum1 = 0
      end if
*
*  set storage for accumulation of cross sections or collision strengths
*
      if (prtflg(4) .ne. 0  .or.  prtflg(5) .ne. 0) then
         xslpi = tlast
         xsn = xslpi + nxsn
         len = nxsn * enum1
         tlast = xsn + len
         do 30 i = 1,len
            x(xsn + i -1) = zero
 30      continue
      end if
*
*  set storage for s-matrices for line broadening
*  and initialise
*
      if (prtflg(7) .ne. 0) then
         itlast = wpadti(tlast + 1)
         iet = itlast
         call prsmat(enum1,x(sce1),ntarg,x(etarg),ix(ltarg),
     x               ix(iet),nmt(7),ncases,sline,nelc,nz,itlast,
     x               x,ix)
         tlast = iadtwp(itlast +1)
         if (tlast .gt. mcore ) then
            write (iout, 1010) tlast,mcore
            stop
         end if
      end if
      rafest = rafin
      raflw1 = raflw
*
*  set store for arrays concerned with successive trial radii
*
      radius = tlast
      rstats = wpadti(radius + maxtry + 1)
      itlast = rstats + maxtry
      tlast = iadtwp(itlast + 1) 
*
*  loop over SLPI cases
*
*  if multipole coefficients have been read decide to calculate
*  line strengths if LS coupling is used
*
      finis = jj
      if (ismax .eq. 0) finis = .true.
*
      do 50 icase = 1,ncases
         call rdhslp(nfth,ncases,icase,ix(lrgl),ix(lrgs),ix(lrgpi),
     x               lrgl2,nspn2,npty2,nchan,nstat,iflg,ix,mcore)
         if (iflg .lt. 0) go to 50 
*
         write (iout, 2000) nspn2,lrgl2,npty2
         if (lrgl2 .lt. lrgle) then
            enum = enum1
            sce = sce1
         else
            enum = enum2
            sce = sce2
         end if
*  if asymptotic distorted wave method is to be used set starting
*  radius as radwm else use the radius from the H-file
*
         if (lrgl2 .gt. lrglr) then
            rfirst = radwm
            adwm = .true.
         else 
            rfirst = rmatr
            adwm = .false.         
         end if
	 print *,lrglr,lglr2,rfirst,radwm,rmatr
*
*  set store ready to read H file
*
         lwmat = nstat * nchan
         leig = nstat  
         eig = tlast
         wmat = eig + leig
         lchl = wpadti (wmat + lwmat + 1) 
         ichl = lchl + nchan
         nltarg = ichl + nchan
         ncn2 = nchan * nchan
         lcf = ismax * ncn2
         cf = iadtwp (nltarg + ntarg + 1)
         cc = cf + lcf
         last = cc + nchan
         ilast = wpadti (last + 1)
         if (last .gt. mcore) then
            write (iout, 1010) last,mcore
            stop
         end if
*
*  read channel information for current SLPI case and print if
*  diaflg(9) is set
*
         mcor1 = mcore - last + 1
         call rdch (nfth,ix(nltarg),ix(lchl),ix(ichl),x(cf),x(eig),
     x              x(wmat),ntarg,nstat,nchan,ncn2,ismax,rmatr,zion,
     x              diaflg(9),x,ix,mcor1)
*
*  calculate line strengths
*
      if (.not. finis) then
         call lstren(ntarg,ix(nltarg),ix(ltarg),ix(starg),nxsn,
     x               x(sline),x(cf),nchan,ismax,ix(lchl),lrgl2,finis)
      end if
*
*  set storage for case dependent arrays
*
         raf = last
         ethr = raf + enum
         et = ethr + nchan
         f = et + nchan
         fp = f + 2 * ncn2
         rmat = fp + 2 * ncn2
         last = rmat + (ncn2 + nchan)/2 + 1
*
*  store relevant buttle coefficients for this case or omit for
*  debug purposes
*
         if (ibuttl .ne. 0) then
            cfbut = last
            last = cfbut + 3 * nchan + 1
            call butco (x(cfbut),x(cfbut1),ix(lchl),nchan,lrang2)
         end if
*
*   calculate channel parameters
*   ethr = threshold energy for each channel
*   cc   = l(l+1) for each channel
*   noch = number of open channels at each energy
*
         e0 = x(etarg)
         call cparm1 (nchan,ntarg,x(etarg),ix(lchl),ix(ichl),x(ethr),e0,
     x                enum,x(sce),ix(noch),x(cc))
*
*  calculate estimated final radius at each energy raf(ie) and its
*  maximum value rafmax
*
         call rafmx (nchan,x(ethr),enum,x(sce),x(cc),rafest,rafmax,
     x               x(raf),rmatr,ion,lamax)      
*
         lmax = maxl(nchan,ix(lchl))
*
*  set store for gailitis wave function package
*
         pt0 = last
         call galptr (nchan,iasy,lmax,pt0,mcore)
         last = pt0
         if (last .gt. mcore) then
            write (iout, 1010) last,mcore
            stop
         end if
*
*  index the channels according to target spins to enable the rmatrix
*  to be divided into spin dependent parts which can be propagated
*  separately - used for the BBM propagator to save space and time
*  not implemented for the LW propagator which is highly vectorised
*  over channels.
*
            ispch = wpadti (last + 1)
            ichsp = ispch + nchan
            last = iadtwp (ichsp + nchan + 1)
*
            call split (nchan,ix(ichl),ntarg,ix(starg),ix(ispch),
     x                  ix(ichsp),nschan,nspin,ns)
*
*  set store for propagators and define the sector ends
*
            asect = last
            mcor1 = mcore - last + 1
            pt0 = last
            ilast = wpadti (last +1)
*
*  the radius at which LW propagator starts is raflw
*  no store is allotted for BBM if raflw = rmatrix radius
*  otherwise raflw is redefined to be at the end of the last BBM sector 
*  note that raflw can be greater than rafmax so that BBM can be 
*  initialised further than the first estimate of the propagation radius
*
            raflw = raflw1
C            raflw = min(raflw,rafmax)
           call prppnt(rfirst,raflw,nschan,ebig,ion,nranmx,idscbb,idbbw,
     x                  nampx,x(last),ix(ilast),nleg,ixmax,delta,pt0,
     x                  mcor1)
            last = pt0
            if (last .gt. mcore) then
               write (iout, 1010) last,mcore
               stop
            end if
            nran = nranmx
            rafin = rafmax
            if ( nran .gt. nbmx ) then
               write (iout,1050) nran, nbmx
               stop
            endif
* 
         if (nran .gt. 0) then
* 
*  open files for BBM propagator
*
         do 55 i = 1,ns
            mch = nschan(i)
            mhd = 2 * mch * ixmax
            mamp = mch * mhd
            irecl = mamp + mamp + mhd
            irecl = irecl * 8
            open (idscbb(i),file = frmp(i),access = 'direct',
     x         recl = irecl,status = 'unknown')
 55      continue
         end if
*
            pt0 = last
*
*  set store for LW propagator
*
            call lwint(nchan,mcore,pt0)
            last = pt0
         if (last .gt. mcore) then
            write (iout, 1010) last,mcore
            stop
         end if
*
*  open file for light-walker propagator
*
         irecl = 2 + nchan + 3*nchan*nchan
         irecl = irecl * 8
         open (idiscr,file = flwp,access = 'direct',
     x         recl = irecl,status = 'unknown')
*
      if (diaflg(8) .eq. 1) then
         write (iout,1060) rafin,iasy,apeps,degeny,iapflg
      end if
*
         ipr = 0
         rlast = raflw
         ming = mingap(x(ethr),nchan,degeny)
         cuplm = cuplim * ming
         if (nran .ne. 0) then
            do 60 i = 1, nran
               nbasis(i) = 0
   60       continue
            call spropi (x(ethr),nchan,nschan,ix(ispch),nran,nbasis,
     x                x(asect),bbloch,rpflg,idscbb,ipr,ismax,ion,
     x                ix(lchl),x(cf),nampx,x)
            if (diaflg(7) .eq. 1) then
               write (iout,1070) ipr,rfirst,raflw,rpflg
               write (iout,1090) nran,nchan,nleg,idscbb,ixmax,ismax
            end if
         end if
* 
        if (raflw .lt. rafin) then
           if (diaflg(7) .eq. 1) then
              write (iout,1061)raflw,rafin,beta
           end if
            nstart = 1
            call slwpot (nstart,x(vmat),x(enrg),x(tc),x(tp),x(tsave),
     x                  nchan,nchan,lwdb(1),lwdb(2),raflw,rafin,
     x                  lwsect,lwkeep,lwran,beta,cuplm,x(space),
     x                  x(tev),x(tpev),ismax,ion,ix(lchl),x(cf),
     x                  x(ethr),idiscr,x(cupest))
            nfinal = nstart
         else
            nfinal = 1
            lwsect(1) = raflw
            lwkeep(1) = nchan
         end if
        if (diaflg(12) .eq. 1) then
           if (raflw .gt. rfirst) write (iout,1110) raflw
           if ( nfinal .gt. 1)   write (iout,1120) lwsect(nfinal)
           write (iout,1140) 
        end if
*
*  set store for kmat - use common/kp/
*
         pt0 = last
         call kptr (nchan,pt0,mcore)
         last = pt0
         kmat = last
         last = kmat + ( nchan * nchan + nchan ) / 2
         if (last .gt. mcore) then
            write (iout, 1010) last,mcore
            stop
         end if
*
*  set store for eigpha and etable
*
         c = 0
         if (prtflg(2) .ne. 0) then
            wrk = last
            phz = wrk + 3 * nchan 
            phzv = phz + enum 
            c = phzv + nchan
            last = c + ncn2
            if (last .gt. mcore) then
               write (iout, 1010) last,mcore
               stop
            end if
            do 40 i = 1,enum
               x(phz + i -1) = zero
 40         continue
         end if
*
*  set store for tmat
*
         if ( prtflg(3) .ne. 0 .or. prtflg(4) .ne. 0 
     x        .or. prtflg(5) .ne. 0 .or. prtflg(7) .ne. 0
     x        .or. diaflg(11) .ne. 0) then
            tr = last
            ti = tr + ncn2
            a = ti + ncn2
            ind = wpadti(a + ncn2 + 1)
            last = iadtwp(ind + nchan + 1) + 1
            if (c .eq. 0) then
               c=last
               last = c + ncn2
            end if
         endif
         if ( last .gt. mcore ) then
            write (iout,1010) last,mcore
            stop
         endif
*
*  set store for crostn and totcol - calculate statistical weights
*
         if ( prtflg(4) .ne. 0 .or. prtflg(5) .ne. 0 .or.
     x        diaflg(11) .ne. 0 ) then
            sk = last
            sw2 = sk + ntarg 
            ji = wpadti(sw2 + ntarg + 1) 
            last = iadtwp(ji + enum + 1) 
            call statw (lrgl2,nspn2,ntarg,sw1,x(sw2),ix(ltarg),
     x                  ix(starg))
         end if
*
         if ( last .gt. mcore ) then
            write (iout,1010) last,mcore
            stop
         endif
         if ( diaflg(15) .ne. 0 ) write (iout,1040) last,mcore
	  if ( diaflg(15) .ne. 0) then
	   write(iout,*) 'fl =',fl
	   fl = 1
	   write(iout,*) 'sce =',sce
	   write(iout,*) 'noch =',noch
	   write(iout,*) 'cfbut =',cfbut
	   write(iout,*) 'rmat =',rmat
	   write(iout,*) 'lchl =',lchl
	   write(iout,*) 'ispch =',ispch
	   write(iout,*) 'ichsp =',ichsp
	   write(iout,*) 'cf =',cf
	   write(iout,*) 'eig =',eig
	   write(iout,*) 'wmat =',wmat
	   write(iout,*) 'etarg =',etarg
	   write(iout,*) 'ichl =',ichl
	   write(iout,*) 'cc =',cc
	   write(iout,*) 'kmat =',kmat
	   write(iout,*) 'wrk =',wrk
	   write(iout,*) 'ind =',ind
	   write(iout,*) 'phz =',phz
	   write(iout,*) 'phzv =',phzv
	   write(iout,*) 'f =',f
	   write(iout,*) 'fp =',fp
	   write(iout,*) 'et =',et
	   write(iout,*) 'ethr =',ethr
	   write(iout,*) 'raf =',raf
	   write(iout,*) 'a =',a
	   write(iout,*) 'c =',c
	   write(iout,*) 'xslpi =',xslpi
	   write(iout,*) 'xsn =',xsn
	   write(iout,*) 'xsnl =',xsnl
	   xsnl=1
	   write(iout,*) 'nltarg =',nltarg
	   write(iout,*) 'ji =',ji
	   write(iout,*) 'tr =',tr
	   write(iout,*) 'ti =',ti
	   write(iout,*) 'iind =',iind
	   write(iout,*) 'jind =',jind
	   write(iout,*) 'sw2 =',sw2
	   write(iout,*) 'sk =',sk
	   write(iout,*) 'asect =',asect
	   write(iout,*) 'fkn =',fkn
	   fkn=1
	   write(iout,*) 'uk =',uk
	   uk=1
	   write(iout,*) 'radius =',radius
	   write(iout,*) 'rstats =',rstats
	   endif
         call phased (prtflg,nmt,nspn2,lrgl2,npty2,nl,x(fl),enum,
     x                x(sce),ix(noch),x(cfbut),x(rmat),
     x                ix(lchl),nstat,nchan,nschan,ix(ispch),ix(ichsp),
     x                x(cf),x(eig),x(wmat),e0,ibuttl,x(etarg),
     x                ix(ichl),x(cc),ion,zion,lamax,rfirst,adwm,
     x                diaflg,x(kmat),
     x                bbloch,x(wrk),ix(ind),ncol,x(phz),x(phzv),
     x                x(f),x(fp),x(et),nbasis,nampx,nranmx,rpflg,
     x                apeps,degeny,iasy,lmax,ewron,icon,quad,title,
     x                x(ethr),raflw,x(raf),lwdb,idiscr,x(a),
     x                x(c),x(xslpi),x(xsn),x(xsnl),ntarg,
     x                ix(nltarg),ix(ji),x(tr),
     x                x(ti),nxsn,ix(iind),
     x                ix(jind),icase,ncases,sw1,x(sw2),x(sk),
     x                x(asect),lwsect,lwkeep,keep,nfinal,lwran,iapflg,
     x                nbutx,x(fkn),x(uk),beta,cuplm,deplim,ncrit,rfact,
     x                gfact,maxtry,x(radius),ix(rstats),rafix,x,ix)
         close (idiscr)
         close (idscbb(1))
         close (idscbb(2)) 
 50   continue
*
      call wtcas (ix(lrgs),ix(lrgl),ix(lrgpi),ncases)
*
*   print line strengths
*
      if (.not. jj .and. ismax .gt. 0) call wrslin(ntarg,xsn,x(sline))
*
*   print total x sections/collision strengths if required
*
      if (prtflg(4) .gt. 1  .or.  prtflg(5) .gt. 1 )then
         ixsn = 2
         if (prtflg(4) .gt. 1) then
            write(iout,1170)
         else
            write(iout,1180)
         end if
         if (enum1 .gt. 0) call pxsntb (enum1,nxsn,
     x       x(xsn),x(sce1),ixsn,ix(iind),ix(jind),ncol)
         if (enum2 .gt. 0) call pxsnl (nl,x(fl),enum2,
     x       nxsn,x(xsnl),x(sce2),ixsn,ix(iind),ix(jind),ncol)
      end if
      return
 1000 format (//, ' program farm', 10x,
     x         ' CPC version  (1994)', //)
 1010 format (' storage overflow ( cntrl ) : last =',i10,' icore =',
     x          i10/'  increase value of icore in main program')
 1020 format (//1x,a80//)
 1040 format (' core monitor ( cntrl ) core used =',i10,' icore =',i10)
 1050 format (' propagator subranges, nran =',i4,' exceeds maximum ',i4)
 1060 format (/,5x,'expansion radius,             rafin  =',d16.8,/,
     x        5x,'terms in series,              iasy   =',i16,/,
     x        5x,'convergence criterion,        apeps  =',d16.8,/,
     x        5x,'channel degeneracy criterion, degeny =',d16.8,/,
     x        5x,'print flags,                  iapflg =',4i4,/)
 1061 format (' r-matrix propagation : light-walker method',//,
     x        ' parameters for r-matrix propagation',/,
     x	      ' initial radius=',f15.5,10x,'final radius=',f15.5,/,
     x	      ' beta=',d15.5,/)
 1070 format (' r-matrix propagator :',/,5x,'ipr   =',i3,' rfirst =',
     x        d16.8,' rlast =',d16.8,/,5x,'rpflg  :',6i3)
 1090 format (' nran   =',i4,' nchan  =',i4,' nleg   =',i4,/,
     x        ' idscbb  =',2i4,' ixmax  =',i4,' ismax  =',i4)
 1110 format (/' Burke Baluja Morgan propagator initialised to r =',
     x           f12.6,' scaled Bohr')
 1120 format (/' Light Walker propagator initialised to r =',f12.6,
     x           ' scaled Bohr')
 1140 format (/' Light Walker propagator will be used to extend the',
     x         '  radius')
 1150 format (' **** error ****  cross sections AND collision ',
     x        'strengths cannot be calculated together')
 1160 format (' **** error **** top up output is required',/,
     x        ' prtflg(4) OR prtflg(5) must be non zero')
 1165 format (' **** error **** top up output is required',/,
     x        ' prtflg(7) must be zero')
 1170 format (//,15x,'TOTAL CROSS-SECTIONS in units of a0**2'/,
     x           15x,'**************************************',/)
 1180 format (//,15x,'TOTAL COLLISION STRENGTHS'/,
     x           15x,'*************************',/)
 2000 format (/,15x,'****************',/,15x,'s l p =',3i3,/
     x           15x,'****************', /)
 2010 format (/,' Internal region data read from file ',a128,/ 
     x        '   nelc =',i4,' nz=',i4,' lrang2=',i4,/
     x        '   number of target states =',i4,
     x        ' highest multipole =',i2/
     x        '   rmatrix radius =',d16.8,' Bohr',//)
 2011 format (' opened output file ',a128)
 2020 format (' number of discrete energies away from thresholds = ',i4)
 2030 format (' ***** scaled units are used by the code:',/
     x        ' for positively charged ions energies are scaled',/
     x        ' by a factor 1/z**2 and radii by z',/
     x        ' where z is the charge on the ion (nz-nelc)',/,
     x        ' for neutrals the scaling factor is 1 ',/
     x        ' ***** data read from namelists is in scaled units:',/
     x        ' energies in scaled rydbergs and radii in scaled Bohr'//)
       end
      subroutine cparm1 (nchan,ntarg,etarg,lchl,ichl,eth,e0,
     x                   enum,sce,noch,cc)
*
* $Id: cparm1.f,v 2.1 94/09/29 18:40:01 vmb Exp Locker: vmb $
*
      implicit integer (a-z)
      real*8 dfloat,atry,etarg,eth,e0,sce,cc,eps,zero
      parameter ( atry=2.0d0,zero=0.0d0 )
      common /io/ iin,iout
      dimension etarg(ntarg),lchl(nchan),ichl(nchan),eth(nchan),
     x          sce(enum),noch(enum),cc(nchan)
*
*     cparm : channel parameters eth, lch 
*
      e0 = etarg(1)
c      write (iout,1000) e0
      do 30 i = 1, nchan
         ii = ichl(i)
         eth(i) = atry * ( etarg(ii) - e0 )
c         write (iout,1010) i,lchl(i),eth(i)
         cc(i) = dfloat (lchl(i)*(lchl(i) + 1))
 30   continue
      do 60 ie = 1,enum
         nopen = 0
         do 70 i = 1,nchan
            eps = sce(ie) - eth(i)
            if (eps .gt. zero) nopen = nopen + 1
  70     continue
         noch(ie)=nopen
  60  continue
      return
*
c 1000 format (/,' cparm1 :',/,' e0 =',d16.8,)
c 1010 format (' i =',i6,' lch =',i6,' eth =',d16.8)
      end
      subroutine crostn (e,ntarg,nltarg,et,sw1,sw2,a,xslpi,xsn,
     x                   idbug,nxsn,sk,zion,ji,iind,jind,tr,ti,
     x                   nopen,ien,nl,nume2,xsnl,il)
*
* $Id: crostn.f,v 2.1 94/09/29 18:40:18 vmb Exp Locker: vmb $
*
      implicit integer (a-z)
      real*8 e,et,a,xslpi,xsn,xsnl,tr,ti,an2,zero,one,half,c1,c2,
     x       sw1,sw2,cros,pi,cx,cxp,cxq,dx,sk,flion2
      parameter ( an2=0.28002836d+00,zero=0.d0,one=1.0d0,half=0.5d0,
     x            pi=3.1415926535897932d+00 )
      dimension nltarg(ntarg),et(*),tr(*),ti(*),ji(*),a(*),sw2(ntarg),
     x          xslpi(nxsn),xsn(nxsn),xsnl(nxsn,nume2,nl),
     x          sk(ntarg),iind(nxsn),jind(nxsn)
      common /io/ iin,iout
*
*     crostn : total cross sections from the t-matrix
*              an2 is the conversion factor from a0**2 to angstroms**2
*
*  calculate energy scaling factor ready to calculate cross-sections
*
      ion = zion
      if (ion .eq. 0) then
         flion2 = one
      else
         flion2 = dfloat (ion * ion)
      end if
      nop2 = nopen * nopen
      do 12 i = 1, nop2
         a(i) = tr(i)**2 + ti(i)**2
   12 continue
      j = 1
      do 14 i = 1, ntarg
         if ( et(j) .gt. zero ) then
            sk(i) = et(j) * flion2
            nlt = i
            j = j + nltarg(i)
         endif
   14 continue
      ix = 0
      c1 = half * pi * sw1 
      nf = 0
      do 60 i = 1, nlt
         ni = nf + 1
         nf = nf + nltarg(i)
         mf = 0
         jup = i
         if (ion .gt. 0) jup = i - 1
         do 40 j = 1,jup
            ix = ix + 1
            mi = mf + 1
            mf = mf + nltarg(j)
            c2 = sw2(j) * sk(j)
            cros = zero
            do 30 nc = ni, nf
               do 20 mc = mi, mf
                  nmc = nopen * ( mc - 1 ) + nc
                  cros = cros + a(nmc)
   20          continue
   30       continue
            xslpi(ix) =  c1 * cros / c2
   40    continue
         ji(ien) = ix
   60 continue
*     debug print of cross-sections for partial wave
      if ( idbug .gt. 0 .and. ix .gt. 0 ) then
         write (iout,1000)e
         ix = 0
         do 90 i = 1, nlt
            jup = i
            if (ion .gt. 0) jup = i - 1
            do 80 j = 1, jup
               ix = ix + 1
               cxp = xslpi(ix)
               cx =  cxp / pi
               cxq = an2 * cxp
               dx = sw2(j) * sk(j) * cx
               if (cxp .ne. zero) then
               write (iout,1010) j,i,cx,cxp,cxq,dx 
               end if
   80       continue
   90    continue
      endif
      if (il .le. 0) then
         do 100 i = 1,ix
            xsn(i) = xsn(i) + xslpi(i)
 100     continue
      else
         do 101 i = 1,ix
            xsnl(i,ien,il) = xsnl(i,ien,il) + xslpi(i)
 101     continue
      end if
      return
*
 1000 format (/' cross sections at energy ',d12.5,'  (scld ryd)',
     x        /'   i   j      pi*a0**2',9x,
     x        'a0**2',11x,'ang**2',4x,'collision strength',/)
 1010 format (2i4,4d16.6)
c 1030 format (1x,6(i3,'-',i3,5x))
c 1040 format (1x,6d12.5)
      end

      subroutine degtar (nelc,nz,ntarg,etarg,ltarg,starg,degeny,
     x                   prtflg,nmt,title)
*
*  make thresholds closer than degeny truly degenerate   
*  print the new energy levels if changed and write headings to
*  output files for k-matrix, t-matrix, cross-section, collision strength
*  and top-up.
*
      implicit integer (a-z)
      real*8 etarg,zero,atry,degeny,etargr,d0
      character*80 title
      character*20 name
      logical change
      parameter ( zero=0.0d0, atry=2.0d0)
      common /io/ iin,iout
      dimension etarg(ntarg),ltarg(ntarg),starg(ntarg),prtflg(*),
     x          nmt(*),name(20)
      data name(1),name(3),name(4),name(5),name(6),name(7)
     x  /' K-MATRIX',' T-MATRIX',' X-SECTION ',' OMEGA ',
     x   ' X-SECT-FOR-TOPUP',' OMEGA-FOR-TOPUP '/
*
      change = .false.
      do 1 i = 2,ntarg
*    prevent vectorization of this loop
      write(iout,*)
         if (abs(etarg(i)-etarg(i-1))*atry .le. degeny) then
            etarg(i) = etarg(i-1)
            change = .true.
         end if
 1    continue
      if (change) then
         write (iout,1000)
         d0 = etarg(1)
         do 10  i=1,ntarg
             etargr = atry * (etarg(i) - d0)
             write (iout,1010) i,ltarg(i),starg(i),etargr
 10      continue
      end if
      if (prtflg(1) .eq. 1  .or.  prtflg(1) .eq. 3) then
         write(nmt(1)) name(1),title
         write(nmt(1)) nelc,nz,ntarg,(etarg(i),i=1,ntarg),
     x                (ltarg(i),i=1,ntarg),(starg(i),i=1,ntarg)
      end if
      if (prtflg(3) .eq. 1  .or.  prtflg(3) .eq. 3) then
         write(nmt(3)) name(3),title
         write(nmt(3)) nelc,nz,ntarg,(etarg(i),i=1,ntarg),
     x                (ltarg(i),i=1,ntarg),(starg(i),i=1,ntarg)
      end if
      if (prtflg(4) .eq. 1  .or.  prtflg(4) .eq. 3) then
         write(nmt(4)) name(4),title
         write(nmt(4)) nelc,nz,ntarg,(etarg(i),i=1,ntarg),
     x                (ltarg(i),i=1,ntarg),(starg(i),i=1,ntarg)
      end if
      if (prtflg(5) .eq. 1  .or.  prtflg(5) .eq. 3) then
         write(nmt(5)) name(5),title
         write(nmt(5)) nelc,nz,ntarg,(etarg(i),i=1,ntarg),
     x                (ltarg(i),i=1,ntarg),(starg(i),i=1,ntarg)
      end if
      if (prtflg(6) .eq. 1  .or.  prtflg(6) .eq. 3) then
         if (prtflg(4) .gt. 0) then
            write(nmt(6)) name(6),title
         else if (prtflg(5) .gt. 0) then
            write(nmt(6)) name(7),title
         end if
         write(nmt(6)) nelc,nz,ntarg,(etarg(i),i=1,ntarg),
     x                (ltarg(i),i=1,ntarg),(starg(i),i=1,ntarg)
      end if
*
 1000 format(20x, 'target states modified by degeny'/
     x       20x, '********************************',//
     x       10x,'index',5x,'total l',3x,'(2*s+1)',8x,'energy'/
     x       43x,'scaled ryd')
 1010 format(3x,3i10,7x,f12.6)
      return
      end
      subroutine eigpha (iph,phz,phzv,nchan,ien,nc,kmat,wrk)
*
* $Id: eigpha.f,v 2.1 94/09/29 18:39:39 vmb Exp Locker: vmb $
*
      implicit integer (a-z)
      real*8 phz,phzv,kmat,wrk,vc,zero
      parameter ( zero=0.d0 )
      common /io/ iin,iout
      dimension kmat(*),phz(*),phzv(nchan),wrk(3*nchan),vc(1)
*
*     eigenphases of k-matrix
*
      call DSPEV ( 'N', 'U', nc, kmat, phzv, vc, nchan, wrk, info )
      if ( info .ne. 0 ) then
         write (iout,1000) info
         stop
      endif
      do 80 no = 1, nc
         phzv(no) = atan( phzv(no) )
   80 continue
      if ( iph .ge. 2 ) write (iout,1010) (phzv(no),no = 1,nc)
      phz(ien) = zero
      do 90 no = 1,nc
         phz(ien) = phz(ien) + phzv(no)
 90   continue
      return
*
 1000 format (' eigpha : error return by dspev, ierr =', i3)
 1010 format (' eigenphases :',/,(6f12.6))
      end
      subroutine etable (icon,quad,title,nume,phz,en,nspn2,lrgl2,
     x                   npty2,nfts)
*
* $Id: etable.f,v 2.1 94/09/29 18:39:41 vmb Exp Locker: vmb $
*
      implicit integer (a-z)
      real*8 quad,quad1,phz,en,pi,one,e,dl,dlx,dlm,dlp,dlx1,
     x       dlx2,dlx3
      character*80 title
      parameter ( pi=3.1415926535897932d+00,one=1.0d0)
      common /io/ iin,iout
      dimension phz(nume),en(nume)
*
*     etable : smoothing of eigenphase sum:
*              icon = 0  no smoothing
*                   = 1  add pis to minimize energy differences
*
*     second-order divided differences, assume first three points smooth
*
      if ( icon .eq. 1 ) then
         do 40 ie = 1, nume
            e = en(ie)
            dl = phz(ie)
            if ( ie .eq. 1 ) then
               phz(ie) = dl + quad * pi
               quad1 = quad
            else
                  dlx = dl + quad1 * pi
                  dlm = dlx - pi
                  dlp = dlx + pi
                  dlx1 = abs(dlx-phz(ie-1))
                  dlx2 = abs(dlm-phz(ie-1))
                  dlx3 = abs(dlp-phz(ie-1))
                  if ( dlx2 .lt. dlx1 ) then
                     if ( dlx3 .ge. dlx2 ) then
                        phz(ie) = dlm
                        quad1 = quad1 - one
                     else
                        phz(ie) = dlp
                        quad1 = quad1 + one
                     endif
                  elseif ( dlx3 .lt. dlx1 ) then
                     phz(ie) = dlp
                     quad1 = quad1 + one
                  else
                     phz(ie) = dlx
                  endif
               endif
   40    continue
      endif
*
*     print table
*
      write (nfts,1000) title
         do 360 ie = 1, nume
            e = en(ie)
               write (nfts,1010) nspn2,lrgl2,npty2,e,phz(ie)
  360    continue
      return
*
 1000 format (1h1,1x,a72,/,30x,'table of eigenphases',//,
     x       1x,'  2s+1   L   Pi      energy      phase sum')
 1010 format (1x,3i5,1x,d16.9,1x,d16.9 )
      end

      subroutine evals (ntarg,nume,ne,esc,etarg,abvthr,belthr,sce,
     x                  enum,ebig,scest,norder)
*
* $Id: evals.f,v 2.1 94/09/29 18:39:42 vmb Exp Locker: vmb $
*
      implicit integer (a-z)
      real*8 esc,etarg,abvthr,belthr,sce,e,zero,etest,einc,estart,ebig,
     x        e1,atry,scest,fix,desmat
      parameter ( zero=0.0d0, atry=2.0d0, fix=1.0d6, desmat=1.0d-8 )
      common /io/ iin,iout
      dimension ne(*),esc(*),etarg(ntarg),sce(nume),scest(nume),
     x          norder(nume)
*
      ebig = zero
      enum = 0
      e1 = etarg(1)
      if ( ne(1) .eq. 0 ) go to 61
      neig = 10
      iec = 1
      do 30 ieg = 1, neig
         neigs = ne(ieg)
         if (neigs .eq. 0) go to 40
         estart = esc(iec)
         if (abs(neigs) .eq. 1 ) then
            einc = zero
            neigs = abs(neigs)
         else if (neigs .lt. -1) then
            neigs = abs(neigs)
            einc = (esc(iec+1) - estart)/(neigs-1)
         else 
            einc = esc(iec+1)
         endif
         iec = iec + 2
         e = estart
         if ( e .lt. zero ) then
            write (iout,1010)
            stop
         endif
         do 20 ie = 1, neigs
            do 10 i = 1, ntarg
               etest = e - (etarg(i) - e1) * atry
               if ( etest .ge. zero ) then
                  if ( etest .le. abvthr ) go to 21
               else
                  if ( - etest .le. belthr ) go to 21
               endif
   10       continue
            enum = enum + 1
            scest(enum) = e
 21         e = e + einc
 20      continue
 30   continue
*
*   since the meshes could overlap etc. reorder the energies in
*   ascending order.  Duplicating energies are set to a very large
*   value, fix, ready to be pruned out
*
 40   call order(scest,enum,norder,fix,desmat)
      do 60  i = 1,enum
         sce(i) = scest(norder(i))
 60   continue
      maxnum = 0
      do 70 i = enum,1,-1
         if (sce(i) .lt. fix) then
            maxnum = i
            go to 75
         end if
 70   continue
 75   enum = maxnum
      ebig = sce(enum)
 61   ebig = max (ebig,(etarg(ntarg)-e1)*atry)
*
*   ebig may need modifying for bound state calculations
      return
 1000 format (' evals : input error, ne(1) =',i6,' nume =',i6)
 1010 format (' evals : input error, e =',d16.8,' negative')
      end
      subroutine kmat (nchan,ra,bsto,aa,bb,iwrk,nopen,f,fp,rmat,
     x                 akmat)                   
*
* $Id: kmat.f,v 2.1 94/09/29 18:40:30 vmb Exp Locker: vmb $
*
      implicit integer (a-z)
      real*8 aa,akmat,bb,bsto,df,f,fp,ra,rmat,kscal,temp,
     x       scalx,rscalx
      dimension rmat(*),akmat(*),f(nchan,nchan,2),fp(nchan,nchan,2),
     x          aa(nchan,nchan),bb(nchan,nchan),iwrk(nchan)
*     x         ,kscal(nchan)
      common /mach/ scalx,rscalx
*     
      k = 2
      do 20 j = 1, nchan
         if ( j .gt. nopen ) k = 1
         do 10 i = 1, nchan
            aa(i,j)   = f(i,j,k)
   10    continue
 20   continue
*
      do 50 j = 1, nchan
         kj = 2
         if ( j .gt. nopen ) kj = 1
         do 40 k = 1, nchan
            df = ra * fp(k,j,kj) - bsto * f(k,j,kj)
            do 30 i = 1, nchan
               kix = max(i,k)
               kin = min(i,k)
               kk = ( kix * ( kix - 1 ) ) / 2 + kin
               aa(i,j) = aa(i,j) - rmat(kk) * df
   30       continue
   40      continue
* mod by ts 7-jan-94 --- scale closed-open k-matrix
         if ( j .gt. nopen ) then
            temp = 1./max(aa(j,j),scalx)
*            kscal(j) = temp
            do 45 i = 1, nchan
               aa(i,j) = aa(i,j) * temp
 45         continue
         endif
* end mod ---
   50 continue
      nc = nchan
      do 80 j = 1, nopen
         do 70 i = 1, nchan
            bb(i,j) = - f(i,j,1)
   70    continue
   80 continue
      do 110 j = 1, nopen
         do 100 k = 1, nchan
            df = ra * fp(k,j,1) - bsto * f(k,j,1)
            do 90 i = 1, nchan
               kix = max(i,k)
               kin = min(i,k)
               kk = ( kix * ( kix - 1 ) ) / 2 + kin
               bb(i,j) = bb(i,j) + rmat( kk) * df
   90       continue
  100    continue
  110 continue
         if ( nc .eq. 1 ) then
            bb(1,1) = bb(1,1) / aa(1,1)
         else
            call sleqn (aa,nchan,nc,bb,nopen,iwrk)
         endif
         k = 0
         do 130 j = 1, nopen
            do 120 i = 1, j
               k = k + 1
               akmat(k) = bb(i,j)
 120        continue
* mod by ts 7-jan-94 --- rescale closed-open k-matrix 
* rescaling is not necessary here as the array bb is not used
* again
*            do 125 i = nopen+1, nchan
*               bb(i,j) = bb(i,j) * kscal(i)
* 125        continue
* end mod ---
  130     continue
*     
      return
      end

      subroutine kptr (nchan,pt0,mcor)
*
* $Id: kptr.f,v 2.1 94/09/29 18:39:44 vmb Exp Locker: vmb $
*
      implicit integer (a-z)
      common /io/ iin,iout
      common /kp/ bb,iwrk,bigvc
*
      bigvc = pt0
      nchsq = nchan * nchan
      bb = bigvc + nchsq
      iwrk = wpadti(bb +  nchsq) +1
      last = iadtwp(iwrk + nchan + 1)
      if ( last .gt. mcor ) then
         write (iout,1000) last,mcor
         stop
      endif
      pt0 = last
      return
 1000 format (' kptr : last =',i10,' core =',i10)
      end
      subroutine llims(ncases,lrgl,lstart,lfin)
*
* $Id: llims.f,v 2.1 94/09/29 18:41:05 vmb Exp Locker: vmb $
*
      implicit integer (a-z)
      dimension lrgl(ncases)
*
      lstart = 999
      lfin = 0
      do 10 il = 1,ncases
         l = lrgl(il)
         lstart = min(lstart, l)
         lfin = max(lfin, l)
 10   continue
      return
      end
      subroutine lstren(ntarg,nltarg,ltarg,starg,nxsn,sline,cf,
     x                 nchan,lamax,lchl,lrgl2,finis)
      implicit integer(a - z)
      real*8 sline,cf,dfloat,wabs1,c,zero,quartr
      logical finis
      dimension nltarg(ntarg),ltarg(ntarg),starg(ntarg),sline(nxsn),
     x          cf(nchan,nchan,lamax),lchl(nchan)
      data zero/0.0d0/,quartr/0.25d0/
*  calculate line strength in LS coupling

      nc2=nltarg(1)
      do 10 it=2,ntarg
         if(nltarg(it).gt.0) then
         nc1=nc2+1
         nc2=nc2+nltarg(it)
         mc2=0
         do 20 jt=1,it-1
            if(nltarg(jt).gt.0) then
            mc1=mc2+1
            mc2=mc2+nltarg(jt)
            k=((it-1)*(it-2))/2+jt
            if(sline(k).lt.zero)then
            do 30 nc=nc1,nc2
               do 40 mc=mc1,mc2
                  if(cf(nc,mc,1).ne.zero) then
                  c=cf(nc,mc,1)/
     x              wabs1(ltarg(it),ltarg(jt),lchl(nc),lchl(mc),lrgl2)
                  c=c*c
                  sline(k)=.25*starg(it)*c/dfloat(max(lchl(nc),
     x                     lchl(mc)))
                  goto 20
                  end if
   40             continue
   30          continue
            sline(k) = zero
            end if
            end if
   20    continue
         end if          
   10 continue
      finis = .true.
      k = 0
      do 50 it = 2,ntarg
         si = starg(it)
         do 60 jt = 1,it - 1
            k = k + 1
            if (starg(jt) .ne. si) sline(k) = zero
            if (sline(k) .lt. zero) finis = .false.
 60      continue
 50   continue
      return
      end

      subroutine lvals (ncases,lrgl,lrgle,nl,fl)
*
* $Id: lvals.f,v 2.1 94/09/29 18:41:03 vmb Exp Locker: vmb $
*
      real*8 fl,f
      common /io/ iin, iout
      dimension lrgl(ncases),fl(*)
      nl = 0
      do 10 n=1,ncases
         l = lrgl(n)
         if (l .ge. lrgle) then
            f = dble(l)
            do 20 i = 1,nl
               if (f .eq. fl(i)) go to 10
 20         continue
            nl = nl + 1
            fl(nl) = f
         end if
 10   continue
      write (iout,1000) nl
      return
 1000 format (' number of l values in top up scheme, nl =',i3)
      end

      function maxl (nchan,l2p)
*
* $Id: maxl.f,v 2.1 94/09/29 18:39:46 vmb Exp Locker: vmb $
*
      implicit integer (a-z)
      dimension l2p(nchan)
      lmax = -1
      do 10 i = 1, nchan
         lmax = max(lmax,l2p(i))
   10 continue
      maxl = lmax
      return
      end
      function mingap (eth,nchan,degeny)
*
* $Id: mingap.f,v 2.1 94/09/29 18:39:35 vmb Exp Locker: vmb $
*
      implicit integer (a-z)
      real*8 eth,degeny,ming,gap,mingap
      dimension eth(nchan)
*
*     calculate the minimum gap between target thresholds 
*
      ming = eth(nchan) - eth(1)
      do 10   i = 2,nchan
         gap = eth(i) - eth(i-1)
         if (gap .gt. degeny) ming = min(ming, gap)
 10   continue
      mingap = ming
      return
      end 
      function necnt (ne)
*
* $Id: necnt.f,v 2.1 94/09/29 18:39:47 vmb Exp Locker: vmb $
*
      implicit integer (a-z)
      common /io/ iin,iout
      dimension ne(10)
*
*     necnt = number of impact energies
*
      ienum = 0
      do 80 ieg = 1, 10
         ienum = ienum + abs(ne(ieg))
   80 continue
      necnt = ienum
      write (iout,1000) necnt
      return
 1000 format (/' necnt : number of energies =',i6)
      end
      subroutine order(en,nchan,norder,fix,desmat)
c
c --- define norder, which points to values of en in ascending order. 
c
c --- input parameters ...
c     (en(m),m=1,nchan) = energies in arbitrary order;
c --- output parameter ...
c     (norder(m),m=1,nchan) = the position of the m-th biggest energy 
c                             in the en array.
c
      implicit integer (a-z)
      real*8 en,fix,desmat,x
      dimension en(nchan),norder(nchan)
c
      norder(1)=1
      if(nchan.eq.1) return
      do 9 m=2,nchan
         j=m
         x=en(j)           
         j1=j-1       
         do 7 i=1,j1
            if (j.ge.m) then
               i1=norder(i)
               if (abs(x - en(i1)) .le. desmat) then
                  en(j) = fix
                  x = fix
               end if
               if (x.ge.en(i1)) go to 7
            end if
            j=j-1     
            norder(j+1)=norder(j)
    7    continue    
         norder(j)=m 
    9 continue    
      return
      end
      subroutine parcol(e,sw1,a,nchan,ichl,lchl,tr,ti,nopen)
*
* $Id: parcol.f,v 2.1 94/09/29 18:40:28 vmb Exp Locker: vmb $
*
*
*   partial collision strengths
*
      implicit integer (a-z)
      real*8 e,sw1,a,tr,ti,half
      parameter ( half=0.5d0)
      common /io/ iin,iout
      dimension ichl(nchan),lchl(nchan),a(*),tr(*),ti(*)
*
      nop2 = nopen * nopen
      do 10 i = 1, nop2
         a(i) = tr(i)**2 + ti(i)**2
   10 continue
*
         write (iout,1000)
         do 20 i = 1,nopen
         do 20 j = i,nopen
            k = nopen * (i-1) + j
            a(k) = a(k) * half * sw1
            write (iout,1020) e,ichl(i),lchl(i),ichl(j),
     x                        lchl(j),a(k)
 20      continue
      return
*
 1000 format (/,' Partial collision strength :',/
     x        10x,'energy',10x,'TI LI    TJ LJ         OMEGA')
 1020 format (5x,d14.6,5x,2i3,3x,2i3,5x,d14.6)
      end
      subroutine phased (prtflg,nmt,nspn2,lrgl2,npty2,nl,fl,
     x                   nume,sce,noch,cfbut,rmat,lchl,nstat,nchan,
     x                   nschan,ispch,ichsp,cf,eig,wmat,e0,ibuttl,
     x                   etargs,ichl,cc,ion,zion,ismax,rmatr,adwm,
     x                   diaflg,kmat,bbloch,
     x                   wrk,ind,ncol,phz,phzv,f,fp,et,nbasis,nampx,
     x                   nranmx,rpflg,apeps,degeny,iasy,lmax,ewron,
     x                   icon,quad,title,ethr,raflw,raf,lwdb,idiscr,
     x                   a,c,xslpi,xsn,xsnl,
     x                   ntarg,nltarg,ji,tr,ti,nxsn,
     x                   iind,jind,icases,ncases,
     x                   sw1,sw2,sk,asect,lwsect,lwkeep,keep,nfinal,
     x                   lwran,iapflg,nbutx,fkn,uk,beta,cuplim,deplim,
     x                   ncrit,rfact,gfact,maxtry,radius,rstats,rafix,
     x                   x,ix)
*
* $Id: phased.f,v 2.1 94/09/29 18:39:51 vmb Exp Locker: vmb $
*
      implicit integer (a-z)
      character*80 title
      real*8 sce,cfbut,rmat,cf,eig,wmat,e0,etargs,rmatr,e,
     x       etot,ee,half,ryev,bbloch,wrk,phz,phzv,f,fp,et,apeps,
     x       raflw,raf,
     x       degeny,ewron,quad,kmat,x,ethr,tr,ti,a,c,xslpi,xsn,
     x       xsnl,sw1,sw2,sk,fkn,uk,cc,radius,rafix, 
     x       asect,lwsect,beta,rfact,gfact,
     x       cuplim,deplim,fl
      logical lwdb,adwm,firste 
      parameter ( half=0.5d0,ryev=13.605d0) 
      common /io/ iin,iout
      common /ie/ ierp,ee
      dimension et(*),sce(nume),cfbut(*),rmat(*),lchl(*),
     x          cf(*),eig(*),wmat(*),noch(*),wrk(*),phz(*),phzv(*),
     x          nbasis(*),etargs(*),ichl(*),kmat(*),f(*),fp(*),rpflg(6),
     x          x(*),ix(*),ind(*),ethr(*),tr(*),ti(*),a(*),c(*),
     x          xslpi(nxsn),xsn(nxsn,nume),xsnl(nxsn,nume,nl),
     x          iind(nxsn),jind(nxsn),fl(nl),nltarg(*),ji(*),sw2(ntarg),
     x          sk(*),iapflg(4),fkn(nbutx),uk(nbutx),cc(*),asect(*),
     x          lwsect(lwran),lwkeep(lwran),keep(lwran),lwdb(2),raf(*),
     x          diaflg(*),prtflg(7),nmt(7),nschan(2),
     x          ispch(nchan),ichsp(nchan),radius(maxtry),rstats(maxtry)
*
      ierp = iout
      il = 0
      do 5 i = 1 ,nl
         if (lrgl2 .eq. nint(fl(i))) il = i
 5    continue
*
*  loop over scattering energies
*
      firste = .true.
      do 100 ie = 1, nume
         e = sce(ie)
         ee = ryev * e
         nopen = noch(ie)
         do 10 i = 1, nchan
            et(i) = e - ethr(i)
 10      continue
*
*  if there are no open channels or in the case of ions fewer than 2
*  open states skip to the next energy
*
         if (nopen .eq. 0) go to 100
         if (ion .gt. 0 .and. prtflg(1) .eq. 0) then
             do 15 i = 1,ntarg
                if (nltarg(i) .gt. 0) then
                    k = i
                    go to 16
                end if
   15        continue
             stop
   16        if (nltarg(k) .ge. nopen) go to 100
          end if
*
*  calculate r-matrix at the boundary
* 
*  use surface amplitudes from H-file or asymptotic distorted wave
*  method
*
         if (.not. adwm) then
            call rmatd (nstat,nchan,eig,wmat,e0,cfbut,ibuttl,ichl,
     X                  etargs,rmatr,e,rmat,fkn,uk)
         else
            call rmatf1(nchan,lchl,et,lmax,ion,iasy,rmatr,rmat,x)
         end if
*
         if ( diaflg(14) .gt. 0 ) then
            etot = e0 + half * e
            write (iout,1010) ie,e,ee,etot,nopen,(et(i),i = 1,nchan)
         endif
         if ( diaflg(2) .gt. 0 ) then
            write (iout,1020) rmatr,e
            call wrttmt (rmat,nchan,ncol)
         endif
*
*  calculate the k-matrix
*
       	 call asyms (ie,e,rmat,kmat,nchan,nschan,ispch,ichsp,
     x               lchl,ion,ismax,cf,rmatr,bbloch,ethr,
     x               f,fp,et,nbasis,nampx,nranmx,rpflg,prtflg,
     x               ncol,apeps,degeny,iasy,lmax,ewron,nopen,
     x               raf,raflw,asect,lwsect,lwkeep,keep,nfinal,lwran,
     x               iapflg,diaflg,idiscr,lwdb,beta,cuplim,deplim,
     x               ncrit,rfact,gfact,maxtry,radius,rstats,rafix,x,ix)
*
         if (prtflg(1) .eq. 1  .or. prtflg(1) .eq. 3) then
            if (firste) then
               numen = nume-ie+1
               write (nmt(1)) nspn2,lrgl2,npty2,numen,nchan,
     x                       (ichl(ich),lchl(ich),ich=1,nchan)
            end if
            nop2 = (nopen * (nopen + 1))/2
            write (nmt(1)) e,nopen,nop2,(kmat(i),i=1,nop2)
         end if
*
*  calcuate the eigenphase sum
*
         if (prtflg(2) .ge. 1) then
            call dcopy ((nopen*nopen+nopen)/2,kmat,1,c,1)
            call eigpha (prtflg(2),phz,phzv,nchan,ie,nopen,c,wrk)
         end if
*
*  calculate the t-matrix
*
         if ( prtflg(3) .ge. 1 .or. prtflg(4) .ge. 1 
     x   .or. prtflg(5) .ge. 1 .or. prtflg(7) .ge. 1
     x   .or. diaflg(11) .ge. 1) then
            call tmat (ie,e,kmat,tr,ti,prtflg(3),nopen,a,c,ind,
     x                 nspn2,lrgl2,npty2,nchan,ichl,lchl,firste,
     x                 nume,nmt(3),ncol)
         end if
*
*  calculate cross sections
*
         if ( prtflg(4) .ge. 1 ) call crostn 
     x                 (e,ntarg,nltarg,et,sw1,sw2,a,xslpi,xsn(1,ie),
     x                  diaflg(10),nxsn,sk,zion,ji,iind,
     x                  jind,tr,ti,nopen,ie,nl,nume,xsnl,il)
*
*  calculate partial collision strengths
*
         if (diaflg(11) .gt. 0 ) call parcol
     x                 (e,sw1,a,nchan,ichl,lchl,tr,ti,nopen)
*
*  calculate total collision strengths
*
         if (prtflg(5) .ge. 1) then
           nlt = ichl(nopen) 
           call totcol (ntarg,nltarg,nlt,sw1,a,xslpi,xsn(1,ie),
     x                  diaflg(10),
     x                  nxsn,ion,ji,iind,jind,tr,ti,nopen,ie,
     x                  nl,nume,xsnl,il)
         end if
         if (prtflg(7) .ge. 1) then
            call smat1(nmt(7),ie,tr,ti,prtflg(7),nopen,nume,
     x                 nspn2,lrgl2,npty2,nltarg,ntarg,lchl,ncol,
     x                 icases,ncases,sce,x,ix)
         end if
      firste = .false.
  100 continue
      if (prtflg(2) .eq. 1 .or. prtflg(2) .eq. 3) then
         call etable (icon,quad,title,nume,phz,sce,nspn2,lrgl2,
     x                npty2,nmt(2))
      end if
      if (prtflg(4) .eq. 1  .or. prtflg(4) .eq. 3) then
         if (il .eq. 0) then
            call wrxsn(nmt(4),nxsn,nume,sce,xsn)
         else
            call wrxsnl(nmt(6),nxsn,il,nume,fl,sce,xsnl)
         end if
      end if
      if (prtflg(5) .eq. 1  .or. prtflg(5) .eq. 3) then
         if (il .eq. 0) then
            call wrxsn(nmt(5),nxsn,nume,sce,xsn)
         else
            call wrxsnl(nmt(6),nxsn,il,nume,fl,sce,xsnl)
         end if
      end if
      if (icases .eq. ncases .and. diaflg(13) .eq. 1) then
         if (maxtry .gt. 10) then
            sum = 0
            do 50 i = 10,maxtry
               sum = sum + rstats(i)
 50         continue
            rstats(10) = sum
         end if
         maxtri = min(10,maxtry)
         write (iout,1100)(rstats(i),i=1,maxtri)
      end if
      return
*
 1010 format (/,i4,' incident energy e =',f10.6,' scaled ryd ',
     x        f10.6,' scaled Bohr ',
     x        /,' etot =',f14.6,' scaled a.u.,  nopen =',i3,/,
     x        ' channel energies (scaled ryd) :',/,(2x,6f12.5))
 1020 format (' r-matrix at  r  =',f9.5,
     x        ' scaled Bohr  e =',f9.5,' scaled ryd :'/)
 1100 format (///1x,'trial radius statistics'/
     x           1x,'***********************'/
     x'    once twice   x3    x4    x5    x6    x7    x8    x9  over9',
     x        /, 1x,10i6 )    
      end
      subroutine prsmat(nume,sce,ntarg,etarg,ltarg,iet,nmt7,
     x                  ncases,sline1,nelc1,nz1,ilast,x,ix)
      implicit integer (a-z)
      real*8 sce,e,etarg,e0,zero,atry,x  
      character*1 num
      character*3 skk
      parameter ( zero=0.0d0, atry=2.0d0  )
*
      common /io/ iin,iout
      common /smpt/ ietc,nslp,ispst,illst,iprst,lln,llx,srmat,
     x              simat,sline,nelc,nz,ldim,nastm
      dimension etarg(ntarg),ltarg(ntarg),sce(nume),
     x          iet(ntarg),x(*),ix(*),num(0:9)
*
      data num/'0','1','2','3','4','5','6','7','8','9'/
*
      sline = sline1
      nelc = nelc1
      nz = nz1
      e = sce(nume)
      nastm = ntarg
      e0 = etarg(1)
      do 10 it = 1,ntarg
         do 20 ie = 1,nume
            if (sce(ie) .ge. (etarg(it) - e0)*atry) then 
               iet(it)=ie
               go to 30
            end if  
 20      continue
         nastm=it-1
         goto 40
 30      lat = ltarg(it)
         irecl=8*2*(((lat+1)*(lat+2))/2)*(nume-iet(it)+1)
         nunit=nmt7+it
         skk = 'S'//num(it/10)//num(it-10*(it/10))
         open(nunit,file=skk,status='unknown',access='direct',
     x        recl=irecl)
 10   continue
 40   ietc = ilast 
      nslp = ietc + nastm
      ispst = nslp + nastm
      ndim = nastm * ncases
      illst = ispst + ndim
      iprst = illst + ndim
      lln = iprst + ndim
      llx = lln + ndim
      srmat = iadtwp(llx + ndim + 1)
      latm = ltarg(1)
      do 50 it = 2,nastm
         latm = max(latm,ltarg(it))
 50   continue
      ldim = ((latm+1)*(latm+2))/2 
      sdim = ldim * nume * nastm
      simat = srmat + sdim
      last = simat + sdim
      ilast = wpadti(last + 1)
      do 60 it = 1,nastm
         ix(nslp + it -1) = 0
 60   continue
      return
      end
          
      subroutine prtdat(prtflg,rafin,raflw,jj,lrglx,lrglr,radwm,
     x                  lamax,iasy,abvthr,belthr,degeny,ewron,
     x                  beta,cuplim,deplim,ncrit)
*
* $Id: prtdat.f,v 2.1 94/09/29 18:39:37 vmb Exp Locker: vmb $
*
      implicit integer (a-z)
      real*8 rafin, raflw, radwm, abvthr, belthr, degeny, ewron, 
     x     beta,cuplim,deplim
      logical jj
      dimension prtflg(7),nmt(7)
      common /io/ iin,iout
*
*     print input data
*
      write (iout,1000)
      if (prtflg(1) .ge. 1) 
     x    write(iout,1001) 
      if (prtflg(2) .ge. 1)
     x    write(iout,1002) 
      if (prtflg(3) .ge. 1) 
     x    write(iout,1003) 
      if (prtflg(4) .ge. 1) 
     x    write(iout,1004) 
      if (prtflg(5) .ge. 1) 
     x    write(iout,1005) 
      if (prtflg(6) .ge. 1) 
     x    write(iout,1006) 
      if (prtflg(7) .ge. 1) 
     x    write(iout,1007) 
      if (jj) then
         write(iout,1020)
      else
         write(iout,1021)
      end if
      write(iout,1022) rafin, raflw
      write(iout,1023)lrglx
      write(iout,1024)lrglr,radwm
      write(iout,1025) lamax,iasy
      write(iout,1026) abvthr, belthr, degeny
      write(iout,1027) ewron
      write(iout,1028) beta, cuplim, deplim, ncrit
      return
*
 1000 format(/' namelist phzin data :')
 1001 format(' K matrices will be calculated')
 1002 format(' eigenphase sums will be calculated')
 1003 format(' T matrices will be calculated')
 1004 format(' X sections will be calculated')
 1005 format(' total collision strengths will be calculated')
 1006 format(' top up data will be calculated')
 1007 format(' S matrices will be calculated')
 1020 format(' JJ coupling is assumed.  L=2*J')
 1021 format(' LS coupling is assumed')
 1022 format(' estimated final radius, rafin =',f10.3,' scld Bohr',/,
     x       ' radius to which BBM propagator should be used,', 
     x       ' raflw =',f10.3,' scld Bohr')
 1023 format(' Exchange is neglected for total L .gt. ',i3)
 1024 format(' Asymptotic Distorted Wave Method used at total L .gt.'
     x         ,i5,/,
     x       '    using an initial radius of',f6.3, 
     x       'scld Bohr at which the R-matrix is calculated')
 1025 format(' no. of multipoles retained in potential, lamax =',i3,/,
     x       ' no. of terms in Gailitis expansion, iasy =',i3)
 1026 format(' scattering energies closer to a threshold',/
     x       ' than abvthr =',d16.8,' scld Ryd',/
     x       ' and  belthr =',d16.8,' scld Ryd are omitted.',/
     x       ' target levels closer than degeny =',d16.8,' scld Ryd',/
     x       ' are taken as degenerate in the Gailitis expansion.')
 1027 format(' Wronskian convergence parameter, ewron =',d16.8)
 1028 format(' Light-Walker sector size parameter beta =',d16.8,/
     x       ' closed channels with coupling less than cuplim =',d16.8,/
     x       ' are dropped if the channel energy .lt. -deplim =',d16.8,
     x       ' scld ryd',/
     x       ' except that ncrit =',i2,' of such channels are retained')
*
      end
      subroutine pxsnl (nl,fl,nume,nxsn,xsnl,en,
     x                  ixsn,iind,jind,ncol)
*
* $Id: pxsnl.f,v 2.1 94/09/29 18:40:14 vmb Exp Locker: vmb $
*
      implicit integer (a-z)
      real*8 xsnl,en,fl
      dimension xsnl(nxsn,nume,nl),en(nume),iind(nxsn),jind(nxsn),
     x          fl(nl)
      common /io/ iin,iout
*
*     pxsnl : print cross section table for each L
*
      do 10 il = 1,nl
         write (iout,1000)il,fl(il)
         call pxsntb (nume,nxsn,xsnl(1,1,il),en,
     x                ixsn,iind,jind,ncol)
 10   continue
      return
*
 1000 format (//' ',i4,' L=',f4.0, '(summed over spin and parity)')
      end
      subroutine pxsntb (nume,nxsn,xsn,en,ixsn,iind,jind,ncol)
*
* $Id: pxsntb.f,v 2.1 94/09/29 18:40:13 vmb Exp Locker: vmb $
*
      implicit integer (a-z)
      real*8 xsn,en
      dimension xsn(nxsn,nume),en(nume),iind(nxsn),jind(nxsn)
      common /io/ iin,iout
*
*     pxsntb : print cross section table
*
      if ( ixsn .ne. 0 ) then
         do 20 i = 1, nxsn, ncol
            ip = min(i+ncol-1,nxsn)
            write (iout,1010) (jind(ij),iind(ij),ij = i,ip)
            do 10 ie = 1, nume
               write (iout,1020) ie,en(ie),(xsn(ntg,ie),ntg = i,ip)
   10       continue
   20    continue
      endif
      return
*
 1010 format (//,4x,'i',3x,'e(scld ryd )',6(i3,'-',i3,5x))
 1020 format (1x,i4,2x,d12.5,6d12.5)
      end
      subroutine rafmx (nchan,eth,enum,sce,cc,rafest,rafmax,raf,
     x                  rmatr,ion,lamax)
*
* $Id: rafmx.f,v 2.1 94/09/29 18:40:19 vmb Exp Locker: vmb $
*
      implicit integer (a-z)
      real*8 eth,sce,cc,eps,nu2,rafest,rafmax,raf,rafi,r,rmatr,
     x     zero,one,three,rion,rion2,atry,dfloat
      parameter ( atry=2.0d0,zero=0.0d0,one=1.0d0,three=3.0d0 )
      common /io/ iin,iout
      dimension eth(nchan),cc(nchan),sce(enum),raf(enum)
*
*     estimate the max value of rafin for the energy range
*
      rafmax = rafest 
*     if there is no long range potential take rafin as default at
*      all energies
*     for positive ions 
*      for open channels take rafin as 3 times the point of inflection
*      for closed channels midway between inner and outer
*     for neutrals
*      for open channels take rafin as .5*point of inflection
*      for closed take default 
*     for negative ions take rafin as default
      if (ion .gt. 0 .and. lamax .gt. 0) then
         rion = dfloat (ion)
         rion2 = rion * rion
         do 40 ie = 1,enum
            rafi = rafest
            r = rafest
            do 50 i = 1,nchan
               eps = (sce(ie) - eth(i))/rion2
               if (eps .ge. zero) then
                  r = three / rion *
     x                  (sqrt(one + eps*cc(i)) - one) / eps
               else
                  nu2 = one / (-eps)
                  if ( nu2 .ge. cc(i) ) r = nu2 / rion
               end if
               rafi = max (rafi,r)
 50         continue
            rafmax = max (rafmax,rafi)
            raf(ie) = rafi
 40      continue
      else if (ion .eq. 0 .and. lamax .gt. 0) then
         do 60 ie = 1,enum
            rafi = rafest
            r = rafest
            do 70 i = 1,nchan
               eps = sce(ie) - eth(i)
               if (eps .gt. zero) then
                  r = sqrt(cc(i)/eps)
               end if
               rafi = max(rafi,r)
 70         continue
            rafmax = max(rafmax,rafi)
            raf(ie) = rafi
 60      continue
      else
         rafmax = rafest
         do 80 ie = 1,enum
            raf(ie) = rafest
 80      continue
      end if 
c     write (iout,1000)rafmax
c1000 format (' estimated rafmax =',f10.4)
      return
*
      end
      subroutine rdcas (ls,ll,lpi,lrgs,lrgl,lrgpi,ncase,ncases)
*
* $Id: rdcas.f,v 2.1 94/09/29 18:39:59 vmb Exp Locker: vmb $
*
      implicit integer (a-z)
      common /io/ iin,iout
      dimension ls(ncase),ll(ncase),lpi(ncase),lrgs(ncase),
     x          lrgl(ncase),lrgpi(ncase)
*
*     copy list of cases
*
      ncases = 0
      do 10  i=1,ncase
         l = ll(i)
         if (l .lt. 0) go to 20
         lrgs(i) = ls(i)
         lrgl(i) = l
         lrgpi(i) = lpi(i)
         ncases = i
 10   continue
 20   return
      end
      subroutine rdlran(jj,llo,lup,ldel,nlran,lrglx,ncases,
     x                  ntarg,starg,mintsp,maxtsp,minlsp,maxlsp)
      implicit integer (a-z)
      logical jj
      common /io/ iin,iout
      dimension starg(ntarg),tsval(10),llo(nlran),lup(nlran),
     x          ldel(nlran)
*
*   calculate the range of target spin and large spin and 
*   modify the ranges of large l to avoid overlap of the ranges
*   calculate the number of slpi cases 
*
*   for j-j coupling large spin is taken as zero and l = 2 * j
*
      if (.not. jj) then
         ns = 1
         s1 = starg(1)
         tsval(1) = s1
         mintsp = s1 
         maxtsp = s1
         do 10 i = 2,ntarg
            tsp = starg(i)
            do 20 j = 1,ns
               if (tsp .eq. tsval(j)) go to 10 
 20         continue
            ns = ns + 1
            tsval(ns) = tsp
            mintsp = min (mintsp, tsp)
            maxtsp = max (maxtsp, tsp)
 10      continue
         ntspin = (maxtsp - mintsp + 2) / 2
         minlsp = mintsp - 1
         maxlsp = maxtsp + 1
         if (minlsp .eq. 0) then
            minlsp = 2
         end if
         nlspin = (maxlsp - minlsp + 2) / 2
      else
         mintsp = 0
         maxtsp = 0
         minlsp = 0
         maxlsp = 0
         ntspin = 1
         nlspin = 1
      end if
      ncases = 0
      nl = 0
      ltop = -1
      do 30 i = 1,nlran
         ld = max(ldel(i),1)
         if (llo(i) .le. ltop ) llo(i) = ltop + ld
         do 40 l = llo(i), lup(i), ld
            if (l .le. lrglx) then
               ncases = ncases + nlspin
            else
               ncases = ncases + ntspin
            end if
            ltop = l
 40      continue
 30   continue
      ncases = ncases * 2
      return
      end
      subroutine rmatd (nstat,nchan,eig,wmat,e0,cfbut,ibuttl,ichl,
     x                  etargs,rmatr,e,rmat,fkn,uk)
*
* $Id: rmatd.f,v 2.1 94/09/29 18:40:31 vmb Exp Locker: vmb $
*
      implicit integer (a-z)
      real*8 half,two,zero,cfbut,e,e0,eb,ediff,eig,etargs,etot,fkn,
     x       ra2,rmat,rmatr,sum,sumb,uk,wmat,but0,dfloat 
      parameter ( zero=0.d0,half=0.5d0,two=2.0d0 )
      dimension rmat(*),eig(nstat),wmat(nstat,nchan),cfbut(3,nchan),
     x          etargs(*),ichl(nchan),fkn(*),uk(*)
*
*     rmatd : nchan-channel r-matrix for energy etot
*
      ra2 = rmatr * rmatr
      etot = e0 + half * e
      kk = 0
      do 70 i2 = 1, nchan
         do 60 i1 = 1, i2
            sum = zero
            do 50 i = 1, nstat
               ediff = eig(i) - etot
               sum = sum + wmat(i,i1) * wmat(i,i2) / ediff
   50       continue
            kk = kk + 1
            rmat(kk) = sum
   60    continue
   70 continue
      if ( ibuttl .ne. 0 ) then
         do 80 i1 = 1, nchan
            eb = two * ( etot - etargs(ichl(i1)) )
            if ( cfbut(3,i1) .ge. dfloat(-10000) ) then
               sumb = eb * ( eb * cfbut(3,i1) + cfbut(2,i1) ) +
     x                cfbut(1,i1)
            else
               nbut = - int( cfbut(3,i1) ) / 10000
               sumb = cfbut(1,i1) * but0(nbut,fkn,uk,cfbut(2,i1) +
     x                ra2*eb)
            endif
                 kk = ( i1 * ( i1 + 1 ) ) / 2
            rmat(kk) = rmat(kk) + sumb
   80    continue
      endif
      return
*
      end
      subroutine rmatf(nchan,l2p,et,ion,rmatr,nfc,fc,fcp,gc,gcp,
     x                 iasy,iasx,xg,xc,xd,xcp,xdp,rmat)
      implicit integer (a-z)
      real*8 et,rmatr,rmat,fc,fcp,gc,gcp,dfloat,zero,one,skj,etaj,
     x       rho,flj,xg,xc,xd,xcp,xdp,zz,etj,u,uder
      parameter ( zero=0.d0, one=1.d0)
      common /io/ iin,iout
      dimension rmat(*),et(nchan),l2p(nchan),
     x          fc(nfc),gc(nfc),fcp(nfc),gcp(nfc),
     x		xg(0:iasy),xc(0:iasx),xd(0:iasx),xcp(0:iasx),
     x		xdp(0:iasx)
*
      zz = dfloat(ion)
      kk = 0
      do 70 i2 = 1, nchan
         do 60 i1 = 1, i2
            kk = kk + 1
            rmat(kk) = zero
   60    continue
   70 continue
      do 20 j = 1,nchan
         jj = (j * (j+1))/2
         lj = l2p(j)
         etj = et(j)
         if (etj .ge. zero) then
            skj = sqrt(dabs(etj))
            etaj = -zz/skj
            rho = rmatr * skj
            flj = dfloat(lj)
            call coulfg(rho,etaj,flj,flj,fc,gc,fcp,gcp,1,0,ifail)
            if (ifail .ne. 0) then
*	       write (iout,1000) ifail,lj,rho,etaj
*              write (iout,*) 'j,lj,skj,et(j)',j,lj,skj,et(j)
*   f = exp(- rho + abs(etaj) * log(rho+rho)) / sqrt(skj)
*   fp = - (skj - abs(etaj) / rmatr) * f
               rmat(jj) = -one / (skj - abs(etaj) ) 
            else
               rmat(jj) = fc(lj+1)/(rho * fcp(lj+1))
            end if
         else if ( ion .ne. 0 ) then
	    call coul (lj,zz,etj,rmatr,u,uder,iasy,xg,xc,xd,xcp,xdp,
     x		       0,0)
c            skj = sqrt(abs(etj))
c            etaj = -zz/skj
c            rmat(jj) = -one / (skj - abs(etaj) ) 
             rmat(jj) = u/(rmatr * uder)
	 else
	    call decay (etj,lj,rmatr,u,uder)
            rmat(jj) = u/(rmatr * uder)
	 endif
 20   continue
      return
 1000 format (' coulfg : ifail =',i4,' lj =',i4,' rho =',d16.8,
     x        ' etaj =',d16.8)
      end
      subroutine rmatf1(nchan,lchl,et,lmax,ion,iasy,rmatr,rmat,x)
      implicit integer (a-z)
      real*8 et,rmatr,rmat,x
      dimension rmat(*),et(nchan),lchl(nchan),x(*)
      common /glpt/ ga1,ga2,fc,fcp,gc,gcp,xg,xc,xd,xcp,xdp,fac1,fac2,
     x              fac3,fac4,iac1,deg,sma,smb
*
      nfc = lmax + 10
      iasx = iasy/2
      iasy1 = iasy-1
      call rmatf(nchan,lchl,et,ion,rmatr,nfc,x(fc),x(fcp),x(gc),x(gcp),
     x           iasy1,iasx,x(xg),x(xc),x(xd),x(xcp),x(xdp),rmat)
      return
      end
      subroutine secend(rafin,nrange,nranmx,asect,isect,nfinal,
     x                  lwsect,lwran)
*
* $Id: secend.f,v 2.1 94/09/29 18:39:49 vmb Exp Locker: vmb $
*
      implicit integer (a-z)
      real*8 rafin,asect,lwsect
      dimension asect(*),lwsect(lwran)
c     hugo addition
	 isect=-1
         do 21 i = 1,nranmx + 1
            if (rafin .le. asect(i)) then
               rafin = asect(i)
               nrange = i-1
               isect = 1
               go to 23
            end if
  21     continue
         nrange = nranmx
         do 22 i = 1,nfinal
            if (rafin .le. lwsect(i)) then
               rafin = lwsect(i)
               isect = i
               go to 23
            end if
 22      continue
	 print *,isect
 23      return
         end
      subroutine sleqn (a,lda,n,b,m,ipvt)
*
* $Id: sleqn.f,v 2.1 94/09/29 18:39:53 vmb Exp Locker: vmb $
*
      implicit integer (a-z)
      real*8 a,b,one
      parameter ( one=1.0d0 )
      common /io/ iin,iout
      dimension a(lda,n),b(lda,m),ipvt(n)
*
      call dgetrf (n,n,a,lda,ipvt,ier)
      if ( ier .ne. 0 ) then
         write (iout,1000) ier
         stop
      endif
      call dgetrs('n',n,n,a,lda,ipvt,b,lda,ier)
      if ( ier .ne. 0 ) then
	 write (iout,1010) ier
	 stop
      endif
      return
 1000 format (' sleqn : error return from dgetrf, info = ',i6)
 1010 format (' sleqn : error return from dgetrs, info = ',i6)
      end
      subroutine smat (ie,iet,trmat,timat,srmat,simat,ipr,nopen,
     x                 nume,nastm,ldim,nspn2,lrgl2,npty2,nltarg,ncol)       
      implicit integer (a-z)
      real*8 trmat,timat,srmat,simat,zero,one,two
      parameter ( zero=0.0d0,one=1.d0,two=2.d0 )
      dimension trmat(*),timat(*),iet(*),nltarg(*),
     x          srmat(ldim,nastm,nume),simat(ldim,nastm,nume)
      common /io/ iin,iout
*
*     tmat : t-matrix computed from k-matrix
*            s = ( 1 + i * k ) / ( 1 - i * k ) ;  s = del + t
*            t = 2 * i * k / ( 1 - i * k )
*
      nf = 0    
      do 60 it = 1, nastm
         if (ie.lt.iet(it)) return     
         if (nltarg(it).eq.0) goto 60       
         ni = nf + 1
         nf = nf + nltarg(it)   
            k = 0     
            do 30 nc = ni, nf
	       k = k + 1
	       nmc = nopen * ( nc - 1 ) + nc
	       srmat(k,it,ie) = trmat(nmc) + one
	       simat(k,it,ie) = timat(nmc)
               do 20 mc = nc+1, nf
                  k = k + 1   
                  nmc = nopen * ( nc - 1 ) + mc
		  srmat(k,it,ie) = trmat(nmc)
		  simat(k,it,ie) = timat(nmc)
   20          continue
   30       continue
   60 continue

*     if ipr = 2,3  print s-matrix
      if ( ipr .gt. 1 ) then
         do 70 it = 1,nastm
            nt = nltarg(it)
            if (nt .gt. 0) then
               write (iout,1020)
               call wrttmt (srmat(1,it,ie),nt,ncol)
               write (iout,1030)
               call wrttmt (simat(1,it,ie),nt,ncol)
            end if
 70      continue
      endif
      return
*
 1020 format (/,' real part of s-matrix :')
 1030 format (/,' imag part of s-matrix :')
      end
      subroutine smat1(nmt7,ie,tr,ti,ipr,nopen,nume,nspn2,lrgl2,
     x                 npty2,nltarg,ntarg,lchl,ncol,icase,ncases,
     x                 sce,x,ix)
      implicit integer (a-z)
      real*8 tr,ti,sce,x
      common /smpt/ iet,nslp,ispst,illst,iprst,lln,llx,srmat,
     x              simat,sline,nelc,nz,ldim,nastm
      dimension tr(*),ti(*),nltarg(ntarg),x(*),ix(*),lchl(nopen),
     x          sce(nume)
*
      call smat (ie,ix(iet),tr,ti,x(srmat),x(simat),ipr,
     x           nopen,nume,nastm,ldim,nspn2,lrgl2,npty2,nltarg,
     x           ncol)
      if ((ipr .eq. 1  .or. ipr .eq. 3)  .and. ie .eq. nume) then
         call wrsxx(nmt7,ix(iet),x(srmat),x(simat),nltarg,lchl,
     x              ix(nslp),ix(lln),ix(llx),ix(ispst),ix(illst),
     x              ix(iprst),ldim,nastm,ncases,nume,nspn2,lrgl2,
     x              npty2)
         if (icase .eq. ncases ) then
             call wrs00(nmt7,nelc,nz,ntarg,ix(nslp),ix(ispst),
     x                 ix(illst),ix(iprst),ix(iet),ix(lln),ix(llx),
     x                 sce,x(sline),nastm,ncases,nume)
         end if
      end if
      return
      end
      subroutine split (nchan,ichl,ntarg,starg,ispch,ichsp,nschan,
     x                  nspin,ns)
*
* $Id: split.f,v 2.1 94/09/29 18:40:21 vmb Exp Locker: vmb $
*
      implicit integer (  a - z )
      dimension ichl(nchan),starg(ntarg),ispch(nchan),ichsp(nchan),
     x          nschan(2),nspin(2)
*
*     split the channels into two possible spins
*     array ispch relates the new channel order to the old
*
      sl = starg (ichl(1))
      nch1 = 1
      ispch(1) = 1
      ichsp(1) = 1
      do 10  ich = 2,nchan
         if (sl .eq. starg(ichl(ich))) nch1 = nch1 + 1
 10   continue
      nschan(1) = nch1
      nspin(1)  = sl
      if (nch1 .eq. nchan) then
         ns = 1
         nschan(2) = 0
         do 20  ich = 2,nchan
            ispch(ich) = ich
            ichsp(ich) = ich 
 20      continue
      else
         nch2 = nch1
         nch1 = 1
         ns = 1
         do 30  ich = 2,nchan
            sn = starg(ichl(ich))
            if (sn .ne. sl) ns = -ns
            if (ns .gt. 0) then
               nch1 = nch1 + 1
               ispch(nch1) = ich
               ichsp(ich) = nch1
            else
               nch2 = nch2 + 1
               ispch(nch2) = ich
               ichsp(ich) = nch2 
            end if
            sl = sn
 30      continue
      ns = 2
      nspin(2) = starg(ichl(nch1 + 1))
      nschan(2) = nchan - nch1
      end if
      return
      end
      subroutine splitr(nchan,rmat,sprmat,ispch)
*
* $Id: splitr.f,v 2.1 94/09/29 18:40:23 vmb Exp Locker: vmb $
*
      implicit integer (a-z)
      real*8 rmat,sprmat
      dimension rmat(*),sprmat(nchan,nchan),ispch(nchan)
*
*     split rmat into spins by rearranging channels
*
      kk = 0
      do 10 i2 = 1,nchan
         ich2 = ispch(i2)
         do 10 i1 = 1,i2
            kk = kk + 1
            ich1 = ispch(i1)
            sprmat(ich1,ich2) = rmat(kk)
            sprmat(ich2,ich1) = rmat(kk)
 10   continue
      kk = 0
      do 20 i2 = 1,nchan
         do 20 i1 = 1,i2
            kk = kk + 1
            rmat(kk) = sprmat(i1,i2)
 20   continue
      return
      end
      subroutine statw (lrgl,nspn,ntarg,sw1,sw2,ltarg,starg)
      implicit integer (a-z)
      real*8 sw1,sw2,dfloat
      dimension sw2(ntarg),ltarg(ntarg),starg(ntarg)
*
*  nspn < 0 corresponds to no-exchange in inner region
*  nspn = 0 corresponds to Breit Pauli in inner region lrgl=2 * J
*           ltarg(i)=2*jtarg(i)
*  
      if (nspn .gt. 0) then
         sw1 = ( 2 * lrgl + 1 ) * nspn
      else if (nspn .lt. 0) then
         sw1 = -2 * ( 2 * lrgl + 1 ) * nspn
      else
         sw1 = lrgl + 1
      end if
      if (nspn .ne. 0) then
         do 10 i = 1, ntarg
            lf = 2 * ltarg(i) + 1
            sw2(i) = dfloat( lf*starg(i) )
   10    continue
      else
         do 20 i = 1,ntarg
            sw2(i) = dfloat(ltarg(i) + 1)
   20    continue
      end if
      return
      end
      subroutine tmat (ie,e,akmat,trmat,timat,ipr,nopen,a,b,ind,
     x                 nspn2,lrgl2,npty2,nchan,ichl,lchl,firste,
     x                 nume,nmt3,ncol)
*
* $Id: tmat.f,v 2.1 94/09/29 18:40:16 vmb Exp Locker: vmb $
*
      implicit integer (a-z)
      real*8 e,akmat,trmat,timat,a,b,x,zero,one,two
      logical firste
      parameter ( zero=0.0d0,one=1.d0,two=2.d0 )
      dimension akmat(*),trmat(*),timat(*),a(*),b(*),ind(*),
     x          ichl(nchan),lchl(nchan)
      common /io/ iin,iout
*
*     tmat : t-matrix computed from k-matrix
*            s = ( 1 + i * k ) / ( 1 - i * k ) ;  s = del + t
*            t = 2 * i * k / ( 1 - i * k )
*
      nop2 = nopen * nopen
      if ( nopen .eq. 1 ) then
         x = akmat(1)
         a(1) = x * x
         b(1) = one / ( one + x * x )
         timat(1) = x * b(1)
         trmat(1) = a(1) * b(1)           
      else
         ix = 1
         do 4 i = 1, nopen
            do 2 j = 1, i
               b((i-1)*nopen+j) = akmat(ix)
               b((j-1)*nopen+i) = akmat(ix)
               ix = ix + 1
    2       continue
    4    continue
         call dcopy (nop2,b,1,timat,1)
         call dgemm ('n','n',nopen,nopen,nopen,one,b,nopen,b,nopen,
     x                zero,a,nopen)
         call dcopy (nop2,a,1,trmat,1)
         do 10 i = 1, nopen
            ii = nopen * ( i - 1 ) + i
            a(ii) = a(ii) + one
   10    continue
         call dgetrf (nopen,nopen,a,nopen,ind,ier)
         if ( ier .ne. 0 ) then
            write (iout,1000) ier
            stop
         endif
         call dgetrs('n',nopen,nopen,a,nopen,ind,trmat,nopen,ier)
         call dgetrs('n',nopen,nopen,a,nopen,ind,timat,nopen,ier)
         if ( ier .ne. 0 ) then
	    write (iout,1010) ier
            stop
         endif
      endif
      do 30 i = 1, nop2
         trmat(i) = - two * trmat(i)
         timat(i) = two * timat(i)
   30 continue
*     if ipr = 1,3 write t-matrix to nmt3 ; = 2,3  print t-matrix
      if ( ipr .gt. 1 ) then
         write (iout,1020)
         call wrtmat (trmat,nopen,nopen,nopen,ncol)
         write (iout,1030)
         call wrtmat (timat,nopen,nopen,nopen,ncol)
      endif
      if ( ipr .eq. 1 .or. ipr .eq. 3  ) then
         if (firste) then
            numen = nume-ie+1
            write (nmt3) nspn2,lrgl2,npty2,numen,nchan,
     x                  (ichl(ich),lchl(ich),ich=1,nchan)
         end if
*  write the lower triangle of the t-matrix to file
         k = 0
         l = -nopen
         do 40 i = 1,nopen
            l = l+nopen
            m = l
            do 50 j = 1,i
               k = k+1
               m = m+1
               a(k) = trmat(m)
               b(k) = timat(m)
 50         continue
 40      continue
         write (nmt3) e,nopen,k,(a(i),b(i),i=1,k)
*         write (nmt3) e,nopen,nop2,(trmat(i),timat(i),i=1,nop2)
      endif
      return
*
 1000 format (' tmat : error return from dgetrf, info = ',i6)
 1010 format (' tmat : error return from dgetrs, info = ',i6)
 1020 format (/,' real part of t-matrix :')
 1030 format (/,' imag part of t-matrix :')
      end
      subroutine topdat(ntarg,etarg,abvthr,belthr,ebig,sce2,
     x                  enum2,ncases,lrgl,lrgle1,nl,fl,lstart,lfin,
     x                  nxsn,xsnl,tlast,x,ix)
*
* $Id: topdat.f,v 2.1 94/09/29 18:41:01 vmb Exp Locker: vmb $
*
      implicit integer (a-z)
      real*8 esc2,x,etarg,abvthr,belthr,ebig,zero
      common /io/ iin, iout
      dimension ne2(10),esc2(20),x(*),ix(*),etarg(ntarg),
     x          lrgl(ncases)
      namelist /topin/ lrgle,ne2,esc2
      data zero/0.0d0/,ne2/10*0/
      read (iin,topin,end=10,err=10)
      go to 11
 10   write (iout,topin)
      stop
 11   lrgle1 = lrgle
      write (iout,1000) lrgle
      nume2 = necnt(ne2)
      sce2 = tlast
      tlast = sce2 + nume2 + 1
      scest = tlast
      norder = wpadti(scest + nume2 + 1)
*
*  store the energy mesh after pruning out energies too close to
*  threshold 
*
      call evals (ntarg,nume2,ne2,esc2,etarg,abvthr,belthr,
     x            x(sce2),enum2,ebig,x(scest),ix(norder))
      if (enum2 .lt. nume2) then
         write (iout,2010) enum2
      end if
      call lvals (ncases,lrgl,lrgle,nl,x(tlast))
      fl =tlast 
      write(iout,*) 'fl =',fl
      tlast =fl + nl
*
      call llims(ncases,lrgl,lstart,lfin)
*
      xsnl = tlast
      len = nl * enum2 * nxsn
      tlast = xsnl + len
*
*  zeroise the xsnl array
*
      do 20 i = 1,len
         x(xsnl + i - 1) = zero
 20   continue
      return
 1000 format (' start top up procedure at L=lrgle = ',i4)
 2010 format (' coarse energy mesh for top-up',/,
     x        ' number of discrete energies away from thresholds = ',i4)
      end
      subroutine totcol (ntarg,nltarg,nlt,sw1,a,omslpi,omega,
     x                   idbug,nxsn,ion,ji,iind,jind,tr,ti,nopen,ien,
     x                   nl,nume2,oml,il)
*
* $Id: totcol.f,v 2.1 94/09/29 18:40:26 vmb Exp Locker: vmb $
*
      implicit integer (a-z)
      real*8 a,omslpi,omega,oml,tr,ti,zero,half,c1,sw1,cros
      parameter ( zero=0.d0,half=0.5d0 )
      dimension nltarg(ntarg),tr(*),ti(*),ji(*),a(*),
     x          omslpi(nxsn),omega(nxsn),iind(nxsn),jind(nxsn),
     x          oml(nxsn,nume2,nl)
      common /io/ iin,iout
*
      nop2 = nopen * nopen
      do 12 i = 1, nop2
         a(i) = tr(i)**2 + ti(i)**2
   12 continue
      ix = 0
      c1 = half * sw1 
      nf = 0
      do 60 i = 1, nlt
         ni = nf + 1
         nf = nf + nltarg(i)
         mf = 0
         jup = i
         if (ion .gt. 0) jup = i-1
         do 40 j = 1, jup
            ix = ix + 1
            mi = mf + 1
            mf = mf + nltarg(j)
            cros = zero
            do 30 nc = ni, nf
               do 20 mc = mi, mf
                  nmc = nopen * ( nc - 1 ) + mc
                  cros = cros + a(nmc)
   20          continue
   30       continue
            omslpi(ix) = c1 * cros 
   40    continue
         ji(ien) = ix
   60 continue
*     debug print of collision strength
      if ( idbug .gt. 0 .and. ix .gt. 0 ) then
         write (iout,1000)
         nloop = ix/6 + 1
         n2 = 0
         do 90 ni = 1, nloop
            n1 = n2 + 1
            n2 = min(n1 + 5,ix)
            write (iout,1010) (jind(ij),iind(ij),ij = n1,n2)
            write (iout,1020) (omslpi(ij),ij = n1,n2) 
   90    continue
      endif
      if (il .le. 0) then
         do 100 i = 1,ix
            omega(i) = omega(i) + omslpi(i)
 100     continue
      else
            do 101 i = 1,ix
               oml(i,ien,il) = oml(i,ien,il) + omslpi(i)
 101        continue
      end if
      return
*
 1000 format (/,10x,'collision strengths'/10x,'*******************')
 1010 format (1x,6(i3,'-',i3,5x))
 1020 format (1x,6d12.5)
      end
      double precision function wabs1(a,b,c,d,f)
*
*  calculates absolute value of racah coefficient
*  w(a,b,c,d,1,f)
*
      implicit integer(a-z)
      real*8  dfloat,half
      common /io/ iin, iout
      data half/0.5d0/
*
      x=0
      y=0
      if (f .le. min(b+d,a+c)
     x    .and. f .ge. max(abs(b-d),abs(a-c))) then
         if (b .eq. a-1) then 
            if (d .eq. c-1) then
               x=(b+d+f+2)*(b+d+f+3)*(b+d-f+1)*(b+d-f+2)
               y=(b+1)*(2*b+1)*(2*b+3)*(d+1)*(2*d+1)*(2*d+3)
            else if (d .eq. c) then
               x=(b+d+f+2)*(-b+d+f)*(b-d+f+1)*(b+d-f+1)
               y=(b+1)*(2*b+1)*(2*b+3)*d*(d+1)*(2*d+1)
            else if (d .eq. c+1) then
               x=(-b+d+f-1)*(-b+d+f)*(b-d+f+1)*(b-d+f+2)
               y=(b+1)*(2*b+1)*(2*b+3)*d*(2*d-1)*(2*d+1)
            end if
         else if (b .eq. a) then
            if (d .eq. c-1) then
               x=(b+d+f+2)*(-b+d+f+1)*(b-d+f)*(b+d-f+1)
               y=b*(b+1)*(2*b+1)*(d+1)*(2*d+1)*(2*d+3)
            else if (d .eq. c) then
               x=b*(b+1)+d*(d+1)-f*(f+1)
               y=b*(b+1)*(2*b+1)*d*(d+1)*(2*d+1)
               x=x*x
            else if (d .eq. c+1) then
               x=(b+d+f+1)*(-b+d+f)*(b-d+f+1)*(b+d-f)
               y=b*(b+1)*(2*b+1)*d*(2*d-1)*(2*d+1)
            end if
         else if (b .eq. a+1) then         
            if (d .eq. c-1) then
               x=(-b+d+f+1)*(-b+d+f+2)*(b-d+f-1)*(b-d+f)
               y=b*(2*b-1)*(2*b+1)*(d+1)*(2*d+1)*(2*d+3)
            else if (d .eq. c) then
               x=(b+d+f+1)*(-b+d+f+1)*(b-d+f)*(b+d-f)
               y=b*(2*b-1)*(2*b+1)*d*(d+1)*(2*d+1)
            else if (d .eq. c+1) then
               x=(b+d+f)*(b+d+f+1)*(b+d-f-1)*(b+d-f)
               y=b*(2*b-1)*(2*b+1)*d*(2*d-1)*(2*d+1)
            end if
         end if
      end if
      if (x .ne. 0  .and. y .ne. 0) then
         wabs1=half*sqrt(dfloat(x)/dfloat(y))
      else
         write(iout,2000)a,b,c,d,f
         stop
      end if
 2000 format(' stop in wabs1 while calculating',/,
     x      ' W(',i2,',',i2,',',i2,',',i2,'; 1,',i2,')')
*
      return
      end
      subroutine wronsk (nchan,nopen,f,fp,idiag1,idiag2,eps,fail)
*
* $Id: wronsk.f,v 2.1 94/09/29 18:39:54 vmb Exp Locker: vmb $
*
      implicit integer (a-z)
      logical fail
      real*8 f,fp,eps,zero,one,sum,tsum
      parameter ( zero=0.0d0,one=1.0d0 )
      common /io/ iin,iout
      dimension f(nchan,nchan,2),fp(nchan,nchan,2)
*
      fail = .false.
      if ( idiag2 .ne. 0 ) then
         write (iout,1010)
         do 20 i = 1, nchan
            write (iout,1000) (f(i,j,1),j = 1,nchan)
   20    continue
         write (iout,1020)
         do 30 i = 1, nchan
            write (iout,1000) (fp(i,j,1),j = 1,nchan)
   30    continue
         write (iout,1030)
         do 40 i = 1, nchan
            write (iout,1000) (f(i,j,2),j = 1,nchan)
   40    continue
         write (iout,1040)
         do  50  i = 1, nchan
            write (iout,1000) ( fp(i,j,2),j = 1,nchan)
   50    continue
      endif
*
*     check multichannel wronskian relations
*
      do 90 i0 = 1, 2
         if ( idiag2 .ne. 0 ) write (iout,1050) i0
         do 90 j1 = 1, nchan
            do 90 j2 = 1, nchan
               sum = zero
               tsum = zero
               do 70 i = 1, nchan
                  sum = sum + fp(i,j1,1) * f(i,j2,i0) - f(i,j1,1) *
     x                  fp(i,j2,i0)
   70          continue
               if ( i0 .eq. 2 .and. j1 .eq. j2 .and. j1 .le. nopen )
     x              tsum = one
               if ( abs(sum-tsum) .gt. eps ) then
                  fail = .true.
                  if (idiag1 .ne. 0) then
                     write (iout,1070) i0,j1,j2,sum
                  end if 
               endif
               if ( idiag2 .ne. 0 ) write (iout,1060) j1,j2,sum
   90 continue
      return
*
 1000 format (1x,4d30.15)
 1010 format ('0regular functions')
 1020 format ('0regular derivatives')
 1030 format ('0irregular functions')
 1040 format ('0irregular derivatives')
 1050 format (/,' multichannel wronskian relations :',/,' i0 =',i3,/)
 1060 format (' i1 =',i3,5x,' i2 =',i3,5x,'wronskian =',d30.17)
 1070 format (' i0 =',i3,4x,' i1 =',i3,4x,' i2 =',i3,4x,'wronskian =',
     x        d26.17)
      end
      subroutine wrs00(nfts,nelc,nz,ntarg,nslp,ispst,illst,iprst,iet,
     x                 lln,llx,sce,sline,nastm,ncases,nume)
*
      implicit integer (a-z)
      real*8 sce,sline,zero
      parameter(zero = 0.0d0)
      dimension iet(*),nslp(*),sce(*),sline(*),
     x		lln(nastm,ncases),llx(nastm,ncases),       
     x		ispst(nastm,ncases),illst(nastm,ncases),
     x		iprst(nastm,ncases)
*
      write(nfts) nz,nelc,ntarg,nastm
      write(3,*) nz,nelc,ntarg,nastm
      write(nfts) nume,(sce(ie),ie=1,nume),(zero,ie=1,nume)
      write(3,*) nume,(sce(ie),ie=1,nume),(zero,ie=1,nume)
      write(nfts) (sline(k),k=1,((ntarg-1)*ntarg)/2)
      write(3,*) (sline(k),k=1,((ntarg-1)*ntarg)/2)
      do 10 it=1,nastm
	 write(nfts) iet(it),nslp(it),(ispst(it,n),illst(it,n),
     x               iprst(it,n),lln(it,n),llx(it,n),n=1,nslp(it))
	 write(3,*) iet(it),nslp(it),(ispst(it,n),illst(it,n),
     x               iprst(it,n),lln(it,n),llx(it,n),n=1,nslp(it))
   10 continue
*
      return
      end

       
       

      subroutine wrslin(ntarg,nxsn,sline)
      implicit integer(a - z)
      real*8 sline,zero
      common /io/ iin, iout
      dimension sline(nxsn)
      data zero/0.0d0/
*  print sline
      write(iout,2000)
      do 10 it=2,ntarg
         do 20 jt=1,it-1
            k=((it-1)*(it-2))/2+jt
            if(sline(k).gt.zero) write(iout,2010)jt,it,sline(k)
   20    continue
   10 continue
 2000 format(//,6x,' Line Strengths',/,6x,' **************',/
     x       ' lower  upper'/' state  state',/ )
 2010 format(2x,i3,4x,i3,4x,f10.4)
      return
      end

      subroutine wrsxx(nfts,iet,srmat,simat,nltarg,lchl,nslp,lln,llx,
     x			ispst,illst,iprst,ldim,nastm,ncases,nume,
     x                  nspn2,lrgl2,npty2)
*
      implicit integer (a-z)
      real*8 srmat,simat
      dimension iet(*),srmat(ldim,nastm,nume),simat(ldim,nastm,nume),
     x		nltarg(*),nslp(*),lln(nastm,ncases),llx(nastm,ncases),       
     x		ispst(nastm,ncases),illst(nastm,ncases),
     x		iprst(nastm,ncases),lchl(*)
*
      nf = 0
      do 10 it = 1, nastm
	 if (nltarg(it).eq.0) goto 10
         ni = nf + 1  
	 nf = nf + nltarg(it)
	 nslp(it) = nslp(it) + 1
	 n = nslp(it)
	 ln = lchl(ni)
	 lx = lchl(nf)
	 lln(it,n) = ln
	 llx(it,n) = lx
	 ispst(it,n) = nspn2
	 illst(it,n) = lrgl2
	 iprst(it,n) = npty2
	 km = ((2 + lx - ln) * (4 + lx - ln)) / 8
	 nunit = nfts + it
	 write(nunit,rec=n) ((srmat(k,it,ie),simat(k,it,ie),k=1,km),
     x			    ie=iet(it),nume)
	 write(3,*)' nunit,n',nunit,n,((srmat(k,it,ie),simat(k,it,ie),
     x   k=1,km),ie=iet(it),nume)
   10 continue
*
      return
      end

       
       

      subroutine wrtmat (a,n,m,l,ncol)
*
* $Id: wrtmat.f,v 2.1 94/09/29 18:39:56 vmb Exp Locker: vmb $
*
      implicit integer (a-z)
      real*8 a
      common /io/ iin,iout
      dimension a(l,*)
*
*     wrtmat : prints an n*m matrix stored in an l*l array
*
      ntim = m / ncol
      nf = 0
      if ( ntim .gt. 0 ) then
         do 20 i = 1, ntim
            ni = nf + 1
            nf = nf + ncol
            do 10 j = 1, n
               write (iout,1000) (a(j,k),k = ni,nf)
   10       continue
            write (iout,1010)
   20    continue
      endif
      ni = nf + 1
      if ( ni .le. m ) then
         do 40 j = 1, n
            write (iout,1000) (a(j,k),k = ni,m)
   40    continue
      endif
      return
*
 1000 format (7d13.5)
 1010 format (/)
      end
      subroutine wrttmt (a,n,ncol)
*
* $Id: wrttmt.f,v 2.1 94/09/29 18:39:58 vmb Exp Locker: vmb $
*
      implicit integer (a-z)
      real*8 a
      common /io/ iin,iout
      dimension a(*)
*
*     wrtmat : prints an n*n matrix stored in a lower triangular array
*
      nf = 0
      do 40 k = 1, n
         ntim = k / ncol
         if ( ntim .gt. 0 ) then
            do 20 i = 1, ntim
               ni = nf + 1
               nf = nf + ncol
               write (iout,1000) (a(kk),kk = ni,nf)
   20       continue
         endif
         ires = mod(k,ncol)
         if ( ires .ge. 1 ) then
            ni = nf + 1
            nf = nf + ires
            write (iout,1000) (a(kk),kk = ni,nf)
         endif
         write (iout,1010)
   40 continue
      return
*
 1000 format (7d13.5)
 1010 format (/)
      end
      subroutine wrxsn(nmt,nxsn,nume1,sce1,xsn)
*
* $Id: wrxsn.f,v 2.1 94/09/29 18:40:58 vmb Exp Locker: vmb $
*
      implicit integer (a-z)
      real*8 sce1,xsn
*
*  write file of total cross sections or total collision strengths
*
*  nume1 = number of energies in fine energy mesh
*  l1max = highest L for which cross sections will have been written
*
      dimension sce1(nume1),xsn(nxsn,nume1)
*
*      rewind(nmt)
      write(nmt)nxsn,nume1
      write(nmt)(sce1(ie1),ie1=1,nume1)
      write(nmt)((xsn(ix,ie1),ix=1,nxsn),ie1=1,nume1)
      backspace(nmt)
      backspace(nmt)
      backspace(nmt)
*
      return
      end
      subroutine wrxsnl(nmt,nxsn,nl,nume2,fl,sce2,xsnl)
*
* $Id: wrxsnl.f,v 2.1 94/09/29 18:41:00 vmb Exp Locker: vmb $
*
      implicit integer (a-z)
      real*8 fl,sce2,xsnl
*
*  write the top up file for  cross-sections or collision strengths
*
*  nl = L dimension of xsnl = max no. of L values attempted at the coarse
*         energy mesh
*  nume2 = number of energies in coarse energy mesh
*
      dimension fl(nl),sce2(nume2),xsnl(nxsn,nume2,nl)
*
*      rewind nmt
      write(nmt)nxsn,nume2,nl
      write(nmt)(sce2(ie2),ie2 = 1,nume2)
      write(nmt)(fl(il),il = 1,nl)
      write(nmt)(((xsnl(ix,ie2,il), ix = 1,nxsn),ie2 = 1,nume2),
     x           il = 1,nl)
      backspace (nmt)
      backspace (nmt)
      backspace (nmt)
      backspace (nmt)
*
      return
      end
      subroutine wtcas (lrgs,lrgl,lrgpi,ncases)
*
* $Id: wtcas.f,v 2.1 94/09/29 18:40:05 vmb Exp Locker: vmb $
*
*
*     print list of cases whose data has not been found
*
      dimension lrgs(ncases),lrgl(ncases),lrgpi(ncases)
*
      common /io/ iin,iout
*
      do 10  i=1,ncases
         if (lrgl(i) .ge. 0) then
            write (iout,1010) lrgs(i),lrgl(i),lrgpi(i)
         end if
 10   continue
      return
 1010 format ('NO DATA FOUND FOR 2S + 1 =',i3,'  L =',i4,'  PI =',i2)
*
      end
      subroutine xsnind(ntarg,nxsn,ion,iind,jind)
*
* $Id: xsnind.f,v 2.1 94/09/29 18:40:57 vmb Exp Locker: vmb $
*
      implicit integer (a-z)
      dimension iind(nxsn),jind(nxsn)
*
*  calculate initial and final target index for each x-section
*
      ij = 0   
      do 11 i = 1,ntarg
         jup = i
         if (ion .gt. 0) jup = i-1
         do 11 j = 1,jup
            ij = ij + 1
            iind(ij) = i
            jind(ij) = j
 11   continue
      return
      end
