      subroutine ampltd (ra1,ra2,hmlt,ampa1,ampa2,eigen,eigo,vecto,
     x                   wrk,lbug,nchan,nhsize,nhd,nleg)
*
* $Id: ampltd.f,v 2.1 94/09/29 18:44:27 vmb Exp Locker: vmb $
*
      implicit real*8 (a-h,o-z)
      parameter ( zero=0.0d0,one=1.0d0 )
      dimension ampa1(nchan,nhd),ampa2(nchan,nhd),eigen(nhd),wrk(3*nhd),
     x          hmlt(nhsize),eigo(nhd),vecto(nhd,nhd)
      common /io/ iin,iout
*
*      ampltd : diagonalizes hamiltonian on subrange, stores eigenvalues
*               uses eigenvectors to compute surface amplitudes
*
*               ra1,ra2 = radii of end points of current subrange
*               hmlt    = hamiltonian ( lower triangle )
*               lbug    = debug switch
*               eigo, vecto, wrk : work space
*               ampa1,ampa2 = surface amplitudes
*               eigen       = eigenvalues of hamiltonian
*
      if ( lbug .gt. 0 ) write (iout,1000)
      ra12 = ra2 - ra1
      call DSPEV ( 'V', 'U', nhd, hmlt, eigo, vecto, nhd, wrk, ierr )
      if ( ierr .ne. 0 ) then
         write (iout,1040) ierr
         stop
      endif
      if ( lbug .gt. 0 ) then
         write (iout,1010) (eigo(neig),neig = 1,nhd)
         if ( lbug .ge. 2 ) then
            do 10 k = 1, nhd
               write (iout,1020) k
               write (iout,1010) (vecto(nvec,k),nvec = 1,nhd)
 10         continue
         endif
      endif
      do 50 k = 1, nhd
         eigen(k) = eigo(k)
         do 20 i = 1, nchan
            ampa1(i,k) = zero
            ampa2(i,k) = zero
   20    continue
         ik = 0
         sign = - one
*      sum over index on legendre polynomials
         do 40 j = 1, nleg
            rootf = sqrt( ( dfloat(2*j -1) ) / ra12 )
            sign = - sign
            roots = sign * rootf
            do 30 i = 1, nchan
               ik = ik + 1
               ampa1(i,k) = ampa1(i,k) + vecto(ik,k) * roots
               ampa2(i,k) = ampa2(i,k) + vecto(ik,k) * rootf
   30       continue
   40    continue
*      output of surface amplitudes
         if ( lbug .ge. 2 ) then
            write (iout,1030) ra1
            write (iout,1010) (ampa1(i,k),i = 1,nchan)
            write (iout,1030) ra2
            write (iout,1010) (ampa2(i,k),i = 1,nchan)
         endif
   50 continue
      return
*
 1000 format (1h1,/,52x,' subroutine ampltd',/,53x,17(' '),
     x        /,5x,'eigenvalues in rydbergs',/)
 1010 format (1x,8f14.7)
 1020 format (/,5x,'eigenvector',i5,/)
 1030 format (/,5x,'amplitude at =',f10.5,3x,'a.u.',/)
 1040 format (' ampltd : error return from dspev, ierr =',i4)
      end
      subroutine jnvert (ndim,nf,b,c,d,a,dd,nbb,nff)
*
* $Id: jnvert.f,v 2.1 94/09/29 18:44:30 vmb Exp Locker: vmb $
*
      implicit real*8 (a-h,o-z)
      parameter ( one=1.0d0 )
      dimension b(nbb),a(nff),d(nff),c(ndim,nf),dd(ndim)
*
*     jnvert : evaluates d = a - c(transpose) * b(inverse) * c
*              cholesky type factorization,
*              nesbet, j. comp. phys 8 (1971) 483
*              lower triangles of a, b, d stored row-wise
*
*     replace b with triangular factorization, nesbet eqns 12, 13
*
      kij = 0
      do 70 i = 1, ndim
         mm = i - 1
         ii = kij
         if ( i .eq. 1 ) go to 40
         jj = 0
         do 30 j = 1, mm
            q = b(kij+j)
            if ( j .eq. 1 ) go to 20
            nn = j - 1
            do 10 nu = 1, nn
               q = q - b(ii+nu) * dd(nu) * b(jj+nu)
   10       continue
   20       jj = jj + j
            b(kij+j) = q / ( b(jj) * dd(j) )
   30    continue
   40    kij = kij + i
         q = b(kij)
         if ( i .eq. 1 ) go to 60
         do 50 nu = 1, mm
            q = q - b(ii+nu) * dd(nu) * b(ii+nu)
   50    continue
   60    dd(i) = sign(one,q)
         b(kij) = sqrt(abs(q))
   70 continue
*
*     replace c by matrix defined by nesbet eqn 14
*
      do 90 ich = 1, nf
         c(1,ich) = c(1,ich) / b(1)
         if ( ndim .eq. 1 ) go to 100
         kij = 1
         do 90 i = 2, ndim
            q = c(i,ich)
            i1 = i - 1
            do 80 k = 1, i1
               q = q - c(k,ich) * b(kij+k)
   80       continue
            kij = kij + i
            c(i,ich) =  q / b(kij)
   90 continue
*
*     set up final matrix in d using nesbet eqn 11
*
  100 kka = 0
      do 130 ich = 1, nf
         do 120 jch = 1, ich
            q = a(kka+jch)
            do 110 k = 1, ndim
               q = q - c(k,ich) * dd(k) * c(k,jch)
  110       continue
            d(kka+jch) = q
  120    continue
         kka = kka + ich
  130 continue
      return
      end
      subroutine legndr (pl,xi,nleg,ixmax)
*
* $Id: legndr.f,v 2.1 94/09/29 18:44:32 vmb Exp Locker: vmb $
*
      implicit real*8 (a-h,o-z)
      parameter ( half=0.5d0,one=1.0d0,onep5=1.5d0,two=2.d0 )
      dimension pl(nleg,ixmax),xi(ixmax)
*
*     legndr : normalized legendre polynomials at quadrature points
*              nleg  = maximum number of legendre polynomials in basis
*              ixmax = number of abscissae
*              xi    = quadrature abscissae
*              pl    = polynomials
*
      nleg1 = nleg-1
      do 20 i = 1, ixmax
         x = xi(i)
         pl(1,i) = sqrt(half)
         pl(2,i) = sqrt(onep5) * x
*
*      generate polynomials to order nleg-1 by recursion
*
         do 10 k = 2, nleg1
            k1 = k - 1
            pl(k+1,i) = ( sqrt(two*k1+one) * x * pl(k,i) - k1 * pl(k1,i)
     x                  / sqrt(two*k1-one) ) * sqrt(two*k+one) / 
     x                  dfloat(k)
   10    continue
   20 continue
      return
      end
      subroutine mesh (asect,nrange,ixmax,xi,npts,r)
*
* $Id: mesh.f,v 2.1 94/09/29 18:44:33 vmb Exp Locker: vmb $
*
      implicit real*8 (a-h,o-z)
      parameter ( half=0.5d0,two=2.d0 )
      dimension xi(ixmax),r(npts),asect(nrange+1)
*
*     mesh : mesh at which potentials are required
*            asect   = radius at end points of each subrange
*            nrange  = number of subranges
*            ixmax   = number of abscissae in quadrature scheme
*            xi      = abscissae
*            npts    = number of points in mesh
*            r(npts) = radii
*
      ra2 = asect(1)
      i1 = - ixmax
*
      do 20 nrang = 1, nrange
         ra1 = ra2
         ra2 = asect (nrang + 1)
         dr2 = half * (ra2 - ra1)
         rmid = ra1 + dr2
         i1 = i1 + ixmax
         i2 = i1 + 2 * ixmax + 1
         do 10 ix = 1, ixmax
            i1 = i1 + 1
            i2 = i2 - 1
            r(i1) = rmid - dr2 * xi(ix)
            r(i2) = rmid + dr2 * xi(ix)
   10    continue
   20 continue
      return
      end
      subroutine potl (nchan,ispch,nbasis,r,v,nmx,lamax,ion,lchl,cf)
*
* $Id: potl.f,v 2.1 94/09/29 18:44:35 vmb Exp Locker: vmb $
*
      implicit real*8 (a-h,o-z)
      parameter ( zero=0.0d0,one=1.0d0,two=2.0d0 )
      dimension r(nbasis),v(nchan,nchan,nbasis),cf(nmx,nmx,lamax),
     x          lchl(nmx),ispch(nmx)
*
*     potl : passes potential to r-matrix propagator routine rprop.
*            potentials are expanded in inverse powers of radial
*            distance ; expansion coefficients in cf
*
      do 40 ichan = 1, nchan
         i = ispch(ichan)
         el2 = dfloat(lchl(i) * ( lchl(i) + 1 ))
         do 30 jchan = 1, nchan
            j = ispch(jchan)
            do 20 ir = 1, nbasis
               vp = zero
               rr = one / r(ir)
               if ( i .eq. j ) vp = - two * dfloat(ion) * rr + el2 * rr
     x                              * rr
               do 10 k = 1, lamax
                  vp = vp + cf(i,j,k) * rr ** (k+1)
   10          continue
               v(ichan,jchan,ir) = vp
   20       continue
   30    continue
   40 continue
      return
      end
      subroutine prop (etotr,ns,ampa11,ampa12,eigen1,nch1,nhd1,
     x                 ampa21,ampa22,eigen2,nch2,nhd2,cr)
*
* $Id: prop.f,v 2.1 94/09/29 18:44:41 vmb Exp Locker: vmb $
*
      implicit real*8 (a-h,o-z)
      parameter ( zero=0.0d0,one=1.d0 )
      dimension  cr(*),ampa11(nch1,nhd1),ampa12(nch1,nhd1),eigen1(nhd1),
     x           ampa21(nch2,nhd2),ampa22(nch2,nhd2),eigen2(nhd2) 
*
*      prop : propagates r-matrix across one sub-range
*
*             etotr       = impact energy
*             ampa1,ampa2 = surface amplitudes for current subrange
*             eigen       = eigenvalues of subrange hamiltonian
*             cr          = on entry the lower triangle of r-multiplied
*                           r-matrix, on exit the propagated matrix
*
      nchan = nch1 + nch2
      nchan1 = nchan * ( nchan + 1 ) / 2
      nchan2 = nchan1 + 1
      nchan3 = nchan1 + nchan * nchan
      nchan4 = nchan3 + 1
      nchan5 = nchan3 + nchan1
      nchan6 = nchan5 + 1
*     curly r-matrices stored in cr, r-multiplied r-matrix already in
*     first nchan1 locations, zero rest
*
      do 10 k = nchan2, nchan5
         cr(k) = zero
   10 continue
      do 40 kl = 1, nhd1
         edenom = one / ( eigen1(kl) - etotr )
         k = 0
         do 20 i = 1, nch1
            fac1 = ampa11(i,kl) * edenom
            fac2 = ampa12(i,kl) * edenom
            do 20 j = 1, i
               k = k + 1
               cr(k) = cr(k) + fac1 * ampa11(j,kl)
               cr(k+nchan3) = cr(k+nchan3) + fac2 * ampa12(j,kl)
   20    continue
*
*     non-symmetric matrix r12
*
         k = nchan1 - nch2
         do 30 j = 1, nch1
            fac1 = edenom * ampa12(j,kl)
            k = k + nch2
            do 30 i = 1, nch1
               k = k + 1
               cr(k) = cr(k) + ampa11(i,kl) * fac1
   30    continue
   40 continue
*
      if (ns .eq. 2) then
         ks1 = (nch1 * (nch1 + 1))/2 
         ks12 = nch1 * nchan 
         do 70 kl = 1, nhd2
            edenom = one / ( eigen2(kl) - etotr )
            k = ks1
            do 50 i = 1, nch2
               fac1 = ampa21(i,kl) * edenom
               fac2 = ampa22(i,kl) * edenom
               k = k + nch1
               do 50 j = 1, i
                  k = k + 1
                  cr(k) = cr(k) + fac1 * ampa21(j,kl)
                  cr(k+nchan3) = cr(k+nchan3) + fac2 * ampa22(j,kl)
   50       continue
*
*     non-symmetric matrix r12
*
            k = nchan1 + ks12
            do 60 j = 1, nch2
               fac1 = edenom * ampa22(j,kl)
               k = k + nch1
               do 60 i = 1, nch2
                  k = k + 1
                  cr(k) = cr(k) + ampa21(i,kl) * fac1
   60       continue
   70    continue
      end if
*     
*     lower triangle of new r-multiplied r-matrix
*
      call jnvert (nchan,nchan,cr(1),cr(nchan2),cr(1),cr(nchan4),
     x             cr(nchan6),nchan2,nchan2)
      return
      end
      subroutine prppnt(rmatr,rlast,nschan,ebig,
     x                  ion,nranmx,idisc,idbbw,nampx,x,ix,
     x                  mleg,ixmx,delt,pt0,mcor1)
*
* $Id: prppnt.f,v 2.1 94/09/29 18:44:43 vmb Exp Locker: vmb $
*
      implicit integer (a-z)
      real*8 x,delta,delt,ebig,rmatr,rlast
      parameter ( nleg=10,delta=6.0d0 )
      parameter ( ixmax=5 )
      common /pptr/ ampa,eigen,bigvc,work
      dimension  x(*),ix(*),nschan(2),idisc(2)
      common /io/ iin, iout
*
      last = pt0
*
*   **** parameters nleg and ixmax should be set to the same values
*   **** in spropi
*   output parameters to cntrl for printing and opening files
*
      mleg = nleg
      ixmx = ixmax
      delt = delta
*
*   calculate nranmx and sector ends and modify rlast to lie on the
*   end of the last sector
*
      asect = 1
      call sect (ebig,ion,rmatr,rlast,delta,nranmx,x(asect))
      lasect =  nranmx + 1
      last = pt0 + lasect
*
*  assign space only if there is to be propagation using the BBM method 
*
      if (nranmx .gt. 0) then
         nch1 = nschan(1)
         nch2 = nschan(2)
         nmax = max (nch1,nch2)
         nhd = nmax * nleg
         nbigvc = ( nhd * ( nhd + 1 ) ) / 2
         nwork = nhd*nhd + 4 * nhd
         lpr = lasect + 2 + nbigvc + nwork + nhd
         nch1s = nch1 * nch1
         nch2s = nch2 * nch2
         nmx2 = nch1s + nch2s
         nmx = nch1 + nch2
         nampx = 4 * ixmax * nmx2 * nranmx
         neigen = 2 * ixmax * nmx * nranmx
         lreqd = lpr + nampx + neigen
*
*  calculation cannot be done in core use disc files
*
         if ( lreqd .gt. mcor1 .or. idbbw .gt. 0) then
            if ( idisc(1) .eq. 0 .or. idisc(2) .eq. 0 ) then
               write (iout,3010) lreqd,mcor1
               stop
            endif
            nampx = 4 * ixmax * nmx2 
            neigen = 2 * ixmax * nmx 
            lreqd = lpr + nampx + neigen
*
*  stop if not enough core even when using disc files
*
            if ( lreqd .gt. mcor1 ) then
               write (iout,3020) lreqd,mcor1
               stop
            endif
         endif
         eigen = last
         ampa = eigen + neigen
         bigvc = ampa + nampx
         work = bigvc + nbigvc
         last = work + nwork + 1
      end if
      pt0 = last
*
 3010 format (' storage overflow (prppnt) : needed =',i10,
     x        ' available =',i10,/,' disk required ')
 3020 format (' storage overflow (prppnt) : needed =',i10,
     x        ' available =',i10)
       return
       end
      subroutine rprop2 (rmat,etotr,nschan,nbasis,ampa,eigen,bigvec,
     x                   nrange,nampx,lbug,nran1,nran2,asect,irev)
*
* $Id: rprop2.f,v 2.1 94/09/29 18:44:44 vmb Exp Locker: vmb $
*
      implicit integer (a-z)
      real*8 rmat,etotr,ampa,eigen,bigvec,eps,a1,a2,ra2,dfloat,asect
      parameter ( eps=1.d-10 )
      common /io/ iin,iout
      common /sp/ idw,ifa,nsampr(2),inpr,nshd(2),nsamp(2),isdisc(2),
     x            mlegp
      dimension rmat(*),nbasis(*),ampa(*),eigen(*),bigvec(*),
     x          asect(*),nschan(2),nramp(2),nreig(2),isnhd(2),
     x          isnamp(2)
*    propagates rmat where the channels are rearranged according to two
*    possible spins.    
*
      nch1 = nschan(1)
      nch2 = nschan(2) 
      ns = 1
      if (nch2 .ne. 0) ns = 2
      mleg = mlegp
      nchan = nch1 + nch2
      if ( lbug .ne. 0 ) write (iout,1060)
      if ( nrange .eq. 0 ) return
      if (irev .eq. 0) then
         nrang1 = min (nran1,nran2)
         nrang2 = max (nran1,nran2)
         jsign = 1
         a1 = asect (nrang1)
         a2 = asect (nrang2+1)
      else
         nrang1 = max (nran1,nran2)
         nrang2 = min (nran1,nran2)
         jsign = -1
         a1 = dfloat (jsign) * asect (nrang1 +1)
         a2 = dfloat (jsign) * asect (nrang2) 
      end if
      if ( idw .eq. 0 ) then
         do 10 is = 1,ns
            nramp(is) = nsamp(is) * ( nrang1 - 1 ) + 1
            nreig(is) = nshd(is) * ( nrang1 - 1 ) + 1
 10      continue
         nramp(2) = nramp(2) + nsampr(1)*2
         nreig(2) = nreig(2) + nshd(1)*nrange
      else
         nramp(1) = 1
         nreig(1) = 1
         nramp(2) = 1 + nsampr(1) * 2
         nreig(2) = 1 + nshd(1)
      endif
*
*     store lower triangle of r-multiplied r-matrix
*
      kmax = ( nchan * ( nchan + 1 ) ) / 2
      do 170 k = 1, kmax
         bigvec(k) = a1 * rmat(k)
  170 continue
*      nhd = mleg * nchan
*      ihd = nhd
*
*     propagate across subranges
*
      do 290 jr = nrang1, nrang2, jsign
         i1 = 1
         j1 = 1
         do 20 is = 1,ns
            isnhd(is) = nbasis(jr) * nschan(is)
            isnamp(is) = 2 *isnhd(is) * nschan(is)
            i2 = i1 + isnhd(is) -1
            j2 = j1 + isnamp(is) -1
            if ( idw .ne. 0 ) then
               read (isdisc(is),rec=jr) (ampa(j),j = j1,j2),
     x                   (eigen(i),i = i1,i2)
               i1 = i2 + 1
               j1 = j2 + 1
            endif
*
            ieg =nreig(is) - 1
            do 240 neig = 1,isnhd(is)
               ieg = ieg + 1
               if ( abs(eigen(ieg)-etotr) .lt. eps ) then
                  write (iout,1050) etotr,eigen(ieg)
                  stop
               endif
  240       continue
 20      continue
*
         if ( irev .eq. 0 ) then
            call prop (etotr,ns,ampa(nramp(1)),ampa(nramp(1)+nsampr(1)),
     x              eigen(nreig(1)),nch1,isnhd(1),ampa(nramp(2)),
     x              ampa(nramp(2)+nsampr(2)),eigen(nreig(2)),nch2,
     x              isnhd(2),bigvec)
            ra2 = asect (jr+1)
         else
            call prop (etotr,ns,ampa(nramp(1)+nsampr(1)),ampa(nramp(1)),
     x           eigen(nreig(1)),nch1,isnhd(1),ampa(nramp(2)+nsampr(2)),
     x           ampa(nramp(2)),eigen(nreig(2)),nch2,isnhd(2),bigvec)
            ra2 = asect (jr)
         end if
         if ( lbug .ne. 0 ) then
            write (iout,1070) ra2
            k = 0
            do 250 j = 1, nchan
               write (iout,1080) (bigvec(k+i),i = 1,j)
               k = k + j
  250       continue
         endif
         if ( idw .eq. 0 ) then
            do 30 is = 1,ns
               nramp(is) = nramp(is) + nsamp(is) * jsign
               nreig(is) = nreig(is) + nshd(is) * jsign
 30         continue
         endif
  290 continue
      do 320 k = 1, kmax
         rmat(k) = bigvec(k) / a2
  320 continue
      return
*
 1050 format (/,' energy',f14.7,' too close to eigenvalue',f14.7)
 1060 format (1h1,/,52x,' subroutine rprop2',/,52x,/)
 1070 format (/,5x,'new  r-matrix at',f10.5,3x,'a.u.',/)
 1080 format (8f14.7)
      end
      subroutine sect(ebig,ion,rmatr,rlast,delt,nrange,asect)
*
* $Id: sect.f,v 2.1 94/09/29 18:44:28 vmb Exp Locker: vmb $
*
      implicit integer (a - z)
      real*8 ebig,rmatr,rlast,delt,asect,delg,dela,k,a,ka,
     x     iok,x,one,oneh,dfloat
*
*   calculate end points of sectors for propagation and store
*   in asect.   each sector spans between two and three loops
*   of the wavefunction.
*
      dimension asect(*)
      parameter (one=1.0d0, oneh=1.5d0)
*
      k = sqrt(ebig)
      a = rmatr
      asect(1) = rmatr
      if (rlast .eq. rmatr) then
         nrange = 0
         return
      end if
      if (ion .gt. 0) then
         n = 1
 10      ka = k * a
         iok = dfloat (ion) / (k * ka)
         x = delt / ka
 20      delg = ka * (x + iok * log (one + x)) / delt
         if (delg .gt. oneh) then
            x = x / delg
            go to 20
         else
            n = n + 1
            a = (one + x) * a
            asect (n) = a
         end if 
         if (a .lt. rlast) go to 10
         nrange = n-1
      else
         dela = delt / k
         nrange = int( (rlast-rmatr) / dela ) + 1
         do 30 i = 2,nrange + 1
            asect(i) = asect(i - 1) + dela
 30      continue
      end if
      rlast = asect(nrange + 1)
      return
      end     
      subroutine setmtr (ra1,ra2,hmlt,wts,pl,v,ethr,bbloch,lbug,
     x                   nchan,ispch,ixmax,mleg,nhsize,nleg)
*
* $Id: setmtr.f,v 2.1 94/09/29 18:44:46 vmb Exp Locker: vmb $
*
      implicit real*8 (a-h,o-z)
      parameter ( zero=0.0d0,one=1.0d0,two=2.d0 )
      dimension ethr(*),ispch(nchan),v(nchan,nchan,*),wts(ixmax),
     x          pl(mleg,ixmax),hmlt(nhsize)
      common /io/ iin,iout
*
*     setmtr : hamiltonian matrix on a subrange
*              ra1,ra2  = end points of subrange
*              xi,wts   = quadrature abscissae and weights
*              pl       = normalized legendre polynomials at abscissae
*              v        = potential matrix evaluated at the abscissae
*              ethr     = threshold energies
*              bbloch   = parameter in bloch operator
*              lbug     = debug switch
*              hmlt     = lower triangle of hamiltonian matrix
*
      if ( lbug .eq. 1 ) write (iout,1000)
      nhs = 0
      ixmax2 = 2 * ixmax + 1
      r12 = one / ( ra2 - ra1 )
      r1b = zero
      r2b = zero
      if ( bbloch .ne. zero ) then
         r1b = r12 * bbloch / ra1
         r2b = r12 * bbloch / ra2
      endif
      rsq = two * r12 * r12
      do 50 nn = 1, nleg
        do 40 i = 1, nchan
            do 30 mm = 1, nn
               jmn = mod(mm+nn,2) + 1
               sgn = (-1) ** jmn
*     matrix elements not involving the potential ( closed form )
               rootmn = sqrt( ( two * dfloat(nn) - one ) * ( two *
     x                  dfloat(mm) - one ) )
               diag = - rootmn * ( r2b + sgn * r1b )
               if ( nn .eq. mm ) diag = diag + ethr(ispch(i))
               if ( jmn .eq. 1 ) diag = diag + rsq * rootmn *
c     x                                  dfloat(nn*(nn-1))
     x                                  dfloat(mm*(mm-1))
               jm = nchan
               if ( mm .eq. nn ) jm = i
               do 20 j = 1, jm
                  sum1 = zero
                  if ( i .eq. j ) sum1 = diag
*     matrix elements of potential using gauss-legendre quadrature.
                  do 10 ix = 1, ixmax
                     sum1 = sum1 + wts(ix) * pl(mm,ix) * pl(nn,ix) *
     x                      ( v(i,j,ixmax2-ix) - sgn * v(i,j,ix) )
   10             continue
*     store result in hmlt
                  nhs = nhs + 1
                  hmlt(nhs) = sum1
   20          continue
   30       continue
   40    continue
   50 continue
      if ( lbug .ne. 0 ) then
         write (iout,1010) nhsize
         write (iout,1020)
         write (iout,1030) (hmlt(nv),nv = 1,nhsize)
      endif
      return
*
 1000 format ('1',/,52x,' subroutine setmtr',/)
 1010 format (/,5x,' hamiltonian array size=',i5,//)
 1020 format (/,5x,' hamiltonian matrix elements',//)
 1030 format (1x,8f14.7)
      end
      subroutine sprop  (ethr,nmx,nchan,is,ispch,nrange,nbasis,asect,
     x                   bbloch,lbug,idisk,ampa,eigen,bigvec,
     x                   work,ipr,lamax,ion,lchl,cf,mleg,ixmax,
     x                   pl,xi,wts)
*
* $Id: sprop.f,v 2.1 94/09/29 18:44:48 vmb Exp Locker: vmb $
*
      implicit integer (a-z)
      real*8 ethr,asect,bbloch,ampa,eigen,bigvec,work,cf,
     x       pl,ra1,ra2,xi,wts
      dimension ethr(nmx),nbasis(nrange),ampa(*),
     x          eigen(*),xi(ixmax),wts(ixmax),pl(mleg,ixmax),
     x          bigvec(*),lbug(3),work(*),lchl(nmx),cf(nmx,nmx,lamax),
     x          asect(*),ispch(*)
      common /io/ iin,iout
      common /sp/ idw,ifa,nsampr(2),inpr,nshd(2),nsamp(2),isdisc(2),
     x            mlegp
*
      isdisc(is)  = idisk
      mlegp = mleg
      ifa = 0
      iprr = abs(ipr)
      if ( iprr .eq. 2 ) ifa = sign(1,ipr)
*     potl data
      mhd = 2 * nchan * ixmax
      nshd(is) = mhd
      mamp = nchan * mhd
      nsamp(is) = mamp
      nampr = mamp * nrange
      nsampr(is) = nampr
      if (idisk .ne. 0) then
         nampr = mamp
         nsampr(is) = nampr
         idw = 1
      endif
      ra2 = asect(1)
      nramp = 1
      nreig = 1
*     loop over subranges
      do 100 nrang = 1, nrange
         ra1 = ra2
         ra2 = asect(nrang+1)
         i1 = nramp 
         i2 = nramp + nampr
         nleg = nbasis(nrang)
         nhd = nchan * nleg
         nhsize = nhd * ( nhd + 1 ) / 2
         namp = 2 * nchan * nhd
*
*     potential matrix including centrifugal term. mesh is stored
*     in eigen, potentials in ampa. potential is set up for entire range
*     (a,b) on the first pass unless a scratch disk used to store
*     amplitudes. potential then set up separately for each subrange.
*
         if ( idw .ne. 0 ) then
            npts = 2 * ixmax
            call mesh (asect(nrang),1,ixmax,xi,npts,eigen)
            if ( lbug(1) .ne. 0 ) write (iout,1040)
     x                            (eigen(i),i = 1,npts)
            call potl (nchan,ispch,npts,eigen,ampa,nmx,lamax,ion,
     x                 lchl,cf)
         else
            if ( nrang .eq. 1 ) then
               npts = 2 * ixmax * nrange
***** nmesh should only be called for first spin if keep it in eigen2
               call mesh (asect,nrange,ixmax,xi,npts,eigen)
               if ( lbug(1) .ne. 0 ) write (iout,1040)
     x                               (eigen(i),i = 1,npts)
               call potl (nchan,ispch,npts,eigen,ampa,nmx,lamax,ion,
     x                    lchl,cf)
            endif
         endif
*
*     matrix elements of hamiltonian on subrange (ra1,ra2) in bigvec
*     diagonalize subrange hamiltonian
*
         call setmtr (ra1,ra2,bigvec,wts,pl,ampa(nramp),ethr,bbloch,
     x                lbug(2),nchan,ispch,ixmax,mleg,nhsize,nleg)
         call ampltd (ra1,ra2,bigvec,ampa(i1),ampa(i2),eigen(nreig),
     x                work,work(nhd+1),work(nhd*nhd+nhd+1),
     x                lbug(3),nchan,nhsize,nhd,nleg)
         if ( idw .eq. 0 ) then
            nramp = nramp + mamp
            nreig = nreig + mhd
         else
            write (idisk,rec=nrang) (ampa(i),i = 1,namp),
     x            (eigen(i),i = 1,nhd)
         endif
  100 continue
      return
*
* 1030 format (/,' no propagation required for a=',f14.7,3x,'b=',f14.7,
*     x        3x,'ebig=',f14.7,/)
 1040 format (' potential matrix is evaluated at the following radii',
     x        /,(12f10.4))
      end
      subroutine spropi (ethr,nmx,nschan,ispch,nrange,nbasis,asect,
     x                   bbloch,lbug,idisk,ipr,lamax,ion,lchl,
     x                   cf,nampx,x)
*
* $Id: spropi.f,v 2.1 94/09/29 18:44:50 vmb Exp Locker: vmb $
*
      implicit integer (a-z)
      real*8 ethr,bbloch,cf,x,asect,xi,wts,pl
      parameter ( mleg=10,ixmax=5 )
      dimension ethr(nmx),nbasis(nrange),lbug(*),lchl(nmx),
     x          nschan(2),ispch(nmx),idisk(2),
     x          cf(nmx,nmx,lamax),x(*),asect(nrange+1),
     x          xi(ixmax),wts(ixmax),pl(mleg,ixmax)
      common /pptr/ ampa,eigen,bigvec,work
      common /sp/ idw,ifa,nsampr(2),inpr,nshd(2),nsamp(2),isdisc(2),
     x            mlegp
*
      common /io/ iin,iout
*
*     gauss-legendre quadrature abscissae and weights
*
      data xi(1)/0.973906528517172d0/,wts(1)/0.066671344308688d0/
      data xi(2)/0.865063366688985d0/,wts(2)/0.149451349150581d0/
      data xi(3)/0.679409568299024d0/,wts(3)/0.219086362515982d0/
      data xi(4)/0.433395394129247d0/,wts(4)/0.269266719309996d0/
      data xi(5)/0.148874338981631d0/,wts(5)/0.295524224714753d0/
*
      ifa = 0
      inpr = 0
      iprr = abs(ipr)
      if ( iprr .eq. 2 ) ifa = sign(1,ipr)
      nleg = 0
      do 20 ir = 1, nrange
         if ( nbasis(ir) .le. 0 ) nbasis(ir) = mleg
         if ( nbasis(ir) .gt. mleg ) nbasis(ir) = mleg
         if ( nbasis(ir) .gt. nleg ) nleg = nbasis(ir)
   20 continue
      if ( lbug(1) .ne. 0 ) then
         if ( inpr .eq. 0 ) then
            write (iout,1010) nleg
         else
            write (iout,1010) (nbasis(ir),ir = 1,nrange)
         endif
      endif
*    evaluate normalized legendre polynomials pl(x) at quadrature points
      call legndr (pl,xi,mleg,ixmax)
      nch1 = nschan(1)
      nch2 = nschan(2)
      nmax = max (nch1,nch2)
      mindim = 2 * nmax * nmax * max(nleg,ixmax)
      nampr = 2 * (nch1*nch1 + nch2*nch2) * ixmax  * nrange
*      check storage space for surface amplitudes and if disk available
      if ( idw .eq. 1  .or.  2*nampr .gt. nampx ) then
         if ( idisk(1) .eq. 0 .or. idisk(2) .eq. 0
     x       .or. mindim .gt. nampx ) then
            write (iout,1020) nmx,nleg,nrange
            stop
         endif
         idw = 1
      endif
      idisc = 0
      if (idw .eq. 1) idisc = idisk(1)
      is = 1
      call sprop (ethr,nmx,nch1,is,ispch,nrange,nbasis,asect,bbloch,
     x            lbug,idisc,
     x            x(ampa),x(eigen),x(bigvec),x(work),
     x            ipr,lamax,ion,lchl,cf,mleg,ixmax,pl,xi,wts)
      if (nch2 .gt. 0) then
         if (idw .eq. 0) then
            nhd1x = 2 * ixmax * nch1 * nrange
            ampas2 = ampa + 2*nch1*nhd1x
            eigen2 = eigen + nhd1x
         else
            idisc = idisk(2)
            ampas2 = ampa
            eigen2 = eigen
         end if
         is = 2
         call sprop (ethr,nmx,nch2,is,ispch(nch1+1),nrange,nbasis,asect,
     x               bbloch,lbug,idisc,x(ampas2),x(eigen2),
     x               x(bigvec),x(work),ipr,lamax,ion,lchl,cf,
     x               mleg,ixmax,pl,xi,wts)
      end if
      return
 1010 format (' number of basis functions included in each subrange is',
     x        /,(20i5),/) 
 1020 format (' insufficient space in ampa and eigen for nchan=',i3,
     x        3x,'nleg=',i2,3x,'nrange=',i3,/)
      end
