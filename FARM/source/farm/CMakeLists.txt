cmake_minimum_required(VERSION 3.16)
project(FARM Fortran)

enable_language(C)
find_package(BLAS REQUIRED)
find_package(LAPACK REQUIRED)

set(
    source

    asy.f
    gal.f
    lwp.f
    phv.f
    rmp.f
    ser.f
)

add_executable(farm ${source})
target_link_libraries(farm ${BLAS_LIBRARIES} ${LAPACK_LIBRARIES})
