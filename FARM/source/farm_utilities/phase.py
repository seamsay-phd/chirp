import itertools
import logging
import os
import re
import sys
import typing

import numpy as np
import scipy as sp

try:
    from itertools import batched
except ImportError:

    def batched(iterable, n):
        if n < 1:
            raise ValueError("n must be at least one")
        it = iter(iterable)
        while batch := tuple(itertools.islice(it, n)):
            yield batch


LOG_LEVEL_MAP = {
    "ALL": logging.NOTSET,
    "DEBUG": logging.DEBUG,
    "INFO": logging.INFO,
    "WARN": logging.WARN,
    "ERROR": logging.ERROR,
    "CRITICAL": logging.CRITICAL,
    "DISABLE": logging.CRITICAL + 1,
}

logging.basicConfig(
    level=LOG_LEVEL_MAP[os.environ.get("LOG_LEVEL", "INFO").upper()],
    format="{asctime} {levelname:>8s} {message}",
    style="{",
    stream=sys.stderr,
)

F77_DOUBLE_RGX = re.compile(r"^(-?\d+\.\d+)[dD]([-+]?\d+)$")
energy_Ryd_per_eV = 0.0734985857


def parse_f77_double(string):
    match = F77_DOUBLE_RGX.match(string)
    if match is None:
        # Maybe it's a valid float, otherwise it's an easy way to get a useful error message!
        return float(string)

    mantissa = float(match.group(1))
    exponent = float(match.group(2))

    return mantissa * 10**exponent


def read_phase_data(file, ionisation_energy_eV=0.0):
    """Read data from a FARM CPCPHASEF file, converting values to eV.

    Parameters
    ----------
    file : str
        The path to the CPCPHASEF file.
    ionisation_energy_eV : float, optional (default 0.0)
        The ionisation energy of the system.
        The energies that FARM outputs are relative to this value, but if provided they will be shifted back to their "true" values.
    """
    with open(file) as io:
        io.readline()
        assert io.readline().strip() == "table of eigenphases"
        assert io.readline().strip() == ""
        assert [h.strip() for h in io.readline().split(maxsplit=4)] == [
            "2s+1",
            "L",
            "Pi",
            "energy",
            "phase sum",
        ]

        energies = []
        phases = []
        for line in io.readlines():
            _, _, _, energy_str, phase_str = (f.strip() for f in line.split())
            energies.append(
                ionisation_energy_eV + parse_f77_double(energy_str) / energy_Ryd_per_eV
            )
            phases.append(parse_f77_double(phase_str))

    return np.array(energies), np.array(phases)


class LorentzianParameters(typing.NamedTuple):
    centre: float
    strength: float
    width: float


class Lorentzian(LorentzianParameters):
    def lineshape(self, E):
        E0 = self.centre
        z = self.strength
        w = self.width
        return z / (1 + ((E - E0) / w) ** 2)


def derivative(energy, phase):
    d_energy = (energy[1:] + energy[:-1]) / 2
    d_phase = np.diff(phase) / np.diff(energy)
    return d_energy, d_phase


def fit_peaks_individually(energy, phase):
    """Find resonances by fitting peaks to the derivative of the phase data.

    Parameters
    ----------
    energy : array_like
        The energy data from the phase file (see `read_phase_data`).
    phase : array_like
        The phase data from the phase file (see `read_phase_data`).
    """
    d_energy, d_phase = derivative(energy, phase)

    tolerance = 0.1

    peaks = []

    curr_phase = phase[0]
    prev_step_end = 0
    i = 0

    while i < len(d_phase) and abs(phase[i] - curr_phase) < tolerance:
        i += 1

    curr_step_start = i

    # We find the resonances by using the expected pi shifts to identify the steps in the output that are characteristic of a resonance.
    # We fit against the derivative of the phase, which should give Lorentzian peaks, as it's generally easier to fit peaks than steps.
    # We use the previous step's end and next step's start to limit the curve fitting to a single peak while still having as much data as possible, which makes the fit better.
    while i < len(d_phase):
        next_phase = curr_phase + np.pi
        while i < len(d_phase) and abs(phase[i] - next_phase) > tolerance:
            i += 1

        curr_step_end = i - 1
        curr_phase = next_phase
        while i < len(d_phase) and abs(phase[i] - curr_phase) < tolerance:
            i += 1

        next_step_start = i

        x = d_energy[prev_step_end:next_step_start]
        y = d_phase[prev_step_end:next_step_start]

        # Scale values to be between close to order 1, to improve fit performance.
        # TODO: Is there a better scaling since these are known not to be normally distributed?

        x_range = np.max(x) - np.min(x)
        x_median = np.median(x)
        x = (x - x_median) / x_range

        y_range = np.max(y) - np.min(y)
        y = y / y_range

        s = curr_step_start - prev_step_end
        m = (curr_step_start + curr_step_end) // 2 - prev_step_end
        e = curr_step_end - prev_step_end
        # Parameters are: centre strength width
        p0 = [x[m], max(y[m], 0), (x[e] - x[s]) / 2]
        lbounds = [x[s], 0, 0]
        ubounds = [x[e], np.inf, x[e] - x[s]]

        logging.debug(
            "Fitting curve to scaled parameters: p0=%s | lbounds=%s | ubounds=%s",
            p0,
            lbounds,
            ubounds,
        )

        def lorentzian(E, E0, z, w):
            return Lorentzian(E0, z, w).lineshape(E)

        p, covariance = sp.optimize.curve_fit(
            lorentzian,
            x,
            y,
            p0,
            bounds=(
                lbounds,
                ubounds,
            ),
        )

        errors = np.sqrt(np.diag(covariance))

        logging.debug("Found scaled peak (errors): %s (%s)", p, errors)

        centre, strength, width = p
        centre = x_median + x_range * centre
        strength = y_range * strength
        width = x_range * width

        c_error, s_error, w_error = errors
        c_error = x_range * c_error
        s_error = y_range * s_error
        w_error = x_range * w_error

        logging.info(
            "Found peak: centre=%.6f (%.6f) | strength=%.6f (%.6f) | width=%.6f (%.6f)",
            centre,
            c_error,
            strength,
            s_error,
            width,
            w_error,
        )

        peaks.append(
            {
                "fit": Lorentzian(centre, strength, width),
                "error": LorentzianParameters(c_error, s_error, w_error),
            }
        )

        if next_step_start == len(d_phase):
            break

        prev_step_end = curr_step_end
        curr_step_start = next_step_start

    return peaks


def fit_peaks(energy, phase, error_factor=10.0):
    """Find peaks by fitting individually then improve the estimates by fitting all at once.

    If there are overlapping resonances in the FARM data, fitting peaks individually can lead to bad results.
    This function first fits the peaks individually, then uses those values as initial guesses for a function that fits all peaks at once.

    In the case that individual fits are good this function will run very quickly, in the case that individual fits are bad then this function is needed anyway.
    Therefore there is no real need to use `fit_peaks_individually`.

    Parameters
    ----------
    energy : array_like
        The energy data from the phase file (see `read_phase_data`).
    phase : array_like
        The phase data from the phase file (see `read_phase_data`).
    use_errors : float, optional (default 10.0)
        If ``None`` then do not use any bounds on the fit.
        Otherwise must be a positive, non-zero float by which the errors from the individual fits will be multiplied to give bounds on the new fit.
    """

    def f(d_energy, *params):
        shape = np.zeros_like(d_energy)

        for centre, strength, width in batched(params, 3):
            shape += Lorentzian(centre, strength, width).lineshape(d_energy)

        return shape

    peaks = fit_peaks_individually(energy, phase)

    d_energy, d_phase = derivative(energy, phase)

    p0 = np.array([param for peak in peaks for param in peak["fit"]])
    if error_factor is None:
        logging.debug("Fitting curve: p0=%s", p0)
        p, covariance = sp.optimize.curve_fit(f, d_energy, d_phase, p0)
    else:
        errors = np.array([param for peak in peaks for param in peak["error"]])
        logging.debug("Original errors: %s", errors)

        # If errors are too small we end up with bounds that are impossible.
        errors = np.maximum(errors, p0 * 0.01)
        logging.debug("Errors used to calculate bounds: %s", errors)

        lbounds = p0 - error_factor * errors
        ubounds = p0 + error_factor * errors

        logging.debug("Fitting curve: p0=%s | lbounds=%s | ubounds=%s", p0, lbounds, ubounds)
        p, covariance = sp.optimize.curve_fit(
            f, d_energy, d_phase, p0, bounds=(lbounds, ubounds)
        )

    errors = np.sqrt(np.diag(covariance))

    logging.info("Found curve: p=%s | errors=%s", p, errors)

    return [
        {
            "fit": Lorentzian(c, s, w),
            "error": LorentzianParameters(c_error, s_error, w_error),
        }
        for (c, c_error), (s, s_error), (w, w_error) in batched(zip(p, errors), 3)
    ]


def plot_resonances(energy_eV, phase, peaks):
    from matplotlib import pyplot as plt

    d_energy = (energy_eV[1:] + energy_eV[:-1]) / 2
    d_phase = np.diff(phase) / np.diff(energy_eV)

    resonances = [p.centre for p in peaks]

    _, [ax_cumulative, ax_derivative] = plt.subplots(1, 2)

    ax_cumulative.plot(energy_eV, phase, color="tab:blue")
    ax_cumulative.vlines(
        resonances,
        [np.min(phase) + i * np.pi for i, _ in enumerate(resonances)],
        [np.min(phase) + (i + 1) * np.pi for i, _ in enumerate(resonances)],
        color="tab:orange",
    )
    ax_cumulative.set_title("Check orange lines match blue steps.")
    ax_cumulative.set_xlabel("Energy (ev)")

    ax_derivative.plot(d_energy, d_phase, color="tab:blue")
    for p in peaks:
        ax_derivative.plot(
            d_energy,
            p.lineshape(d_energy),
            linestyle="dashed",
            color="tab:orange",
        )
    ax_derivative.set_title("Check dotted peaks match solid peak without noise.")
    ax_derivative.set_xlabel("Energy (ev)")

    plt.show()


def main(file, ionisation_energy_eV, output, plot):
    energy_eV, phase = read_phase_data(file, ionisation_energy_eV)
    peaks = [p["fit"] for p in fit_peaks_individually(energy_eV, phase)]

    def write(io, peaks):
        print(
            "{:21s} {:13s} {:10s}".format(
                "Resonance Energy (eV)", "Strength", "Width (eV)"
            ),
            file=io,
        )
        for peak in peaks:
            print(
                f"{peak.centre:21.6f} {peak.strength:13.6f} {peak.width:10.6f}", file=io
            )

    if output is None:
        write(sys.stdout, peaks)
    else:
        with open(output, "w") as io:
            write(io, peaks)

    if plot:
        plot_resonances(energy_eV, phase, peaks)


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(
        description="Find resonance locations from FARM phase data."
    )

    parser.add_argument(
        "file", default="CPCPHASEF", help="path to FARM phase file (default: CPCPHASEF)"
    )
    parser.add_argument(
        "-i",
        "--ionisation-energy-eV",
        type=float,
        default=0.0,
        help="FARM energies are relative to the system's ionisation energy, by supplying this option energies will be output relative to the ground state",
    )
    parser.add_argument(
        "-o",
        "--output",
        help="write results to this file (results are printed to stdout by default)",
    )
    parser.add_argument(
        "-p",
        "--plot",
        action="store_true",
        help="show plots of the extracted resonances overlaid on the FARM data (requires matplotlib)",
    )

    args = parser.parse_args()
    main(args.file, args.ionisation_energy_eV, args.output, args.plot)
