import os

import numpy as np
import pytest
from farm_utilities import phase


def return_raw_and_fit(path):
    energy, phi = phase.read_phase_data(path)

    peaks = phase.fit_peaks(energy, phi)
    d_energy, d_phi = phase.derivative(energy, phi)

    return d_energy, d_phi, peaks


DATA_DIR = os.path.dirname(__file__)
CASES = {
    "Neon-1Se": return_raw_and_fit(os.path.join(DATA_DIR, "Neon", "1Se", "CPCPHASEF")),
    "Neon-1Po": return_raw_and_fit(os.path.join(DATA_DIR, "Neon", "1Po", "CPCPHASEF")),
    "Neon-1De": return_raw_and_fit(os.path.join(DATA_DIR, "Neon", "1De", "CPCPHASEF")),
}


def reconstruct(d_energy, peaks):
    fitted = np.zeros_like(d_energy)
    for peak in peaks:
        fitted += peak["fit"].lineshape(d_energy)

    return fitted


@pytest.mark.parametrize(
    ["key", "value", "error"],
    [
        params
        for key, (_, _, peaks) in CASES.items()
        for peak in peaks
        for params in (
            (key + "-centre", peak["fit"].centre, peak["error"].centre),
            (key + "-strength", peak["fit"].strength, peak["error"].strength),
            (key + "-width", peak["fit"].width, peak["error"].width),
        )
    ],
)
def test_fitting_errors(key, value, error):
    _ = key
    assert error / value < 0.01


# Don't test Neon-1Se as it has one noise peak that is bigger than the nearby actual peak.
@pytest.mark.parametrize(
    ["d_energy", "d_phi", "peaks"], [CASES["Neon-1Po"], CASES["Neon-1De"]]
)
def test_maximum_absolute_deviation(d_energy, d_phi, peaks):
    fitted = reconstruct(d_energy, peaks)

    peak_heights = [peak["fit"].lineshape(peak["fit"].centre) for peak in peaks]
    mad = np.max(np.abs(fitted - d_phi))
    min_peak = np.min(peak_heights)

    assert mad / min_peak < 0.54


@pytest.mark.parametrize(["d_energy", "d_phi", "peaks"], list(CASES.values()))
def test_root_mean_square_error(d_energy, d_phi, peaks):
    fitted = reconstruct(d_energy, peaks)

    rms_error = np.sqrt(np.mean(((fitted - d_phi) / fitted) ** 2))
    rms_value = np.sqrt(np.mean(fitted**2))

    assert rms_error / rms_value < 0.1
