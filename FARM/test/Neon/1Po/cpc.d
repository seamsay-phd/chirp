&PHZFIL
    filh = '../H'
    fout = 'log.out'
    fkmat = 'CPCKMATB'
    fpha = 'CPCPHASEF'
    ftmat = 'CPCTMATB'
    fom = 'CPCOMEGAB'
&END
&PHZIN
    title = 'Neon'
    ne1 =  11706
    esc1 = 1.54218106 0.0000367
    ls = 1
    ll = 1
    lpi = 1
    prtflg = 3 3 3 0 3 0
    diaflg = 0 0 0 0 0 0 0 0 1 1 0 1 1
    lrglx = 10
&END
