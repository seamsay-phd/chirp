&PHZFIL
    filh = 'H'
    fout = 'log.out'
    fkmat = 'CPCKMATB'
    fpha = 'CPCPHASEF'
    ftmat = 'CPCTMATB'
    fom = 'CPCOMEGAB'
&END
&PHZIN
    title = 'Neon'
    ne1 =  117060
    esc1 = 1.54218106 0.00000367
    ls = 1
    ll = 0
    lpi = 0
    prtflg = 3 3 3 0 3 0
    diaflg = 0 0 0 0 0 0 0 0 1 1 0 1 1
    lrglx = 10
&END
