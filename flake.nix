{
  description = "Chirp Project";

  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
  };

  outputs = {
    self,
    flake-utils,
    nixpkgs,
  }:
    flake-utils.lib.eachDefaultSystem (system: let
      pkgs = import nixpkgs {inherit system;};
    in {
      devShells.default = let
        python = pkgs.python3.withPackages (ps:
          with ps; [
            f90nml
            flake8
            ipympl
            ipython
            h5py
            jupyter
            matplotlib
            mock
            numpy
            pandas
            pytest
            requests
            scipy
          ]);
      in
        pkgs.mkShell {
          buildInputs = [python] ++ (with pkgs; [black blas cmake gcc gfortran isort lapack mypy]);

          shellHook = ''
            export PYTHONPATH="$PYTHONPATH:$PWD/RMT/source:$PWD/FARM/source:$PWD"
            export CC=${pkgs.gcc}/bin/gcc
            export CXX=${pkgs.gcc}/bin/g++
            export FC=${pkgs.gfortran}/bin/gfortran
          '';
        };

      formatter = pkgs.alejandra;
    });
}
