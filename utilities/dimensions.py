# TODO: Use a Python unit library.
import inspect

import numpy as np

SPEED_OF_LIGHT_au = 137.03599908330932

AMPLITUDE_au_per_sqrt_INTENSITY_100_TW_per_cm2 = 0.05336
PERIOD_au_ENERGY_au = 2 * np.pi
WAVELENGTH_au_ENERGY_au = PERIOD_au_ENERGY_au * SPEED_OF_LIGHT_au


class Dimension:
    # NOTE: This method is used to bypass `__init__`, forcing people to be explicit about units.
    @classmethod
    def _init(cls, value):
        obj = cls()
        obj._value = value
        return obj

    def serialise(self, **kwargs):
        def is_serialise_method(obj):
            return inspect.ismethod(obj) and obj.__name__.startswith("to_")

        start = len("to_")

        data = {}
        for name, method in inspect.getmembers(self, is_serialise_method):
            signature = inspect.signature(method)
            args = {
                arg: value
                for arg, value in kwargs.items()
                if arg in signature.parameters
            }
            data[name[start:]] = method(**args)

        return data


class Angle(Dimension):
    rad = np.pi
    deg = 180

    @classmethod
    def from_pi(cls, value):
        # Get value in [0, 2).
        # NOTE: This is due to Python's `%` operator being floored modulo, other languages may use truncated modulo and require an extra `value = (value + 2) % 2`.
        value %= 2
        # Get value in [-1, 1).
        if value >= 1:
            value -= 2

        return cls._init(value)

    def to_pi(self):
        return self._value

    @classmethod
    def from_rad(cls, value):
        return cls.from_pi(value / cls.rad)

    def to_rad(self):
        return self.to_pi() * self.rad

    @classmethod
    def from_deg(cls, value):
        return cls.from_pi(value / cls.deg)

    def to_deg(self):
        return self.to_pi() * self.deg


class Energy(Dimension):
    eV = 27.211324570273
    J = 4.359744722223276e-18
    Ry = 2

    @classmethod
    def from_au(cls, value):
        return cls._init(value)

    def to_au(self):
        return self._value

    @classmethod
    def from_eV(cls, value):
        return cls._init(value / cls.eV)

    def to_eV(self):
        return self._value * self.eV

    @classmethod
    def from_Ry(cls, value):
        return cls._init(value / cls.Ry)

    def to_Ry(self):
        return self._value * self.Ry

    @classmethod
    def from_J(cls, value):
        return cls._init(value / cls.J)

    def to_J(self):
        return self._value * self.J


class PhotonEnergy(Energy):
    def wavelength(self):
        return PhotonWavelength.from_au(WAVELENGTH_au_ENERGY_au / self.to_au())

    def period(self):
        return PhotonPeriod.from_au(PERIOD_au_ENERGY_au / self.to_au())


class Length(Dimension):
    nm = 0.0529177210903

    @classmethod
    def from_au(cls, value):
        return cls._init(value)

    def to_au(self):
        return self._value

    @classmethod
    def from_nm(cls, value):
        return cls._init(value / cls.nm)

    def to_nm(self):
        return self._value * self.nm


class PhotonWavelength(Length):
    def energy(self):
        return PhotonEnergy.from_au(WAVELENGTH_au_ENERGY_au / self.to_au())


class Time(Dimension):
    fs = 2.4188843265857e-2

    @classmethod
    def from_au(cls, value):
        return cls._init(value)

    def to_au(self):
        return self._value

    @classmethod
    def from_fs(cls, value):
        return cls._init(value / cls.fs)

    def to_fs(self):
        return self._value * self.fs

    @classmethod
    def from_steps(cls, value, delta_t):
        return cls._init(value * delta_t.to_au())

    def to_steps(self, delta_t):
        return self._value / delta_t.to_au()

    @classmethod
    def from_cycles(cls, value, period):
        return cls._init(value * period.to_au())

    def to_cycles(self, period):
        return self._value / period.to_au()


class PhotonPeriod(Time):
    def energy(self):
        return PhotonEnergy.from_au(PERIOD_au_ENERGY_au / self.to_au())


class EFieldIntensity(Dimension):
    @classmethod
    def from_100_TW_per_cm2(cls, value):
        return cls._init(value)

    def to_100_TW_per_cm2(self):
        return self._value

    def amplitude(self):
        return EFieldAmplitude.from_au(
            np.sqrt(self.to_100_TW_per_cm2())
            * AMPLITUDE_au_per_sqrt_INTENSITY_100_TW_per_cm2
        )


class EFieldAmplitude(Dimension):
    @classmethod
    def from_au(cls, value):
        return cls._init(value)

    def to_au(self):
        return self._value

    def intensity(self):
        return EFieldIntensity.from_100_TW_per_cm2(
            (self.to_au() / AMPLITUDE_au_per_sqrt_INTENSITY_100_TW_per_cm2) ** 2
        )


class Chirp(Dimension):
    @classmethod
    def from_au(cls, value):
        return cls._init(value)

    def to_au(self):
        return self._value
