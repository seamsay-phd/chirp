import itertools
import json
import logging
import os
import re
import subprocess

import h5py
import numpy as np
import pandas as pd
from rmt_utilities import rmtutil, windowing

from ..constants import NIR_INTENSITY_100_TW_per_cm2
from ..fano import FIT_ENERGY_RANGE_HIGH_eV, FIT_ENERGY_RANGE_LOW_eV
from .runner import calculation_is_running

ENERGY_RGX = re.compile(r"^xuv\=\d{2}\.\d{3}eV-nir\=\d.\d{3}eV$")
INTENSITY_RGX = re.compile(r"^int=\d\.\d{3}$")
DELAY_RGX = re.compile(r"^delay\=[-+]\d\d\.\d{3}fs$")

CHANNEL_INFO_FILE = "CoupledChannelInfo"
MOMENTUM_SPECTRUM_FILE = "OuterWave_momentum_spectrum.txt"
OUTER_WAVE_STEM = "OuterWave"
TAS_FILE = "TAS_0001_z"

ABSORPTION_X = "E"
ABSORPTION_Y = "z"
MOMENTUM_X = "k"
MOMENTUM_Y = "rho"


def load_efield(calc_dir):
    efield_path = os.path.join(calc_dir, "EField.json")
    with open(efield_path) as f:
        efield = json.load(f)

    ir_energy = efield["NIR"]["energy"]["eV"]
    ir_intensity = efield["NIR"]["intensity"]["100_TW_per_cm2"]
    uv_energy = efield["XUV"]["energy"]["eV"]
    delay = efield["NIR"]["peak"]["fs"] - efield["XUV"]["peak"]["fs"]

    if delay == -0.0:
        delay = 0.0

    return {
        "ir-energy": ir_energy,
        "uv-energy": uv_energy,
        "delay": delay,
        "ir-intensity": ir_intensity,
    }, efield


def load_absorption(calc_dir, efield=None):
    if efield is None:
        _, efield = load_efield(calc_dir)

    tas_path = os.path.join(calc_dir, TAS_FILE)
    if not os.path.exists(tas_path):
        nir_end = round(efield["NIR"]["end"]["steps"])
        xuv_end = round(efield["XUV"]["end"]["steps"])
        window_start = max(nir_end, xuv_end)

        logging.info(
            "Generating absorption spectrum with a linear window starting at step `%d` for calculation `%s`.",
            window_start,
            calc_dir,
        )
        calc = rmtutil.RMTCalc(calc_dir)
        tas = calc.ATAS(window=windowing.LinearStep(window_start))
        tas.write(fname="TAS_", units="eV")
    else:
        logging.info(
            "`%s` exists in `%s`, not running `RMT_gen_tas`.", TAS_FILE, calc_dir
        )

    return pd.read_table(tas_path, sep=" ", names=[ABSORPTION_X, ABSORPTION_Y])


def load_momentum(calc_dir, _=None):
    state_dir = os.path.join(calc_dir, "state")

    state_files = os.listdir(state_dir)
    if (
        # Quick to find files that indicate `reform` hasn't been run.
        "atomic_structure" not in state_files
        or "DiscardedPop" not in state_files
        # Required files.
        or CHANNEL_INFO_FILE not in state_files
        or MOMENTUM_SPECTRUM_FILE not in state_files
        or not any(f.startswith(OUTER_WAVE_STEM) for f in state_files)
    ):
        current_dir = os.path.abspath(os.getcwd())
        try:
            os.chdir(state_dir)
            logging.info("Running reform for calculation state: %s", state_dir)
            subprocess.run(
                ["reform", "--momentum"],
                check=True,
            )
        finally:
            os.chdir(current_dir)
    else:
        logging.info(
            "All required files exist, so not running reform on calculation state: %s",
            state_dir,
        )

    return pd.read_table(
        os.path.join(state_dir, MOMENTUM_SPECTRUM_FILE),
        names=[MOMENTUM_X, MOMENTUM_Y],
        sep=r"\s+",
    )


def assign_data(root, order, datasets, dataframe=None):
    group = root
    for by, name, value, unit in order:
        by_group = group.require_group(f"by-{by}")
        group = by_group.require_group(name)
        if "value" not in group.attrs:
            group.attrs["value"] = value
            if unit is not None:
                group.attrs["unit"] = unit

    if any(ds is not None for ds in datasets.values()) and dataframe is not None:
        raise ValueError("Both `dataframe` and datasets were given.")
    elif dataframe is not None:
        return {
            name: group.create_dataset(name, data=dataframe.loc[:, name].to_numpy())
            for name in datasets.keys()
        }
    elif any(ds is not None for ds in datasets.values()):
        if any(ds is None for ds in datasets.values()):
            raise ValueError("Note all datasets were given.")

        for name, dataset in datasets.items():
            group[name] = dataset
    else:
        raise ValueError("Neither `dataframe` nor the datasets were given.")


def package_uv_only(hdf_path, uv_only_path, loader, columns):
    with h5py.File(hdf_path, "a") as hdf:
        if "uv-only" not in hdf.keys():
            xuv_only = loader(
                uv_only_path,
                {"NIR": {"end": {"steps": 0}}, "XUV": {"end": {"steps": 3765}}},
            )
            xuv_only_grp = hdf.create_group("uv-only")
            for column in columns:
                xuv_only_grp.create_dataset(
                    column, data=xuv_only.loc[:, column].to_numpy()
                )


def package(hdf_path, data_dir, loader, columns):
    with h5py.File(hdf_path, "a") as hdf:
        for energy_dir in os.listdir(data_dir):
            match = ENERGY_RGX.match(energy_dir)
            if match is None:
                logging.info(
                    "Ignoring energy directory with unrecognised format: %s",
                    energy_dir,
                )
                continue

            energy_path = os.path.join(data_dir, energy_dir)

            for intensity_dir in os.listdir(energy_path):
                match = INTENSITY_RGX.match(intensity_dir)
                if match is None:
                    logging.info(
                        "Ignoring intensity directory with unrecognised format: %s",
                        intensity_dir,
                    )
                    continue

                intensity_path = os.path.join(energy_path, intensity_dir)

                for delay_dir in os.listdir(intensity_path):
                    match = DELAY_RGX.match(delay_dir)
                    if match is None:
                        logging.info(
                            "Ignoring delay directory with unrecognised format: %s",
                            delay_dir,
                        )
                        continue

                    delay_path = os.path.join(intensity_path, delay_dir)
                    if calculation_is_running(delay_path):
                        logging.info(
                            "Ignoring directory as job is still running: %s",
                            delay_path,
                        )
                        continue

                    data, efield = load_efield(delay_path)

                    ir_energy = data["ir-energy"]
                    ir_energy_name = f"{ir_energy:5.3f}eV"

                    uv_energy = data["uv-energy"]
                    uv_energy_name = f"{uv_energy:6.3f}eV"

                    ir_intensity = data["ir-intensity"]
                    ir_intensity_name = f"{ir_intensity:.3f}"

                    delay = data["delay"]
                    delay_name = f"{delay:+07.3f}fs"

                    keys = (
                        ("ir-energy", ir_energy_name, ir_energy, "eV"),
                        ("uv-energy", uv_energy_name, uv_energy, "eV"),
                        ("ir-intensity", ir_intensity_name, ir_intensity, ""),
                        ("delay", delay_name, delay, "fs"),
                    )

                    not_found = object()
                    key = "/".join(f"by-{group}/{name}" for group, name, _, _ in keys)
                    exists = not hdf.get(key, not_found) is not_found
                    if exists:
                        logging.info(
                            "Ignoring directory as data already packaged: %s",
                            delay_path,
                        )
                        continue

                    data = loader(delay_path, efield)

                    datasets = assign_data(
                        hdf,
                        keys,
                        {name: None for name in columns},
                        dataframe=data,
                    )
                    for order in itertools.permutations(keys):
                        if all(order[i][0] == keys[i][0] for i in range(len(keys))):
                            continue

                        assign_data(hdf, order, datasets)


def spectrum_hdf_key(
    uv_energy_eV=None, ir_energy_eV=None, delay_fs=None, intensity=None
):
    if (
        uv_energy_eV is None
        and ir_energy_eV is None
        and delay_fs is None
        and intensity is None
    ):
        return "/uv-only"

    key = ""

    if uv_energy_eV is not None:
        key += f"/by-uv-energy/{uv_energy_eV:6.3f}eV"

    if ir_energy_eV is not None:
        key += f"/by-ir-energy/{ir_energy_eV:5.3f}eV"

    if delay_fs is not None:
        try:
            key += f"/by-delay/{delay_fs:+z07.3f}fs"
        except ValueError:
            if delay_fs == -0.0:
                delay_fs = 0.0

            key += f"/by-delay/{delay_fs:+07.3f}fs"

    if intensity is not None:
        key += f"/by-ir-intensity/{intensity:.3f}"

    return key


def spectrum_hdf(
    hdf,
    uv_energy_eV,
    ir_energy_eV,
    delay_fs,
    intensity,
    x_name,
    y_name,
    x_low=-np.inf,
    x_high=np.inf,
):
    key = spectrum_hdf_key(uv_energy_eV, ir_energy_eV, delay_fs, intensity)

    try:
        group = hdf[key]
    except KeyError:
        logging.error("Key not found: %s", key)
        raise

    if x_name in group:
        x = group[x_name][:]

        wanted = (x_low < x) & (x < x_high)
        return x[wanted], group[y_name][wanted]
    else:
        return group


def spectrum_tas(path, E_low_eV, E_high_eV):
    data = np.loadtxt(path)
    wanted = (E_low_eV < data[:, 0]) & (data[:, 0] < E_high_eV)
    return data[wanted, 0], data[wanted, 1]


def spectrum_energy_intensity_delay_dir(
    root, uv_eV, ir_eV, delay_fs, intensity, E_low_eV, E_high_eV
):
    path = root

    if ir_eV is None and uv_eV is not None:
        path = os.path.join(path, f"xuv={uv_eV:.3f}eV")
    elif ir_eV is not None and uv_eV is not None:
        path = os.path.join(path, f"xuv={uv_eV:.3f}eV-nir={ir_eV:.3f}eV")

    if intensity is not None:
        path = os.path.join(path, f"int={intensity:.3f}")

    if delay_fs is not None:
        path = os.path.join(path, f"delay={delay_fs:+z07.3f}fs")

    path = os.path.join(path, "TAS_0001_z")

    return spectrum_tas(path, E_low_eV, E_high_eV)


def absorption(
    data,
    uv_energy_eV,
    ir_energy_eV,
    delay_fs,
    intensity=NIR_INTENSITY_100_TW_per_cm2,
    E_low_eV=FIT_ENERGY_RANGE_LOW_eV,
    E_high_eV=FIT_ENERGY_RANGE_HIGH_eV,
):
    if isinstance(data, h5py.File):
        result = spectrum_hdf(
            data,
            uv_energy_eV,
            ir_energy_eV,
            delay_fs,
            intensity,
            ABSORPTION_X,
            ABSORPTION_Y,
            E_low_eV,
            E_high_eV,
        )
    elif os.path.isfile(data) and os.path.basename(data).startswith("TAS_"):
        result = spectrum_tas(data, E_low_eV, E_high_eV)
    else:
        result = spectrum_energy_intensity_delay_dir(
            data,
            uv_energy_eV,
            ir_energy_eV,
            delay_fs,
            intensity,
            E_low_eV,
            E_high_eV,
        )

    if isinstance(result, h5py.Group):
        return result
    else:
        E, z = result
        return E, -z


def momentum(
    hdf,
    uv_energy_eV,
    ir_energy_eV,
    delay_fs,
    intensity=NIR_INTENSITY_100_TW_per_cm2,
    k_low=-np.inf,
    k_high=np.inf,
):
    return spectrum_hdf(
        hdf,
        uv_energy_eV,
        ir_energy_eV,
        delay_fs,
        intensity,
        MOMENTUM_X,
        MOMENTUM_Y,
        k_low,
        k_high,
    )
