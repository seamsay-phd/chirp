import os
import shutil
import subprocess
import textwrap
import time
import typing

import f90nml
import numpy as np

from .. import constants
from .. import dimensions as dim
from .. import laser, logging


class TimeLimit(typing.NamedTuple):
    hours: int
    minutes: int = 0
    seconds: int = 0


def conf(
    inner_cores,
    outer_cores,
    final_t_au,
    steps_per_run_approx,
    jobname,
    reduced_L_max=None,
    x_last_others=constants.X_LAST_OTHERS,
    x_last_master=None,
):
    if x_last_master is None:
        # Must be a multiple of 4.
        x_last_master = 4 * int(constants.X_LAST_OTHERS * constants.MASTER_PROP / 4)

    if reduced_L_max is not None:
        extras = {"reduced_L_max": reduced_L_max}
    else:
        extras = {}

    return {
        "InputData": {
            "version_root": jobname,
            "no_of_pes_to_use_inner": inner_cores,
            "no_of_pes_to_use_outer": outer_cores,
            "final_t": final_t_au,
            "steps_per_run_approx": steps_per_run_approx,
            "x_last_master": x_last_master,
            "x_last_others": x_last_others,
            "adjust_gs_energy": False,
            "ml_max": 0,
            "use_field_from_file": True,
            "use_2colour_field": False,
            "frequency": 0.0,
            "intensity": 0.0,
            "frequency_xuv": 0.0,
            "intensity_xuv": 0.0,
            "keep_checkpoints": False,
            "checkpts_per_run": 10,
            "dipole_output_desired": True,
            "dipole_velocity_output": False,
            "timings_desired": False,
            "absorb_desired": True,
            **extras,
        }
    }


def slurm(nodes, jobname, time_limit):
    rmt = os.path.join(constants.PROJ_DIR, "RMT", "build", "bin", "rmt.x")
    return textwrap.dedent(
        f"""\
        #!/bin/bash

        #SBATCH --job-name={jobname}
        #SBATCH --account=e813

        #SBATCH --qos=standard
        #SBATCH --partition=standard

        #SBATCH --nodes={nodes}
        #SBATCH --ntasks-per-node=128
        #SBATCH --cpus-per-task=1
        #SBATCH --time={time_limit.hours:02d}:{time_limit.minutes:02d}:{time_limit.seconds:02d}


        export OMP_NUM_THREADS=1
        export SRUN_CPUS_PER_TASK="$SLURM_CPUS_PER_TASK"
        srun --distribution=block:block --hint=nomultithread {rmt}
    """
    )


def spaces_in_queue():
    # Don't suppress this error as it should only ever be run when on a Slurm system.
    t = subprocess.run(
        ["squeue", "--me", "--format=%i"],
        check=True,
        encoding="utf8",
        capture_output=True,
    ).stdout

    # If there is no final newline then the number of newlines is one less than the number of lines.
    # Also need to unconditionally remove one to account for the header line.
    jobs = t.count("\n") + (not t.endswith("\n")) - 1

    return constants.MAX_JOBS_IN_QUEUE - jobs


def calculation_is_running(directory):
    try:
        # `check=False` only prevents checking the result, `FileNotFound` would still be thrown.
        output = subprocess.run(
            ["squeue", "--me", "--format=%Z"],
            check=True,
            encoding="utf8",
            capture_output=True,
        ).stdout
    except FileNotFoundError:
        return False

    lines = output.splitlines()
    return any(line == directory for line in lines if line != "WORK_DIR" and line != "")


def calculation_already_run(directory):
    return os.path.exists(directory) and (
        os.path.exists(os.path.join(directory, "CurrentPosition"))
        or calculation_is_running(directory)
    )


def time_delays(min_delay, max_delay, delay_delta):
    return (
        dim.Time.from_fs(min_delay + i * delay_delta)
        for i in range(round((max_delay - min_delay) / delay_delta) + 1)
    )


def run(
    submit: bool,
    overwrite: bool,
    outdir,
    orig_datadir,
    e_field_data: laser.EFieldData,
    ringing_factor: float = 3.0,
    time_limit: TimeLimit = TimeLimit(hours=2, minutes=30),
    reduced_L_max: typing.Optional[int] = None,
    inner_cores: int = constants.INNER_CORES,
    outer_cores: int = 4 * constants.CPUS_PER_NODE,
    x_last_others: int = 100,
    x_last_master: typing.Optional[int] = None,
):
    datadir = os.path.relpath(orig_datadir, outdir)
    logging.debug(
        "Calculated relative data path: Original = %s | Out = %s | Relative = %s"
    )

    if overwrite and calculation_is_running(outdir):
        logging.warning("Not overwriting as calculation is in progress: %s", outdir)
        return
    elif os.path.exists(outdir) and overwrite:
        logging.info("Overwriting calculation: %s", outdir)
        shutil.rmtree(outdir)
    elif os.path.exists(outdir) and not calculation_already_run(outdir):
        logging.info("Calculation set up but not run, running now: %s", outdir)
        shutil.rmtree(outdir)
    elif os.path.exists(outdir):
        logging.info(
            "Skipping calculation as RMT has already been run on directory: %s", outdir
        )
        return

    os.makedirs(outdir)

    currentdir = os.getcwd()
    try:
        os.chdir(outdir)
        laser.jsonify("EField.json", e_field_data)

        field_start_steps = round(e_field_data.start().to_steps(e_field_data.delta_t))
        field_duration_steps = round(
            e_field_data.duration().to_steps(e_field_data.delta_t)
        )

        steps_per_run_approx = round(ringing_factor * field_duration_steps)
        assert steps_per_run_approx > field_start_steps + field_duration_steps
        final_t = dim.Time.from_au(steps_per_run_approx * e_field_data.delta_t.to_au())

        logging.debug(
            "Time parameters: Factor = %.3f | Steps = %.3f | Time = %.3ffs = %.3fau",
            ringing_factor,
            steps_per_run_approx,
            final_t.to_fs(),
            final_t.to_au(),
        )

        e_field_data.write_inp("EField.inp", final_t)

        nodes = int(np.ceil((inner_cores + outer_cores) / constants.CPUS_PER_NODE))

        configuration = conf(
            inner_cores,
            outer_cores,
            final_t.to_au(),
            steps_per_run_approx,
            e_field_data.jobname(),
            reduced_L_max,
            x_last_others,
            x_last_master,
        )
        with open("input.conf", "w") as f:
            f90nml.write(configuration, f)

        submission = slurm(nodes, e_field_data.jobname(), time_limit)
        with open("submit.slurm", "w") as f:
            f.write(submission)

        os.symlink(os.path.join(datadir, "d"), "d")
        os.symlink(os.path.join(datadir, "d00"), "d00")
        os.symlink(os.path.join(datadir, "H"), "H")
        os.symlink(os.path.join(datadir, "Splinewaves"), "Splinewaves")
        os.symlink(os.path.join(datadir, "Splinedata"), "Splinedata")

        if submit:
            while (spaces := spaces_in_queue()) <= 2:
                logging.info("Sleeping for 60s as spaces left in queue is: %d", spaces)
                time.sleep(60)

            subprocess.run(["sbatch", "submit.slurm"], check=True)
    finally:
        os.chdir(currentdir)
