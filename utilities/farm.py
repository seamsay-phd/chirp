import io
import os
import subprocess
import sys

import f90nml

ENERGY_Ryd_per_eV = 0.0734985857

FILES = {
    "H": "H",
    "out": "log.out",
    "kmat": "CPCKMATB",
    "phase": "CPCPHASEF",
    "tmat": "CPCTMATB",
    "omega": "CPCOMEGAB",
    "lwpscr": "lwpscr",
}


class FARMFailedError(Exception):
    pass


def configuration(
    energy_low_eV,
    energy_high_eV,
    energy_step_eV,
    multiplicity,
    azimuthal,
    parity,
    ionisation_energy_eV=0.0,
    files=FILES,
):
    energy_low_eV -= ionisation_energy_eV
    energy_high_eV -= ionisation_energy_eV

    energy_low_Ryd = energy_low_eV * ENERGY_Ryd_per_eV
    energy_high_Ryd = energy_high_eV * ENERGY_Ryd_per_eV
    energy_step_Ryd = energy_step_eV * ENERGY_Ryd_per_eV

    n_energies = int((energy_high_Ryd - energy_low_Ryd) / energy_step_Ryd)

    return {
        "phzfil": {
            "filh": files["H"],
            "fout": files["out"],
            "fkmat": files["kmat"],
            "fpha": files["phase"],
            "ftmat": files["tmat"],
            "fom": files["omega"],
            "flwp": files["lwpscr"],
        },
        "phzin": {
            "title": "Neon",
            "ne1": n_energies,
            "esc1": [energy_low_Ryd, energy_step_Ryd],
            "ls": multiplicity,
            "ll": azimuthal,
            "lpi": parity,
            "prtflg": [3, 3, 3, 0, 3, 0],
            "diaflg": [
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                1,
                1,
                0,
                1,
                1,
            ],
            "lrglx": 10,
        },
    }


def run(
    conf,
    overwrite=False,
    executable="farm",
    files=FILES,
    output=sys.stderr,
):
    if not overwrite and all(os.path.exists(path) for path in files.values()):
        return

    with io.StringIO() as buffer:
        f90nml.write(conf, buffer)
        buffer.seek(0)
        text = buffer.read()

    subprocess.run(
        [executable],
        input=text,
        stdout=output,
        text=True,
        check=True,
    )

    # If file is empty the calculation failed.
    if any(os.stat(path).st_size == 0 for path in files.values()):
        raise FARMFailedError()

    return text
