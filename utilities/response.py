import numpy as np
from rmt_utilities import rmtutil, windowing

from . import logging


class SuperGaussian(windowing.Window):
    def window(self, length):
        t_dash = np.linspace(0, 2.5, length)
        mask = np.exp(-(t_dash**4))
        return mask


def calculate(directory):
    logging.info("Calculating dipole responses: %s", directory)
    calc = rmtutil.RMTCalc(directory)

    logging.info("Calculating absorption spectrum: %s", directory)
    tas = calc.ATAS(window=SuperGaussian())
    logging.info("Writing absorption spectrum: %s", directory)
    tas.write(fname="TAS_", units="eV")

    logging.info("Calculating dipole response: %s", directory)
    response = calc.dipole_response(window=SuperGaussian())
    logging.info("Writing dipole response: %s", directory)
    response.write(fname="response_", units="eV")

    logging.info("Finished: %s", directory)
