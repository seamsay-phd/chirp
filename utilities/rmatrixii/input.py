import collections
import itertools
from dataclasses import dataclass

import f90nml

from utilities import logging

try:
    from itertools import batched
except ImportError:

    def batched(iterable, n):
        if n < 1:
            raise ValueError("n must be at least one")
        it = iter(iterable)
        while batch := tuple(itertools.islice(it, n)):
            yield batch


class ImmutableError(Exception):
    def __init__(self):
        super().__init__("Object is immutable and can't be changed.")


class OrbitalOccupancyError(Exception):
    def __init__(self, electrons, orbital):
        capacity = orbital.capacity()
        if electrons < 0:
            message = (
                f"Number of electrons ({electrons}) in {orbital} is fewer than zero."
            )
        elif electrons > capacity:
            message = f"Number of electrons ({electrons}) in {orbital} is grater than the capacity ({capacity})."
        else:
            message = (
                f"Unknown occupancy error for {orbital} with {electrons} electron(s)."
            )

        super().__init__(message)


class BaseConfiguration(dict):
    def __getitem__(self, key):
        if key in self:
            return super().__getitem__(key)
        else:
            return 0

    def __eq__(self, other):
        return self._key() == other._key()

    def __lt__(self, other):
        return self._key() < other._key()

    def _key(self):
        return tuple((key, self[key]) for key in sorted(self.keys()))


class IndexConfiguration(BaseConfiguration):
    def __hash__(self):
        return hash(self._key())

    def __setitem__(self, orbital, electron):
        raise ImmutableError()


class OrbitConfiguration(BaseConfiguration):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # NOTE: Use of `list` here enables us to iterate over the dictionary while mutating it.
        for orbital in list(self.keys()):
            self._check_orbital(orbital)

    def _check_orbital(self, orbital):
        if not (0 <= self[orbital] <= orbital.capacity()):
            raise OrbitalOccupancyError(self[orbital], orbital)

        if self[orbital] == 0:
            del self[orbital]


class MutableConfiguration(OrbitConfiguration):
    def __setitem__(self, orbital, electrons):
        super().__setitem__(orbital, electrons)

        self._check_orbital(orbital)

    def freeze(self):
        """Convert into a hashable dictionary that can't be mutated."""
        return Configuration(self)


class Configuration(OrbitConfiguration):
    def __hash__(self):
        return hash(self._key())

    def __setitem__(self, orbital, electron):
        raise ImmutableError()

    def thaw(self):
        """Convert into a mutable dictionary."""
        return MutableConfiguration(self)


# RM2 works on the singly ionised atom, hence one less than standard Neon.
ELECTRONS = 9


@dataclass(frozen=True)
class Term:
    multiplicity: int
    L: int
    parity: int

    def __str__(self):
        return str(Orbital(self.multiplicity, self.L)).upper() + (
            "" if self.parity == 0 else "o"
        )


@dataclass(frozen=True, order=True)
class Orbital:
    n: int
    l: int

    def capacity(self):
        return 2 * (2 * self.l + 1)

    def __str__(self):
        if self.l == 0:
            letter = "s"
        elif self.l == 1:
            letter = "p"
        elif self.l == 2:
            letter = "d"
        elif 3 <= self.l < 7:
            letter = chr(ord("f") + self.l - 3)
        elif 7 <= self.l < 12:
            letter = chr(ord("k") + self.l - 7)
        elif 12 <= self.l < 14:
            letter = chr(ord("q") + self.l - 12)
        elif 14 <= self.l < 21:
            letter = chr(ord("t") + self.l - 14)
        else:
            return repr(self)

        return f"{self.n}{letter}"


@dataclass
class Slater:
    a: int
    n: float
    N: float


GROUND_STATE_CONFIGURATION = Configuration(
    {
        Orbital(1, 0): 2,
        Orbital(2, 0): 2,
        Orbital(2, 1): 5,
    }
)

assert sum(GROUND_STATE_CONFIGURATION.values()) == ELECTRONS

SLATER_COEFFICIENTS = {
    Orbital(1, 0): [
        Slater(1, 9.86116, 0.91448),
        Slater(1, 16.7831, 0.03022),
        Slater(2, 3.8793, 0.00507),
        Slater(2, 8.83998, 0.06892),
        Slater(2, 2.52757, -0.00107),
    ],
    Orbital(2, 0): [
        Slater(1, 9.86116, -0.22341),
        Slater(1, 16.7831, -0.00647),
        Slater(2, 3.8793, 0.52587),
        Slater(2, 8.83998, -0.10559),
        Slater(2, 2.52757, 0.57763),
    ],
    Orbital(3, 0): [
        Slater(1, 8.38119, 0.08738),
        Slater(2, 3.13177, -0.33585),
        Slater(3, 1.13580, 1.03273),
        Slater(4, 0.67236, 0.01116),
    ],
    Orbital(4, 0): [
        Slater(1, 7.15909, 0.32821),
        Slater(2, 3.18052, -1.47114),
        Slater(3, 2.43487, 1.82941),
        Slater(4, 1.20865, -0.77757),
    ],
    Orbital(2, 1): [
        Slater(2, 2.57103, 0.58722),
        Slater(2, 4.72364, 0.30522),
        Slater(2, 1.75238, 0.16718),
        Slater(2, 9.811, 0.01385),
    ],
    Orbital(3, 1): [
        Slater(2, 3.52778, 0.26225),
        Slater(3, 0.92661, -1.00075),
    ],
    Orbital(4, 1): [
        Slater(2, 3.69501, 1.16577),
        Slater(3, 2.55525, -1.40511),
        Slater(4, 1.07172, 0.50884),
    ],
    Orbital(3, 2): [
        Slater(3, 3.26724, 0.64105),
        Slater(4, 1.98164, 0.48492),
    ],
}

EXPERIMENTAL_THRESHOLDS = [
    [
        Term(2, 1, 1),
        [
            (0.0000, 1.5),
            (780.4240, 0.5),
        ],
    ],
    [
        Term(2, 0, 0),
        [
            (217047.5980, 0.5),
        ],
    ],
    [
        Term(4, 1, 0),
        [
            (219130.7609, 2.5),
            (219648.4248, 1.5),
            (219947.4453, 0.5),
        ],
    ],
    [
        Term(2, 1, 0),
        [
            (224087.0092, 1.5),
            (224699.2716, 0.5),
        ],
    ],
    [
        Term(4, 1, 1),
        [
            (246192.4130, 2.5),
            (246415.0144, 1.5),
            (246597.6805, 0.5),
        ],
    ],
    [
        Term(2, 2, 0),
        [
            (246394.1202, 2.5),
            (246397.4810, 1.5),
        ],
    ],
    [
        Term(4, 2, 1),
        [
            (249108.6138, 3.5),
            (249445.9632, 2.5),
            (249695.5051, 1.5),
            (249839.6186, 0.5),
        ],
    ],
    [
        Term(2, 2, 1),
        [
            (251011.1511, 2.5),
            (251522.0967, 1.5),
        ],
    ],
    [
        Term(2, 0, 1),
        [
            (252798.4654, 0.5),
        ],
    ],
    [
        Term(4, 0, 1),
        [
            (252953.5198, 1.5),
        ],
    ],
    [
        Term(2, 1, 1),
        [
            (254164.9888, 1.5),
            (254292.1683, 0.5),
        ],
    ],
]


# Include all orbitals for n=1, n=2, and n=3, but only the s and p orbitals for n=4.
ORBITALS = [Orbital(n, l) for n in range(1, 5) for l in range(n if n < 4 else 2)]

# Orbitals where all electrons must stay in their ground state orbitals.
LOCKED_ORBITALS = {Orbital(1, 0)}

# Technically these are all the configurations where all three excited electrons have been excited into the same state, but that's a difficult rule to encode.
# Disable formatter so that we can line up the first three orbitals.
# fmt: off
FORBIDDEN_CONFIGURATIONS = set(
    Configuration(configuration)
    for configuration in [
        {Orbital(1, 0): 2, Orbital(2, 0): 2, Orbital(2, 1): 2, Orbital(3, 1): 3},
        {Orbital(1, 0): 2, Orbital(2, 0): 2, Orbital(2, 1): 2, Orbital(3, 2): 3},
        {Orbital(1, 0): 2, Orbital(2, 0): 2, Orbital(2, 1): 2, Orbital(4, 1): 3},
        {Orbital(1, 0): 2, Orbital(2, 0): 1, Orbital(2, 1): 3, Orbital(3, 1): 3},
        {Orbital(1, 0): 2, Orbital(2, 0): 1, Orbital(2, 1): 3, Orbital(3, 2): 3},
        {Orbital(1, 0): 2, Orbital(2, 0): 1, Orbital(2, 1): 3, Orbital(4, 1): 3},
        {Orbital(1, 0): 2,                   Orbital(2, 1): 4, Orbital(3, 1): 3},
        {Orbital(1, 0): 2,                   Orbital(2, 1): 4, Orbital(3, 2): 3},
        {Orbital(1, 0): 2,                   Orbital(2, 1): 4, Orbital(4, 1): 3},
    ]
)
# fmt: on


# Input file uses 1-indexing, but I didn't want to have to manually change everything!
__HARDCODED_INDEX_CONFIGURATIONS = set(
    IndexConfiguration({key - 1: value for key, value in configuration.items()})
    for configuration in [
        {1: 2, 2: 2, 3: 5},
        {1: 2, 2: 2, 3: 4, 4: 1},
        {1: 2, 2: 2, 3: 4, 5: 1},
        {1: 2, 2: 2, 3: 4, 6: 1},
        {1: 2, 2: 2, 3: 4, 7: 1},
        {1: 2, 2: 2, 3: 4, 8: 1},
        {1: 2, 2: 1, 3: 6},
        {1: 2, 3: 6, 4: 1},
        {1: 2, 3: 6, 5: 1},
        {1: 2, 3: 6, 6: 1},
        {1: 2, 3: 6, 7: 1},
        {1: 2, 3: 6, 8: 1},
        {1: 2, 2: 1, 3: 5, 4: 1},
        {1: 2, 2: 1, 3: 5, 5: 1},
        {1: 2, 2: 1, 3: 5, 6: 1},
        {1: 2, 2: 1, 3: 5, 7: 1},
        {1: 2, 2: 1, 3: 5, 8: 1},
        {1: 2, 2: 1, 3: 4, 4: 2},
        {1: 2, 2: 1, 3: 4, 5: 2},
        {1: 2, 2: 1, 3: 4, 6: 2},
        {1: 2, 2: 1, 3: 4, 7: 2},
        {1: 2, 2: 1, 3: 4, 8: 2},
        {1: 2, 2: 1, 3: 4, 4: 1, 5: 1},
        {1: 2, 2: 1, 3: 4, 4: 1, 6: 1},
        {1: 2, 2: 1, 3: 4, 4: 1, 7: 1},
        {1: 2, 2: 1, 3: 4, 4: 1, 8: 1},
        {1: 2, 2: 1, 3: 4, 5: 1, 6: 1},
        {1: 2, 2: 1, 3: 4, 5: 1, 7: 1},
        {1: 2, 2: 1, 3: 4, 5: 1, 8: 1},
        {1: 2, 2: 1, 3: 4, 6: 1, 7: 1},
        {1: 2, 2: 1, 3: 4, 6: 1, 8: 1},
        {1: 2, 2: 1, 3: 4, 7: 1, 8: 1},
        {1: 2, 3: 5, 4: 1, 5: 1},
        {1: 2, 3: 5, 4: 1, 6: 1},
        {1: 2, 3: 5, 4: 1, 7: 1},
        {1: 2, 3: 5, 4: 1, 8: 1},
        {1: 2, 3: 5, 5: 1, 6: 1},
        {1: 2, 3: 5, 5: 1, 7: 1},
        {1: 2, 3: 5, 5: 1, 8: 1},
        {1: 2, 3: 5, 6: 1, 7: 1},
        {1: 2, 3: 5, 6: 1, 8: 1},
        {1: 2, 3: 5, 7: 1, 8: 1},
        {1: 2, 3: 5, 4: 2},
        {1: 2, 3: 5, 5: 2},
        {1: 2, 3: 5, 6: 2},
        {1: 2, 3: 5, 7: 2},
        {1: 2, 3: 5, 8: 2},
        {1: 2, 2: 2, 3: 3, 4: 2},
        {1: 2, 2: 2, 3: 3, 5: 2},
        {1: 2, 2: 2, 3: 3, 6: 2},
        {1: 2, 2: 2, 3: 3, 7: 2},
        {1: 2, 2: 2, 3: 3, 8: 2},
        {1: 2, 2: 2, 3: 3, 4: 1, 5: 1},
        {1: 2, 2: 2, 3: 3, 4: 1, 6: 1},
        {1: 2, 2: 2, 3: 3, 4: 1, 7: 1},
        {1: 2, 2: 2, 3: 3, 4: 1, 8: 1},
        {1: 2, 2: 2, 3: 3, 5: 1, 6: 1},
        {1: 2, 2: 2, 3: 3, 5: 1, 7: 1},
        {1: 2, 2: 2, 3: 3, 5: 1, 8: 1},
        {1: 2, 2: 2, 3: 3, 6: 1, 7: 1},
        {1: 2, 2: 2, 3: 3, 6: 1, 8: 1},
        {1: 2, 2: 2, 3: 3, 7: 1, 8: 1},
        {1: 2, 2: 2, 3: 2, 4: 2, 5: 1},
        {1: 2, 2: 2, 3: 2, 4: 2, 6: 1},
        {1: 2, 2: 2, 3: 2, 4: 2, 7: 1},
        {1: 2, 2: 2, 3: 2, 4: 2, 8: 1},
        {1: 2, 2: 2, 3: 2, 4: 1, 5: 2},
        {1: 2, 2: 2, 3: 2, 5: 2, 6: 1},
        {1: 2, 2: 2, 3: 2, 5: 2, 7: 1},
        {1: 2, 2: 2, 3: 2, 5: 2, 8: 1},
        {1: 2, 2: 2, 3: 2, 4: 1, 6: 2},
        {1: 2, 2: 2, 3: 2, 5: 1, 6: 2},
        {1: 2, 2: 2, 3: 2, 6: 2, 7: 1},
        {1: 2, 2: 2, 3: 2, 6: 2, 8: 1},
        {1: 2, 2: 2, 3: 2, 4: 1, 7: 2},
        {1: 2, 2: 2, 3: 2, 5: 1, 7: 2},
        {1: 2, 2: 2, 3: 2, 6: 1, 7: 2},
        {1: 2, 2: 2, 3: 2, 7: 2, 8: 1},
        {1: 2, 2: 2, 3: 2, 4: 1, 8: 2},
        {1: 2, 2: 2, 3: 2, 5: 1, 8: 2},
        {1: 2, 2: 2, 3: 2, 6: 1, 8: 2},
        {1: 2, 2: 2, 3: 2, 7: 1, 8: 2},
        {1: 2, 2: 2, 3: 2, 4: 1, 5: 1, 6: 1},
        {1: 2, 2: 2, 3: 2, 4: 1, 5: 1, 7: 1},
        {1: 2, 2: 2, 3: 2, 4: 1, 5: 1, 8: 1},
        {1: 2, 2: 2, 3: 2, 4: 1, 6: 1, 7: 1},
        {1: 2, 2: 2, 3: 2, 4: 1, 6: 1, 8: 1},
        {1: 2, 2: 2, 3: 2, 4: 1, 7: 1, 8: 1},
        {1: 2, 2: 2, 3: 2, 5: 1, 6: 1, 7: 1},
        {1: 2, 2: 2, 3: 2, 5: 1, 6: 1, 8: 1},
        {1: 2, 2: 2, 3: 2, 5: 1, 7: 1, 8: 1},
        {1: 2, 2: 2, 3: 2, 6: 1, 7: 1, 8: 1},
        {1: 2, 2: 1, 3: 3, 4: 2, 5: 1},
        {1: 2, 2: 1, 3: 3, 4: 2, 6: 1},
        {1: 2, 2: 1, 3: 3, 4: 2, 7: 1},
        {1: 2, 2: 1, 3: 3, 4: 2, 8: 1},
        {1: 2, 2: 1, 3: 3, 4: 1, 5: 2},
        {1: 2, 2: 1, 3: 3, 5: 2, 6: 1},
        {1: 2, 2: 1, 3: 3, 5: 2, 7: 1},
        {1: 2, 2: 1, 3: 3, 5: 2, 8: 1},
        {1: 2, 2: 1, 3: 3, 4: 1, 6: 2},
        {1: 2, 2: 1, 3: 3, 5: 1, 6: 2},
        {1: 2, 2: 1, 3: 3, 6: 2, 7: 1},
        {1: 2, 2: 1, 3: 3, 6: 2, 8: 1},
        {1: 2, 2: 1, 3: 3, 4: 1, 7: 2},
        {1: 2, 2: 1, 3: 3, 5: 1, 7: 2},
        {1: 2, 2: 1, 3: 3, 6: 1, 7: 2},
        {1: 2, 2: 1, 3: 3, 7: 2, 8: 1},
        {1: 2, 2: 1, 3: 3, 4: 1, 8: 2},
        {1: 2, 2: 1, 3: 3, 5: 1, 8: 2},
        {1: 2, 2: 1, 3: 3, 6: 1, 8: 2},
        {1: 2, 2: 1, 3: 3, 7: 1, 8: 2},
        {1: 2, 2: 1, 3: 3, 4: 1, 5: 1, 6: 1},
        {1: 2, 2: 1, 3: 3, 4: 1, 5: 1, 7: 1},
        {1: 2, 2: 1, 3: 3, 4: 1, 5: 1, 8: 1},
        {1: 2, 2: 1, 3: 3, 4: 1, 6: 1, 7: 1},
        {1: 2, 2: 1, 3: 3, 4: 1, 6: 1, 8: 1},
        {1: 2, 2: 1, 3: 3, 4: 1, 7: 1, 8: 1},
        {1: 2, 2: 1, 3: 3, 5: 1, 6: 1, 7: 1},
        {1: 2, 2: 1, 3: 3, 5: 1, 6: 1, 8: 1},
        {1: 2, 2: 1, 3: 3, 5: 1, 7: 1, 8: 1},
        {1: 2, 2: 1, 3: 3, 6: 1, 7: 1, 8: 1},
        {1: 2, 3: 4, 4: 2, 5: 1},
        {1: 2, 3: 4, 4: 2, 6: 1},
        {1: 2, 3: 4, 4: 2, 7: 1},
        {1: 2, 3: 4, 4: 2, 8: 1},
        {1: 2, 3: 4, 4: 1, 5: 2},
        {1: 2, 3: 4, 5: 2, 6: 1},
        {1: 2, 3: 4, 5: 2, 7: 1},
        {1: 2, 3: 4, 5: 2, 8: 1},
        {1: 2, 3: 4, 4: 1, 6: 2},
        {1: 2, 3: 4, 5: 1, 6: 2},
        {1: 2, 3: 4, 6: 2, 7: 1},
        {1: 2, 3: 4, 6: 2, 8: 1},
        {1: 2, 3: 4, 4: 1, 7: 2},
        {1: 2, 3: 4, 5: 1, 7: 2},
        {1: 2, 3: 4, 6: 1, 7: 2},
        {1: 2, 3: 4, 7: 2, 8: 1},
        {1: 2, 3: 4, 4: 1, 8: 2},
        {1: 2, 3: 4, 5: 1, 8: 2},
        {1: 2, 3: 4, 6: 1, 8: 2},
        {1: 2, 3: 4, 7: 1, 8: 2},
        {1: 2, 3: 4, 4: 1, 5: 1, 6: 1},
        {1: 2, 3: 4, 4: 1, 5: 1, 7: 1},
        {1: 2, 3: 4, 4: 1, 5: 1, 8: 1},
        {1: 2, 3: 4, 4: 1, 6: 1, 7: 1},
        {1: 2, 3: 4, 4: 1, 6: 1, 8: 1},
        {1: 2, 3: 4, 4: 1, 7: 1, 8: 1},
        {1: 2, 3: 4, 5: 1, 6: 1, 7: 1},
        {1: 2, 3: 4, 5: 1, 6: 1, 8: 1},
        {1: 2, 3: 4, 5: 1, 7: 1, 8: 1},
        {1: 2, 3: 4, 6: 1, 7: 1, 8: 1},
    ]
)


def __generate_configuration(ground_state, removed, added, total_electrons):
    logging.debug("Constructing configuration: removed=%s added=%s", removed, added)
    assert len(removed) == len(added)

    configuration = ground_state.thaw()
    logging.debug("Starting configuration: %s", configuration)

    for orbital in removed:
        configuration[orbital] -= 1

    logging.debug("Configuration after removal: %s", configuration)

    assert all(electrons >= 0 for electrons in configuration.values())
    assert all(
        electrons <= orbital.capacity() for orbital, electrons in configuration.items()
    )
    assert sum(configuration.values()) == total_electrons - len(removed)

    for orbital in added:
        configuration[orbital] += 1

    logging.debug("Configuration after replacement: %s", configuration)

    assert all(electrons >= 0 for electrons in configuration.values())
    assert all(
        electrons <= orbital.capacity() for orbital, electrons in configuration.items()
    )
    assert sum(configuration.values()) == total_electrons

    return configuration.freeze()


def __excited_electrons(configuration, ground_state):
    logging.debug(
        "Calculating excited electrons: configuration=%s ground=%s",
        configuration,
        ground_state,
    )
    excited = sum(
        electrons - configuration[orbital]
        for orbital, electrons in ground_state.items()
    )
    logging.debug("Calculated excited electrons: %d", excited)
    return excited


def generate_configurations(
    orbitals,
    ground_state,
    locked_orbitals,
    forbidden_configurations,
    excited_electrons,
):
    total_electrons = sum(ground_state.values())

    holes = {
        orbital: orbital.capacity() - ground_state.get(orbital, 0)
        for orbital in orbitals
    }

    ground_state_orbitals = set(
        orbital
        for orbital, electrons in ground_state.items()
        if electrons > 0 and orbital not in locked_orbitals
    )
    hole_orbitals = set(
        orbital for orbital, electron_holes in holes.items() if electron_holes > 0
    )

    combinations = {0: [ground_state]}
    for excitations in range(1, excited_electrons + 1):
        combinations[excitations] = []

        for removed in itertools.combinations_with_replacement(
            ground_state_orbitals, excitations
        ):
            removed_by_orbital = collections.Counter(removed)
            if any(
                removed_electrons > ground_state[orbital]
                for orbital, removed_electrons in removed_by_orbital.items()
            ):
                continue

            for added in itertools.combinations_with_replacement(
                hole_orbitals, excitations
            ):
                added_by_orbital = collections.Counter(added)
                if any(
                    added_electrons > holes[orbital]
                    for orbital, added_electrons in added_by_orbital.items()
                ):
                    continue

                # Exciting an electron into a hole left by another excitation is a configuration that will have already been created using one less excitations.
                if not added_by_orbital.keys().isdisjoint(removed_by_orbital.keys()):
                    continue

                configuration = __generate_configuration(
                    ground_state, removed, added, total_electrons
                )

                assert all(
                    ground_state[orbital] == configuration[orbital]
                    for orbital in locked_orbitals
                )

                if configuration not in forbidden_configurations:
                    combinations[excitations].append(configuration)

    configurations = [
        configuration
        for configurations in combinations.values()
        for configuration in configurations
    ]

    assert len(set(configurations)) == len(configurations)
    assert all(
        __excited_electrons(configuration, ground_state) <= excited_electrons
        for configuration in configurations
    )

    return configurations


def index_configurations(configurations, orbitals):
    orbital_map = {orbital: index for index, orbital in enumerate(orbitals)}
    index_configurations = [
        IndexConfiguration(
            {
                orbital_map[orbital]: electrons
                for orbital, electrons in configuration.items()
            }
        )
        for configuration in configurations
    ]

    return index_configurations


def __check_triply_excited_configurations():
    configurations = generate_configurations(
        ORBITALS,
        GROUND_STATE_CONFIGURATION,
        LOCKED_ORBITALS,
        FORBIDDEN_CONFIGURATIONS,
        3,
    )

    indexed_configurations = index_configurations(configurations, ORBITALS)

    assert len(indexed_configurations) == len(__HARDCODED_INDEX_CONFIGURATIONS)
    assert set(indexed_configurations) == __HARDCODED_INDEX_CONFIGURATIONS


__check_triply_excited_configurations()


def generate_input(
    io,
    thresholds,
    orbitals,
    configurations,
    ground_state,
    slater_coefficients,
    experimental_thresholds,
    total_angular_momenta=9,
):
    orbital_set = set(orbitals)
    assert orbital_set.issuperset(ground_state.keys())
    assert slater_coefficients.keys() == orbital_set

    et_count = collections.Counter(e_term for e_term, _ in experimental_thresholds)
    assert et_count == thresholds

    has_experimental = any(len(js) > 0 for _, js in experimental_thresholds)
    electrons = sum(ground_state.values())
    input_nml = {
        "inorb": len(orbitals),
        "lrgle2": total_angular_momenta,
        "maxorb": len(orbitals),
        "ncfg": len(configurations),
        "nelc": electrons,
        "nexp": 1 if has_experimental else 0,
        "nset": len(thresholds),
        "nz": electrons + 1,
        "bsto": 0.0,
        "buttle": True,
        "ek2max": 2.5504287,
        "farm": True,
        "ibop": 0,
        "ihbug": [None, 0, None, None, None, 0, None, None, 0],
        "lfixn": 20,
        "lrgld1": 20,
        "lrgld2": -1,
        "lrgle1": 0,
        "lrgslo": 1,
        "lrgsup": 1,
        "mpol": 1,
        "ndiag": 1,
        "nrang2": 40,
        "nix": -1,
        "ra": 20.0,
        "restart": False,
        "xdr": False,
    }

    f90nml.write({"input": input_nml}, io)

    for term, n in thresholds.items():
        print(f" {term.L:2d} {term.multiplicity:2d} {term.parity:2d}  {n:2d}", file=io)

    for orbital in orbitals:
        print(f"  {orbital.n:2d} {orbital.l:2d}", end="", file=io)

    print(file=io)
    print("0 0 0", file=io)

    indexed = index_configurations(configurations, orbitals)
    for iconfs in batched(indexed, 24):
        iconfs = [f"{len(iconf):2d}" for iconf in iconfs]
        print(" ", *iconfs, file=io)

    for iconf in indexed:
        indices, electrons = zip(*sorted(iconf.items()))
        indices = [f"{i + 1:2d}" for i in indices]
        electrons = [f"{e:2d}" for e in electrons]

        print("", *indices, file=io)
        print("", *electrons, file=io)

    max_n = {}
    for orbital in orbitals:
        if orbital.l not in max_n:
            max_n[orbital.l] = orbital.n
        else:
            max_n[orbital.l] = max(max_n[orbital.l], orbital.n)

    def first(l):
        return l[0]

    max_n = [f"{n:2d}" for _, n in sorted(max_n.items(), key=first)]
    print("", *max_n, file=io)
    # This is not a typo, unless you know what you're doing you want two of the same line.
    print("", *max_n, file=io)

    def l_then_n(orbital):
        try:
            return (orbital.l, orbital.n)
        except AttributeError:
            return (orbital[0].l, orbital[0].n)

    for _, coefficients in sorted(slater_coefficients.items(), key=l_then_n):
        print("", len(coefficients), file=io)

        a = [f"{s.a:10d}" for s in coefficients]
        print("", *a, file=io)

        n = [f"{s.n:10.5f}" for s in coefficients]
        print("", *n, file=io)

        N = [f"{s.N:10.5f}" for s in coefficients]
        print("", *N, file=io)

    for _, os in itertools.groupby(sorted(orbitals, key=l_then_n), key=lambda o: o.l):
        occupancy = [ground_state.get(o, 0) for o in os]
        occ_strs = [f"{occ:2d}" for occ in occupancy]
        print("", *occ_strs, file=io)

    if has_experimental:
        # If there are duplicated thresholds then you need two separate entries for that threshold with the higher energy one later in the list.
        # For reference see RMatrixII (commit a59d1044) source/ham/stghrd.f90 lines 674-695 (`observed_levels` loop).
        print("", len(experimental_thresholds), file=io)

        for term, js in experimental_thresholds:
            adjust = 1 if len(js) > 0 else 0
            print(
                f" {term.L:2d} {term.multiplicity:2d} {term.parity:2d}  {adjust}",
                file=io,
            )

        for term, js in experimental_thresholds:
            if len(js) == 0:
                continue

            print("", len(js), file=io)
            for per_cm, j in js:
                print(f" {per_cm:14.6f} {j:4.1f}", file=io)

    print(file=io)


def __generate_6_threshold_input(io, excitations):
    experimental_thresholds = EXPERIMENTAL_THRESHOLDS[:6]
    thresholds = {term: 1 for term, _ in experimental_thresholds}
    generate_input(
        io,
        thresholds,
        ORBITALS,
        generate_configurations(
            ORBITALS,
            GROUND_STATE_CONFIGURATION,
            LOCKED_ORBITALS,
            FORBIDDEN_CONFIGURATIONS,
            excitations,
        ),
        GROUND_STATE_CONFIGURATION,
        SLATER_COEFFICIENTS,
        experimental_thresholds,
    )


def generate_6_threshold_input(io_or_path, excitations=3):
    if isinstance(io_or_path, str):
        with open(io_or_path, "w") as io:
            __generate_6_threshold_input(io, excitations)
    else:
        __generate_6_threshold_input(io_or_path, excitations)


def __generate_11_threshold_input(io, excitations):
    thresholds = collections.Counter(term for term, _ in EXPERIMENTAL_THRESHOLDS)
    generate_input(
        io,
        thresholds,
        ORBITALS,
        generate_configurations(
            ORBITALS,
            GROUND_STATE_CONFIGURATION,
            LOCKED_ORBITALS,
            FORBIDDEN_CONFIGURATIONS,
            excitations,
        ),
        GROUND_STATE_CONFIGURATION,
        SLATER_COEFFICIENTS,
        EXPERIMENTAL_THRESHOLDS,
    )


def generate_11_threshold_input(io_or_path, excitations=3):
    if isinstance(io_or_path, str):
        with open(io_or_path, "w") as io:
            __generate_11_threshold_input(io, excitations)
    else:
        __generate_11_threshold_input(io_or_path, excitations)
