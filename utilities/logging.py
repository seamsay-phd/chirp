import logging
import os
import sys
from logging import critical, debug, error, info, warning

LOG_LEVEL_MAP = {
    "ALL": logging.DEBUG - 1,
    "DEBUG": logging.DEBUG,
    "INFO": logging.INFO,
    "WARN": logging.WARN,
    "ERROR": logging.ERROR,
    "CRITICAL": logging.CRITICAL,
    "DISABLE": logging.CRITICAL + 1,
}

logging.basicConfig(
    level=LOG_LEVEL_MAP[os.environ.get("LOG_LEVEL", "INFO").upper()],
    format="{asctime} {levelname:>8s} {message}",
    style="{",
    stream=sys.stderr,
)
