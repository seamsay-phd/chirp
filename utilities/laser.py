import json
import sys
import typing

import numpy as np
import scipy as sp

from . import dimensions as dim
from .constants import (
    NIR_CYCLES,
    XUV_CYCLES,
    XUV_INTENSITY_FACTOR,
    DELTA_T_au,
    NIR_INTENSITY_100_TW_per_cm2,
)


def jsonify(file, obj):
    with open(file, "w") as io:
        json.dump(obj.serialise(), io, indent=4, sort_keys=True)
        print(file=io)


class PulseData:
    def __init__(
        self,
        amplitude: dim.EFieldAmplitude,
        energy: dim.PhotonEnergy,
        chirp: dim.Chirp,
        start: dim.Time,
        peak: dim.Time,
        end: dim.Time,
        fwhm: dim.Time,
        phase: dim.Angle = dim.Angle.from_pi(0.0),
    ):
        self.amplitude = amplitude
        self.intensity = amplitude.intensity()

        self.energy = energy
        self.period = energy.period()
        self.wavelength = energy.wavelength()

        self.start = start
        self.peak = peak
        self.end = end
        self.fwhm = fwhm
        self.duration = dim.Time.from_au(end.to_au() - start.to_au())

        self.phase = phase

        self.chirp = chirp
        if self.chirp.to_au() == 0.0:
            self.chirped_start = self.start
            self.chirped_end = self.end
            self.chirped_duration = self.duration
        else:
            raise Exception("Chirped pulses no longer supported!")

            # Doubled as you only want one peak of the envelope.
            T = 2 * self.duration.to_au()
            c = self.chirp.to_au()

            # t0 = t - peak
            # f = 1/T
            # envelope = cos(2pi * f * t0)^2 .
            # envelope = 0 when 2pi * f * t = pi (n + 1/2) .
            # After chirping f = (1 + c * t0) / T .

            # First 0 at +ve t0 occurs when n = 0 , so 2pi * f * t0 = pi/2 .
            # 2pi * (1 + c * t0) * t0 / T = pi / 2
            # c * t0^2 + t0 - T/4 = 0 .
            # t0 = (-1 ± sqrt(1 + c * T)) / 2c .
            # Specified +ve t0, so t0 = (-1 + sqrt(1 + c * T)) / 2c .

            # First 0 at -ve t0 occurs when n = -1, so 2pi * f * 10 = -pi/2 .
            # c t0^2 + t0 + T/4 = 0 .
            # t0 = (-1 ± sqrt(1 - c * T)) / 2c .
            # For all valid choices of c, sqrt(1 - c * T) >= 0.
            # We want the _first_ negative t0, so choose the +ve choice.
            # Therefore t0 = (-1 + sqrt(1 - c * T)) / 2c .

            f = 1 / T
            if abs(c) > f:
                raise ValueError(
                    f"Chirp ({c:.6f}au) must not be greater than the frequency ({f:.6f}au) of the pulse envelope."
                )

            self.chirped_start = dim.Time.from_au(
                self.peak.to_au() + (-1 + np.sqrt(1 - c * T)) / (2 * c)
            )
            self.chirped_end = dim.Time.from_au(
                self.peak.to_au() + (-1 + np.sqrt(1 + c * T)) / (2 * c)
            )

            self.chirped_duration = dim.Time.from_au(
                self.chirped_end.to_au() - self.chirped_start.to_au()
            )

    def spectrum(self):
        end = self.end.to_au() * 100
        n = 1000000
        delta_t = end / n

        t = np.linspace(0, end, n)
        field = self.pulse(t, dim.Time.from_au(delta_t))

        E = dim.Energy.from_au(sp.fft.rfftfreq(t.size, delta_t) * 2 * np.pi).to_eV()
        s = delta_t * sp.fft.rfft(field)

        return E, s

    def serialise(self, delta_t: dim.Time):
        return {
            key: value.serialise(delta_t=delta_t, period=self.period)
            for key, value in vars(self).items()
        }


class PulseDataSineSq(PulseData):
    def pulse(self, t_au, delta_t):
        s = round(self.start.to_steps(delta_t))
        e = round(self.end.to_steps(delta_t)) + 1
        tc = t_au - self.peak.to_au()

        if self.chirp.to_au() == 0.0:
            s = round(self.start.to_steps(delta_t))
            e = round(self.end.to_steps(delta_t)) + 1

            m0 = (s + e) // 2
            m_1 = m0 - 1
            m1 = m0 + 1
            am = np.argmin(np.abs(tc))
            assert am == m_1 or am == m0 or am == m1
        else:
            s = round(self.chirped_start.to_steps(delta_t))
            e = round(self.chirped_end.to_steps(delta_t)) + 1

            s0 = s
            s_1 = s - 1
            s1 = s + 1
            am = np.argmin(np.abs(t_au - self.chirped_start.to_au()))
            assert am == s_1 or am == s0 or am == s1

            e0 = e
            e_1 = e - 1
            e1 = e + 1
            am = np.argmin(np.abs(t_au - self.chirped_end.to_au()))
            assert am == e_1 or am == e0 or am == e1

        c = 1 + self.chirp.to_au() * tc

        Te = 2 * self.duration.to_au()
        we = c * 2 * np.pi / Te
        assert np.all(we[s:e] > 0)

        wp = c * self.energy.to_au()
        assert np.all(wp[s:e] > 0)

        # NOTE: Chirp is not currently supported.
        assert np.allclose(we, 2 * np.pi / Te)
        assert np.allclose(wp, self.energy.to_au())

        E0 = self.amplitude.to_au()

        Ee = np.zeros_like(tc)
        Ee[s:e] += np.cos(we[s:e] * tc[s:e]) ** 2

        Ep = np.zeros_like(tc)
        Ep[s:e] += np.cos(wp[s:e] * tc[s:e])

        return E0 * Ee * Ep


class PulseDataGaussian(PulseData):
    @staticmethod
    def earliest_peak(
        envelope_amplitude: dim.EFieldAmplitude,
        efield_amplitude: dim.EFieldAmplitude,
        fwhm: dim.Time,
    ):
        # The amplitude of the envelope \(A\) at some time \(t\) given the field strength \(E_0\), FWHM \(w\), and peak \(t_0\), is
        # \[A = E_0 \exp \left(-4 \ln 2 {\left(\frac{t - t_0}{w}\right)}^2\right)  \,.\]
        # This means that for the envelope to have dropped to a given amplitude at a given time, the peak must be at
        # \[t_0 = t + w \sqrt{\frac{\ln \frac{A}{E_0}}{-4 \ln 2}}  \,.\]
        A = envelope_amplitude.to_au()
        E0 = efield_amplitude.to_au()
        w = fwhm.to_au()
        t0 = w * np.sqrt(np.log(A / E0) / (-4 * np.log(2)))
        return dim.Time.from_au(t0)

    def pulse(self, t_au, delta_t):
        e = round(self.end.to_steps(delta_t))
        # Since the gaussian pulses always start at t=0, define the tail based on the peak and the end.
        tail = e - round(0.1 * (e - self.peak.to_steps(delta_t)))

        tc = t_au - self.peak.to_au()
        wp = self.energy.to_au()
        fwhm = self.fwhm.to_au()

        E0 = self.amplitude.to_au()

        E = np.zeros_like(tc)
        E[:e] = (
            E0
            * np.exp(-4 * np.log(2) * (tc[:e] / fwhm) ** 2)
            * np.cos(wp * tc[:e] - self.phase.to_rad())
        )

        window = np.blackman(2 * (e - tail))
        window = window[e - tail :]
        E[tail:e] *= window

        return E


class PulseDataArbitrary(PulseData):
    def __init__(self, t, E, amplitude, **kwargs):
        self.spline = sp.interpolate.make_interp_spline(t.to_au(), E, bc_type="clamped")
        self.amplitude = amplitude

        self.serialisable = ["amplitude"]
        for k, v in kwargs.items():
            self.serialisable.append(k)
            setattr(self, k, v)

    def pulse(self, t_au, _):
        return self.amplitude.to_au() * self.spline(t_au)

    def serialise(self, delta_t):
        return {
            key: getattr(self, key).serialise(delta_t=delta_t, period=self.period)
            for key in self.serialisable
        }


class EFieldData:
    delta_t: dim.Time = dim.Time.from_au(DELTA_T_au)

    def t(self, final_t: dim.Time):
        steps_per_run = final_t.to_steps(self.delta_t)
        return np.arange(steps_per_run) * self.delta_t.to_au()

    def e_field(self, final_t: dim.Time):
        t = self.t(final_t)
        x = np.zeros_like(t)
        y = np.zeros_like(t)
        z = np.zeros_like(t)

        field_start = self.start().to_steps(self.delta_t)
        # E field must be greater than zero at first time step.
        z[: round(field_start) + 1] += 1e-90

        z += self.pulse(t)

        return np.vstack([t, x, y, z]).T

    def write_inp(self, file, final_t):
        np.savetxt(file, self.e_field(final_t))


class EFieldDataXUVOnly(EFieldData):
    xuv: PulseData

    def __init__(
        self,
        xuv: PulseData,
    ):
        self.xuv = xuv

    @classmethod
    def sine_squared(
        cls,
        energy: dim.PhotonEnergy,
        duration: dim.Time,
        intensity: dim.EFieldIntensity = dim.EFieldIntensity.from_100_TW_per_cm2(
            NIR_INTENSITY_100_TW_per_cm2 * XUV_INTENSITY_FACTOR
        ),
    ):
        start = dim.Time.from_au(cls.delta_t.to_au() * 10)
        end = dim.Time.from_au(start.to_au() + duration.to_au())
        peak = dim.Time.from_au((start.to_au() + end.to_au()) / 2)
        fwhm = dim.Time.from_au(duration.to_au() * 3 / 4)
        return cls(
            PulseDataSineSq(
                intensity.amplitude(),
                energy,
                dim.Chirp.from_au(0.0),
                start,
                peak,
                end,
                fwhm,
            )
        )

    @classmethod
    def gaussian(
        cls,
        energy: dim.Energy,
        fwhm: typing.Optional[dim.Time] = None,
        intensity: dim.EFieldIntensity = dim.EFieldIntensity.from_100_TW_per_cm2(
            NIR_INTENSITY_100_TW_per_cm2 * XUV_INTENSITY_FACTOR
        ),
    ):
        if fwhm is None:
            fwhm = dim.Time.from_au(
                dim.Time.from_cycles(XUV_CYCLES, energy.period()).to_au() / 2
            )

        peak = PulseDataGaussian.earliest_peak(
            dim.EFieldAmplitude.from_au(1e-32), intensity.amplitude(), fwhm
        )

        start = dim.Time.from_au(0.0)
        end = dim.Time.from_au(2 * peak.to_au())

        return cls(
            PulseDataGaussian(
                intensity.amplitude(),
                energy,
                dim.Chirp.from_au(0.0),
                start,
                peak,
                end,
                fwhm,
            )
        )

    def start(self):
        return self.xuv.start

    def duration(self):
        return self.xuv.duration

    def pulse(self, t):
        return self.xuv.pulse(t, self.delta_t)

    def dirname(self):
        return [f"xuv={self.xuv.energy.to_eV():.3f}eV"]

    def jobname(self):
        return "xuv-only"

    def serialise(self):
        return {"XUV": self.xuv.serialise(self.delta_t)}


class EFieldDataNIRAndXUV(EFieldData):
    nir: PulseData
    xuv: PulseData

    def __init__(self, nir: PulseData, xuv: PulseData):
        self.nir = nir
        self.xuv = xuv

    @classmethod
    def sine_squ(
        cls,
        nir_energy: dim.PhotonEnergy,
        xuv_energy: dim.PhotonEnergy,
        delay: dim.Time,
        nir_intensity: dim.EFieldIntensity = dim.EFieldIntensity.from_100_TW_per_cm2(
            NIR_INTENSITY_100_TW_per_cm2
        ),
        xuv_intensity: dim.EFieldIntensity = dim.EFieldIntensity.from_100_TW_per_cm2(
            NIR_INTENSITY_100_TW_per_cm2 * XUV_INTENSITY_FACTOR
        ),
        nir_duration: typing.Optional[dim.Time] = None,
        xuv_duration: typing.Optional[dim.Time] = None,
        nir_chirp: dim.Chirp = dim.Chirp.from_au(0.0),
    ):
        def fwhm(duration):
            return dim.Time.from_au(duration.to_au() * 3 / 4)

        if nir_duration is None:
            nir_duration = dim.Time.from_cycles(NIR_CYCLES, nir_energy.period())

        if xuv_duration is None:
            xuv_duration = dim.Time.from_cycles(XUV_CYCLES, xuv_energy.period())

        xuv_fwhm = fwhm(xuv_duration)
        nir_fwhm = fwhm(nir_duration)

        nir_start = dim.Time.from_fs(0.01 + max(0.0, delay.to_fs()))

        nir_peak = dim.Time.from_au(nir_start.to_au() + nir_duration.to_au() / 2)
        xuv_peak = dim.Time.from_au(nir_peak.to_au() - delay.to_au())

        xuv_start = dim.Time.from_au(xuv_peak.to_au() - xuv_duration.to_au() / 2)
        nir_end = dim.Time.from_au(nir_start.to_au() + nir_duration.to_au())
        xuv_end = dim.Time.from_au(xuv_start.to_au() + xuv_duration.to_au())

        return cls(
            PulseDataSineSq(
                nir_intensity.amplitude(),
                nir_energy,
                nir_chirp,
                nir_start,
                nir_peak,
                nir_end,
                nir_fwhm,
            ),
            PulseDataSineSq(
                xuv_intensity.amplitude(),
                xuv_energy,
                dim.Chirp.from_au(0.0),
                xuv_start,
                xuv_peak,
                xuv_end,
                xuv_fwhm,
            ),
        )

    @classmethod
    def gaussian(
        cls,
        nir_energy: dim.PhotonEnergy,
        xuv_energy: dim.PhotonEnergy,
        delay: dim.Time,
        nir_intensity: dim.EFieldIntensity = dim.EFieldIntensity.from_100_TW_per_cm2(
            NIR_INTENSITY_100_TW_per_cm2
        ),
        xuv_intensity: dim.EFieldIntensity = dim.EFieldIntensity.from_100_TW_per_cm2(
            NIR_INTENSITY_100_TW_per_cm2 * XUV_INTENSITY_FACTOR
        ),
        nir_fwhm: typing.Optional[dim.Time] = None,
        xuv_fwhm: typing.Optional[dim.Time] = None,
        nir_phase: dim.Angle = dim.Angle.from_pi(0.0),
        xuv_phase: dim.Angle = dim.Angle.from_pi(0.0),
    ):
        if nir_fwhm is None:
            nir_fwhm = dim.Time.from_cycles(NIR_CYCLES / 2, nir_energy.period())
        if xuv_fwhm is None:
            xuv_fwhm = dim.Time.from_cycles(XUV_CYCLES / 2, xuv_energy.period())

        nir_half_duration = PulseDataGaussian.earliest_peak(
            dim.EFieldAmplitude.from_au(1e-16), nir_intensity.amplitude(), nir_fwhm
        )
        xuv_half_duration = PulseDataGaussian.earliest_peak(
            dim.EFieldAmplitude.from_au(1e-32), xuv_intensity.amplitude(), xuv_fwhm
        )

        # At large positive delays (XUV first) we want to ensure the full XUV pulse is included.
        nir_peak = dim.Time.from_au(
            max(nir_half_duration.to_au(), delay.to_au() + xuv_half_duration.to_au())
        )
        xuv_peak = dim.Time.from_au(nir_peak.to_au() - delay.to_au())

        nir_start = xuv_start = dim.Time.from_au(0.0)
        nir_end = xuv_end = dim.Time.from_au(
            max(
                nir_peak.to_au() + nir_half_duration.to_au(),
                xuv_peak.to_au() + xuv_half_duration.to_au(),
            )
        )

        return cls(
            PulseDataGaussian(
                nir_intensity.amplitude(),
                nir_energy,
                dim.Chirp.from_au(0.0),
                nir_start,
                nir_peak,
                nir_end,
                nir_fwhm,
                nir_phase,
            ),
            PulseDataGaussian(
                xuv_intensity.amplitude(),
                xuv_energy,
                dim.Chirp.from_au(0.0),
                xuv_start,
                xuv_peak,
                xuv_end,
                xuv_fwhm,
                xuv_phase,
            ),
        )

    def delay(self):
        return dim.Time.from_au(self.nir.peak.to_au() - self.xuv.peak.to_au())

    def start(self):
        return dim.Time.from_au(min(self.nir.start.to_au(), self.xuv.start.to_au()))

    def duration(self):
        return dim.Time.from_au(
            max(self.nir.end.to_au(), self.xuv.end.to_au())
            - min(self.nir.start.to_au(), self.xuv.start.to_au())
        )

    def pulse(self, t):
        return self.nir.pulse(t, self.delta_t) + self.xuv.pulse(t, self.delta_t)

    def serialise(self):
        return {
            "NIR": self.nir.serialise(self.delta_t),
            "XUV": self.xuv.serialise(self.delta_t),
        }

    def dirname(self):
        directories = []
        if self.nir.chirp.to_au() != 0.0:
            directories.append(f"chirp={self.nir.chirp.to_eV():.6f}au")

        directories.append(
            f"xuv={self.xuv.energy.to_eV():.3f}eV-nir={self.nir.energy.to_eV():.3f}eV"
        )
        directories.append(f"int={self.nir.intensity.to_100_TW_per_cm2():.3f}")

        delay_fs = self.delay().to_fs()

        if sys.version_info.major > 3 or (
            sys.version_info.major == 3 and sys.version_info.minor >= 11
        ):
            directories.append(f"delay={delay_fs:+z07.3f}fs")
        else:
            if delay_fs == -0.0:
                delay_fs = 0.0

            directories.append(f"delay={delay_fs:+07.3f}fs")

        return directories

    def jobname(self):
        return "TODO-jobname"

    def serialise(self):
        return {
            "NIR": self.nir.serialise(self.delta_t),
            "XUV": self.xuv.serialise(self.delta_t),
        }


class EFieldData29thHarmonic(EFieldDataNIRAndXUV):
    @classmethod
    def sine_squ_29_harm(self, xuv_energy: dim.PhotonEnergy, delay: dim.Time, **kwargs):
        nir_energy = dim.PhotonEnergy.from_au(xuv_energy.to_au() / 29)

        # As a rule of thumb XUV pulse is at maximum a quarter of the IR period.
        max_xuv_duration = dim.Time.from_au(nir_energy.period().to_au() / 4)
        xuv_cycles = int(
            np.floor(max_xuv_duration.to_au() / xuv_energy.period().to_au())
        )

        return super().sine_squ(
            nir_energy,
            xuv_energy,
            delay,
            xuv_duration=dim.Time.from_cycles(xuv_cycles, xuv_energy.period()),
            **kwargs,
        )

    def jobname(self):
        name = f"x={self.xuv.energy.to_eV():.3f}-d={self.delay().to_fs():+06.3f}"
        assert len(name) <= 18, name
        return name


class EFieldDataExactNIR(EFieldDataNIRAndXUV):
    @classmethod
    def sine_squ_exact_nir(
        cls,
        nir_energy: dim.PhotonEnergy,
        xuv_energy: dim.PhotonEnergy,
        delay: dim.Time,
        **kwargs,
    ):
        return super().sine_squ(
            nir_energy,
            xuv_energy,
            delay,
            **kwargs,
        )

    def jobname(self):
        name = f"n={self.nir.energy.to_eV():.3f}-d={self.delay().to_fs():+06.3f}"
        assert len(name) <= 18, name
        return name
