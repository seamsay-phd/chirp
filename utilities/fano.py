import typing

import numpy as np
import scipy as sp
import scipy.optimize
from matplotlib import pyplot as plt

E_NIR_MAX = 4


class FanoParameters(typing.NamedTuple):
    centre: float
    strength_per_width: float
    phase: float
    width: float

    @property
    def strength(self):
        return self.strength_per_width * self.width


class Fano(FanoParameters):
    def lineshape(self, E):
        E0 = self.centre
        z = self.strength_per_width
        p = self.phase
        G = self.width

        return (
            E
            * z
            * G
            * (G * np.cos(p) / 2 - (E - E0) * np.sin(p))
            / ((E - E0) ** 2 + G**2 / 4)
        )


# Includes 2p(4)3s3p and 2s2p(6)4p resonances.
FIT_ENERGY_RANGE_LOW_eV = 44.0
FIT_ENERGY_RANGE_HIGH_eV = 47.2

RESONANCE_2s3p = 45.146
EXPECTED_LINEWIDTH_2s3p = 0.0487
LOWER_2s3p = FanoParameters(np.nan, 1e-4, -np.pi, 0.5 * EXPECTED_LINEWIDTH_2s3p)
INITIAL_2s3p = Fano(RESONANCE_2s3p, 3e-3, 0, EXPECTED_LINEWIDTH_2s3p)
UPPER_2s3p = FanoParameters(np.nan, np.inf, np.pi, 2 * EXPECTED_LINEWIDTH_2s3p)

RESONANCE_2p43s3p = 44.790
LOWER_2p43s3p = FanoParameters(np.nan, 1e-4, -np.pi, 0.05)
INITIAL_2p43s3p = Fano(RESONANCE_2p43s3p, 1e-3, 1.2, 0.055)
UPPER_2p43s3p = FanoParameters(np.nan, 1e-2, np.pi, 0.06)

RESONANCE_2s4p = 46.746
LOWER_2s4p = FanoParameters(np.nan, 1e-4, -np.pi, 0.03)
INITIAL_2s4p = Fano(RESONANCE_2s4p, 1e-3, 1.0, 0.04)
UPPER_2s4p = FanoParameters(np.nan, 1e-2, np.pi, 0.05)

LOWER_BG = FanoParameters(44.95, 1e-90, 0.0, 0.6)
INITIAL_BG = Fano(45.25, 1e-10, 1.5, 0.7)
UPPER_BG = FanoParameters(45.4, 1e-2, np.pi / 2, 1.1)


def lineshape(energy, o_m, o_c, *fanos: Fano):
    shape = np.zeros_like(energy)

    for f in fanos:
        shape += f.lineshape(energy)

    return shape - (o_m * energy + o_c)


def fit(energy, spectrum):
    def f(
        E,
        offset,
        s_2s3p,
        p_2s3p,
        w_2s3p,
        s_2p43s3p,
        p_2p43s3p,
        w_2p43s3p,
        s_2s4p,
        p_2s4p,
        w_2s4p,
        c_bg,
        s_bg,
        p_bg,
        w_bg,
    ):
        fano_2s3p = Fano(RESONANCE_2s3p, s_2s3p, p_2s3p, w_2s3p)
        fano_2p43s3p = Fano(RESONANCE_2p43s3p, s_2p43s3p, p_2p43s3p, w_2p43s3p)
        fano_2s4p = Fano(RESONANCE_2s4p, s_2s4p, p_2s4p, w_2s4p)
        fano_bg = Fano(c_bg, s_bg, p_bg, w_bg)
        return lineshape(E, offset, fano_2s3p, fano_2p43s3p, fano_2s4p, fano_bg)

    p0 = [0, *INITIAL_2s3p[1:], *INITIAL_2p43s3p[1:], *INITIAL_2s4p[1:], *INITIAL_BG]
    lbounds = [-1, *LOWER_2s3p[1:], *LOWER_2p43s3p[1:], *LOWER_2s4p[1:], *LOWER_BG]
    ubounds = [1, *UPPER_2s3p[1:], *UPPER_2p43s3p[1:], *UPPER_2s4p[1:], *UPPER_BG]

    (
        offset,
        s_2s3p,
        p_2s3p,
        w_2s3p,
        s_2p43s3p,
        p_2p43s3p,
        w_2p43s3p,
        s_2s4p,
        p_2s4p,
        w_2s4p,
        c_bg,
        s_bg,
        p_bg,
        w_bg,
    ), covariance = scipy.optimize.curve_fit(
        f,
        energy,
        spectrum,
        p0,
        bounds=(lbounds, ubounds),
        maxfev=999999,
    )

    errors = np.sqrt(np.diag(covariance))
    (
        e_offset,
        e_s_2s3p,
        e_p_2s3p,
        e_w_2s3p,
        e_s_2p43s3p,
        e_p_2p43s3p,
        e_w_2p43s3p,
        e_s_2s4p,
        e_p_2s4p,
        e_w_2s4p,
        e_c_bg,
        e_s_bg,
        e_p_bg,
        e_w_bg,
    ) = errors

    return (
        (offset, e_offset),
        (
            Fano(RESONANCE_2s3p, s_2s3p, p_2s3p, w_2s3p),
            FanoParameters(0.0, e_s_2s3p, e_p_2s3p, e_w_2s3p),
        ),
        (
            Fano(RESONANCE_2p43s3p, s_2p43s3p, p_2p43s3p, w_2p43s3p),
            FanoParameters(0.0, e_s_2p43s3p, e_p_2p43s3p, e_w_2p43s3p),
        ),
        (
            Fano(RESONANCE_2s4p, s_2s4p, p_2s4p, w_2s4p),
            FanoParameters(0.0, e_s_2s4p, e_p_2s4p, e_w_2s4p),
        ),
        (Fano(c_bg, s_bg, p_bg, w_bg), FanoParameters(e_c_bg, e_s_bg, e_p_bg, e_w_bg)),
    )


def fit_and_plot_resonance(energy, spectrum, debug=False):
    (offset, _), (fano_2s3p, _), (fano_2p43s3p, _), (fano_2s4p, _), (fano_bg, _) = fit(
        energy, spectrum
    )

    output = []

    if debug:
        output.append(
            f"{fano_2p43s3p.centre:.3f} {fano_2p43s3p.strength_per_width:.6f} {fano_2p43s3p.phase:+.3f} {fano_2p43s3p.width:.3f}"
        )

    output.append(
        f"{fano_2s3p.centre:.3f} {fano_2s3p.strength_per_width:.6f} {fano_2s3p.phase:+.3f} {fano_2s3p.width:.3f}"
    )
    output.append(
        f"{fano_bg.centre:.3f} {fano_bg.strength_per_width:.6f} {fano_bg.phase:+.3f} {fano_bg.width:.3f}"
    )

    if debug:
        output.append(
            f"{fano_2s4p.centre:.3f} {fano_2s4p.strength_per_width:.6f} {fano_2s4p.phase:+.3f} {fano_2s4p.width:.3f}"
        )

    print(*output, f"{offset:+.3f}", sep=" | ")

    return plot_resonance_fit(
        energy, spectrum, offset, fano_2s3p, fano_2p43s3p, fano_2s4p, fano_bg, debug
    )


def plot_resonance_fit(
    energy, spectrum, offset, fano_2s3p, fano_2p43s3p, fano_2s4p, fano_bg, debug=False
):
    fig, [ax_c, ax_s] = plt.subplots(1, 2)

    ax_c.plot(energy, spectrum, label="Data")
    ax_c.plot(
        energy,
        lineshape(energy, offset, fano_2s3p, fano_2p43s3p, fano_2s4p, fano_bg),
        label="Fit",
    )

    ax_s.plot(energy, spectrum, label="Data")
    ax_s.plot(energy, fano_2s3p.lineshape(energy) - offset, label="2s2p(6)3p")
    ax_s.plot(energy, fano_bg.lineshape(energy) - offset, label="Unknown")
    if debug:
        ax_s.plot(energy, fano_2p43s3p.lineshape(energy) - offset, label="2p(4)3s3p")
        ax_s.plot(energy, fano_2s4p.lineshape(energy) - offset, label="2s2p(6)4p")

    ax_c.legend()
    ax_s.legend()

    fig.supxlabel("Energy (eV)")

    return fig


def background_spectrum(
    E, photon_spectrum, cross_section, *fanos, absorptions=1, emissions=1
):
    convolution_grid = np.linspace(0, 100, 10000)

    nir_energy = convolution_grid[convolution_grid <= E_NIR_MAX]
    nir_spectrum = cross_section * photon_spectrum(nir_energy)

    od_energy = convolution_grid
    od_spectrum = lineshape(od_energy, 0.0, 0.0, *fanos)

    spectrum = od_spectrum

    for _ in range(absorptions):
        spectrum = sp.signal.convolve(spectrum, nir_spectrum)

    for _ in range(emissions):
        spectrum = sp.signal.convolve(spectrum, nir_spectrum[::-1])

    spectrum = spectrum[
        emissions * (nir_spectrum.size - 1) : -absorptions * (nir_spectrum.size - 1)
    ]
    assert (
        spectrum.size == convolution_grid.size
    ), f"{spectrum.size} == {convolution_grid.size}"

    return sp.interpolate.make_interp_spline(convolution_grid, spectrum)(E)
