import os
import subprocess

from utilities.rmatrixii import input

STAGE_DIR = os.path.abspath(os.path.dirname(__file__))
PROJ_DIR = os.path.dirname(STAGE_DIR)

INPUT_FILE_NAME = "neon.inp"


def run(excitations):
    directory = os.path.join(STAGE_DIR, f"excitations={excitations}", "RMatrixII")
    if os.path.exists(directory):
        return

    os.makedirs(directory)

    input_file = os.path.join(directory, INPUT_FILE_NAME)
    with open(input_file, "w") as io:
        input.generate_6_threshold_input(
            io,
            excitations=excitations,
        )

    subprocess.run(
        [
            os.path.join(PROJ_DIR, "rmatrixii.bash"),
            os.path.join(PROJ_DIR, "RMatrixII", "build", "bin"),
            input_file,
            directory,
            os.path.join(directory, "rad.out"),
            os.path.join(directory, "ang.out"),
            os.path.join(directory, "ham.out"),
        ],
        check=True,
    )


def main():
    run(1)
    run(2)


if __name__ == "__main__":
    main()
