import itertools
import os

import numpy as np
from farm_utilities import phase as farm

import utilities.farm

STAGE_DIR = os.path.dirname(os.path.abspath(__file__))
PROJ_DIR = os.path.dirname(STAGE_DIR)


def dirname(
    _energy_low_eV, _energy_high_eV, _energy_step_eV, multiplicity, azimuthal, parity
):
    return f"s={multiplicity:02}-l={azimuthal:02}-p={parity:02}"


def run_read_find(
    excitation_dir,
    energy_low_eV,
    energy_high_eV,
    energy_step_eV,
    multiplicity,
    azimuthal,
    parity,
    ionisation_energy_eV,
    interest_low_eV=None,
    interest_high_eV=None,
    overwrite=False,
):
    excitation_dir = os.path.relpath(excitation_dir)
    outdir = os.path.join(
        excitation_dir,
        "FARM",
        dirname(
            energy_low_eV,
            energy_high_eV,
            energy_step_eV,
            multiplicity,
            azimuthal,
            parity,
        ),
    )
    data_dir = os.path.join(excitation_dir, "RMatrixII")
    h_file = os.path.join(data_dir, "H")

    files = {
        key: os.path.join(outdir, name) for key, name in utilities.farm.FILES.items()
    }
    files["H"] = h_file

    if not os.path.exists(outdir):
        os.makedirs(outdir)

        conf = utilities.farm.run(
            utilities.farm.configuration(
                energy_low_eV,
                energy_high_eV,
                energy_step_eV,
                multiplicity,
                azimuthal,
                parity,
                ionisation_energy_eV=ionisation_energy_eV,
                files=files,
            ),
            overwrite=overwrite,
            executable=os.path.join(PROJ_DIR, "FARM", "build", "bin", "farm"),
            files=files,
        )

        with open(os.path.join(outdir, "cpc.d"), "w") as io:
            print(conf, file=io)

    energies_eV, phases = farm.read_phase_data(files["phase"], ionisation_energy_eV)

    peaks = [peak["fit"] for peak in farm.fit_peaks(energies_eV, phases)]
    peaks_eV = np.array([peak.centre for peak in peaks])

    interest = np.repeat(True, len(peaks))

    if interest_low_eV is not None:
        interest &= interest_low_eV <= peaks_eV

    if interest_high_eV is not None:
        interest &= peaks_eV <= interest_high_eV

    peaks = list(itertools.compress(peaks, interest))

    return peaks


def run(excitations, energy_low_eV, energy_high_eV, ionisation_energy_eV):
    excitation_dir = os.path.join(STAGE_DIR, f"excitations={excitations}")
    energy_step_eV = (energy_high_eV - energy_low_eV) / 50000

    for name, s, l, pi in [("1Se", 1, 0, 0), ("1Po", 1, 1, 1), ("1De", 1, 2, 0)]:
        peaks = run_read_find(
            excitation_dir,
            energy_low_eV,
            energy_high_eV,
            energy_step_eV,
            s,
            l,
            pi,
            ionisation_energy_eV,
        )
        with open(os.path.join(excitation_dir, f"{name}.txt"), "w") as io:
            print(
                "{:11s} {:10s} {:8s}".format("Centre (eV)", "Width (eV)", "Strength"),
                file=io,
            )
            for peak in peaks:
                print(
                    f"{peak.centre:11.3f} {peak.width:10.3f} {peak.strength:8.3f}",
                    file=io,
                )


def main():
    run(1, 43, 51.5, 1.8605745 / utilities.farm.ENERGY_Ryd_per_eV)
    run(2, 42, 48.8, 1.6324686 / utilities.farm.ENERGY_Ryd_per_eV)


if __name__ == "__main__":
    main()
