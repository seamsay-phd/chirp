import argparse
import logging
import os
import shutil
import subprocess
import time

import f90nml
import numpy as np

from utilities import dimensions as dim
from utilities import laser, logging
from utilities.rmt import runner

STAGE_DIR = os.path.dirname(__file__)
PROJ_DIR = os.path.dirname(STAGE_DIR)

DELTAR_au = 0.08
BOX_SIZE_APPROX_au = 4000
X_LAST_OTHERS = 100

CPUS_PER_NODE = 128
INNER_CORES = 128


def run(
    submit: bool, overwrite: bool, excitations: int, e_field_data: laser.EFieldData
):
    shareddir = os.path.join(STAGE_DIR, f"excitations={excitations}")
    outdir = os.path.join(shareddir, "RMT", *e_field_data.dirname())
    datadir = os.path.relpath(os.path.join(shareddir, "RMatrixII"), outdir)

    if runner.calculation_already_run(outdir) and not overwrite:
        logging.info(
            "Skipping calculation as RMT has already been run on directory: %s", outdir
        )
        return

    if os.path.exists(outdir):
        logging.info("Removing directory to calculate afresh: %s", outdir)
        shutil.rmtree(outdir)

    logging.info(
        "Running job with: Excitations = %d | NIR = %.3feV | XUV = %.3feV | Delay = %.3ffs",
        excitations,
        e_field_data.nir.energy.to_eV(),
        e_field_data.xuv.energy.to_eV(),
        e_field_data.delay().to_fs(),
    )

    os.makedirs(outdir)

    currentdir = os.getcwd()
    try:
        os.chdir(outdir)
        laser.jsonify("EField.json", e_field_data)

        nir_start_steps = round(e_field_data.nir.start.to_steps(e_field_data.delta_t))
        nir_steps = round(e_field_data.nir.duration.to_steps(e_field_data.delta_t))

        ringing_time = 4 * nir_steps
        assert ringing_time > 2 * (nir_start_steps + nir_steps)
        steps_per_run_approx = ringing_time
        final_t = dim.Time.from_au(steps_per_run_approx * e_field_data.delta_t.to_au())

        e_field_data.write_inp("EField.inp", final_t)

        outer_approx = BOX_SIZE_APPROX_au / DELTAR_au / X_LAST_OTHERS
        nodes = int(np.ceil((INNER_CORES + outer_approx) / CPUS_PER_NODE))

        configuration = runner.conf(
            INNER_CORES,
            nodes,
            final_t.to_au(),
            steps_per_run_approx,
            e_field_data.jobname(),
        )
        with open("input.conf", "w") as f:
            f90nml.write(configuration, f)

        submission = runner.slurm(nodes, e_field_data.jobname())
        with open("submit.slurm", "w") as f:
            f.write(submission)

        os.symlink(os.path.join(datadir, "d"), "d")
        os.symlink(os.path.join(datadir, "d00"), "d00")
        os.symlink(os.path.join(datadir, "H"), "H")
        os.symlink(os.path.join(datadir, "Splinewaves"), "Splinewaves")
        os.symlink(os.path.join(datadir, "Splinedata"), "Splinedata")

        if submit:
            while (spaces := runner.spaces_in_queue()) <= 2:
                logging.info("Sleeping for 60s as spaces left in queue is: %d", spaces)
                time.sleep(60)

            subprocess.run(["sbatch", "submit.slurm"], check=True)
    finally:
        os.chdir(currentdir)


def time_delays(min_delay, max_delay, delay_delta):
    return (
        dim.Time.from_fs(min_delay + i * delay_delta)
        for i in range(round((max_delay - min_delay) / delay_delta) + 1)
    )


def main(submit=False, overwrite=False):
    min_delay_fs = -5.0
    max_delay_fs = 10.0
    delay_delta_fs = 1.0

    for excitations, xuv_eV, nir_eV in [(1, 49.213, 1.420), (2, 45.940, 1.621)]:
        for delay in time_delays(min_delay_fs, max_delay_fs, delay_delta_fs):
            # Targets 1De 46.651eV (in normal calculations) state.
            run(
                submit,
                overwrite,
                excitations,
                laser.EFieldDataExactNIR.sine_squ_exact_nir(
                    dim.PhotonEnergy.from_eV(nir_eV),
                    dim.PhotonEnergy.from_eV(xuv_eV),
                    delay,
                ),
            )

    # 2 excitation, high and low energy NIR to see if background resonance still there.
    run(
        submit,
        overwrite,
        2,
        laser.EFieldDataExactNIR.sine_squ_exact_nir(
            dim.PhotonEnergy.from_eV(2.0),
            dim.PhotonEnergy.from_eV(45.940),
            dim.Time.from_fs(5.0),
        ),
    )
    run(
        submit,
        overwrite,
        2,
        laser.EFieldDataExactNIR.sine_squ_exact_nir(
            dim.PhotonEnergy.from_eV(1.3),
            dim.PhotonEnergy.from_eV(45.940),
            dim.Time.from_fs(5.0),
        ),
    )

    logging.info("All calculations started.")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("-s", "--submit", action="store_true", help="submit the job")
    parser.add_argument(
        "-o",
        "--overwrite",
        action="store_true",
        help="remove and regenerate old data",
    )

    args = parser.parse_args()
    main(args.submit, args.overwrite)
