&phzfil
    filh = '10/excitations=1/RMatrixII/H'
    fkmat = '10/excitations=1/FARM/s=01-l=01-p=01/CPCKMATB'
    fom = '10/excitations=1/FARM/s=01-l=01-p=01/CPCOMEGAB'
    fout = '10/excitations=1/FARM/s=01-l=01-p=01/log.out'
    fpha = '10/excitations=1/FARM/s=01-l=01-p=01/CPCPHASEF'
    ftmat = '10/excitations=1/FARM/s=01-l=01-p=01/CPCTMATB'
/

&phzin
    diaflg = 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 1
    esc1 = 1.2998646850999995, 1.2494759569e-05
    ll = 1
    lpi = 1
    lrglx = 10
    ls = 1
    ne1 = 50000
    prtflg = 3, 3, 3, 0, 3, 0
    title = 'Neon'
/

