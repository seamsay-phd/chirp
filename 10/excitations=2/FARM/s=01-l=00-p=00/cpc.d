&phzfil
    filh = '10/excitations=2/RMatrixII/H'
    fkmat = '10/excitations=2/FARM/s=01-l=00-p=00/CPCKMATB'
    fom = '10/excitations=2/FARM/s=01-l=00-p=00/CPCOMEGAB'
    fout = '10/excitations=2/FARM/s=01-l=00-p=00/log.out'
    fpha = '10/excitations=2/FARM/s=01-l=00-p=00/CPCPHASEF'
    ftmat = '10/excitations=2/FARM/s=01-l=00-p=00/CPCTMATB'
/

&phzin
    diaflg = 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 1
    esc1 = 1.4544719994, 9.995807655199996e-06
    ll = 0
    lpi = 0
    lrglx = 10
    ls = 1
    ne1 = 49999
    prtflg = 3, 3, 3, 0, 3, 0
    title = 'Neon'
/

