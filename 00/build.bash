set -eu

git submodule update --init

cd "$1"
cmake -S source/ -B build/
cmake --build build/
cd ..
