import math
import os
import subprocess
import tempfile
import textwrap

from utilities import constants

STAGE_DIR = os.path.abspath(os.path.dirname(__file__))
PROJ_DIR = os.path.dirname(STAGE_DIR)


def kelvin(tasks):
    return textwrap.dedent(
        f"""\
        #!/bin/bash

        #SBATCH --job-name=farm-thresholds
        #SBATCH --time=00:30:00
        #SBATCH --ntasks={tasks}
        #SBATCH --partition=k2-hipri

        module add libs/gcc/13.2.0
        module add libs/blas/3.12.0/gcc-13.2.0
        module add libs/lapack/3.12.0/gcc-13.2.0

        export TMPDIR="$HOME/localscratch"
        export LOG_LEVEL=debug
        python3 {STAGE_DIR}/farm.py 1>"$SLURM_JOB_NAME-$SLURM_JOBID.out" 2>"$SLURM_JOB_NAME-$SLURM_JOBID.err"
        """
    )


def archer(tasks):
    nodes = int(math.ceil(tasks / constants.CPUS_PER_NODE))
    tmpdir = tempfile.mkdtemp()
    return textwrap.dedent(
        f"""\
        #!/bin/bash

        #SBATCH --job-name=farm-thresholds
        #SBATCH --account=e813

        #SBATCH --qos=standard
        #SBATCH --partition=standard

        #SBATCH --nodes={nodes}
        #SBATCH --ntasks-per-node={constants.CPUS_PER_NODE}
        #SBATCH --cpus-per-task=1
        #SBATCH --time=01:00:00

        module load cray-python
        source '{STAGE_DIR}/venv/bin/activate'

        export TMPDIR='{tmpdir}'
        export LOG_LEVEL=debug
        export PYTHONPATH="$PYTHONPATH:{PROJ_DIR}:{PROJ_DIR}/FARM/source"
        python3 '{STAGE_DIR}/farm.py' 1>"$SLURM_JOB_NAME-$SLURM_JOBID.out" 2>"$SLURM_JOB_NAME-$SLURM_JOBID.err"
        """
    )


def main(script):
    os.chdir(STAGE_DIR)

    slurm = "submit.slurm"

    with open(slurm, "w") as io:
        print(script(32), file=io)

    subprocess.run(["sbatch", slurm])


if __name__ == "__main__":
    cluster = os.getenv("CLUSTER")
    if cluster == "ARCHER2":
        script = archer
    elif cluster == "KELVIN2":
        script = kelvin
    else:
        raise Exception(f"Unknown cluster: {cluster}")

    main(script)
