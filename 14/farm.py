import multiprocessing
import os
import subprocess
import sys
import tempfile

import numpy as np

import utilities
import utilities.farm

from farm_utilities import phase

STAGE_DIR = os.path.dirname(os.path.abspath(__file__))
PROJ_DIR = os.path.dirname(STAGE_DIR)


def run(energy_low_eV, energy_high_eV, energy_step_eV, S, L, pi):
    ionisation_energy_eV = utilities.dimensions.Energy.from_Ry(1.5552698).to_eV()

    with tempfile.TemporaryDirectory() as directory:
        os.chdir(directory)

        os.symlink(os.path.join(PROJ_DIR, "01", "H"), "H")

        try:
            utilities.farm.run(
                utilities.farm.configuration(
                    energy_low_eV,
                    energy_high_eV,
                    energy_step_eV,
                    S,
                    L,
                    pi,
                    ionisation_energy_eV=ionisation_energy_eV,
                ),
                executable=os.path.join(PROJ_DIR, "FARM", "build", "bin", "farm"),
                output=subprocess.DEVNULL,
            )
        except utilities.farm.FARMFailedError:
            return False, (energy_low_eV, energy_high_eV, S, L, pi)

        return True, (energy_low_eV, energy_high_eV, S, L, pi)


def run_star(arg):
    return run(*arg)


def maybe_int(s):
    try:
        return int(s)
    except TypeError:
        return s


def coalesce(old):
    old = sorted(old)
    new = []

    i = j = 0
    while j < len(old) - 1:
        (l, _) = old[i]
        (_, h) = old[j]
        (l1, _) = old[j + 1]

        # Use a high point slightly higher than it should be to account for some floating point error.
        # The minimum range in 0.1eV so this shouldn't lead to any false positives.
        if l1 <= 1.000001 * h:
            j += 1
        else:
            new.append((l, h))
            i = j = j + 1

    if len(old) > 0:
        new.append((old[i][0], old[j][1]))

    return new


if __name__ == "__main__":
    from utilities import logging

    searches = [
        (43.0, 66.0, step, 1, L, pi)
        for L, pi, step in [
            (0, 0, 0.0001),
            (1, 1, 0.0001),
            (2, 0, 0.0001),
            (3, 1, 0.00001),
        ]
    ]
    min_range_eV = 0.1

    logging.info(
        "Search parameters: min_range_eV=%.3f searches=%r",
        min_range_eV,
        searches,
    )

    successful_searches = []
    failed_searches = []
    attempted = set(searches)

    with multiprocessing.Pool(maybe_int(os.getenv("SLURM_NTASKS"))) as pool:
        while len(searches) > 0:
            new_searches = []
            for success, params in pool.map(run_star, searches):
                l, h, S, L, pi = params
                m = (l + h) / 2

                l = round(l, 3)
                m = round(m, 3)
                h = round(h, 3)

                if success:
                    successful_searches.append(params)
                elif (m - l) < min_range_eV or (h - m) < min_range_eV:
                    failed_searches.append(params)
                else:
                    new_low = (l, m, S, L, pi)
                    if new_low not in attempted:
                        new_searches.append(new_low)

                    new_high = (m, h, S, L, pi)
                    if new_high not in attempted:
                        new_searches.append(new_high)

            by_qn = {}
            for l, h, S, L, pi in new_searches:
                key = (S, L, pi)
                by_qn.setdefault(key, []).append((l, h))

            searches = new_searches
            for key, value in by_qn.items():
                for l, h in coalesce(value):
                    params = (l, h, *key)
                    if params not in attempted:
                        searches.append(params)

            attempted.update(searches)

            logging.info("Finished search iteration: searches=%r", searches)

    logging.info(
        "Finshed searches: len(successful)=%d len(failed)=%d",
        len(successful_searches),
        len(failed_searches),
    )

    for s in successful_searches:
        logging.debug("Success: %r", s)

    for s in failed_searches:
        logging.debug("Failed: %r", s)

    thresholds_1Se = [
        (l, h)
        for (l, h, S, L, pi) in successful_searches
        if S == 1 and L == 0 and pi == 0
    ]
    thresholds_1Po = [
        (l, h)
        for (l, h, S, L, pi) in successful_searches
        if S == 1 and L == 1 and pi == 1
    ]
    thresholds_1De = [
        (l, h)
        for (l, h, S, L, pi) in successful_searches
        if S == 1 and L == 2 and pi == 0
    ]
    thresholds_1Fo = [
        (l, h)
        for (l, h, S, L, pi) in successful_searches
        if S == 1 and L == 3 and pi == 1
    ]

    for name, (S, L, pi), thresholds in [
        ("1Se", (1, 0, 0), thresholds_1Se),
        ("1Po", (1, 1, 1), thresholds_1Po),
        ("1De", (1, 2, 0), thresholds_1De),
        ("1Fo", (1, 3, 1), thresholds_1Fo),
    ]:
        logging.info("Coalescing ranges: len(thresholds)=%d", len(thresholds))

        brackets = coalesce(thresholds)

        logging.info("Coalesced ranges: len(brackets)=%d", len(brackets))

        print(name)
        for l, h in brackets:
            print(f"{l} - {h}")

        E = []
        phi = []
        for l, h in brackets:
            logging.info(
                "Running: low_eV=%.3f high_eV=%.3f S=%d L=%d pi=%d", l, h, S, L, pi
            )

            energy_step_eV = 0.0001

            ionisation_energy_eV = utilities.dimensions.Energy.from_Ry(
                1.5552698
            ).to_eV()

            curr_dir = os.getcwd()
            with tempfile.TemporaryDirectory() as directory:
                os.chdir(directory)

                os.symlink(os.path.join(PROJ_DIR, "01", "H"), "H")

                utilities.farm.run(
                    utilities.farm.configuration(
                        l,
                        h,
                        energy_step_eV,
                        S,
                        L,
                        pi,
                        ionisation_energy_eV=ionisation_energy_eV,
                    ),
                    executable=os.path.join(PROJ_DIR, "FARM", "build", "bin", "farm"),
                    output=subprocess.DEVNULL,
                )

                E_, phi_ = phase.read_phase_data(utilities.farm.FILES["phase"])

            os.chdir(curr_dir)
            E.extend(E_)
            phi.extend(phi_)

        logging.info("Saving: name=%s directory=%s", name, os.getcwd())
        np.savetxt(f"{name}.txt", np.column_stack([E, phi]), header="E phi")
