import argparse
import os

import numpy as np

from utilities import dimensions as dim
from utilities import laser
from utilities.rmt import runner

STAGE_DIR = os.path.dirname(__file__)
PROJ_DIR = os.path.dirname(STAGE_DIR)


def main(submit=False, overwrite=False):
    datadir = os.path.join(PROJ_DIR, "01")

    nir = dim.PhotonEnergy.from_eV(1.423)
    xuv = dim.PhotonEnergy.from_eV(45.547)

    for delay in (-3, 0, 3, 5):
        runner.run(
            submit,
            overwrite,
            os.path.join(
                STAGE_DIR,
                "calculations",
                f"delay={delay:+07.3f}fs",
            ),
            datadir,
            laser.EFieldDataNIRAndXUV.gaussian(
                nir,
                xuv,
                dim.Time.from_fs(delay),
                nir_phase=dim.Angle.from_pi(1 / 2),
            ),
            time_limit=runner.TimeLimit(hours=10),
        )


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("-s", "--submit", action="store_true", help="submit the job")
    parser.add_argument(
        "-o",
        "--overwrite",
        action="store_true",
        help="remove and regenerate old data",
    )

    args = parser.parse_args()
    main(args.submit, args.overwrite)
