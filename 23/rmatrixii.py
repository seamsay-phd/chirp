import argparse
import collections
import os
import pathlib
import subprocess
import textwrap

from utilities import logging
from utilities.rmatrixii import input

STAGE_DIR = os.path.abspath(os.path.dirname(__file__))
PROJ_DIR = os.path.dirname(STAGE_DIR)

INPUT_FILE_NAME = "neon.inp"


def run():
    thr_2Po = input.EXPERIMENTAL_THRESHOLDS[0]
    assert thr_2Po[0] == input.Term(2, 1, 1)

    thr_2S = input.EXPERIMENTAL_THRESHOLDS[1]
    assert thr_2S[0] == input.Term(2, 0, 0)

    exp_thresholds = [thr_2Po, thr_2S]

    ground = input.OrbitConfiguration(
        {
            input.Orbital(n=1, l=0): 2,
            input.Orbital(n=2, l=0): 2,
            input.Orbital(n=2, l=1): 5,
        }
    )
    excited = input.OrbitConfiguration(
        {
            input.Orbital(n=1, l=0): 2,
            input.Orbital(n=2, l=0): 1,
            input.Orbital(n=2, l=1): 6,
        }
    )

    configurations = [ground, excited]

    thresholds = collections.Counter(term for term, _ in exp_thresholds)

    logging.info(
        "Running RMatrixII: thresholds=%s configurations=%s",
        "-".join(str(t) for t in thresholds.keys()),
        [
            {str(k): v for k, v in configuration.items()}
            for configuration in configurations
        ],
    )

    directory = os.path.relpath(os.path.join(STAGE_DIR, "calculations", "RMatrixII"))
    if os.path.exists(directory):
        logging.info("Skipping calculation as it's already been run.")
        return

    os.makedirs(directory)

    input_file = os.path.join(directory, INPUT_FILE_NAME)
    with open(input_file, "w") as io:
        input.generate_input(
            io,
            thresholds,
            input.ORBITALS,
            configurations,
            input.GROUND_STATE_CONFIGURATION,
            input.SLATER_COEFFICIENTS,
            exp_thresholds,
        )

    subprocess.run(
        [
            os.path.join(PROJ_DIR, "rmatrixii.bash"),
            os.path.join(PROJ_DIR, "RMatrixII", "build", "bin"),
            input_file,
            directory,
            os.path.join(directory, "rad.out"),
            os.path.join(directory, "ang.out"),
            os.path.join(directory, "ham.out"),
        ],
        check=True,
    )


def submit():
    script = pathlib.Path(__file__).absolute()
    directory = script.parent
    venv = directory.paren.joinpath("22", ".venv", "bin", "activate")

    submission_script = f"""\
        #!/bin/bash

        #SBATCH --job-name=rm2-term-coupling
        #SBATCH --account=e813

        #SBATCH --qos=standard
        #SBATCH --partition=standard

        #SBATCH --nodes=1
        #SBATCH --ntasks-per-node=1
        #SBATCH --cpus-per-task=1
        #SBATCH --time=12:00:00

        source {venv}
        export PYTHONPATH="$PYTHONPATH:{directory.parent}"

        export MPLCONFIGDIR={directory.joinpath(".mpl")}

        export OMP_NUM_THREADS=1
        export SRUN_CPUS_PER_TASK="$SLURM_CPUS_PER_TASK"

        srun --distribution=block:block --hint=nomultithread python3 {script} --run
    """
    submission_script = textwrap.dedent(submission_script)

    submission_path = directory.joinpath("rmatrixii.slurm")

    with open(submission_path, "w") as io:
        print(submission_script, file=io)

    subprocess.run(["sbatch", submission_path], check=True)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    group = parser.add_mutually_exclusive_group(required=True)

    group.add_argument("-s", "--submit", action="store_true")
    group.add_argument("-r", "--run", action="store_true")

    args = parser.parse_args()

    if args.submit + args.run != 1:
        raise Exception("only one option should be possible")
    elif args.submit:
        submit()
    elif args.run:
        run()
    else:
        raise Exception("boolean logic is broken")
