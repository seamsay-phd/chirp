import numpy as np
from matplotlib import pyplot as plt
from scipy import fft

from utilities import constants
from utilities import dimensions as dim
from utilities.rmt import packaging


def calculate_delay_scan(
    hdf, xuv_eV=45.547, nir_eV=1.423, intensity=constants.NIR_INTENSITY_100_TW_per_cm2
):
    nir = dim.PhotonEnergy.from_eV(1.423)
    one_period_delay_fs = nir.period().to_fs()
    delays_fs = np.linspace(0, one_period_delay_fs, 16)

    min_k = 1.0
    max_k = 1.6
    min_k = 0
    max_k = 2
    energy_eV = None
    signal = []

    for delay in delays_fs:
        k, rho = packaging.momentum(
            hdf, xuv_eV, nir_eV, delay, intensity, k_low=min_k, k_high=max_k
        )
        E = dim.PhotonEnergy.from_au(k**2 / 2).to_eV()

        if energy_eV is None:
            energy_eV = E
        else:
            if energy_eV.shape != E.shape or not np.allclose(energy_eV, E):
                rho = np.interp(energy_eV, E, rho)

        signal.append(rho)

    s_grid = np.vstack(signal)

    return energy_eV, delays_fs, s_grid


def calculate_ft(
    hdf, xuv_eV=45.547, nir_eV=1.423, intensity=constants.NIR_INTENSITY_100_TW_per_cm2
):
    energy_eV, delay_fs, s_grid = calculate_delay_scan(hdf, xuv_eV, nir_eV, intensity)

    transform = fft.rfft(s_grid, axis=0)
    freqs = fft.rfftfreq(len(delay_fs), d=1 / len(delay_fs))

    return energy_eV, freqs, transform


def plot_delay_scan(
    hdf, xuv_eV=45.547, nir_eV=1.423, intensity=constants.NIR_INTENSITY_100_TW_per_cm2
):
    energy, delays_fs, signal = calculate_delay_scan(hdf, xuv_eV, nir_eV, intensity)

    e_grid, d_grid = np.meshgrid(energy, delays_fs)

    fig, ax = plt.subplots(1)

    pcm = ax.pcolormesh(e_grid, d_grid, signal, shading="gouraud")
    fig.colorbar(pcm)

    ax.annotate(
        "XUV (Pump)\nFirst",
        (np.min(energy), np.max(delays_fs)),
        (0.01, 0.99),
        textcoords="axes fraction",
        horizontalalignment="left",
        verticalalignment="top",
        color="white",
    )
    ax.annotate(
        "NIR (Probe)\nFirst",
        (np.min(energy), np.min(delays_fs)),
        (0.01, 0.01),
        textcoords="axes fraction",
        horizontalalignment="left",
        color="white",
    )

    ax.set_xlabel("Energy (eV)")
    ax.set_ylabel("Delay (fs)")
    ax.set_title(f"Time-Delay Scan | XUV = {xuv_eV:.3f}eV | NIR = {nir_eV:.3f}eV")

    return fig
