import argparse
import logging
import os
import sys

from utilities.rmt import packaging

LOG_LEVEL_MAP = {
    "DEBUG": logging.DEBUG,
    "INFO": logging.INFO,
    "WARN": logging.WARN,
    "ERROR": logging.ERROR,
}

logging.basicConfig(
    level=LOG_LEVEL_MAP[os.environ.get("LOG_LEVEL", "INFO").upper()],
    format="{asctime} {levelname:>8s} {message}",
    style="{",
    stream=sys.stderr,
)

STAGE_DIR = os.path.abspath(os.path.dirname(__file__))
PROJ_DIR = os.path.dirname(STAGE_DIR)
DATA_DIR = os.path.join(PROJ_DIR, "12")


def run(absorption=True, momentum=False):
    if absorption:
        hdf_path = os.path.join(STAGE_DIR, "absorption.hdf5")
        columns = ["E", "z"]
        packaging.package(hdf_path, DATA_DIR, packaging.load_absorption, columns)
        packaging.package_uv_only(
            hdf_path,
            os.path.join(PROJ_DIR, "03", "xuv-only"),
            packaging.load_absorption,
            columns,
        )

    if momentum:
        hdf_path = os.path.join(STAGE_DIR, "momentum.hdf5")
        columns = ["k", "rho"]
        packaging.package(hdf_path, DATA_DIR, packaging.load_momentum, columns)
        packaging.package_uv_only(
            hdf_path,
            os.path.join(PROJ_DIR, "03", "xuv-only"),
            packaging.load_momentum,
            columns,
        )


def main(absorption=True, momentum=False):
    run(absorption, momentum)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "-a", "--absorption", action="store_true", help="package absorption spectrum"
    )
    parser.add_argument(
        "-m", "--momentum", action="store_true", help="package momentum spectrum"
    )

    args = parser.parse_args()

    absorption = args.absorption
    momentum = args.momentum
    if not absorption and not momentum:
        absorption = True

    main(absorption, momentum)
