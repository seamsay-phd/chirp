import os

import h5py
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

from utilities import constants
from utilities import dimensions as dim
from utilities import fano
from utilities.rmt import packaging

NEGATIVE_DELAY_fs = -5.0
POSITIVE_DELAY_fs = 5.0
MIDDLE_DELAYS_fs = np.array([-0.3, -0.2, -0.1, 0.0, 0.1, 0.2, 0.3])


def infer_delays(hdf, xuv_eV, nir_eV, intensity):
    group = hdf[
        packaging.spectrum_hdf_key(
            uv_energy_eV=xuv_eV, ir_energy_eV=nir_eV, intensity=intensity
        )
    ]["by-delay"]

    delays = np.zeros(len(group.keys()))
    for i, delay in enumerate(group.keys()):
        assert delay.endswith("fs")
        delays[i] = float(delay[:-2])

    delays.sort()

    return delays


def ponderomotive_energy_eV(e_intensity, e_energy_eV):
    e_intensity = dim.EFieldIntensity.from_100_TW_per_cm2(e_intensity)
    e_energy = dim.PhotonEnergy.from_eV(e_energy_eV)

    return dim.Energy.from_au(
        e_intensity.amplitude().to_au() ** 2 / (4 * e_energy.to_au() ** 2)
    ).to_eV()


def calculate_intensity_scaling(hdf, xuv_eV=45.547, nir_eV=1.423):
    group = hdf[f"by-uv-energy/{xuv_eV:6.3f}eV/by-ir-energy/{nir_eV:5.3f}eV"]

    expected_delays = {
        f"{delay:+z07.3f}fs"
        for delay in [NEGATIVE_DELAY_fs, POSITIVE_DELAY_fs, *MIDDLE_DELAYS_fs]
    }
    intensities = np.array(
        [
            float(intensity)
            for intensity in group["by-ir-intensity"].keys()
            if all(
                delay in expected_delays
                for delay in group[f"by-ir-intensity/{intensity}/by-delay"].keys()
            )
        ]
    )

    df = pd.DataFrame(index=pd.Index(intensities, name="intensity"))
    for index, name in ((1, "2s3p"), (4, "bg")):
        prev_zero_phase = None
        zero_phase_mult = 0
        prev_pos_phase = None
        pos_phase_mult = 0

        phase_neg = np.zeros_like(intensities)
        phase_zero = np.zeros_like(intensities)
        phase_pos = np.zeros_like(intensities)
        strength_neg = np.zeros_like(intensities)
        strength_zero = np.zeros_like(intensities)
        strength_pos = np.zeros_like(intensities)
        for i, intensity in enumerate(intensities):
            neg_E, neg_z = packaging.absorption(
                hdf, xuv_eV, nir_eV, NEGATIVE_DELAY_fs, intensity
            )
            neg_fanos = fano.fit(neg_E, neg_z)
            neg_f = neg_fanos[index][0]

            pos_E, pos_z = packaging.absorption(
                hdf, xuv_eV, nir_eV, POSITIVE_DELAY_fs, intensity
            )
            pos_fanos = fano.fit(pos_E, pos_z)
            pos_f = pos_fanos[index][0]

            pos_phase = pos_f.phase
            if prev_pos_phase is not None and pos_phase - prev_pos_phase > np.pi:
                pos_phase_mult += 1
            prev_pos_phase = pos_phase
            pos_phase -= 2 * np.pi * pos_phase_mult

            E, z = packaging.absorption(hdf, xuv_eV, nir_eV, 0.0, intensity)
            zero_fanos = fano.fit(E, z)
            zero_f = zero_fanos[index][0]

            zero_phase = zero_f.phase
            if prev_zero_phase is not None and zero_phase - prev_zero_phase > np.pi:
                zero_phase_mult += 1
            prev_zero_phase = zero_phase
            zero_phase -= 2 * np.pi * zero_phase_mult

            phase_neg[i] = neg_f.phase
            phase_zero[i] = zero_phase
            phase_pos[i] = pos_phase

            strength_neg[i] = neg_f.strength
            strength_zero[i] = zero_f.strength
            strength_pos[i] = pos_f.strength

        df[f"{name}_phase_neg"] = phase_neg / np.pi
        df[f"{name}_phase_zero"] = phase_zero / np.pi
        df[f"{name}_phase_pos"] = phase_pos / np.pi
        df[f"{name}_strength_neg"] = strength_neg
        df[f"{name}_strength_zero"] = strength_zero
        df[f"{name}_strength_pos"] = strength_pos

    return df


def calculate_and_plot_intensity_scaling(hdf, xuv_eV=45.547, nir_eV=1.423):
    fig, [[ax_m_s, ax_m_p], [ax_b_s, ax_b_p]] = plt.subplots(2, 2)
    data = calculate_intensity_scaling(hdf, xuv_eV, nir_eV)

    for ax_s, ax_p, name in ((ax_m_s, ax_m_p, "2s3p"), (ax_b_s, ax_b_p, "bg")):
        for ax, var, f in ((ax_s, "strength", "z"), (ax_p, "phase", r"\phi")):
            for delay, label in (
                ("neg", rf"${f}(-5\text{{fs}})$"),
                ("zero", rf"${f}(0\text{{fs}})$"),
                ("pos", rf"${f}(5\text{{fs}})$"),
            ):
                ax.plot(
                    ponderomotive_energy_eV(data.index, nir_eV),
                    data[f"{name}_{var}_{delay}"],
                    label=label,
                )

            ax.legend()
            ax.set_title(f"{name} {var}")

    fig.supxlabel("ponderomotive energy (eV)")
    fig.suptitle(f"xuv = {xuv_eV:.3f}eV | nir = {nir_eV:.3f}eV")

    return fig


def plot_cloud(
    hdf,
    xuv_eV,
    nir_eV,
    intensity=constants.NIR_INTENSITY_100_TW_per_cm2,
    delays_fs=None,
):
    if delays_fs is None:
        delays_fs = infer_delays(hdf, xuv_eV, nir_eV, intensity)

    re = np.zeros_like(delays_fs)
    im = np.zeros_like(delays_fs)

    for i, delay in enumerate(delays_fs):
        E, z = packaging.absorption(hdf, xuv_eV, nir_eV, delay, intensity)
        (_, (fit, _), _, _, _) = fano.fit(E, z)

        n = fit.strength * np.exp(1j * fit.phase)

        re[i] = np.real(n)
        im[i] = np.imag(n)

    fig, ax = plt.subplots(1, 1)

    ax.plot(re, im)
    ax.annotate(f"{delays_fs[0]:+07.3f}fs", (re[0], im[0]))
    ax.annotate(f"{delays_fs[-1]:+07.3f}fs", (re[-1], im[-1]))

    return fig


if __name__ == "__main__":
    hdf = h5py.File(os.path.join(os.path.dirname(__file__), "absorption.hdf5"))

    for nir_eV in (0.712, 1.423, 2.846):
        calculate_and_plot_intensity_scaling(hdf, 45.547, nir_eV)

    plt.show()
