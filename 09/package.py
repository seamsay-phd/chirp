import argparse
import itertools
import json
import logging
import os
import re
import subprocess
import sys

import h5py
import numpy as np
import pandas as pd
from rmt_utilities import rmtutil, windowing

LOG_LEVEL_MAP = {
    "DEBUG": logging.DEBUG,
    "INFO": logging.INFO,
    "WARN": logging.WARN,
    "ERROR": logging.ERROR,
}

logging.basicConfig(
    level=LOG_LEVEL_MAP[os.environ.get("LOG_LEVEL", "INFO").upper()],
    format="{asctime} {levelname:>8s} {message}",
    style="{",
    stream=sys.stderr,
)

STAGE_DIR = os.path.abspath(os.path.dirname(__file__))
PROJ_DIR = os.path.dirname(STAGE_DIR)
DATA_DIR = os.path.join(PROJ_DIR, "08")

CHANNEL_INFO_FILE = "CoupledChannelInfo"
MOMENTUM_SPECTRUM_FILE = "OuterWave_momentum_spectrum.txt"
OUTER_WAVE_STEM = "OuterWave"
TAS_FILE = "TAS_0001_z"

ENERGY_RGX = re.compile(r"^xuv\=\d{2}\.\d{3}eV-nir\=\d.\d{3}eV$")
INTENSITY_RGX = re.compile(r"^int=\d\.\d{3}$")
DELAY_RGX = re.compile(r"^delay\=[-+]\d\d\.\d{3}fs$")
WAVE_RGX = re.compile(r"^(?:Inner|Outer)Wave(\d\d\d\d)$")


NOT_FINISHED_DIRECTORIES = set(
    filter(
        lambda l: l != "WORK_DIR" and l != "",
        subprocess.run(
            ["squeue", "--me", "--format=%Z"],
            check=True,
            encoding="utf8",
            capture_output=True,
        ).stdout.split("\n"),
    )
)


def load_efield(calc_dir):
    efield_path = os.path.join(calc_dir, "EField.json")
    with open(efield_path) as f:
        efield = json.load(f)

    ir_energy = efield["NIR"]["energy"]["eV"]
    ir_intensity = efield["NIR"]["intensity"]["100_TW_per_cm2"]
    uv_energy = efield["XUV"]["energy"]["eV"]
    delay = efield["NIR"]["peak"]["fs"] - efield["XUV"]["peak"]["fs"]

    if delay == -0.0:
        delay = 0.0

    return {
        "ir-energy": ir_energy,
        "uv-energy": uv_energy,
        "delay": delay,
        "ir-intensity": ir_intensity,
    }, efield


def load_spectrum(calc_dir, efield=None):
    if efield is None:
        _, efield = load_efield(calc_dir)

    tas_path = os.path.join(calc_dir, TAS_FILE)
    if not os.path.exists(tas_path):
        nir_end = round(efield["NIR"]["end"]["steps"])
        xuv_end = round(efield["XUV"]["end"]["steps"])
        window_start = max(nir_end, xuv_end)

        logging.info(
            "Generating spectrum with a linear window starting at step `%d` for calculation `%s`.",
            window_start,
            calc_dir,
        )
        calc = rmtutil.RMTCalc(calc_dir)
        tas = calc.ATAS(window=windowing.LinearStep(window_start))
        tas.write(fname="TAS_", units="eV")
    else:
        logging.info(
            "`%s` exists in `%s`, not running `RMT_gen_tas`.", TAS_FILE, calc_dir
        )

    return pd.read_table(tas_path, sep=" ", names=["E", "z"])


def require_reform(state_dir):
    state_files = os.listdir(state_dir)
    if (
        # Quick to find files that indicate `reform` hasn't been run.
        "atomic_structure" not in state_files
        or "DiscardedPop" not in state_files
        # Required files.
        or CHANNEL_INFO_FILE not in state_files
        or MOMENTUM_SPECTRUM_FILE not in state_files
        or not any(f.startswith(OUTER_WAVE_STEM) for f in state_files)
    ):
        current_dir = os.path.abspath(os.getcwd())
        try:
            os.chdir(state_dir)
            logging.info("Running reform for calculation state: %s", state_dir)
            subprocess.run(
                [os.path.join(PROJ_DIR, "RMT", "build", "bin", "reform"), "--momentum"],
                check=True,
            )
        finally:
            os.chdir(current_dir)
    else:
        logging.info(
            "All required files exist, so not running reform on calculation state: %s",
            state_dir,
        )


def load_momentum(calc_dir, _=None):
    state_dir = os.path.join(calc_dir, "state")
    require_reform(state_dir)

    return pd.read_table(
        os.path.join(state_dir, MOMENTUM_SPECTRUM_FILE),
        names=["k", "rho"],
        sep=r"\s+",
    )


def load_wavefunction(calc_dir, _=None):
    state_dir = os.path.join(calc_dir, "state")
    require_reform(state_dir)

    df = pd.read_table(os.path.join(state_dir, CHANNEL_INFO_FILE), sep=r"\s+", header=0)
    df.loc[:, "Channel"] = df.index
    df.loc[:, "Probability"] = np.nan

    for channel in df.index:
        # NumPy is a good 25% faster than Pandas.
        chan_wf = np.loadtxt(os.path.join(state_dir, f"{OUTER_WAVE_STEM}{channel:04d}"))
        rows, _ = chan_wf.shape

        far_away = chan_wf[rows // 2 :, :]
        df.loc[channel, "Probability"] = np.trapz(
            far_away[:, 1] ** 2 + far_away[:, 2] ** 2, far_away[:, 0]
        )

    return df


def assign_data(root, order, datasets, dataframe=None):
    group = root
    for by, name, value, unit in order:
        by_group = group.require_group(f"by-{by}")
        group = by_group.require_group(name)
        if "value" not in group.attrs:
            group.attrs["value"] = value
            if unit is not None:
                group.attrs["unit"] = unit

    if any(ds is not None for ds in datasets.values()) and dataframe is not None:
        raise ValueError("Both `dataframe` and datasets were given.")
    elif dataframe is not None:
        return {
            name: group.create_dataset(name, data=dataframe.loc[:, name].to_numpy())
            for name in datasets.keys()
        }
    elif any(ds is not None for ds in datasets.values()):
        if any(ds is None for ds in datasets.values()):
            raise ValueError("Note all datasets were given.")

        for name, dataset in datasets.items():
            group[name] = dataset
    else:
        raise ValueError("Neither `dataframe` nor the datasets were given.")


def run(spectra, momenta, wavefunctions):
    types = []

    if spectra:
        types.append(("spectra.hdf5", load_spectrum, ("E", "z")))

    if momenta:
        types.append(("momenta.hdf5", load_momentum, ("k", "rho")))

    if wavefunctions:
        types.append(
            (
                "wavefunctions.hdf5",
                load_wavefunction,
                (
                    "Channel",
                    "TargetN",
                    "TargetL",
                    "ElecL",
                    "TotalL",
                    "Parity",
                    "Probability",
                ),
            )
        )

    for hdf_path, loader, names in types:
        with h5py.File(os.path.join(STAGE_DIR, hdf_path), "a") as hdf:
            if "uv-only" not in hdf.keys():
                xuv_only = loader(
                    os.path.join(PROJ_DIR, "03", "xuv-only"),
                    {"NIR": {"end": {"steps": 0}}, "XUV": {"end": {"steps": 3765}}},
                )
                xuv_only_grp = hdf.create_group("uv-only")
                for name in names:
                    xuv_only_grp.create_dataset(
                        name, data=xuv_only.loc[:, name].to_numpy()
                    )

            for energy_dir in os.listdir(DATA_DIR):
                match = ENERGY_RGX.match(energy_dir)
                if match is None:
                    logging.info(
                        "Ignoring energy directory with unrecognised format: %s",
                        energy_dir,
                    )
                    continue

                energy_path = os.path.join(DATA_DIR, energy_dir)

                for intensity_dir in os.listdir(energy_path):
                    match = INTENSITY_RGX.match(intensity_dir)
                    if match is None:
                        logging.info(
                            "Ignoring intensity directory with unrecognised format: %s",
                            intensity_dir,
                        )
                        continue

                    intensity_path = os.path.join(energy_path, intensity_dir)

                    for delay_dir in os.listdir(intensity_path):
                        match = DELAY_RGX.match(delay_dir)
                        if match is None:
                            logging.info(
                                "Ignoring delay directory with unrecognised format: %s",
                                delay_dir,
                            )
                            continue

                        delay_path = os.path.join(intensity_path, delay_dir)
                        if delay_path in NOT_FINISHED_DIRECTORIES:
                            logging.info(
                                "Ignoring directory as job is still running: %s",
                                delay_path,
                            )
                            continue

                        data, efield = load_efield(delay_path)

                        ir_energy = data["ir-energy"]
                        ir_energy_name = f"{ir_energy:5.3f}eV"

                        uv_energy = data["uv-energy"]
                        uv_energy_name = f"{uv_energy:6.3f}eV"

                        ir_intensity = data["ir-intensity"]
                        ir_intensity_name = f"{ir_intensity:.3f}"

                        delay = data["delay"]
                        delay_name = f"{delay:+07.3f}fs"

                        keys = (
                            ("ir-energy", ir_energy_name, ir_energy, "eV"),
                            ("uv-energy", uv_energy_name, uv_energy, "eV"),
                            ("ir-intensity", ir_intensity_name, ir_intensity, ""),
                            ("delay", delay_name, delay, "fs"),
                        )

                        not_found = object()
                        key = "/".join(
                            f"by-{group}/{name}" for group, name, _, _ in keys
                        )
                        exists = not hdf.get(key, not_found) is not_found
                        if exists:
                            logging.info(
                                "Ignoring directory as data already packaged: %s",
                                delay_path,
                            )
                            continue

                        data = loader(delay_path, efield)

                        datasets = assign_data(
                            hdf,
                            keys,
                            {name: None for name in names},
                            dataframe=data,
                        )
                        for order in itertools.permutations(keys):
                            if all(order[i][0] == keys[i][0] for i in range(len(keys))):
                                continue

                            assign_data(hdf, order, datasets)


def main(spectra=True, momenta=False, wavefunctions=False):
    run(spectra, momenta, wavefunctions)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("-s", "--spectra", action="store_true", help="package spectra")
    parser.add_argument("-m", "--momenta", action="store_true", help="package momenta")
    parser.add_argument(
        "-w", "--wavefunctions", action="store_true", help="package wavefunctions"
    )

    args = parser.parse_args()

    spectra = args.spectra
    momenta = args.momenta
    wavefunctions = args.wavefunctions
    if not spectra and not momenta and not wavefunctions:
        spectra = True

    main(spectra, momenta, wavefunctions)
