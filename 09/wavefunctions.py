import os
import string

import h5py
import pandas as pd

from utilities.rmatrixii.input import Term

STAGE_DIR = os.path.join(os.path.dirname(__file__))

TARGETS = [
    Term(2, 1, 1),
    Term(2, 0, 0),
    Term(4, 1, 0),
    Term(2, 1, 0),
    Term(4, 1, 1),
    Term(2, 2, 0),
    Term(4, 2, 1),
    Term(2, 2, 1),
    Term(2, 0, 1),
    Term(4, 0, 1),
    Term(2, 1, 1),
]

ORBITAL_MAP = [
    "s",
    "p",
    "d",
    *(
        l
        for l in string.ascii_lowercase
        if l not in ("a", "b", "c", "d", "e", "j", "s", "p")
    ),
]


def term(spin, target_L, elec_l, parity):
    target_orbital = ORBITAL_MAP[target_L].upper()
    elec_orbital = ORBITAL_MAP[elec_l]
    parity_letter = "eo"[parity]
    multiplicity = round(2 * spin) + 1
    return f"{multiplicity}{target_orbital}{parity_letter} + {elec_orbital}"


def load(hdf, xuv_energy_eV, nir_energy_eV, delay_fs, intensity=None):
    if intensity is None:
        root = hdf
    else:
        root = hdf[f"ir-intensity/{intensity:5.3f}"]

    if nir_energy_eV is None:
        grp = root["uv-only"]
    else:
        xuv_key = f"by-uv-energy/{xuv_energy_eV:6.3f}eV"
        nir_key = f"by-ir-energy/{nir_energy_eV:5.3f}eV"
        delay_key = f"by-delay/{delay_fs:+z07.3f}fs"
        grp = root[f"{xuv_key}/{nir_key}/{delay_key}"]

    return pd.DataFrame(
        data={
            n: grp[n][:]
            for n in ("TargetN", "TargetL", "ElecL", "TotalL", "Parity", "Probability")
        },
        index=pd.Series(grp["Channel"][:], name="Channel"),
    )


def tabulate(df, proportion=0.1):
    maximum = df["Probability"].max()
    predominant = df.loc[df["Probability"] > maximum * proportion].sort_values(
        "Probability", ascending=False
    )

    print(
        "{:7s}    {:8s}    {:8s}    {:6s}    {:7s}    {:6s}    {:11s}    {:7s}".format(
            "Channel",
            "Target N",
            "Target L",
            "Elec L",
            "Total L",
            "Parity",
            "Probability",
            "Term",
        )
    )
    for channel in predominant.index:
        target_N = predominant.loc[channel, "TargetN"]
        target_L = predominant.loc[channel, "TargetL"]
        elec_l = predominant.loc[channel, "ElecL"]
        total_L = predominant.loc[channel, "TotalL"]
        parity = predominant.loc[channel, "Parity"]
        probability = predominant.loc[channel, "Probability"]
        channel_term = term(
            (TARGETS[target_N - 1].multiplicity - 1) / 2,
            target_L,
            elec_l,
            TARGETS[target_N - 1].parity,
        )
        print(
            f"{channel:7d}    {target_N:8d}    {target_L:8d}    {elec_l:6d}    {total_L:7d}    {parity:6d}    {probability:11.3e}    {channel_term:7s}"
        )


def plot_channels_archer(channels):
    from matplotlib import pyplot as plt

    _, ax = plt.subplots(1)

    for channel in channels:
        df = pd.read_table(
            f"OuterWave{channel:04d}", names=["r", "Re_psi", "Im_psi"], sep=r"\s+"
        )
        ax.plot(df["r"], df["Re_psi"] ** 2 + df["Im_psi"] ** 2, label=f"{channel:04d}")

    ax.set_xlabel("Radius (a.u.)")
    ax.set_ylabel("Probability Density")
    ax.legend()

    plt.show()


def main():
    with h5py.File(os.path.join(STAGE_DIR, "wavefunctions.hdf5")) as hdf:
        for name, xuv, nir, delay, intensity in (
            ("Low Intensity 0fs Delay", 45.547, 1.571, 0.0, 0.001),
            ("XUV Only", None, None, None, None),
            ("-5fs | NIR (Probe) First", 45.547, 1.571, -5.0, None),
            (" 0fs", 45.547, 1.571, 0.0, None),
            (" 5fs | XUV (Pump) First", 45.547, 1.571, 5.0, None),
            ("10fs | XUV (Pump) First", 45.547, 1.571, 10.0, None),
        ):
            print(name)
            df = load(hdf, xuv, nir, delay, intensity)
            tabulate(df)
            print("\n")


if __name__ == "__main__":
    main()
