import logging
import os
import sys

import matplotlib as mpl
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from utilities import fano
from utilities.constants import NIR_INTENSITY_100_TW_per_cm2

LOG_LEVEL_MAP = {
    "ALL": logging.DEBUG - 1,
    "DEBUG": logging.DEBUG,
    "INFO": logging.INFO,
    "WARN": logging.WARN,
    "ERROR": logging.ERROR,
    "CRITICAL": logging.CRITICAL,
    "DISABLE": logging.CRITICAL + 1,
}

logging.basicConfig(
    level=LOG_LEVEL_MAP[os.environ.get("LOG_LEVEL", "INFO").upper()],
    format="{asctime} {levelname:>8s} {message}",
    style="{",
    stream=sys.stderr,
)

SHORT_DELAYS = np.linspace(-5, 5, 21)
LONG_DELAYS = np.linspace(-5, 10, 31)


def spectrum(
    hdf,
    uv_energy_eV,
    ir_energy_eV,
    delay_fs,
    intensity=NIR_INTENSITY_100_TW_per_cm2,
    E_low_eV=fano.FIT_ENERGY_RANGE_LOW_eV,
    E_high_eV=fano.FIT_ENERGY_RANGE_HIGH_eV,
):
    uv_energy_key = f"by-uv-energy/{uv_energy_eV:6.3f}eV"
    ir_energy_key = f"by-ir-energy/{ir_energy_eV:5.3f}eV"
    ir_intensity_key = f"by-ir-intensity/{intensity:.3f}"
    delay_key = f"by-delay/{delay_fs:+z07.3f}fs"

    group = hdf[f"{uv_energy_key}/{ir_energy_key}/{ir_intensity_key}/{delay_key}"]
    E = group["E"][:]

    wanted = (E_low_eV < E) & (E < E_high_eV)
    return E[wanted], -group["z"][wanted]


def plot_delay_scan(
    hdf,
    xuv_energy_eV,
    nir_energy_eV,
    delay_fs=LONG_DELAYS,
    intensity=NIR_INTENSITY_100_TW_per_cm2,
    E_low_eV=fano.FIT_ENERGY_RANGE_LOW_eV,
    E_high_eV=fano.FIT_ENERGY_RANGE_HIGH_eV,
):
    energy = None
    signal = []

    for d in delay_fs:
        E, z = spectrum(
            hdf, xuv_energy_eV, nir_energy_eV, d, intensity, E_low_eV, E_high_eV
        )

        if energy is not None:
            if energy.shape != E.shape or not np.allclose(energy, E):
                z = np.interp(energy, E, z)
        else:
            energy = E

        signal.append(z)

    signal = np.vstack(signal)

    fig, ax = plt.subplots(1)

    e_grid, d_grid = np.meshgrid(energy, delay_fs)

    pcm = ax.pcolormesh(e_grid, d_grid, signal, shading="gouraud", vmin=0.1, vmax=0.6)
    fig.colorbar(pcm)

    ax.annotate(
        "XUV (Pump)\nFirst",
        (np.min(energy), np.max(delay_fs)),
        (0.01, 0.99),
        textcoords="axes fraction",
        horizontalalignment="left",
        verticalalignment="top",
        color="white",
    )
    ax.annotate(
        "NIR (Probe)\nFirst",
        (np.min(energy), np.min(delay_fs)),
        (0.01, 0.01),
        textcoords="axes fraction",
        horizontalalignment="left",
        color="white",
    )

    ax.set_xlabel("Energy (eV)")
    ax.set_ylabel("Delay (fs)")
    ax.set_title(
        f"Time-Delay Scan | XUV = {xuv_energy_eV:.3f}eV | NIR = {nir_energy_eV:.3f}eV"
    )

    return fig


def plot_resonance_fit(
    hdf,
    xuv_energy_eV,
    nir_energy_eV,
    delay_fs,
    intensity=NIR_INTENSITY_100_TW_per_cm2,
    debug=False,
):
    E, z = spectrum(hdf, xuv_energy_eV, nir_energy_eV, delay_fs, intensity)
    fig = fano.fit_and_plot_resonance(E, z, debug=debug)
    fig.suptitle(
        f"Resonance Fit | XUV = {xuv_energy_eV:.3f}eV | NIR = {nir_energy_eV:.3f}eV (Int = {intensity:.1g}) | Delay = {delay_fs:.3f}fs"
    )

    return fig


def calculate_delay_fits(
    hdf,
    xuv_energy_eV,
    nir_energy_eV,
    delay_fs=LONG_DELAYS,
    intensity=NIR_INTENSITY_100_TW_per_cm2,
):
    centres_2s3p = np.zeros_like(delay_fs)
    centre_errors_2s3p = np.zeros_like(delay_fs)

    phases_2s3p = np.zeros_like(delay_fs)
    phase_errors_2s3p = np.zeros_like(delay_fs)

    strengths_2s3p = np.zeros_like(delay_fs)
    strength_errors_2s3p = np.zeros_like(delay_fs)

    widths_2s3p = np.zeros_like(delay_fs)
    width_errors_2s3p = np.zeros_like(delay_fs)

    centres_bg = np.zeros_like(delay_fs)
    centre_errors_bg = np.zeros_like(delay_fs)

    phases_bg = np.zeros_like(delay_fs)
    phase_errors_bg = np.zeros_like(delay_fs)

    strengths_bg = np.zeros_like(delay_fs)
    strength_errors_bg = np.zeros_like(delay_fs)

    widths_bg = np.zeros_like(delay_fs)
    width_errors_bg = np.zeros_like(delay_fs)

    for i, d in enumerate(delay_fs):
        E, z = spectrum(hdf, xuv_energy_eV, nir_energy_eV, d, intensity)
        logging.info(
            "Fitting spectrum: xuv = %.3f | nir = %.3f | delay = %+.3f",
            xuv_energy_eV,
            nir_energy_eV,
            d,
        )
        (
            (o, e_o),
            (f_2s3p, e_2s3p),
            (f_2p43s3p, e_2p43s3p),
            (f_2s4p, e_2s4p),
            (f_bg, e_bg),
        ) = fano.fit(E, z)
        logging.debug(
            "Found fit: offset = %.3f (%.3f)"
            " | 2s2p(6)3p: E0 = %.3f (%.3f) z = %.3f (%.3f) phi = %.3f (%.3f) Gamma = %.3f (%.3f)"
            " | 2p(4)3s3p: E0 = %.3f (%.3f) z = %.3f (%.3f) phi = %.3f (%.3f) Gamma = %.3f (%.3f)"
            " | 2s2p(6)4p: E0 = %.3f (%.3f) z = %.3f (%.3f) phi = %.3f (%.3f) Gamma = %.3f (%.3f)"
            " | Unknown  : E0 = %.3f (%.3f) z = %.3f (%.3f) phi = %.3f (%.3f) Gamma = %.3f (%.3f)",
            o,
            e_o,
            *(a for t in zip(f_2s3p, e_2s3p) for a in t),
            *(a for t in zip(f_2p43s3p, e_2p43s3p) for a in t),
            *(a for t in zip(f_2s4p, e_2s4p) for a in t),
            *(a for t in zip(f_bg, e_bg) for a in t),
        )

        centres_2s3p[i] = f_2s3p.centre
        centre_errors_2s3p[i] = e_2s3p.centre

        phases_2s3p[i] = f_2s3p.phase / np.pi
        phase_errors_2s3p[i] = e_2s3p.phase

        # TODO: Handle this in the Fano class.
        strengths_2s3p[i] = f_2s3p.strength_per_width * f_2s3p.width
        strength_errors_2s3p[i] = np.sqrt(
            (f_2s3p.strength_per_width * e_2s3p.width) ** 2
            + (f_2s3p.width * e_2s3p.strength_per_width) ** 2
        )

        widths_2s3p[i] = f_2s3p.width
        width_errors_2s3p[i] = e_2s3p.width

        centres_bg[i] = f_bg.centre
        centre_errors_bg[i] = e_bg.centre

        phases_bg[i] = f_bg.phase / np.pi
        phase_errors_bg[i] = e_bg.phase

        # TODO: Handle this in the Fano class.
        strengths_bg[i] = f_bg.strength_per_width * f_bg.width
        strength_errors_bg[i] = np.sqrt(
            (f_bg.strength_per_width * e_bg.width) ** 2
            + (f_bg.width * e_bg.strength_per_width) ** 2
        )

        widths_bg[i] = f_bg.width
        width_errors_bg[i] = e_bg.width

    phases_2s3p[phases_2s3p > 0.6] -= 2

    return pd.DataFrame(
        {
            "2s3p_c": centres_2s3p,
            "2s3p_c_e": centre_errors_2s3p,
            "2s3p_p": phases_2s3p,
            "2s3p_p_e": phase_errors_2s3p,
            "2s3p_s": strengths_2s3p,
            "2s3p_s_e": strength_errors_2s3p,
            "2s3p_w": widths_2s3p,
            "2s3p_w_e": width_errors_2s3p,
            "bg_c": centres_bg,
            "bg_c_e": centre_errors_bg,
            "bg_p": phases_bg,
            "bg_p_e": phase_errors_bg,
            "bg_s": strengths_bg,
            "bg_s_e": strength_errors_bg,
            "bg_w": widths_bg,
            "bg_w_e": width_errors_bg,
        },
        index=delay_fs,
    )


def plot_delay_fits(
    delay_fits,
    xuv_energy_eV,
    nir_energy_eV,
    intensity=NIR_INTENSITY_100_TW_per_cm2,
):
    fig, axes = plt.subplots(4, 2)
    [
        [ax_c_2s3p, ax_c_bg],
        [ax_p_2s3p, ax_p_bg],
        [ax_s_2s3p, ax_s_bg],
        [ax_w_2s3p, ax_w_bg],
    ] = axes

    fig.supxlabel("Time Delay (fs)")
    fig.suptitle(
        f"XUV = {xuv_energy_eV:.3f}eV | NIR = {nir_energy_eV:.3f}eV ({intensity:.3f})"
    )

    ax_c_2s3p.errorbar(
        delay_fits.index,
        delay_fits["2s3p_c"],
        yerr=delay_fits["2s3p_c_e"],
        ecolor="tab:orange",
    )
    ax_c_bg.errorbar(
        delay_fits.index,
        delay_fits["bg_c"],
        yerr=delay_fits["bg_c_e"],
        ecolor="tab:orange",
    )

    ax_p_2s3p.errorbar(
        delay_fits.index,
        delay_fits["2s3p_p"],
        yerr=delay_fits["2s3p_p_e"],
        ecolor="tab:orange",
    )
    ax_p_bg.errorbar(
        delay_fits.index,
        delay_fits["bg_p"],
        yerr=delay_fits["bg_p_e"],
        ecolor="tab:orange",
    )

    ax_s_2s3p.errorbar(
        delay_fits.index,
        delay_fits["2s3p_s"],
        yerr=delay_fits["2s3p_s_e"],
        ecolor="tab:orange",
    )
    ax_s_bg.errorbar(
        delay_fits.index,
        delay_fits["bg_s"],
        yerr=delay_fits["bg_s_e"],
        ecolor="tab:orange",
    )

    ax_w_2s3p.errorbar(
        delay_fits.index,
        delay_fits["2s3p_w"],
        yerr=delay_fits["2s3p_w_e"],
        ecolor="tab:orange",
    )
    ax_w_bg.errorbar(
        delay_fits.index,
        delay_fits["bg_w"],
        yerr=delay_fits["bg_w_e"],
        ecolor="tab:orange",
    )

    ax_c_2s3p.set_title(r"$2s2p^63p \, {}^\mathrm{1}\mathrm{P}_\mathrm{o}$")
    ax_c_bg.set_title("Background")

    ax_c_2s3p.set_ylabel("Centre (eV)")
    ax_p_2s3p.set_ylabel(r"Phase ($\pi$)")
    ax_s_2s3p.set_ylabel("Strength")
    ax_w_2s3p.set_ylabel("Width (eV)")

    for left, right in axes:
        l_min, l_max = left.get_ylim()
        r_min, r_max = right.get_ylim()

        b_min = min(l_min, r_min)
        b_max = max(l_max, r_max)

        left.set_ylim((b_min, b_max))
        right.set_ylim((b_min, b_max))

        right.yaxis.tick_right()

    # A little bit more space at the bottom of the width fit.
    a_min, a_max = ax_w_2s3p.get_ylim()
    ax_w_2s3p.set_ylim((a_min - 0.1, a_max))
    assert ax_w_bg.get_ylim() == (a_min, a_max)
    ax_w_bg.set_ylim((a_min - 0.1, a_max))

    for axis in axes.flatten():
        axis.grid(True, which="both")
        axis.tick_params(which="major", grid_alpha=1.0)
        axis.tick_params(which="minor", grid_alpha=0.2)

        axis.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator(5))
        axis.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())

    rows, cols = axes.shape
    for r in range(rows - 1):
        for c in range(cols):
            axis = axes[r, c]
            axis.set_xlim(axes[rows - 1, c].get_xlim())
            axis.xaxis.set_tick_params(labelbottom=False)

    return fig


def calculate_and_plot_delay_fits(
    hdf,
    xuv_energy_eV,
    nir_energy_eV,
    delay_fs=LONG_DELAYS,
    intensity=NIR_INTENSITY_100_TW_per_cm2,
):
    delay_fits = calculate_delay_fits(
        hdf, xuv_energy_eV, nir_energy_eV, delay_fs, intensity
    )

    return plot_delay_fits(
        delay_fits,
        xuv_energy_eV,
        nir_energy_eV,
        intensity,
    )
