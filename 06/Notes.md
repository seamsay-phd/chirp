* Look at what NIR frequencies are overlapping with the XUV pulse when unexpected phase change is happening and see if using those frequencies without chirp does anything.
* Run chirp and time delay scan for {chirp = -0.001:-0.0005:-0.007}x{td = -1:0.2:4}.
