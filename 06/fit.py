import numpy as np
from matplotlib import pyplot as plt
from utilities import fano

CHIRPS = np.array(
    [
        0.0,
        -0.00005,
        -0.0001,
        -0.00015,
        -0.0002,
        -0.00025,
        -0.0003,
        -0.00035,
        -0.0004,
        -0.00045,
        -0.0005,
        -0.00055,
        -0.0006,
    ]
)

DELAYS = np.array(
    [
        -1.0,
        -0.8,
        -0.6,
        -0.4,
        -0.2,
        0.0,
        0.2,
        0.4,
        0.6,
        0.8,
        1.0,
        1.2,
        1.4,
        1.6,
        1.8,
        2.0,
        2.2,
        2.4,
        2.6,
        2.8,
        3.0,
    ]
)


def spectrum(hdf, uv_chirp_au, ir_chirp_au, delay_fs, E_low_eV=44.0, E_high_eV=47.2):
    uv_chirp_key = f"by-uv-chirp/{uv_chirp_au:+z09.6f}au"
    ir_chirp_key = f"by-ir-chirp/{ir_chirp_au:+z09.6f}au"
    delay_key = f"by-delay/{delay_fs:+z010.6f}fs"

    group = hdf[f"{uv_chirp_key}/{ir_chirp_key}/{delay_key}"]
    E = group["E"][:]

    wanted = (E_low_eV < E) & (E < E_high_eV)
    return E[wanted], -group["z"][wanted]


def delay_to_energy(delay, chirp, peak_energy_eV):
    from utilities import dimensions

    peak_energy = dimensions.PhotonEnergy.from_eV(peak_energy_eV)
    instantaneous_energy = dimensions.PhotonEnergy.from_au(
        peak_energy.to_au() * (1 + chirp * delay)
    )
    return instantaneous_energy.to_eV()


def create_energy_mirror_axis(ax_d, chirp, peak_energy, axis="x"):
    if axis == "x":
        ax_e = ax_d.twinx()
    elif axis == "y":
        ax_e = ax_d.twiny()
    else:
        raise ValueError("`axis` must be `x` or `y`.")

    ax_d.callbacks.connect(
        "ylim_changed",
        lambda ax_d: update_energy_mirror_axis(ax_d, ax_e, chirp, peak_energy, axis),
    )

    label = "Instantaneous NIR Energy (eV)"
    if axis == "x":
        ax_e.set_ylabel(label)
    elif axis == "y":
        ax_e.set_xlabel(label)
    else:
        raise ValueError("`axis` must be `x` or `y`.")

    update_energy_mirror_axis(ax_d, ax_e, chirp, peak_energy, axis)


def update_energy_mirror_axis(ax_d, ax_e, chirp, peak_energy, axis):
    if axis == "x":
        ds, df = ax_d.get_ylim()
        ax_e.set_ylim(
            delay_to_energy(ds, chirp, peak_energy),
            delay_to_energy(df, chirp, peak_energy),
        )
    elif axis == "y":
        ds, df = ax_d.get_xlim()
        ax_e.set_ylim(
            delay_to_energy(ds, chirp, peak_energy),
            delay_to_energy(df, chirp, peak_energy),
        )
    else:
        raise ValueError("`axis` must be `x` or `y`.")

    ax_e.figure.canvas.draw()


def plot_delay_scan(hdf, nir_chirp, delay_fs=DELAYS, E_low_eV=44, E_high_eV=47):
    energy = None
    signal = []

    for d in delay_fs:
        E, z = spectrum(hdf, 0.0, nir_chirp, d, E_low_eV, E_high_eV)

        if energy is not None:
            assert np.allclose(E, energy)
        else:
            energy = E

        signal.append(z)

    signal = np.vstack(signal)

    fig, ax = plt.subplots(1)

    e_grid, d_grid = np.meshgrid(energy, delay_fs)
    pcm = ax.pcolormesh(e_grid, d_grid, signal, shading="auto")

    if not np.isclose(nir_chirp, 0.0):
        create_energy_mirror_axis(ax, nir_chirp, 45.547 / 29)

    fig.colorbar(pcm)

    ax.annotate(
        "XUV (Pump)\nFirst",
        (np.min(energy), np.max(delay_fs)),
        (0.01, 0.99),
        textcoords="axes fraction",
        horizontalalignment="left",
        verticalalignment="top",
        color="white",
    )
    ax.annotate(
        "NIR (Probe)\nFirst",
        (np.min(energy), np.min(delay_fs)),
        (0.01, 0.01),
        textcoords="axes fraction",
        horizontalalignment="left",
        color="white",
    )

    ax.grid(False)
    ax.set_xlabel("Energy (eV)")
    ax.set_ylabel("Delay (fs)")
    ax.set_title(f"Time-Delay Scan | NIR Chirp = {nir_chirp:.5f}")


def plot_chirp_delay_scan(hdf, variable="phase", chirp=CHIRPS, delay=DELAYS):
    """
    Heatmap of resonance phase where each vertical slice is a single chirp and each horizontal slice is a single delay.
    """
    c_grid, d_grid = np.meshgrid(chirp, delay)
    value = np.zeros((len(chirp), len(delay)))
    for i, c in enumerate(chirp):
        for j, d in enumerate(delay):
            E, z = spectrum(hdf, 0.0, c, d)
            _, p, _ = fano.fit(E, z)
            value[i, j] = getattr(p, variable)

    fig, ax = plt.subplots(1)

    pcm = ax.pcolormesh(c_grid, d_grid, value.T, shading="auto")

    fig.colorbar(pcm)

    ax.annotate(
        "XUV (Pump)\nFirst",
        (np.min(chirp), np.max(delay)),
        (0.99, 0.99),
        textcoords="axes fraction",
        horizontalalignment="right",
        verticalalignment="top",
        color="white",
    )
    ax.annotate(
        "NIR (Probe)\nFirst",
        (np.min(chirp), np.min(delay)),
        (0.99, 0.01),
        textcoords="axes fraction",
        horizontalalignment="right",
    )

    ax.grid(False)
    ax.set_xlabel("Chirp")
    ax.set_ylabel("Delay (fs)")
    ax.set_title(f"{variable.title()} Heatmap")


def plot_delay_scan_by_chirp(hdf, chirp=CHIRPS, delay=DELAYS):
    """
    Series of delay-phase plots for each chirp.
    """
    fig, [ax_s, ax_p, ax_w] = plt.subplots(3, 1)

    for c in chirp:
        strength = np.zeros(len(delay))
        phase = np.zeros(len(delay))
        linewidth = np.zeros(len(delay))
        for i, d in enumerate(delay):
            E, z = spectrum(hdf, 0.0, c, d)
            _, p, _ = fano.fit(E, z)

            strength[i] = p.strength
            phase[i] = p.phase
            linewidth[i] = p.width

        label = f"Chirp = {c:.5f}au"
        ax_s.plot(delay, strength, label=label)
        ax_p.plot(delay, phase)
        ax_w.plot(delay, linewidth)

    ax_w.set_xlabel("Delay (fs)")
    ax_s.set_ylabel("Strength")
    ax_p.set_ylabel("Phase")
    ax_w.set_ylabel("Linewidth")

    fig.legend()


def plot_resonance_fit(hdf, ir_chirp_au, delay_fs, debug=False):
    E, z = spectrum(hdf, 0.0, ir_chirp_au, delay_fs)
    fig = fano.fit_and_plot_resonance(E, z, debug=debug)
    fig.suptitle(f"Delay = {delay_fs:.3f}fs | NIR Chirp = {ir_chirp_au:.5f}")
