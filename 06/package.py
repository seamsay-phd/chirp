import json
import logging
import os
import re
import subprocess
import sys

import h5py
import numpy as np
import pandas as pd
from rmt_utilities import rmtutil, windowing

LOG_LEVEL_MAP = {
    "DEBUG": logging.DEBUG,
    "INFO": logging.INFO,
    "WARN": logging.WARN,
    "ERROR": logging.ERROR,
}

logging.basicConfig(
    level=LOG_LEVEL_MAP[os.environ.get("LOG_LEVEL", "INFO").upper()],
    format="{asctime} {levelname:>8s} {message}",
    style="{",
    stream=sys.stderr,
)

STAGE_DIR = os.path.abspath(os.path.dirname(__file__))
PROJ_DIR = os.path.dirname(STAGE_DIR)
DATA_DIR = os.path.join(PROJ_DIR, "05")
TAS_FILE = "TAS_0001_z"

DELAY_RGX = re.compile(r"^delay_([-+]\d{2}\.\d{6})fs$")
CHIRP_RGX = re.compile(r"^(?:ir|uv)_chirp_([-+]\d\.\d{6})au$")


NOT_FINISHED_DIRECTORIES = set(
    filter(
        lambda l: l != "WORK_DIR" and l != "",
        subprocess.run(
            ["squeue", "--me", "--format=%Z"],
            check=True,
            encoding="utf8",
            capture_output=True,
        ).stdout.split("\n"),
    )
)


def load_spectrum(calc_dir):
    if calc_dir in NOT_FINISHED_DIRECTORIES:
        logging.info("Ignoring directory as job is still running: %s", calc_dir)
        return

    pulse_path = os.path.join(calc_dir, "pulse.json")
    if os.path.exists(pulse_path):
        with open(pulse_path) as f:
            pulse = json.load(f)

        ir_chirp = pulse["NIR"]["chirp"]["au"]
        uv_chirp = pulse["XUV"]["chirp"]["au"]
        delay = pulse["NIR"]["middle"]["fs"] - pulse["XUV"]["middle"]["fs"]

        if ir_chirp == -0.0:
            ir_chirp = 0.0

        if uv_chirp == -0.0:
            uv_chirp = 0.0

        if delay == -0.0:
            delay = 0.0

        nir_end = int(round(pulse["NIR"]["roots"]["last"]["steps"]))
        xuv_end = int(round(pulse["XUV"]["roots"]["last"]["steps"]))
        window_start = max(nir_end, xuv_end)
    else:
        # Pulse data should exist for everything except the XUV-only calculation.
        assert os.path.basename(calc_dir) == "xuv_high_intensity"

        ir_chirp = np.nan
        uv_chirp = 0.0
        delay = np.nan
        window_start = 3765

    logging.info(
        "Processing run with: delay = %+010.6ffs | ir-chirp = %+012.9fau | uv-chirp = %+012.9fau",
        delay,
        ir_chirp,
        uv_chirp,
    )

    tas_path = os.path.join(calc_dir, TAS_FILE)
    if not os.path.exists(tas_path):
        logging.info(
            "Generating spectrum with a linear window starting at step `%d` for calculation `%s`.",
            window_start,
            calc_dir,
        )
        calc = rmtutil.RMTCalc(calc_dir)
        tas = calc.ATAS(window=windowing.LinearStep(window_start))
        tas.write(fname="TAS_", units="eV")
    else:
        logging.info(
            "`%s` exists in `%s`, not running `RMT_gen_tas`.", TAS_FILE, calc_dir
        )

    df = pd.read_table(tas_path, sep=" ", names=["E", "z"])
    return {"ir_chirp": ir_chirp, "uv_chirp": uv_chirp, "delay": delay, "data": df}


def assign_data(hdf, order, data=None, energy=None, signal=None):
    group = hdf
    for by, name, value, unit in order:
        by_group = group.require_group(f"by-{by}")
        group = by_group.require_group(name)
        if "value" not in group.attrs:
            group.attrs["value"] = value
            group.attrs["unit"] = unit

    if (energy is not None or signal is not None) and data is not None:
        raise ArgumentError("Both `data` and datasets were given.")
    elif data is not None:
        energy = group.create_dataset("E", data=data["E"].to_numpy())
        energy.attrs["unit"] = "eV"
        signal = group.create_dataset("z", data=data["z"].to_numpy())

        return energy, signal
    elif energy is not None or signal is not None:
        if energy is None or signal is None:
            raise ArgumentError("Only one of `data` or `signal` was given.")

        group["E"] = energy
        group["z"] = signal
    else:
        raise ArgumentError("Neither `data` nor the datasets were given.")


def main():
    attrs = {}
    df = pd.DataFrame()

    with h5py.File(os.path.join(os.path.dirname(__file__), "spectra.hdf5"), "w") as hdf:
        xuv_only_spectrum = load_spectrum(
            os.path.join(PROJ_DIR, "03", "xuv_high_intensity")
        )

        xuv_only_group = hdf.create_group("uv-only")
        xuv_only_energy = xuv_only_group.create_dataset(
            "E", data=xuv_only_spectrum["data"]["E"].to_numpy()
        )
        xuv_only_energy.attrs["unit"] = "eV"
        xuv_only_group.create_dataset(
            "z", data=xuv_only_spectrum["data"]["z"].to_numpy()
        )

        for ir_chirp_dir in os.listdir(DATA_DIR):
            match = CHIRP_RGX.match(ir_chirp_dir)
            if match is None:
                logging.info(
                    "Ignoring IR directory with unrecognised format: %s", ir_chirp_dir
                )
                continue

            ir_chirp_path = os.path.join(DATA_DIR, ir_chirp_dir)

            for uv_chirp_dir in os.listdir(ir_chirp_path):
                match = CHIRP_RGX.match(uv_chirp_dir)
                if match is None:
                    logging.info(
                        "Ignoring UV directory with unrecognised format: %s",
                        uv_chirp_dir,
                    )
                    continue

                uv_chirp_path = os.path.join(ir_chirp_path, uv_chirp_dir)

                for delay_dir in os.listdir(uv_chirp_path):
                    match = DELAY_RGX.match(delay_dir)
                    if match is None:
                        logging.info(
                            "Ignoring delay directory with unrecognised format: %s",
                            delay_dir,
                        )
                        continue

                    delay_path = os.path.join(uv_chirp_path, delay_dir)
                    spectrum = load_spectrum(delay_path)

                    ir_chirp = spectrum["ir_chirp"]
                    ir_chirp_name = f"{ir_chirp:+09.6f}au"

                    uv_chirp = spectrum["uv_chirp"]
                    uv_chirp_name = f"{uv_chirp:+09.6f}au"

                    delay = spectrum["delay"]
                    delay_name = f"{delay:+010.6f}fs"

                    energy, signal = assign_data(
                        hdf,
                        [
                            ("ir-chirp", ir_chirp_name, ir_chirp, "au"),
                            ("uv-chirp", uv_chirp_name, uv_chirp, "au"),
                            ("delay", delay_name, delay, "fs"),
                        ],
                        data=spectrum["data"],
                    )

                    assign_data(
                        hdf,
                        [
                            ("ir-chirp", ir_chirp_name, ir_chirp, "au"),
                            ("delay", delay_name, delay, "fs"),
                            ("uv-chirp", uv_chirp_name, uv_chirp, "au"),
                        ],
                        energy=energy,
                        signal=signal,
                    )

                    assign_data(
                        hdf,
                        [
                            ("uv-chirp", uv_chirp_name, uv_chirp, "au"),
                            ("ir-chirp", ir_chirp_name, ir_chirp, "au"),
                            ("delay", delay_name, delay, "fs"),
                        ],
                        energy=energy,
                        signal=signal,
                    )

                    assign_data(
                        hdf,
                        [
                            ("uv-chirp", uv_chirp_name, uv_chirp, "au"),
                            ("delay", delay_name, delay, "fs"),
                            ("ir-chirp", ir_chirp_name, ir_chirp, "au"),
                        ],
                        energy=energy,
                        signal=signal,
                    )

                    assign_data(
                        hdf,
                        [
                            ("delay", delay_name, delay, "fs"),
                            ("ir-chirp", ir_chirp_name, ir_chirp, "au"),
                            ("uv-chirp", uv_chirp_name, uv_chirp, "au"),
                        ],
                        energy=energy,
                        signal=signal,
                    )

                    assign_data(
                        hdf,
                        [
                            ("delay", delay_name, delay, "fs"),
                            ("uv-chirp", uv_chirp_name, uv_chirp, "au"),
                            ("ir-chirp", ir_chirp_name, ir_chirp, "au"),
                        ],
                        energy=energy,
                        signal=signal,
                    )


if __name__ == "__main__":
    main()
