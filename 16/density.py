import json
import math
import numpy as np
import os

from matplotlib import pyplot as plt
from rmt_utilities import rmtutil, windowing

STAGE_DIR = os.path.abspath(os.path.dirname(__file__))
PROJ_DIR = os.path.dirname(STAGE_DIR)


def main():
    calc_dir = os.path.join(
        PROJ_DIR, "12", "xuv=45.547eV-nir=1.423eV", "int=0.010", "delay=+05.000fs"
    )

    with open(os.path.join(calc_dir, "EField.json")) as io:
        efield_data = json.load(io)
        wstart = efield_data["NIR"]["end"]["steps"]

    window = windowing.LinearStep(math.ceil(wstart))

    calc = rmtutil.RMTCalc(calc_dir)
    calc._initExpecFiles(None)
    ddat = calc._FFT(calc.expec_z, cutoff=80 * rmtutil.eV, pad=8, window=window)
    Edat = calc._FFT(calc.field, cutoff=80 * rmtutil.eV, pad=8, window=window)

    assert np.allclose(ddat["Freq"], Edat["Freq"])

    energy = (ddat["Freq"] / rmtutil.eV).to_numpy()
    value = ddat["0001_z"].to_numpy() / Edat["0001_z"].to_numpy()
    conjugate = np.conjugate(value)

    E_lo_eV = 44
    E_hi_eV = 53

    reduced_energy = energy[(E_lo_eV < energy) & (energy < E_hi_eV)]
    reduced_value = value[(E_lo_eV < energy) & (energy < E_hi_eV)]
    reduced_conjugate = conjugate[(E_lo_eV < energy) & (energy < E_hi_eV)]

    density = np.outer(reduced_value, reduced_conjugate)

    fig, ax = plt.subplots(1, 1)
    # import matplotlib as mpl
    # ax.pcolormesh(reduced_energy, reduced_energy, np.abs(density), norm=mpl.colors.PowerNorm(0.3))

    E_per_step = np.mean(np.diff(reduced_energy))

    for k in range(reduced_energy.size):
        ax.plot(reduced_energy[k:], np.diag(np.abs(density), k) + k)

    fig.savefig("density.png")


if __name__ == "__main__":
    main()
