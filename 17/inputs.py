import argparse
import os
import sys

import numpy as np

from utilities import constants
from utilities import dimensions as dim
from utilities import laser
from utilities.rmt import runner

STAGE_DIR = os.path.dirname(__file__)
PROJ_DIR = os.path.dirname(STAGE_DIR)


def main(submit=False, overwrite=False):
    datadir = os.path.join(PROJ_DIR, "01")

    nir = dim.PhotonEnergy.from_eV(1.423)
    xuv = dim.PhotonEnergy.from_eV(45.547)

    runner.run(
        submit,
        overwrite,
        os.path.join(
            STAGE_DIR, "gaussian", "xuv-int=0.0001", f"xuv={xuv.to_eV():.3f}eV"
        ),
        datadir,
        laser.EFieldDataXUVOnly.gaussian(xuv),
        ringing_factor=160,
        time_limit=runner.TimeLimit(hours=6),
    )

    runner.run(
        submit,
        overwrite,
        os.path.join(
            STAGE_DIR, "gaussian", "xuv-int=0.0100", f"xuv={xuv.to_eV():.3f}eV"
        ),
        datadir,
        laser.EFieldDataXUVOnly.gaussian(
            xuv,
            intensity=dim.EFieldIntensity.from_100_TW_per_cm2(
                constants.NIR_INTENSITY_100_TW_per_cm2
            ),
        ),
        ringing_factor=160,
        time_limit=runner.TimeLimit(hours=18),
    )

    for fwhm_cycles in (3, 5, 7, 9):
        delays = np.arange(-3, 10)
        if fwhm_cycles == 3:
            delays = np.union1d(delays, np.arange(-20, 20))
            delays = np.union1d(delays, np.linspace(-3, 3, 13))
            delays = np.union1d(delays, np.linspace(-2, 2, 41))
            delays = np.union1d(delays, [-99, -40, 20, 40, 99])

            delays.sort()

        for delay_fs in delays:
            delay = dim.Time.from_fs(delay_fs)
            fwhm = dim.Time.from_cycles(fwhm_cycles, nir.period())
            gaussian = laser.EFieldDataNIRAndXUV.gaussian(
                nir, xuv, delay, nir_fwhm=fwhm
            )

            if sys.version_info.major > 3 or (
                sys.version_info.major == 3 and sys.version_info.minor >= 11
            ):
                delay_str = f"delay={delay_fs:+z07.3f}fs"
            else:
                if delay_fs == -0.0:
                    delay_fs = 0.0

                delay_str = f"delay={delay_fs:+07.3f}fs"

            if abs(delay_fs) > 20:
                time_limit = runner.TimeLimit(hours=20)
            else:
                time_limit = runner.TimeLimit(hours=2 * fwhm_cycles + fwhm_cycles // 3)

            runner.run(
                submit,
                overwrite,
                os.path.join(
                    STAGE_DIR, "gaussian", f"nir-fwhm={fwhm_cycles}cycles", delay_str
                ),
                datadir,
                gaussian,
                time_limit=time_limit,
            )

    delay = dim.Time.from_fs(5)
    for cycles in (12, 18, 24):
        sine_squared = laser.EFieldDataExactNIR.sine_squ_exact_nir(
            nir, xuv, delay, nir_duration=dim.Time.from_cycles(cycles, nir.period())
        )
        runner.run(
            submit,
            overwrite,
            os.path.join(STAGE_DIR, "sine-squared", f"cycles={cycles}"),
            datadir,
            sine_squared,
        )


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("-s", "--submit", action="store_true", help="submit the job")
    parser.add_argument(
        "-o",
        "--overwrite",
        action="store_true",
        help="remove and regenerate old data",
    )

    args = parser.parse_args()
    main(args.submit, args.overwrite)
