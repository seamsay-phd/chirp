import argparse
import os
import shutil

import f90nml
import numpy as np


def extract_time_from_hstat(io):
    _, check = io.readline().strip().split(maxsplit=1)
    assert check == "Previous Stage number", repr(check)

    t_index, check = io.readline().strip().split(maxsplit=1)
    assert check == "Most recently used value of TimeIndex", repr(check)
    t_index = int(t_index)

    check = io.readline().strip()
    assert check == "", repr(check)

    check, _ = io.readline().strip().rsplit(maxsplit=1)
    assert check == "Version                      =", repr(check)

    check, _ = io.readline().strip().rsplit(maxsplit=1)
    assert check == "Time (units of field period) =", repr(check)

    check, steps_per_output = io.readline().strip().rsplit(maxsplit=1)
    assert check == "Timesteps_Per_Output         =", repr(check)
    steps_per_output = int(steps_per_output)

    check = io.readline().strip()
    assert check == "", repr(check)

    check, _ = io.readline().strip().rsplit(maxsplit=1)
    assert check == "Number of PEs                =", repr(check)

    check, _ = io.readline().strip().rsplit(maxsplit=1)
    assert check == "Number of PEs Inner Region   =", repr(check)

    check, _ = io.readline().strip().rsplit(maxsplit=1)
    assert check == "Number of PEs Outer Region   =", repr(check)

    check = io.readline().strip()
    assert check == "", repr(check)

    check, _ = io.readline().strip().rsplit(maxsplit=1)
    assert check == "Z-N (charge of nucleus - number of electrons) =", repr(check)

    check, _ = io.readline().strip().rsplit(maxsplit=1)
    assert check == "lplusp                        =", repr(check)

    check, _ = io.readline().strip().rsplit(maxsplit=1)
    assert check == "Set_ML_Max                    =", repr(check)

    check, _ = io.readline().strip().rsplit(maxsplit=1)
    assert check == "ML_Max                        =", repr(check)

    check, _ = io.readline().strip().rsplit(maxsplit=1)
    assert check == "r_at_region_boundary          =", repr(check)

    check = io.readline().strip()
    assert check == "", repr(check)

    check, delta_t = io.readline().strip().rsplit(maxsplit=1)
    assert check == "DeltaT (au)                  =", repr(check)
    delta_t = float(delta_t)

    return t_index, delta_t, steps_per_output


# TODO: We should be able to do this truncation in-place, but I could only get that to work with no buffering and it's quicker to copy then delete with buffering than read with no-buffering.
def truncate_to_copy(
    source, destination, delta_t, t_index, timesteps_per_line, line_offset, calc_offset
):
    s = timesteps_per_line
    co = calc_offset
    lo = line_offset
    with open(source) as src_io, open(destination, "w") as dst_io:
        # First move to the position in the file which is _before_ the timestep we want to keep (so we can read the timestep we do want and run a sense-check).
        # +1 for the headers.
        for i in range(t_index // s + lo + 1):
            dst_io.write(src_io.readline())

        line = src_io.readline()

        # Then run a sense to check make sure we're at the right line.
        time, *_ = line.split()
        time = float(time)
        # +1 because a time index of zero corresponds to the first non-zero time.
        assert np.isclose(time, (s * (t_index // s) + 1 + co) * delta_t, rtol=1e-9)

        dst_io.write(line)


def main(calculation):
    configuration = f90nml.read(os.path.join(calculation, "input.conf"))
    version_root = configuration["inputdata"]["version_root"]
    suffix = f"{version_root:020s}"

    with open(os.path.join(calculation, f"hstat.{suffix}")) as io:
        t_index, delta_t, steps_per_output = extract_time_from_hstat(io)

    filenames = []

    # `EField.*` is the mean of current and previous step, so a half timestep offset is needed.
    # `expec_*` files (unlike all others) write a line for time zero, so an extra line needs to be read.
    # NOTE: A time index of zero corresponds to a time of `delta_t`.
    for root, timesteps_per_line, line_offset, calc_offset in (
        ("EField", 1, 0, -0.5),
        ("expec_z_all", 1, 1, 0),
        ("expec_v_all", 1, 1, 0),
        ("pop_all", steps_per_output, 0, 0),
        ("pop_inn", steps_per_output, 0, 0),
        ("pop_out", steps_per_output, 0, 0),
    ):
        name = f"{root}.{suffix}"
        source = os.path.join(calculation, name)
        destination = os.path.join(calculation, f"TEMPORARY-TRUNC-FILE.{name}")

        filenames.append((source, destination))

        truncate_to_copy(
            source,
            destination,
            delta_t,
            t_index,
            timesteps_per_line,
            line_offset,
            calc_offset,
        )

    for original, truncated in filenames:
        os.replace(truncated, original)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "calculation", help="directory in which the calculation was run"
    )
    args = parser.parse_args()

    main(args.calculation)
