import json
import os
import sys

import h5py
import matplotlib as mpl
import numpy as np
import scipy.optimize
from matplotlib import pyplot as plt
from utilities import fano, logging

directory = os.getcwd()
sys.path.append(directory)

report = sys.argv[1]
seminar = sys.argv[2]


logging.info(
    "Generating unchirped plots: directory=%s report=%s seminar=%s",
    directory,
    report,
    seminar,
)

# NOTE: Relies on the `sys.path` line above.
import spectra


def plot_resonance_fits(hdf, xuv_energy_eV, nir_energy_eV, delays_fs):
    assert len(delays_fs) == 4

    fig, axes = plt.subplots(2, 2)

    for delay_fs, ax, order in zip(delays_fs, axes.flatten(), ["a", "b", "c", "d"]):
        E, z = spectra.spectrum(hdf, xuv_energy_eV, nir_energy_eV, delay_fs)
        (o, _), (f_2s3p, _), _, _, (f_bg, _) = fano.fit(E, z)

        ax.plot(E, z, label="Absorption Spectrum")
        ax.plot(
            E,
            fano.lineshape(E, o, f_2s3p),
            label=r"$2s2p^63p \; {}^\mathrm{1}\mathrm{P}_\mathrm{o}$",
        )
        ax.plot(E, fano.lineshape(E, o, f_bg), label="Background")

        ax.grid(True, which="both")
        ax.tick_params(which="major", grid_alpha=1.0)
        ax.tick_params(which="minor", grid_alpha=0.2)

        ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator(5))
        ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())

        ax.annotate(f"({order})", (0.1, 0.9), xycoords="axes fraction")

    ymin = np.inf
    ymax = -np.inf
    xmin = np.inf
    xmax = -np.inf
    for ax in axes.flatten():
        this_ymin, this_ymax = ax.get_ylim()
        ymin = min(ymin, this_ymin)
        ymax = max(ymax, this_ymax)

        this_xmin, this_xmax = ax.get_xlim()
        xmin = min(xmin, this_xmin)
        xmax = max(xmax, this_xmax)

    for ax in axes.flatten():
        ax.set_ylim((ymin, ymax))
        ax.set_xlim((xmin, xmax))

    axes[0, 0].xaxis.set_tick_params(labelbottom=False)
    axes[0, 1].xaxis.set_tick_params(labelbottom=False)
    axes[0, 1].yaxis.set_tick_params(labelleft=False)
    axes[1, 1].yaxis.set_tick_params(labelleft=False)

    handles, labels = axes.flatten()[-1].get_legend_handles_labels()
    for ax in axes.flatten():
        _, this_labels = ax.get_legend_handles_labels()
        assert labels == this_labels

    fig.legend(handles, labels)
    fig.supxlabel("Energy (eV)")
    fig.supylabel("Optical Density")

    return fig


def plot_xuv_only(hdf, low_energy_eV, high_energy_eV):
    E_full = hdf["uv-only"]["E"][:]
    E = E_full[(low_energy_eV < E_full) & (E_full < high_energy_eV)]
    z = -hdf["uv-only"]["z"][(low_energy_eV < E_full) & (E_full < high_energy_eV)]

    fig, ax = plt.subplots(1)
    ax.plot(E, z)
    ax.annotate(
        r"$2s2p^63p \; {}^\mathrm{1}\mathrm{P}_\mathrm{o}$", (45.344, np.max(z))
    )
    ax.grid(True, which="both")

    ax.set_xlabel("Energy (eV)")
    ax.set_ylabel("Optical Density")

    return fig


def plot_delay_scans(hdf, xuv_energy_eV, nir_energies_eV, delays_fs):
    assert len(nir_energies_eV) == 2
    fig, axes = plt.subplots(1, 2)

    for ax, nir_energy_eV, delay_fs in zip(axes, nir_energies_eV, delays_fs):
        energy = None
        signal = []

        for d in delay_fs:
            E, z = spectra.spectrum(
                hdf, xuv_energy_eV, nir_energy_eV, d
            )

            if energy is not None:
                if energy.shape != E.shape or not np.allclose(energy, E):
                    z = np.interp(energy, E, z)
            else:
                energy = E

            signal.append(z)

        signal = np.vstack(signal)

        e_grid, d_grid = np.meshgrid(energy, delay_fs)

        pcm = ax.pcolormesh(e_grid, d_grid, signal, shading="gouraud", vmin=0.1, vmax=0.6)

        ax.set_title(f"NIR = {nir_energy_eV:.3f}eV")

    fig.colorbar(pcm)

    axes[0].annotate(
        "XUV (Pump)\nFirst",
        (np.min(energy), np.max(delay_fs)),
        (0.01, 0.99),
        textcoords="axes fraction",
        horizontalalignment="left",
        verticalalignment="top",
    )
    axes[0].annotate(
        "NIR (Probe)\nFirst",
        (np.min(energy), np.min(delay_fs)),
        (0.01, 0.01),
        textcoords="axes fraction",
        horizontalalignment="left",
        color="white",
    )

    axes[0].annotate("(a)", (0.9, 0.9), xycoords="axes fraction")
    axes[1].annotate("(b)", (0.9, 0.9), xycoords="axes fraction", color="white")

    fig.supxlabel("Energy (eV)")
    axes[0].set_ylabel("Delay (fs)")
    axes[1].yaxis.set_tick_params(labelleft=False)

    xmin0, xmax0 = axes[0].get_xlim()
    ymin0, ymax0 = axes[0].get_ylim()
    xmin1, xmax1 = axes[1].get_xlim()
    ymin1, ymax1 = axes[1].get_ylim()

    xmin = min(xmin0, xmin1)
    xmax = max(xmax0, xmax1)
    ymin = min(ymin0, ymin1)
    ymax = max(ymax0, ymax1)
    for ax in axes:
        ax.set_xlim((xmin, xmax))
        ax.set_ylim((ymin, ymax))

    return fig


def plot_time_scan_fits(delay_fits, resonance, min_time):
    fig, axes = plt.subplots(2, 2)
    [[ax_c, ax_p], [ax_s, ax_w]] = axes

    delays = delay_fits.loc[delay_fits.index >= min_time, :]

    ax_c.errorbar(
        delays.index,
        delays[f"{resonance}_c"],
        yerr=delays[f"{resonance}_c_e"],
        ecolor="tab:orange",
    )

    ax_p.errorbar(
        delays.index,
        delays[f"{resonance}_p"],
        yerr=delays[f"{resonance}_p_e"],
        ecolor="tab:orange",
    )

    ax_s.errorbar(
        delays.index,
        delays[f"{resonance}_s"],
        yerr=delays[f"{resonance}_s_e"],
        ecolor="tab:orange",
    )

    ax_w.errorbar(
        delays.index,
        delays[f"{resonance}_w"],
        yerr=delays[f"{resonance}_w_e"],
        ecolor="tab:orange",
    )

    ax_p.yaxis.tick_right()
    ax_w.yaxis.tick_right()
    ax_p.yaxis.set_label_position("right")
    ax_w.yaxis.set_label_position("right")

    ax_c.set_ylabel("Centre (eV)")
    ax_p.set_ylabel(r"Phase ($\pi$)")
    ax_s.set_ylabel("Strength")
    ax_w.set_ylabel("Width (eV)")

    ax_c.xaxis.set_tick_params(labelbottom=False)
    ax_p.xaxis.set_tick_params(labelbottom=False)

    ax_s.ticklabel_format(axis='y', style="sci", scilimits=(0, 0))

    for axis in axes.flatten():
        axis.grid(True, which="both")
        axis.tick_params(which="major", grid_alpha=1.0)
        axis.tick_params(which="minor", grid_alpha=0.2)

        axis.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator(5))
        axis.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())

    fig.subplots_adjust(right=0.875)
    fig.supxlabel("Time Delay (fs)")

    return fig


def plot_fit_no_background(hdf, xuv_energy_eV, nir_energy_eV, delay_fs):
    energy, spectrum = spectra.spectrum(hdf, xuv_energy_eV, nir_energy_eV, delay_fs)

    def f(
        E,
        offset,
        s_2s3p,
        p_2s3p,
        w_2s3p,
        s_2p43s3p,
        p_2p43s3p,
        w_2p43s3p,
        s_2s4p,
        p_2s4p,
        w_2s4p,
    ):
        fano_2s3p = spectra.fano.Fano(
            spectra.fano.RESONANCE_2s3p, s_2s3p, p_2s3p, w_2s3p
        )
        fano_2p43s3p = spectra.fano.Fano(
            spectra.fano.RESONANCE_2p43s3p, s_2p43s3p, p_2p43s3p, w_2p43s3p
        )
        fano_2s4p = spectra.fano.Fano(
            spectra.fano.RESONANCE_2s4p, s_2s4p, p_2s4p, w_2s4p
        )
        return spectra.fano.lineshape(E, offset, fano_2s3p, fano_2p43s3p, fano_2s4p)

    p0 = [
        0,
        *spectra.fano.INITIAL_2s3p[1:],
        *spectra.fano.INITIAL_2p43s3p[1:],
        *spectra.fano.INITIAL_2s4p[1:],
    ]
    lbounds = [
        -1,
        *spectra.fano.LOWER_2s3p[1:],
        *spectra.fano.LOWER_2p43s3p[1:],
        *spectra.fano.LOWER_2s4p[1:],
    ]
    ubounds = [
        1,
        *spectra.fano.UPPER_2s3p[1:],
        *spectra.fano.UPPER_2p43s3p[1:],
        *spectra.fano.UPPER_2s4p[1:],
    ]

    (
        offset,
        s_2s3p,
        p_2s3p,
        w_2s3p,
        s_2p43s3p,
        p_2p43s3p,
        w_2p43s3p,
        s_2s4p,
        p_2s4p,
        w_2s4p,
    ), _ = scipy.optimize.curve_fit(
        f,
        energy,
        spectrum,
        p0,
        bounds=(lbounds, ubounds),
        maxfev=999999,
    )

    fig_summed, ax_summed = plt.subplots(1, 1)

    ax_summed.plot(energy, spectrum, label="Original")
    ax_summed.plot(
        energy,
        spectra.fano.lineshape(
            energy,
            offset,
            spectra.fano.Fano(spectra.fano.RESONANCE_2s3p, s_2s3p, p_2s3p, w_2s3p),
            spectra.fano.Fano(
                spectra.fano.RESONANCE_2p43s3p, s_2p43s3p, p_2p43s3p, w_2p43s3p
            ),
            spectra.fano.Fano(spectra.fano.RESONANCE_2s4p, s_2s4p, p_2s4p, w_2s4p),
        ),
        label="Fit",
    )
    ax_summed.legend()

    ax_summed.set_xlabel("Energy (eV)")
    ax_summed.set_ylabel("Optical Density")

    fig_separate, ax_separate = plt.subplots(1, 1)

    ax_separate.plot(energy, spectrum, label="Original")
    ax_separate.plot(
        energy,
        spectra.fano.lineshape(
            energy,
            offset,
            spectra.fano.Fano(spectra.fano.RESONANCE_2s3p, s_2s3p, p_2s3p, w_2s3p),
        ),
        label=r"$2s2p^63p$ Resonance",
    )
    ax_separate.legend()

    ax_separate.set_xlabel("Energy (eV)")
    ax_separate.set_ylabel("Optical Density")

    return fig_summed, fig_separate

def plot_fit_background(hdf, xuv_energy_eV, nir_energy_eV, delay_fs):
    energy, spectrum = spectra.spectrum(hdf, xuv_energy_eV, nir_energy_eV, delay_fs)
    (o, _), (f_2s3p, _), (f_2p43s3p, _), (f_2s4p, _), (f_bg, _) = spectra.fano.fit(energy, spectrum)

    fig_summed, ax_summed = plt.subplots(1, 1)

    ax_summed.plot(energy, spectrum, label="Original")
    ax_summed.plot(
        energy,
        spectra.fano.lineshape(
            energy,
            o,
            f_2s3p, f_2p43s3p, f_2s4p, f_bg
        ),
        label="Fit",
    )
    ax_summed.legend()

    ax_summed.set_xlabel("Energy (eV)")
    ax_summed.set_ylabel("Optical Density")

    fig_separate, ax_separate = plt.subplots(1, 1)

    ax_separate.plot(energy, spectrum, label="Original")
    ax_separate.plot(
        energy,
        spectra.fano.lineshape(
            energy,
            o,
            f_2s3p,
        ),
        label=r"$2s2p^63p$ Resonance",
    )
    ax_separate.plot(
        energy,
        spectra.fano.lineshape(
            energy,
            o,
            f_bg,
        ),
        label=r"Background Resonance",
    )
    ax_separate.legend()

    ax_separate.set_xlabel("Energy (eV)")
    ax_separate.set_ylabel("Optical Density")

    return fig_summed, fig_separate


hdf = h5py.File("spectra.hdf5")

fig = plot_xuv_only(hdf, 44.4, 48.1)
fig.savefig(os.path.join(report, "xuv-only-spectrum.pdf"))
fig.savefig(os.path.join(seminar, "xuv-only-spectrum.png"))

fig = spectra.plot_delay_scan(hdf, 45.547, 1.376, delay_fs=np.linspace(-5, 10, 31))
fig.axes[0].set_title("")
fig.savefig(os.path.join(seminar, "TimeDelay-NIR=1.376eV.png"))

fig = plot_delay_scans(hdf, 45.547, [1.571, 1.376], [np.linspace(-5, 5, 21), np.linspace(-5, 10, 31)])
fig.savefig(os.path.join(report, "TimeDelay.pdf"))

fig = plot_resonance_fits(hdf, 45.547, 1.376, [-5, 0, 5, 10])
fig.savefig(os.path.join(report, "ResonanceFits.pdf"))

xuv_energy_eV = 45.547
nir_energy_eV = 1.376
delay_fs = np.linspace(-5, 10, 31)
delay_fits = spectra.calculate_delay_fits(hdf, xuv_energy_eV, nir_energy_eV, delay_fs)

fig = plot_time_scan_fits(delay_fits, "2s3p", -5)
fig.savefig(os.path.join(report, "DelayScanFits-NIR=1.376eV-2s3p.pdf"))
fig.savefig(os.path.join(seminar, "DelayScanFits-NIR=1.376eV-2s3p.png"))

fig = plot_time_scan_fits(delay_fits, "bg", -1)
fig.savefig(os.path.join(report, "DelayScanFits-NIR=1.376eV-Background.pdf"))
fig.savefig(os.path.join(seminar, "DelayScanFits-NIR=1.376eV-Background.png"))

fig_summed, fig_separate = plot_fit_no_background(hdf, xuv_energy_eV, nir_energy_eV, 5)
fig_summed.savefig(os.path.join(seminar, "ResonanceFit-Summed-NoBackground-Delay=5fs.png"))
fig_separate.savefig(os.path.join(seminar, "ResonanceFit-NoBackground-Delay=5fs.png"))

fig_summed, fig_separate = plot_fit_background(hdf, xuv_energy_eV, nir_energy_eV, 5)
fig_summed.savefig(os.path.join(seminar, "ResonanceFit-Summed-Delay=5fs.png"))
fig_separate.savefig(os.path.join(seminar, "ResonanceFit-Delay=5fs.png"))
