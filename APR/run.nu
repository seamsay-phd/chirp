let root = $env.FILE_PWD | path dirname
let scripts = $env.FILE_PWD

let report = $scripts | path join report
mkdir $report

let seminar = $scripts | path join seminar
mkdir $seminar

do {
    cd ($root | path join 09)
    python ($scripts | path join unchirped.py) $report $seminar
}