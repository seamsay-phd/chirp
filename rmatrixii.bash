#!/usr/bin/env bash

set -euo pipefail

bindir="$(realpath "$1")"
input="$(realpath "$2")"
outdir="$(realpath "$3")"
radlog="$(realpath "$4")"
anglog="$(realpath "$5")"
hamlog="$(realpath "$6")"

ulimit -s unlimited
export OMP_NUM_THREADS=1

cd "$outdir"

"$bindir/rad.x" <"$input" | tee "$radlog"
"$bindir/ang.x" <"$input" | tee "$anglog"
"$bindir/ham.x" <"$input" | tee "$hamlog"
