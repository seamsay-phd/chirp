import collections
import os
import subprocess

from utilities.rmatrixii import input

STAGE_DIR = os.path.abspath(os.path.dirname(__file__))
PROJ_DIR = os.path.dirname(STAGE_DIR)

INPUT_FILE_NAME = "neon.inp"


def run(name, experimental_thresholds):
    n_ang_mom = 35
    excitations = 3

    directory = os.path.relpath(os.path.join(STAGE_DIR, "RMatrixII", name))
    if os.path.exists(directory):
        return

    os.makedirs(directory)

    thresholds = collections.Counter(term for term, _ in experimental_thresholds)

    input_file = os.path.join(directory, INPUT_FILE_NAME)
    with open(input_file, "w") as io:
        input.generate_input(
            io,
            thresholds,
            input.ORBITALS,
            input.generate_configurations(
                input.ORBITALS,
                input.GROUND_STATE_CONFIGURATION,
                input.LOCKED_ORBITALS,
                input.FORBIDDEN_CONFIGURATIONS,
                excitations,
            ),
            input.GROUND_STATE_CONFIGURATION,
            input.SLATER_COEFFICIENTS,
            experimental_thresholds,
            n_ang_mom,
        )

    subprocess.run(
        [
            os.path.join(PROJ_DIR, "rmatrixii.bash"),
            os.path.join(PROJ_DIR, "RMatrixII", "build", "bin"),
            input_file,
            directory,
            os.path.join(directory, "rad.out"),
            os.path.join(directory, "ang.out"),
            os.path.join(directory, "ham.out"),
        ],
        check=True,
    )


def main():
    run("With-2Se", input.EXPERIMENTAL_THRESHOLDS)

    no_2Se_threshold = input.EXPERIMENTAL_THRESHOLDS.copy()
    assert no_2Se_threshold[1][0] == input.Term(2, 0, 0)
    del no_2Se_threshold[1]
    run("Without-2Se", no_2Se_threshold)


if __name__ == "__main__":
    main()
