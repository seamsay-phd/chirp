import argparse
import os

import numpy as np

from utilities import constants
from utilities import dimensions as dim
from utilities import laser, logging
from utilities.rmt import runner

STAGE_DIR = os.path.dirname(__file__)
PROJ_DIR = os.path.dirname(STAGE_DIR)


def run(submit: bool, overwrite: bool, subdir: str, e_field_data: laser.EFieldData):
    outdir = os.path.join(STAGE_DIR, "RMT", subdir, *e_field_data.dirname())
    datadir = os.path.join(STAGE_DIR, "RMatrixII", subdir)

    ringing_factor = 3.0
    logging.info(
        "Running job (%s) with: NIR = %.3feV (Intensity = %.3f) | XUV = %.3feV | Delay = %.3ffs",
        subdir,
        e_field_data.nir.energy.to_eV(),
        e_field_data.nir.intensity.to_100_TW_per_cm2(),
        e_field_data.xuv.energy.to_eV(),
        e_field_data.delay().to_fs(),
    )

    runner.run(
        submit,
        overwrite,
        outdir,
        datadir,
        e_field_data,
        ringing_factor,
        time_limit=runner.TimeLimit(hours=16),
        inner_cores=256,
        outer_cores=1024,
        x_last_others=52,
        x_last_master=16,
    )


def main(submit=False, overwrite=False):
    xuv_energy = dim.PhotonEnergy.from_eV(45.547)
    nir_energy = dim.PhotonEnergy.from_eV(1.423)

    for subdir in ["With-2Se", "Without-2Se"]:
        for intensity in [1 / 10, 1 / 2]:
            nir_intensity = dim.EFieldIntensity.from_100_TW_per_cm2(intensity)

            for delay_fs in [-20, -10, -5, -3, 0, 3, 5, 10, 20, 40]:
                delay = dim.Time.from_fs(delay_fs)

                e_field = laser.EFieldDataNIRAndXUV.gaussian(
                    nir_energy, xuv_energy, delay, nir_intensity
                )

                run(submit, overwrite, subdir, e_field)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("-s", "--submit", action="store_true", help="submit the job")
    parser.add_argument(
        "-o",
        "--overwrite",
        action="store_true",
        help="remove and regenerate old data",
    )

    args = parser.parse_args()
    main(args.submit, args.overwrite)
