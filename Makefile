RMT_SOURCE := $(shell find RMT/source/modules/ -iname '*.f90')
RMT_SOURCE += $(shell find RMT/source/modules/ -iname 'CMakeLists.txt')
RMT_SOURCE += $(shell find RMT/source/programs/ -iname '*.f90')
RMT_SOURCE += $(shell find RMT/source/programs/ -iname 'CMakeLists.txt')

RMATRIXII_SOURCE := $(shell find RMatrixII/source/ -iname '*.f90')
RMATRIXII_SOURCE += $(shell find RMatrixII/source/ -iname 'CMakeLists.txt')

FARM_SOURCE := $(shell find FARM/source/ -iname '*.f')
FARM_SOURCE += $(shell find FARM/source/ -iname 'CMakeLists.txt')

.PHONY: fetch
fetch:
	./scripts/fetch-data.py

.PHONY: stage00
stage00: RMT/build/bin/rmt.x RMatrixII/build/bin/ang.x RMatrixII/build/bin/ham.x RMatrixII/build/bin/rad.x FARM/build/bin/farm

RMT/build/bin/rmt.x: $(RMT_SOURCE)
	00/build.bash RMT

RMatrixII/build/bin/ang.x RMatrixII/build/bin/ham.x RMatrixII/build/bin/rad.x: $(RMatrixII_SOURCE)
	00/build.bash RMatrixII

FARM/build/bin/farm: $(FARM_SOURCE)
	00/build.bash FARM

.PHONY: stage01
stage01: 01/d 01/d00 01/H 01/Splinewaves 01/Splinedata

01/d 01/d00 01/H 01/Splinewaves 01/Splinedata: 00/neon.inp
	01/rmatrixii.bash build/RMatrixII/bin 00/neon.inp 01 01/rad.out 01/ang.out 01/ham.out
