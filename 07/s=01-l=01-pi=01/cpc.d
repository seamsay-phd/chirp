&phzfil
    filh = './s=01-l=01-pi=01/H'
    fkmat = './s=01-l=01-pi=01/CPCKMATB'
    flwp = './s=01-l=01-pi=01/lwpscr'
    fom = './s=01-l=01-pi=01/CPCOMEGAB'
    fout = './s=01-l=01-pi=01/log.out'
    fpha = './s=01-l=01-pi=01/CPCPHASEF'
    ftmat = './s=01-l=01-pi=01/CPCTMATB'
/

&phzin
    diaflg = 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 1
    esc1 = 1.5718753257778995, 3.674929285e-06
    ll = 1
    lpi = 1
    lrglx = 10
    ls = 1
    ne1 = 105059
    prtflg = 3, 3, 3, 0, 3, 0
    title = 'Neon'
/

