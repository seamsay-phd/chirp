import itertools
import os
import re
import sys

import farm_utilities.phase as farm
import h5py
import numpy as np
import utilities.farm
from matplotlib import pyplot as plt

LITERATURE_DATA_1Se = [
    (
        "2s2p(6)3s",
        43.67,
    ),  # Sharp, J. M., Comer, J. & Hicks, P. J. Autoionizing transitions in neon studied by low-energy electron impact. J. Phys. B: Atom. Mol. Phys. 8, 2512 (1975).
    (
        "2s2p(6)4s",
        46.64,
    ),  # Jureta, J. J., Marinković, B. P., Milosavljević, A. R. & Avaldi, L. Singly and doubly excited states in ejected electron spectra of neon at high incident electron energies. Eur. Phys. J. D 69, 74 (2015).
    (
        "2s2p(6)5s",
        47.52,
    ),  # Jureta, J. J., Marinković, B. P., Milosavljević, A. R. & Avaldi, L. Singly and doubly excited states in ejected electron spectra of neon at high incident electron energies. Eur. Phys. J. D 69, 74 (2015).    ("Unknown", np.nan),
    (
        "2s2p(6)6s",
        47.99,
    ),  # Jureta, J. J., Marinković, B. P., Milosavljević, A. R. & Avaldi, L. Singly and doubly excited states in ejected electron spectra of neon at high incident electron energies. Eur. Phys. J. D 69, 74 (2015).
    ("Unknown", np.nan),
    ("Unknown", np.nan),
]

LITERATURE_DATA_1Po = [
    (
        "2p(4)3s3p",
        44.980,
    ),  # Codling, K., Madden, R. P. & Ederer, D. L. Resonances in the Photo-Ionization Continuum of Ne I (20-150 eV). Phys. Rev. 155, 26–37 (1967).
    ("2s2p(6)3p", 45.547),  # NIST
    ("2s2p(6)4p", 47.123),  # NIST
    ("2s2p(6)5p", 47.694),  # NIST
    ("Unknown", np.nan),
    ("2s2p(6)6p", 47.967),  # NIST
    ("2s2p(6)7p", 48.116),  # NIST
    # ("2s2p(6)8p", 48.207),  # NIST
    # ("2s2p(6)9p", 48.271),  # NIST
    # ("2s2p(6)10p", 48.312),  # NIST
    # ("2s2p(6)11p", 48.344),  # NIST
    # ("2s2p(6)12p", 48.365),  # NIST
]

LITERATURE_DATA_1De = [
    ("Unknown", np.nan),
    ("Unknown", np.nan),
    ("Unknown", np.nan),
    ("Unknown", np.nan),
    ("Unknown", np.nan),
    ("Unknown", np.nan),
    ("Unknown", np.nan),
]

# Ionisation energy is as stated in RMatrixII output.
IONISATION_ENERGY_eV = 1.555269 / utilities.farm.ENERGY_Ryd_per_eV
STAGE_DIR = os.path.abspath(os.path.dirname(__file__))
PROJ_DIR = os.path.dirname(STAGE_DIR)
DATA_DIR = os.path.join(PROJ_DIR, "01")

REAL_RGX = re.compile(r"^(-?\d+\.\d+)[dD]([-+]?\d+)$")


def dirname(energy_low_eV, energy_high_eV, energy_step_eV, spinlet, azimuthal, parity):
    return f"s={spinlet:02d}-l={azimuthal:02d}-pi={parity:02d}"


def run_read_find(
    energy_low_eV,
    energy_high_eV,
    energy_step_eV,
    spinlet,
    azimuthal,
    parity,
    literature_data,
    interest_low_eV=None,
    interest_high_eV=None,
    overwrite=False,
):
    outdir = os.path.join(
        os.path.relpath(STAGE_DIR, "."),
        dirname(
            energy_low_eV, energy_high_eV, energy_step_eV, spinlet, azimuthal, parity
        ),
    )
    h_file = os.path.join(DATA_DIR, "H")

    files = {
        key: os.path.join(outdir, name) for key, name in utilities.farm.FILES.items()
    }

    if not os.path.exists(outdir):
        os.makedirs(outdir)
        os.symlink(os.path.relpath(h_file, outdir), os.path.join(outdir, "H"))

        conf = utilities.farm.run(
            utilities.farm.configuration(
                energy_low_eV,
                energy_high_eV,
                energy_step_eV,
                spinlet,
                azimuthal,
                parity,
                ionisation_energy_eV=IONISATION_ENERGY_eV,
                files=files,
            ),
            overwrite=overwrite,
            executable=os.path.join(PROJ_DIR, "FARM", "build", "bin", "farm"),
            files=files,
        )

        with open(os.path.join(outdir, "cpc.d"), "w") as io:
            print(conf, file=io)

    energies_eV, phases = farm.read_phase_data(files["phase"], IONISATION_ENERGY_eV)

    peaks = [peak["fit"] for peak in farm.fit_peaks(energies_eV, phases)]
    peaks_eV = np.array([peak.centre for peak in peaks])

    assert len(peaks) == len(
        literature_data
    ), "{} peaks but {} literature entries".format(len(peaks), len(literature_data))

    states, lit_energies_eV = map(np.array, zip(*literature_data))

    interest = np.repeat(True, len(peaks))

    if interest_low_eV is not None:
        interest &= interest_low_eV <= peaks_eV

    if interest_high_eV is not None:
        interest &= peaks_eV <= interest_high_eV

    peaks = list(itertools.compress(peaks, interest))
    states = states[interest]
    lit_energies_eV = lit_energies_eV[interest]

    return farm.derivative(energies_eV, phases), peaks, states, lit_energies_eV


def tabulate_state_energies(states, lit_energies_eV, farm_energies_eV, io=sys.stdout):
    assert len(states) == len(lit_energies_eV) == len(farm_energies_eV)

    print(
        "{:10s} {:22s} {:16s} {:9s}".format(
            "State",
            "Literature Energy (eV)",
            "FARM Energy (eV)",
            "Diff (eV)",
        ),
        file=io,
    )
    for state, lit_energy_eV, farm_energy_eV in zip(
        states, lit_energies_eV, farm_energies_eV
    ):
        farm_diff_eV = farm_energy_eV - lit_energy_eV

        print(
            f"{state:10s} {lit_energy_eV:22.3f} {farm_energy_eV:16.3f} {farm_diff_eV:9.3f}",
            file=io,
        )


def tabulate_couplings(
    first_excited_state,
    first_excited_energy,
    second_excited_states,
    second_excited_energies,
):
    assert len(second_excited_states) == len(second_excited_energies)

    s1 = first_excited_state
    e1 = first_excited_energy

    print("{:22s} {:22s}".format("1st Excitation", "2nd Excitation"))
    print(
        "{:10s} {:11s} {:10s} {:11s} {:20s}".format(
            "State", "Energy (eV)", "State", "Energy (eV)", "Coupling Energy (eV)"
        )
    )
    for s2, e2 in zip(second_excited_states, second_excited_energies):
        de = e2 - e1
        print(f"{s1:10s} {e1:11.3f} {s2:10s} {e2:11.3f} {de:20.3f}")


def tabulate_states(peaks, io=sys.stdout):
    print(
        "{:21s} {:13s} {:10s}".format("Resonance Energy (eV)", "Strength", "Width (eV)")
    )
    for peak in peaks:
        print(f"{peak.centre:21.6f} {peak.strength:13.6f} {peak.width:10.6f}", file=io)


def main():
    resonance_eV = 45.547
    ir_max_eV = 3
    # Rough ionisation energy of 2s electron, FARM craps out at this point.
    threshold_eV = 47.8

    energy_low_eV = resonance_eV - ir_max_eV
    energy_high_eV = min(resonance_eV + ir_max_eV, threshold_eV)
    energy_step_eV = 0.00005

    interest_high_eV = energy_high_eV

    _, peaks_1Po, states_1Po, lit_energies_eV_1Po = run_read_find(
        energy_low_eV,
        energy_high_eV,
        energy_step_eV,
        1,
        1,
        1,
        LITERATURE_DATA_1Po,
        interest_high_eV=interest_high_eV,
    )
    peaks_eV_1Po = np.array([peak.centre for peak in peaks_1Po])
    print("1Po")
    tabulate_state_energies(states_1Po, lit_energies_eV_1Po, peaks_eV_1Po)

    peak_eV_2s3p = peaks_eV_1Po[1]

    _, peaks_1Se, states_1Se, lit_energies_eV_1Se = run_read_find(
        energy_low_eV,
        energy_high_eV,
        energy_step_eV,
        1,
        0,
        0,
        LITERATURE_DATA_1Se,
        interest_high_eV=interest_high_eV,
    )
    peaks_eV_1Se = np.array([peak.centre for peak in peaks_1Se])
    print()
    print("1Se")
    tabulate_state_energies(states_1Se, lit_energies_eV_1Se, peaks_eV_1Se)

    _, peaks_1De, states_1De, lit_energies_eV_1De = run_read_find(
        energy_low_eV,
        energy_high_eV,
        energy_step_eV,
        1,
        2,
        0,
        LITERATURE_DATA_1De,
        interest_high_eV=interest_high_eV,
    )
    peaks_eV_1De = np.array([peak.centre for peak in peaks_1De])
    print()
    print("1De")
    tabulate_state_energies(states_1De, lit_energies_eV_1De, peaks_eV_1De)

    print()
    print("1Se Coupling Energies")
    tabulate_couplings("2s2p(6)3p", peak_eV_2s3p, states_1Se, peaks_eV_1Se)

    print()
    print("1De Coupling Energies")
    tabulate_couplings("2s2p(6)3p", peak_eV_2s3p, states_1De, peaks_eV_1De)

    print()
    print("1Po State Peaks")
    tabulate_states(peaks_1Po)
    with open(os.path.join(STAGE_DIR, "1Po.txt"), "w") as io:
        tabulate_states(peaks_1Po, io)

    print()
    print("1Se State Peaks")
    tabulate_states(peaks_1Se)
    with open(os.path.join(STAGE_DIR, "1Se.txt"), "w") as io:
        tabulate_states(peaks_1Se, io)

    print()
    print("1De State Peaks")
    tabulate_states(peaks_1De)
    with open(os.path.join(STAGE_DIR, "1De.txt"), "w") as io:
        tabulate_states(peaks_1De, io)


if __name__ == "__main__":
    main()
