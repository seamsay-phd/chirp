import os
import re

import numpy as np
import scipy.interpolate
import scipy.optimize
from matplotlib import pyplot as plt

from utilities import constants
from utilities import dimensions as dim
from utilities import fano, laser
from utilities.rmt import packaging

STAGE_DIR = os.path.dirname(os.path.abspath(__file__))
PROJ_DIR = os.path.dirname(STAGE_DIR)


def fit_uv(energy, spectrum):
    def f(
        E,
        o_m,
        o_c,
        c_low,
        s_low,
        p_low,
        w_low,
        c_high,
        s_high,
        p_high,
        w_high,
    ):
        f_low = fano.Fano(c_low, s_low, p_low, w_low)
        f_high = fano.Fano(c_high, s_high, p_high, w_high)

        return fano.lineshape(E, o_m, o_c, f_low, f_high)

    p0 = [
        # Offset
        0.003,  # Gradient
        -0.41,  # Intercept
        # Lower Energy Resonance
        44.8,  # Centre
        0.001,  # Strength
        0.23 * np.pi,  # Phase
        0.03,  # Width
        # Higher Energy Resonance
        46.55,  # Centre
        0.007,  # Strength
        0.37 * np.pi,  # Phase
        0.08,  # Width
    ]
    lbounds = [
        # Offset
        -1,  # Gradient
        -1,  # Intercept
        # Lower Energy Resonance
        44,  # Centre
        0,  # Strength
        -np.pi,  # Phase
        0,  # Width
        # Higher Energy Resonance
        45,  # Centre
        0,  # Strength
        -np.pi,  # Phase
        0,  # Width
    ]
    ubounds = [
        # Offset
        1,  # Gradient
        1,  # Intercept
        # Lower Energy Resonance
        45,  # Centre
        np.inf,  # Strength
        np.pi,  # Phase
        np.inf,  # Width
        # Higher Energy Resonance
        47,  # Centre
        np.inf,  # Strength
        np.pi,  # Phase
        np.inf,  # Width
    ]

    parameters, covariance = scipy.optimize.curve_fit(
        f, energy, spectrum, p0, bounds=(lbounds, ubounds)
    )
    errors = np.sqrt(np.diag(covariance))

    (
        o_m,
        o_c,
        c_low,
        s_low,
        p_low,
        w_low,
        c_high,
        s_high,
        p_high,
        w_high,
    ) = parameters

    (
        err_o_m,
        err_o_c,
        err_c_low,
        err_s_low,
        err_p_low,
        err_w_low,
        err_c_high,
        err_s_high,
        err_p_high,
        err_w_high,
    ) = errors

    return (
        (o_m, err_o_m),
        (o_c, err_o_c),
        (
            fano.Fano(c_low, s_low, p_low, w_low),
            fano.FanoParameters(err_c_low, err_s_low, err_p_low, err_w_low),
        ),
        (
            fano.Fano(c_high, s_high, p_high, w_high),
            fano.FanoParameters(err_c_high, err_s_high, err_p_high, err_w_high),
        ),
    )


def fit_fixed_background(
    energy,
    spectrum,
    fi_low,
    fi_high,
    background,
):
    def f(
        E,
        o_m,
        o_c,
        c_low,
        s_low,
        p_low,
        w_low,
        c_high,
        s_high,
        p_high,
        w_high,
    ):
        f_low = fano.Fano(c_low, s_low, p_low, w_low)
        f_high = fano.Fano(c_high, s_high, p_high, w_high)

        return background + fano.lineshape(E, o_m, o_c, f_low, f_high)

    p0 = [
        # Offset
        0.003,  # Gradient
        -0.41,  # Intercept
        # Lower Energy Resonance
        fi_low.centre,
        fi_low.strength_per_width,
        fi_low.phase,
        fi_low.width,
        # Higher Energy Resonance
        fi_high.centre,
        fi_high.strength_per_width,
        fi_high.phase,
        fi_high.width,
    ]
    lbounds = [
        # Offset
        -1,  # Gradient
        -1,  # Intercept
        # Lower Energy Resonance
        44,  # Centre
        0,  # Strength
        -np.pi,  # Phase
        0,  # Width
        # Higher Energy Resonance
        45,  # Centre
        0,  # Strength
        -np.pi,  # Phase
        0,  # Width
    ]
    ubounds = [
        # Offset
        1,  # Gradient
        1,  # Intercept
        # Lower Energy Resonance
        45,  # Centre
        np.inf,  # Strength
        np.pi,  # Phase
        np.inf,  # Width
        # Higher Energy Resonance
        47,  # Centre
        np.inf,  # Strength
        np.pi,  # Phase
        np.inf,  # Width
    ]

    parameters, covariance = scipy.optimize.curve_fit(
        f, energy, spectrum, p0, bounds=(lbounds, ubounds)
    )
    errors = np.sqrt(np.diag(covariance))

    (
        o_m,
        o_c,
        c_low,
        s_low,
        p_low,
        w_low,
        c_high,
        s_high,
        p_high,
        w_high,
    ) = parameters

    (
        err_o_m,
        err_o_c,
        err_c_low,
        err_s_low,
        err_p_low,
        err_w_low,
        err_c_high,
        err_s_high,
        err_p_high,
        err_w_high,
    ) = errors

    return (
        (o_m, err_o_m),
        (o_c, err_o_c),
        (
            fano.Fano(c_low, s_low, p_low, w_low),
            fano.FanoParameters(err_c_low, err_s_low, err_p_low, err_w_low),
        ),
        (
            fano.Fano(c_high, s_high, p_high, w_high),
            fano.FanoParameters(err_c_high, err_s_high, err_p_high, err_w_high),
        ),
    )


def fit_cross_section(energy, spectrum, fi_low, nir_spectrum, fuv_low, initial_cs):
    def f(
        E,
        cross_section,
        o_m,
        o_c,
        c_low,
        s_low,
        p_low,
        w_low,
    ):
        background = fano.background_spectrum(
            E,
            nir_spectrum,
            cross_section,
            fuv_low,
        )
        f_low = fano.Fano(c_low, s_low, p_low, w_low)

        return background + fano.lineshape(E, o_m, o_c, f_low)

    p0 = [
        initial_cs,  # Cross Section
        # Offset
        0.003,  # Gradient
        -0.41,  # Intercept
        # Lower Energy Resonance
        fi_low.centre,
        fi_low.strength_per_width,
        fi_low.phase,
        fi_low.width,
    ]
    lbounds = [
        0.0,  # Cross Section
        # Offset
        -1,  # Gradient
        -1,  # Intercept
        # Lower Energy Resonance
        44,  # Centre
        0,  # Strength
        -np.pi,  # Phase
        0,  # Width
    ]
    ubounds = [
        np.inf,  # Cross Section
        # Offset
        1,  # Gradient
        1,  # Intercept
        # Lower Energy Resonance
        45,  # Centre
        np.inf,  # Strength
        np.pi,  # Phase
        np.inf,  # Width
    ]

    parameters, covariance = scipy.optimize.curve_fit(
        f, energy, spectrum, p0, bounds=(lbounds, ubounds)
    )
    errors = np.sqrt(np.diag(covariance))

    (
        cross_section,
        o_m,
        o_c,
        c_low,
        s_low,
        p_low,
        w_low,
    ) = parameters

    (
        err_cross_section,
        err_o_m,
        err_o_c,
        err_c_low,
        err_s_low,
        err_p_low,
        err_w_low,
    ) = errors

    return (
        (cross_section, err_cross_section),
        (o_m, err_o_m),
        (o_c, err_o_c),
        (
            fano.Fano(c_low, s_low, p_low, w_low),
            fano.FanoParameters(err_c_low, err_s_low, err_p_low, err_w_low),
        ),
    )


def fit_background_fanos(energy, spectrum, initial_fanos, xuv_fanos):
    def f(
        E,
        o_m,
        o_c,
        c_low,
        s_low,
        p_low,
        w_low,
        cbg_low,
        sbg_low,
        pbg_low,
        wbg_low,
        c_high,
        s_high,
        p_high,
        w_high,
        cbg_high,
        sbg_high,
        pbg_high,
        wbg_high,
    ):
        f_low = fano.Fano(c_low, s_low, p_low, w_low)
        fbg_low = fano.Fano(cbg_low, sbg_low, pbg_low, wbg_low)
        f_high = fano.Fano(c_high, s_high, p_high, w_high)
        fbg_high = fano.Fano(cbg_high, sbg_high, pbg_high, wbg_high)

        return fano.lineshape(E, o_m, o_c, f_low, fbg_low, f_high, fbg_high)

    if_low, if_high = initial_fanos
    xuv_low, xuv_high = xuv_fanos

    p0 = [
        # Offset
        0.003,  # Gradient
        -0.41,  # Intercept
        # Lower Energy Resonance
        if_low.centre,
        if_low.strength_per_width,
        if_low.phase,
        if_low.width,
        # Lower Background Resonance
        xuv_low.centre,
        1e-6,  # Strength
        xuv_low.phase,
        xuv_low.width,
        # Higher Energy Resonance
        if_high.centre,
        if_high.strength_per_width,
        if_high.phase,
        if_high.width,
        # Higher Background Resonance
        xuv_high.centre,
        1e-6,  # Strength
        xuv_high.phase,
        xuv_high.width,
    ]
    lbounds = [
        # Offset
        -1,  # Gradient
        -1,  # Intercept
        # Lower Energy Resonance
        44,  # Centre
        0,  # Strength
        -np.pi,  # Phase
        0,  # Width
        # Lower Background Resonance
        44,  # Centre
        0,  # Strength
        -np.pi,  # Phase
        0,  # Width
        # Higher Energy Resonance
        45,  # Centre
        0,  # Strength
        -np.pi,  # Phase
        0,  # Width
        # Higher Background Resonance
        45,  # Centre
        0,  # Strength
        -np.pi,  # Phase
        0,  # Width
    ]
    ubounds = [
        # Offset
        1,  # Gradient
        1,  # Intercept
        # Lower Energy Resonance
        45,  # Centre
        np.inf,  # Strength
        np.pi,  # Phase
        np.inf,  # Width
        # Lower Background Resonance
        45,  # Centre
        np.inf,  # Strength
        np.pi,  # Phase
        np.inf,  # Width
        # Higher Energy Resonance
        47,  # Centre
        np.inf,  # Strength
        np.pi,  # Phase
        np.inf,  # Width
        # Higher Background Resonance
        47,  # Centre
        np.inf,  # Strength
        np.pi,  # Phase
        np.inf,  # Width
    ]

    parameters, covariance = scipy.optimize.curve_fit(
        f, energy, spectrum, p0, bounds=(lbounds, ubounds)
    )
    errors = np.sqrt(np.diag(covariance))

    (
        o_m,
        o_c,
        c_low,
        s_low,
        p_low,
        w_low,
        cbg_low,
        sbg_low,
        pbg_low,
        wbg_low,
        c_high,
        s_high,
        p_high,
        w_high,
        cbg_high,
        sbg_high,
        pbg_high,
        wbg_high,
    ) = parameters

    (
        err_o_m,
        err_o_c,
        err_c_low,
        err_s_low,
        err_p_low,
        err_w_low,
        err_cbg_low,
        err_sbg_low,
        err_pbg_low,
        err_wbg_low,
        err_c_high,
        err_s_high,
        err_p_high,
        err_w_high,
        err_cbg_high,
        err_sbg_high,
        err_pbg_high,
        err_wbg_high,
    ) = errors

    return (
        (o_m, err_o_m),
        (o_c, err_o_c),
        (
            fano.Fano(c_low, s_low, p_low, w_low),
            fano.FanoParameters(err_c_low, err_s_low, err_p_low, err_w_low),
        ),
        (
            fano.Fano(cbg_low, sbg_low, pbg_low, wbg_low),
            fano.FanoParameters(err_cbg_low, err_sbg_low, err_pbg_low, err_wbg_low),
        ),
        (
            fano.Fano(c_high, s_high, p_high, w_high),
            fano.FanoParameters(err_c_high, err_s_high, err_p_high, err_w_high),
        ),
        (
            fano.Fano(cbg_high, sbg_high, pbg_high, wbg_high),
            fano.FanoParameters(err_cbg_high, err_sbg_high, err_pbg_high, err_wbg_high),
        ),
    )


def fit_background_gaussian(energy, spectrum):
    def f(
        E,
        o_m,
        o_c,
        c_low,
        s_low,
        p_low,
        w_low,
        cbg_low,
        sbg_low,
        wbg_low,
        c_high,
        s_high,
        p_high,
        w_high,
        cbg_high,
        sbg_high,
        wbg_high,
    ):
        f_low = fano.Fano(c_low, s_low, p_low, w_low)
        f_high = fano.Fano(c_high, s_high, p_high, w_high)

        g_low = sbg_low * np.exp(-4 * np.log(2) * ((E - cbg_low) / wbg_low) ** 2)
        g_high = sbg_high * np.exp(-4 * np.log(2) * ((E - cbg_high) / wbg_high) ** 2)

        return g_low + g_high + fano.lineshape(E, o_m, o_c, f_low, f_high)

    p0 = [
        # Offset
        0.003,  # Gradient
        -0.41,  # Intercept
        # Lower Energy Resonance
        44.8,  # Centre
        0.001,  # Strength
        0.23 * np.pi,  # Phase
        0.03,  # Width
        # Lower Background Gaussian
        44.8,  # Centre
        0.0001,  # Strength
        0.3,  # Width
        # Higher Energy Resonance
        46.55,  # Centre
        0.008,  # Strength
        0.37 * np.pi,  # Phase
        0.08,  # Width
        # Higher Background Gaussian
        46.55,  # Centre
        0.008,  # Strength
        0.3,  # Width
    ]
    lbounds = [
        # Offset
        -1,  # Gradient
        -1,  # Intercept
        # Lower Energy Resonance
        44,  # Centre
        0,  # Strength
        -np.pi,  # Phase
        0,  # Width
        # Lower Background Gaussian
        44,  # Centre
        0,  # Strength
        0,  # Width
        # Higher Energy Resonance
        45,  # Centre
        0,  # Strength
        -np.pi,  # Phase
        0,  # Width
        # Higher Background Gaussian
        46.2,  # Centre
        0,  # Strength
        0,  # Width
    ]
    ubounds = [
        # Offset
        1,  # Gradient
        1,  # Intercept
        # Lower Energy Resonance
        45,  # Centre
        np.inf,  # Strength
        np.pi,  # Phase
        np.inf,  # Width
        # Lower Background Gaussian
        45,  # Centre
        np.inf,  # Strength
        np.inf,  # Width
        # Higher Energy Resonance
        47,  # Centre
        np.inf,  # Strength
        np.pi,  # Phase
        0.1,  # Width
        # Higher Background Gaussian
        47,  # Centre
        0.5,  # Strength
        np.inf,  # Width
    ]

    parameters, covariance = scipy.optimize.curve_fit(
        f, energy, spectrum, p0, bounds=(lbounds, ubounds)
    )
    errors = np.sqrt(np.diag(covariance))

    (
        o_m,
        o_c,
        c_low,
        s_low,
        p_low,
        w_low,
        cbg_low,
        sbg_low,
        wbg_low,
        c_high,
        s_high,
        p_high,
        w_high,
        cbg_high,
        sbg_high,
        wbg_high,
    ) = parameters

    (
        err_o_m,
        err_o_c,
        err_c_low,
        err_s_low,
        err_p_low,
        err_w_low,
        err_cbg_low,
        err_sbg_low,
        err_wbg_low,
        err_c_high,
        err_s_high,
        err_p_high,
        err_w_high,
        err_cbg_high,
        err_sbg_high,
        err_wbg_high,
    ) = errors

    return (
        (o_m, err_o_m),
        (o_c, err_o_c),
        (
            fano.Fano(c_low, s_low, p_low, w_low),
            fano.FanoParameters(err_c_low, err_s_low, err_p_low, err_w_low),
        ),
        (
            (cbg_low, sbg_low, wbg_low),
            (err_cbg_low, err_sbg_low, err_wbg_low),
        ),
        (
            fano.Fano(c_high, s_high, p_high, w_high),
            fano.FanoParameters(err_c_high, err_s_high, err_p_high, err_w_high),
        ),
        (
            (cbg_high, sbg_high, wbg_high),
            (err_cbg_high, err_sbg_high, err_wbg_high),
        ),
    )


def main():
    results_dir = os.path.join(STAGE_DIR, "RMT")

    E_xuv_full, s_xuv_full = packaging.absorption(
        results_dir, 45.547, None, None, None, E_high_eV=55
    )
    fig, ax = plt.subplots(1, 1)
    ax.plot(E_xuv_full, s_xuv_full)
    ax.set_title("Full XUV-Only Absortption Spectrum")
    ax.set_xlabel("Energy (eV)")

    # NOTE: I suspect the lower energy resonance is the 2s3p one, as its phase matches the other Rydberg resonances.
    E_xuv, s_xuv = packaging.absorption(
        results_dir, 45.547, None, None, None, E_high_eV=48.5
    )
    (
        (muv, _),
        (cuv, _),
        (fuv_low, _),
        (fuv_high, _),
    ) = fit_uv(E_xuv, s_xuv)

    fig, ax = plt.subplots(1, 1)
    ax.plot(E_xuv, s_xuv, label="Data")
    ax.plot(
        E_xuv,
        fano.lineshape(E_xuv, muv, cuv, fuv_low, fuv_high),
        label="Fit",
        linestyle="dashed",
    )
    ax.set_title("Fitted XUV-Only Absortption Spectrum")
    ax.set_xlabel("Energy (eV)")
    ax.legend()

    e_field = laser.EFieldDataNIRAndXUV.gaussian(
        dim.PhotonEnergy.from_eV(1.423),
        dim.PhotonEnergy.from_eV(45.547),
        dim.Time.from_au(0.0),
        nir_intensity=dim.EFieldIntensity.from_100_TW_per_cm2(
            5 * constants.NIR_INTENSITY_100_TW_per_cm2
        ),
    )
    nir_energy_grid, nir_spectrum_grid = e_field.nir.spectrum()
    nir_spectrum = scipy.interpolate.make_interp_spline(
        nir_energy_grid, nir_spectrum_grid
    )

    fig, ax = plt.subplots(1, 1)
    ax.plot(E_xuv, fano.lineshape(E_xuv, 0.0, 0.0, fuv_low, fuv_high), label="XUV-Only")
    ax.plot(
        E_xuv,
        fano.background_spectrum(
            E_xuv,
            nir_spectrum,
            0.03,
            fuv_low,
        ),
        label="Background",
        linestyle="dashed",
    )
    ax.set_title("Background Spectrum")
    ax.set_xlabel("Energy (eV)")
    ax.legend()

    E_nir_p5, s_nir_p5 = packaging.absorption(
        results_dir,
        45.547,
        1.423,
        5,
        5 * constants.NIR_INTENSITY_100_TW_per_cm2,
        E_high_eV=48.5,
    )

    fig, ax = plt.subplots(1, 1)
    ax.plot(E_xuv, s_xuv, label="XUV-Only")
    ax.plot(E_nir_p5, s_nir_p5, label="+5fs Delay")
    ax.set_xlabel("Energy (eV)")
    ax.legend()

    fig, ax = plt.subplots(1, 1)
    ax.plot(E_xuv, s_xuv, label="XUV-Only")

    delays = np.array(
        [
            float(re.match("delay=([+-]\d\d\.\d\d\d)fs", entry).group(1))
            for entry in os.listdir(
                os.path.join(results_dir, "xuv=45.547eV-nir=1.423eV", "int=0.050")
            )
        ]
    )
    delays.sort()
    delays = delays
    n = 3

    for d in delays:
        E_nir_p3, s_nir_p3 = packaging.absorption(
            results_dir,
            45.547,
            1.423,
            d,
            5 * constants.NIR_INTENSITY_100_TW_per_cm2,
            E_high_eV=48.5,
        )
        ax.plot(
            E_nir_p3,
            s_nir_p3,
            label=f"{d:.1f}fs",
            alpha=(d - np.min(delays) + n) / (delays.size + n),
            lw=0.5,
        )

    plt.legend()
    plt.show()

    prev_E_nir = E_xuv
    prev_s_nir = s_xuv

    # for d in delays:
    #     E_nir, s_nir = packaging.absorption(
    #         results_dir,
    #         45.547,
    #         1.423,
    #         d,
    #         5 * constants.NIR_INTENSITY_100_TW_per_cm2,
    #         E_high_eV=48.5,
    #     )

    #     fig, ax = plt.subplots(1, 1)

    #     ax.plot(E_xuv, s_xuv, label="XUV Only", linestyle="dashed")
    #     ax.plot(prev_E_nir, prev_s_nir, label="Previous Delay", linestyle="dotted")
    #     ax.plot(E_nir, s_nir, label="Current Delay")
    #     ax.legend()

    #     plt.show()

    #     prev_E_nir = E_nir
    #     prev_s_nir = s_nir

    fi_low = fuv_low
    fi_high = fuv_high
    initial_cs = 0.0

    for d in delays:
        E_nir, s_nir = packaging.absorption(
            results_dir,
            45.547,
            1.423,
            d,
            5 * constants.NIR_INTENSITY_100_TW_per_cm2,
            E_high_eV=45.5,
        )
        (
            (cross_section, _),
            (m, _),
            (c, _),
            (f_low, _),
        ) = fit_cross_section(E_nir, s_nir, fi_low, nir_spectrum, fuv_low, initial_cs)

        print(
            m,
            c,
            cross_section,
            f_low,
        )

        fig, ax = plt.subplots(1, 1)

        ax.plot(E_nir, s_nir, label="Data")
        ax.plot(
            E_nir,
            fano.background_spectrum(E_nir, nir_spectrum, cross_section, fuv_low)
            + fano.lineshape(E_nir, m, c, f_low),
            label="Fit",
        )
        ax.plot(
            E_nir,
            fano.background_spectrum(E_nir, nir_spectrum, cross_section, fuv_low)
            + fano.lineshape(E_nir, m, c),
            label="Background",
            linestyle="dotted",
        )
        ax.legend()

        plt.show()

        fi_low = f_low
        initial_cs = cross_section


if __name__ == "__main__":
    main()
