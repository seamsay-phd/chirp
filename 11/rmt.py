import argparse
import os

import numpy as np

from utilities import constants
from utilities import dimensions as dim
from utilities import laser, logging
from utilities.rmt import runner

STAGE_DIR = os.path.dirname(__file__)
PROJ_DIR = os.path.dirname(STAGE_DIR)

REFERENCE_NIR_eV = 1.423


def run(submit: bool, overwrite: bool, e_field_data: laser.EFieldData):
    outdir = os.path.join(STAGE_DIR, "RMT", *e_field_data.dirname())
    datadir = os.path.join(STAGE_DIR, "RMatrixII")

    if hasattr(e_field_data, "nir"):
        ringing_factor = 3.0
        logging.info(
            "Running job with: NIR = %.3feV (Intensity = %.3f) | XUV = %.3feV | Delay = %.3ffs",
            e_field_data.nir.energy.to_eV(),
            e_field_data.nir.intensity.to_100_TW_per_cm2(),
            e_field_data.xuv.energy.to_eV(),
            e_field_data.delay().to_fs(),
        )
    else:
        ringing_factor = 40.0
        logging.info(
            "Running XUV-only job: XUV = %.3feV", e_field_data.xuv.energy.to_eV()
        )

    runner.run(
        submit,
        overwrite,
        outdir,
        datadir,
        e_field_data,
        ringing_factor,
        time_limit=runner.TimeLimit(hours=8),
    )


def e_field(nir_eV, time_fs, intensity_factor):
    return laser.EFieldDataNIRAndXUV.gaussian(
        dim.PhotonEnergy.from_eV(nir_eV),
        dim.PhotonEnergy.from_eV(45.547),
        dim.Time.from_fs(time_fs),
        nir_intensity=dim.EFieldIntensity.from_100_TW_per_cm2(
            np.round(constants.NIR_INTENSITY_100_TW_per_cm2 * intensity_factor, 3)
        ),
    )


def main(submit=False, overwrite=False):
    xuv_energy = dim.PhotonEnergy.from_eV(45.547)
    run(submit, overwrite, laser.EFieldDataXUVOnly.gaussian(xuv_energy))

    run(submit, overwrite, e_field(REFERENCE_NIR_eV, -5, 0.5))
    run(submit, overwrite, e_field(REFERENCE_NIR_eV, 0, 0.5))
    run(submit, overwrite, e_field(REFERENCE_NIR_eV, 5, 0.5))

    run(submit, overwrite, e_field(REFERENCE_NIR_eV, -5, 1))
    run(submit, overwrite, e_field(REFERENCE_NIR_eV, 0, 1))
    run(submit, overwrite, e_field(REFERENCE_NIR_eV, 5, 1))

    for delay_fs in np.linspace(-10, 10, 21):
        run(submit, overwrite, e_field(REFERENCE_NIR_eV, delay_fs, 5))

    run(submit, overwrite, e_field(REFERENCE_NIR_eV, -99, 5))
    run(submit, overwrite, e_field(REFERENCE_NIR_eV, -40, 5))
    run(submit, overwrite, e_field(REFERENCE_NIR_eV, -20, 5))
    run(submit, overwrite, e_field(REFERENCE_NIR_eV, 20, 5))
    run(submit, overwrite, e_field(REFERENCE_NIR_eV, 40, 5))
    run(submit, overwrite, e_field(REFERENCE_NIR_eV, 99, 5))

    run(submit, overwrite, e_field(1, -5, 5))
    run(submit, overwrite, e_field(1, 0, 5))
    run(submit, overwrite, e_field(1, 5, 5))

    run(submit, overwrite, e_field(2, -5, 5))
    run(submit, overwrite, e_field(2, 0, 5))
    run(submit, overwrite, e_field(2, 5, 5))


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("-s", "--submit", action="store_true", help="submit the job")
    parser.add_argument(
        "-o",
        "--overwrite",
        action="store_true",
        help="remove and regenerate old data",
    )

    args = parser.parse_args()
    main(args.submit, args.overwrite)
