import os
import subprocess

from utilities.rmatrixii import input

STAGE_DIR = os.path.abspath(os.path.dirname(__file__))
PROJ_DIR = os.path.dirname(STAGE_DIR)

INPUT_FILE_NAME = "neon.inp"


def run():
    locked_orbitals = input.LOCKED_ORBITALS.copy()

    configurations = input.generate_configurations(
        input.ORBITALS,
        input.GROUND_STATE_CONFIGURATION,
        locked_orbitals,
        input.FORBIDDEN_CONFIGURATIONS,
        3,
    )

    exp_thresholds = input.EXPERIMENTAL_THRESHOLDS.copy()[:6]
    assert exp_thresholds[1][0] == input.Term(2, 0, 0)
    del exp_thresholds[1]

    thresholds = {term: 1 for term, _ in exp_thresholds}

    directory = os.path.relpath(os.path.join(STAGE_DIR, "RMatrixII"))
    if os.path.exists(directory):
        return

    os.makedirs(directory)

    input_file = os.path.join(directory, INPUT_FILE_NAME)
    with open(input_file, "w") as io:
        input.generate_input(
            io,
            thresholds,
            input.ORBITALS,
            configurations,
            input.GROUND_STATE_CONFIGURATION,
            input.SLATER_COEFFICIENTS,
            exp_thresholds,
        )

    subprocess.run(
        [
            os.path.join(PROJ_DIR, "rmatrixii.bash"),
            os.path.join(PROJ_DIR, "RMatrixII", "build", "bin"),
            input_file,
            directory,
            os.path.join(directory, "rad.out"),
            os.path.join(directory, "ang.out"),
            os.path.join(directory, "ham.out"),
        ],
        check=True,
    )


def main():
    run()


if __name__ == "__main__":
    main()
