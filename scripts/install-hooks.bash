#!/usr/bin/env bash

script_dir="$(dirname "$0")"
hook_dir="$script_dir/hooks"
git_hook_dir="$(dirname "$script_dir")/.git/hooks"

for path in "$hook_dir"/*
do
    abs_path="$(realpath "$path")"
    git_path="$git_hook_dir/$(basename "$path")"

    ln -s "$abs_path" "$git_path"
done
