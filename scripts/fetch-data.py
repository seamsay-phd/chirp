#!/usr/bin/env python3

import functools
import hashlib
import json
import logging
import os.path
import sys

import requests

LOG_LEVEL_MAP = {
    "DEBUG": logging.DEBUG,
    "INFO": logging.INFO,
    "WARN": logging.WARN,
    "ERROR": logging.ERROR,
}

logging.basicConfig(
    level=LOG_LEVEL_MAP[os.environ.get("LOG_LEVEL", "INFO").upper()],
    format="{asctime} {levelname:>8s} {message}",
    style="{",
    stream=sys.stderr,
)


@functools.cache
def fetch_file_links(record):
    metadata = requests.get(f"https://zenodo.org/api/records/{record}").json()
    files = {}
    for f in metadata["files"]:
        key = f["key"]
        assert key not in files

        url = f["links"]["self"]
        size = f["size"]

        checksum = f["checksum"]
        assert checksum.startswith("md5:")
        checksum = checksum[4:]

        files[key] = {
            "url": url,
            "size": size,
            "checksum": checksum,
        }

    return files


def md5(data):
    hasher = hashlib.md5()
    hasher.update(data)
    return hasher.hexdigest()


def download(directory, name, record):
    links = fetch_file_links(record)
    metadata = links[name]
    remote_md5 = metadata["checksum"]

    path = os.path.join(directory, name)
    if os.path.isfile(path):
        with open(path, "rb") as f:
            logging.debug("Caclulating local MD5 of '%s'.", path)
            local_md5 = md5(f.read())

        if remote_md5 == local_md5:
            desc = "Identical"
        else:
            desc = "Different"

        while True:
            response = input(
                f"{desc} file '{path}' already exists. Overwrite (Y/N)? "
            ).upper()
            if response == "Y":
                break
            elif response == "N":
                return
            else:
                print(f"Please enter 'Y' or 'N', not {response}.")

    logging.info(
        "Downloading '%s' (%.2f MiB) into '%s'.",
        name,
        metadata["size"] / 1024 / 1024,
        directory,
    )
    data = requests.get(metadata["url"]).content

    logging.debug(
        "Checking data length (%d) matches expected (%d).", len(data), metadata["size"]
    )
    assert len(data) == metadata["size"]

    logging.debug(
        "Calculating MD5 of file '%s' downloaded from Zenodo record '%s'.", name, record
    )
    downloaded_md5 = md5(data)
    logging.debug(
        "Checking downloaded MD5 ('%s') matches expected ('%s').",
        downloaded_md5,
        remote_md5,
    )
    assert downloaded_md5 == remote_md5

    with open(path, "wb") as f:
        f.write(data)


def run(directory, name=None):
    with open(os.path.join(directory, "zenodo.json")) as f:
        records = json.load(f)

    if name is None:
        for name, record in records.items():
            download(directory, name, record)
    else:
        download(directory, name, records[name])


if __name__ == "__main__":
    import os

    if len(sys.argv) < 2:
        for directory in filter(
            lambda d: os.path.isfile(os.path.join(d, "zenodo.json")), os.listdir()
        ):
            run(directory)
    else:
        for arg in sys.argv[1:]:
            if os.path.isdir(arg) and os.path.isfile(os.path.join(arg, "zenodo.json")):
                run(arg)
            else:
                directory = os.path.dirname(arg)
                name = os.path.basename(arg)

                if os.path.isfile(os.path.join(directory, "zenodo.json")):
                    run(directory, name)
                else:
                    print(
                        f"Argument '{arg}' is not a directory, or a path in a directory, with a 'zenodo.json' file.",
                        file=sys.stderr,
                    )
