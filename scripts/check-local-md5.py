#!/usr/bin/env python3

import functools
import hashlib
import json
import os.path
import sys

import requests


@functools.cache
def fetch_file_links(record):
    metadata = requests.get(f"https://zenodo.org/api/records/{record}").json()
    files = {}
    for f in metadata["files"]:
        key = f["key"]
        assert key not in files

        url = f["links"]["self"]
        size = f["size"]

        checksum = f["checksum"]
        assert checksum.startswith("md5:")
        checksum = checksum[4:]

        files[key] = {
            "url": url,
            "size": size,
            "checksum": checksum,
        }

    return files


def md5(data):
    hasher = hashlib.md5()
    hasher.update(data)
    return hasher.hexdigest()


def run(directories):
    mismatches = []

    for directory in directories:
        with open(os.path.join(directory, "zenodo.json")) as f:
            zenodo = json.load(f)

        for name, record in zenodo.items():
            path = os.path.join(directory, name)
            if os.path.isfile(path):
                expected = fetch_file_links(record)[name]["checksum"]
                with open(path, "rb") as f:
                    actual = md5(f.read())

                if actual != expected:
                    mismatches.append((path, expected, actual))

    if len(mismatches) > 0:
        for path, expected, actual in mismatches:
            print(f"Mismatch! {actual} != {expected}: {path}", file=sys.stderr)

        sys.exit(3)


if __name__ == "__main__":
    import os

    if len(sys.argv) < 2:
        run(
            filter(
                lambda d: os.path.isfile(os.path.join(d, "zenodo.json")), os.listdir()
            )
        )
    else:
        directory = sys.argv[1]
        run([directory])
