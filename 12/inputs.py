import argparse
import os

import numpy as np

from utilities import constants
from utilities import dimensions as dim
from utilities import laser, logging
from utilities.rmt import runner

STAGE_DIR = os.path.dirname(__file__)
PROJ_DIR = os.path.dirname(STAGE_DIR)

REFERENCE_NIR_eV = 1.423


def run(submit: bool, overwrite: bool, e_field_data: laser.EFieldData):
    outdir = os.path.join(STAGE_DIR, *e_field_data.dirname())
    datadir = os.path.join(PROJ_DIR, "01")

    logging.info(
        "Running job with: NIR = %.3feV (Intensity = %.3f) | XUV = %.3feV | Delay = %.3ffs",
        e_field_data.nir.energy.to_eV(),
        e_field_data.nir.intensity.to_100_TW_per_cm2(),
        e_field_data.xuv.energy.to_eV(),
        e_field_data.delay().to_fs(),
    )

    runner.run(submit, overwrite, outdir, datadir, e_field_data)


def run_longer_xuv(submit: bool, overwrite: bool, xuv_cycles: int):
    xuv_energy = dim.PhotonEnergy.from_eV(45.547)

    nir_cycles = 6
    nir_energy = dim.PhotonEnergy.from_eV(REFERENCE_NIR_eV)

    e_field_data = laser.EFieldDataSineSq(
        nir_energy,
        xuv_energy,
        dim.Time.from_fs(5),
        xuv_duration=dim.Time.from_cycles(xuv_cycles, xuv_energy.period()),
    )

    logging.info(
        "Running job with: NIR = %.3feV (Intensity = %.3f) | XUV = %.3feV (Cycles = %02d) | Delay = %.3ffs",
        e_field_data.nir.energy.to_eV(),
        e_field_data.nir.intensity.to_100_TW_per_cm2(),
        e_field_data.xuv.energy.to_eV(),
        xuv_cycles,
        e_field_data.delay().to_fs(),
    )

    runner.run(
        submit,
        overwrite,
        os.path.join(
            STAGE_DIR,
            f"xuv_cycles={xuv_cycles:02d}-nir_cycles={nir_cycles:02d}",
            *e_field_data.dirname(),
        ),
        os.path.join(PROJ_DIR, "01"),
        e_field_data,
    )


def e_field(nir_eV, time_fs, intensity_factor):
    return laser.EFieldDataExactNIR.sine_squ_exact_nir(
        dim.PhotonEnergy.from_eV(nir_eV),
        dim.PhotonEnergy.from_eV(45.547),
        dim.Time.from_fs(time_fs),
        nir_intensity=dim.EFieldIntensity.from_100_TW_per_cm2(
            np.round(constants.NIR_INTENSITY_100_TW_per_cm2 * intensity_factor, 3)
        ),
    )


def main(submit=False, overwrite=False):
    for intensity_factor in np.linspace(0.1, 3, 10):
        for nir_factor in (0.5, 1, 2):
            nir_eV = REFERENCE_NIR_eV * nir_factor

            run(
                submit,
                overwrite,
                e_field(nir_eV, -5, intensity_factor),
            )

            run(
                submit,
                overwrite,
                e_field(nir_eV, 0, intensity_factor),
            )

            run(
                submit,
                overwrite,
                e_field(nir_eV, 5, intensity_factor),
            )

    nir = dim.PhotonEnergy.from_eV(REFERENCE_NIR_eV)
    one_period_delay = nir.period().to_fs()
    for time_fs in np.linspace(0, one_period_delay, 16):
        run(
            submit,
            overwrite,
            e_field(REFERENCE_NIR_eV, time_fs, 1),
        )

    for time_fs in np.linspace(-10, 10, 41):
        run(
            submit,
            overwrite,
            e_field(REFERENCE_NIR_eV, time_fs, 1),
        )

    for time_fs in np.linspace(-3, 3, 61):
        run(
            submit,
            overwrite,
            e_field(REFERENCE_NIR_eV, time_fs, 1),
        )

    for nir_eV in np.linspace(REFERENCE_NIR_eV / 3, REFERENCE_NIR_eV, 30):
        run(
            submit,
            overwrite,
            e_field(nir_eV, 5, 1),
        )

    for nir_eV in np.linspace(REFERENCE_NIR_eV, 3 * REFERENCE_NIR_eV, 30):
        run(submit, overwrite, e_field(nir_eV, 5, 1))

    for nir_eV in np.linspace(1.65, 1.8, 16):
        run(submit, overwrite, e_field(nir_eV, 5, 1))

    for nir_eV in np.linspace(2.5, 2.6, 11):
        run(submit, overwrite, e_field(nir_eV, 5, 1))

    # Lower XUV energy to avoid populating Rydberg series.
    run(
        submit,
        overwrite,
        laser.EFieldDataExactNIR.sine_squ_exact_nir(
            dim.PhotonEnergy.from_eV(REFERENCE_NIR_eV),
            dim.PhotonEnergy.from_eV(45.547 - 13 / 2),
            dim.Time.from_fs(5),
        ),
    )
    run(
        submit,
        overwrite,
        laser.EFieldDataExactNIR.sine_squ_exact_nir(
            dim.PhotonEnergy.from_eV(REFERENCE_NIR_eV),
            dim.PhotonEnergy.from_eV(45.547 - 1 - 13 / 2),
            dim.Time.from_fs(5),
        ),
    )
    run(
        submit,
        overwrite,
        laser.EFieldDataExactNIR.sine_squ_exact_nir(
            dim.PhotonEnergy.from_eV(REFERENCE_NIR_eV),
            dim.PhotonEnergy.from_eV(45.547 - 2 - 13 / 2),
            dim.Time.from_fs(5),
        ),
    )
    run(
        submit,
        overwrite,
        laser.EFieldDataExactNIR.sine_squ_exact_nir(
            dim.PhotonEnergy.from_eV(REFERENCE_NIR_eV),
            dim.PhotonEnergy.from_eV(45.547 - 3 - 13 / 2),
            dim.Time.from_fs(5),
        ),
    )

    # Longer XUV duration to avoid populating Rydberg series.
    run_longer_xuv(submit, overwrite, 28)
    run_longer_xuv(submit, overwrite, 35)
    run_longer_xuv(submit, overwrite, 42)
    run_longer_xuv(submit, overwrite, 49)
    run_longer_xuv(submit, overwrite, 56)
    run_longer_xuv(submit, overwrite, 63)
    run_longer_xuv(submit, overwrite, 70)
    run_longer_xuv(submit, overwrite, 77)
    run_longer_xuv(submit, overwrite, 84)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("-s", "--submit", action="store_true", help="submit the job")
    parser.add_argument(
        "-o",
        "--overwrite",
        action="store_true",
        help="remove and regenerate old data",
    )

    args = parser.parse_args()
    main(args.submit, args.overwrite)
