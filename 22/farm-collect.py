import pathlib
import re

import numpy as np
from farm_utilities.phase import read_phase_data
from matplotlib import pyplot as plt

from utilities import dimensions as dim
from utilities import logging

IONISATION_RGX = re.compile(r"^-(\d\.\d{7}) Ryd$")

stage_dir = pathlib.Path(__file__).absolute().parent
threshold_dirs = list(stage_dir.joinpath("calculations").iterdir())

for threshold_dir in threshold_dirs:
    ionisation = dim.Energy.from_Ry(
        float(
            IONISATION_RGX.match(
                threshold_dir.joinpath("ground.txt").read_text()
            ).group(1)
        )
    )

    farm_dir = threshold_dir / "FARM"

    for state in ("1S", "1Po", "1D"):
        energy_dirs = sorted(farm_dir.glob(f"{state}-*"))

        Es = []
        phis = []

        for d in energy_dirs:
            phase_path = d / "CPCPHASEF"
            if phase_path.stat().st_size <= 0:
                logging.info("Phase file is empty, skipping: %s", d)
                continue

            E, phi = read_phase_data(
                phase_path, ionisation_energy_eV=ionisation.to_eV()
            )
            Es.append(E)
            phis.append(phi / np.pi)

        E_phi = sorted(zip(Es, phis), key=lambda Ep: Ep[0][0])

        E_eV = []
        phi_pi = []

        for E, phi in E_phi:
            E_eV.extend(E)
            phi_pi.extend(phi - phi[0])

        E_eV = np.array(E_eV)
        phi_pi = np.array(phi_pi)

        assert np.unique(E_eV).size == E_eV.size

        np.savetxt(
            threshold_dir / f"{state}.txt",
            np.column_stack([E_eV, phi_pi]),
            header="Energy_eV Phi_pi",
        )
