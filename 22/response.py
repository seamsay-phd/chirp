import argparse
import multiprocessing
import pathlib
import re
import textwrap
import subprocess

from utilities import logging, response

XUV_RE = re.compile(r"^xuv=\d\d\.\d\d\deV$")
NIR_RE = re.compile(r"^xuv=\d\d\.\d\d\deV-nir=\d\.\d\d\deV$")


def run(dirs):
    processes = [
        multiprocessing.Process(target=response.calculate, args=(dir,)) for dir in dirs
    ]

    for process in processes:
        process.start()

    for process in processes:
        process.join()

    for dir, process in zip(dirs, processes):
        if process.exitcode != 0:
            logging.error("Response calculation failed: %s", dir)
        else:
            logging.info("Response calculation succeeded: %s", dir)


def submit(n_dirs):
    script = pathlib.Path(__file__).absolute()
    directory = script.parent
    venv = directory.joinpath(".venv", "bin", "activate")

    n = min(n_dirs + 1, 128)

    submission_script = f"""\
        #!/bin/bash

        #SBATCH --job-name=response-term-coupling
        #SBATCH --account=e813

        #SBATCH --qos=standard
        #SBATCH --partition=standard

        #SBATCH --nodes=1
        #SBATCH --ntasks-per-node=1
        #SBATCH --cpus-per-task={n}
        #SBATCH --time=12:00:00

        source {venv}
        export PYTHONPATH="$PYTHONPATH:{directory.parent}:{directory.parent.joinpath("RMT", "source")}"

        export MPLCONFIGDIR={directory.joinpath(".mpl")}

        export OMP_NUM_THREADS=1
        export SRUN_CPUS_PER_TASK="$SLURM_CPUS_PER_TASK"

        srun --distribution=block:block --hint=nomultithread python3 {script} --run
    """
    submission_script = textwrap.dedent(submission_script)

    submission_path = directory.joinpath("response.slurm")

    with open(submission_path, "w") as io:
        print(submission_script, file=io)

    subprocess.run(["sbatch", submission_path], check=True)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    group = parser.add_mutually_exclusive_group(required=True)

    group.add_argument("-c", "--check", action="store_true")
    group.add_argument("-s", "--submit", action="store_true")
    group.add_argument("-r", "--run", action="store_true")

    args = parser.parse_args()

    calculations_dir = pathlib.Path(__file__).absolute().parent.joinpath("calculations")
    threshold_dirs = list(calculations_dir.iterdir())

    xuv_dirs = [
        dir
        for threshold_dir in threshold_dirs
        for dir in threshold_dir.joinpath("RMT").iterdir()
        if XUV_RE.fullmatch(dir.name) is not None
    ]

    nir_dirs = [
        dir
        for threshold_dir in threshold_dirs
        for dir in threshold_dir.joinpath("RMT").iterdir()
        if NIR_RE.fullmatch(dir.name) is not None
    ]
    delay_dirs = [delay_dir for nir_dir in nir_dirs for delay_dir in nir_dir.iterdir()]

    dirs = [
        dir
        for dir in xuv_dirs + delay_dirs
        if not dir.joinpath("response_0001_z_reduced_real").exists()
        or not dir.joinpath("response_0001_z_reduced_imag").exists()
        or not dir.joinpath("TAS_0001_z").exists()
    ]

    if args.check + args.submit + args.run != 1:
        raise Exception("only one option should be possible")
    elif args.check:
        for dir in dirs:
            print(dir)
    elif args.submit:
        submit(len(dirs))
    elif args.run:
        multiprocessing.set_start_method("spawn")
        run(dirs)
    else:
        raise Exception("boolean logic is broken")
