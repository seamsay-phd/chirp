import argparse
import multiprocessing
import os
import pathlib
import re
import subprocess
import textwrap

import f90nml

from utilities import dimensions as dim
from utilities import farm, logging
from utilities.rmatrixii.input import Term

LOW_eV = 44
HIGH_eV = 55


# This works because we only ever care about the highest two energies, so the fact that one of the calculations has two 2Po thresholds doesn't matter.
def get_energy(energies, term):
    for t, E in reversed(energies):
        if t == term:
            return E


def run_calculation(thresholds, state, low_eV, high_eV, step_eV):
    assert high_eV - low_eV > 10 * step_eV

    thr_directory = (
        pathlib.Path(__file__)
        .absolute()
        .parent.joinpath("calculations", "-".join(str(t) for t in thresholds))
    )
    assert thr_directory.exists()

    rmatrixii_directory = thr_directory.joinpath("RMatrixII")
    assert rmatrixii_directory.exists()

    farm_directory = thr_directory.joinpath(
        "FARM", f"{str(state)}-{low_eV:.3f}-{high_eV:.3f}"
    )

    ground_rgx = re.compile(r"^-(\d\.\d{7}) Ryd$")
    with open(thr_directory.joinpath("ground.txt")) as io:
        ground_str = io.read()
    ionisation_energy = dim.Energy.from_Ry(float(ground_rgx.match(ground_str).group(1)))

    h_file_absolute = rmatrixii_directory.joinpath("H")
    h_file = os.path.relpath(h_file_absolute, farm_directory)

    files = farm.FILES.copy()
    files["H"] = str(h_file)

    logging.info(
        "Running FARM: thresholds=`%s` state=%s low=%.3feV high=%.3feV step=%.5feV",
        " ".join(str(t) for t in thresholds),
        state,
        low_eV,
        high_eV,
        step_eV,
    )

    configuration = farm.configuration(
        low_eV,
        high_eV,
        step_eV,
        state.multiplicity,
        state.L,
        state.parity,
        ionisation_energy_eV=ionisation_energy.to_eV(),
        files=files,
    )

    with open(farm_directory.joinpath("cpc.d"), "w") as io:
        f90nml.write(configuration, io)

    project_directory = pathlib.Path(__file__).absolute().parent.parent

    os.chdir(farm_directory)
    farm.run(
        configuration,
        executable=project_directory.joinpath("FARM", "build", "bin", "farm"),
        files=files,
    )

    logging.info(
        "Finished FARM: thresholds=`%s` state=%s low=%.3feV high=%.3feV step=%.5feV",
        " ".join(str(t) for t in thresholds),
        state,
        low_eV,
        high_eV,
        step_eV,
    )


def run(calculations):
    processes = []

    for calc in calculations:
        energies, state, low, low_off, high, high_off, step_eV = calc

        if low is None:
            low_eV = LOW_eV
        else:
            low_eV = get_energy(energies, low) + low_off * step_eV

        if high is None:
            high_eV = HIGH_eV
        else:
            high_eV = get_energy(energies, high) - high_off * step_eV

        thresholds = tuple(t for t, _ in energies)

        args = (thresholds, state, low_eV, high_eV, step_eV)

        thr_directory = (
            pathlib.Path(__file__)
            .absolute()
            .parent.joinpath("calculations", "-".join(str(t) for t in thresholds))
        )
        assert thr_directory.exists()
        farm_directory = thr_directory.joinpath(
            "FARM", f"{str(state)}-{low_eV:.3f}-{high_eV:.3f}"
        )

        if farm_directory.exists():
            logging.info(
                "Skipping FARM: thresholds=`%s` state=%s low=%.3feV high=%.3feV step=%.5feV",
                " ".join(str(t) for t in thresholds),
                state,
                low_eV,
                high_eV,
                step_eV,
            )
            continue

        farm_directory.mkdir(parents=True)

        process = multiprocessing.Process(target=run_calculation, args=args)
        process.start()
        processes.append((args, process))

    for _, p in processes:
        p.join()

    for (c, t, low_eV, high_eV, step_eV), p in processes:
        if p.exitcode != 0:
            logging.error(
                "FARM failed: thresholds=`%s` state=%s low=%.3feV high=%.3feV step=%.5feV",
                " ".join(str(t) for t in c),
                t,
                low_eV,
                high_eV,
                step_eV,
            )
        else:
            logging.info(
                "FARM successful: thresholds=`%s` state=%s low=%.3feV high=%.3feV step=%.5feV",
                " ".join(str(t) for t in c),
                t,
                low_eV,
                high_eV,
                step_eV,
            )


def submit(n_calcs):
    script = pathlib.Path(__file__).absolute()
    directory = script.parent
    venv = directory.joinpath(".venv", "bin", "activate")

    n = min(n_calcs, 128)

    submission_script = f"""\
        #!/bin/bash

        #SBATCH --job-name=farm-term-coupling
        #SBATCH --account=e813

        #SBATCH --qos=standard
        #SBATCH --partition=standard

        #SBATCH --nodes=1
        #SBATCH --ntasks-per-node=1
        #SBATCH --cpus-per-task={n}
        #SBATCH --time=04:00:00

        source {venv}
        export PYTHONPATH="$PYTHONPATH:{directory.parent}"

        export MPLCONFIGDIR={directory.joinpath(".mpl")}

        export OMP_NUM_THREADS=1
        export SRUN_CPUS_PER_TASK="$SLURM_CPUS_PER_TASK"

        srun --distribution=block:block --hint=nomultithread python3 {script} --run
    """
    submission_script = textwrap.dedent(submission_script)

    submission_path = directory.joinpath("farm.slurm")

    with open(submission_path, "w") as io:
        print(submission_script, file=io)

    subprocess.run(["sbatch", submission_path], check=True)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    group = parser.add_mutually_exclusive_group(required=True)

    group.add_argument("-c", "--check", action="store_true")
    group.add_argument("-s", "--submit", action="store_true")
    group.add_argument("-r", "--run", action="store_true")

    args = parser.parse_args()

    # fmt: off
    thresholds = [
        [(Term(2, 1, 1), 21.27697), (Term(2, 0, 0), 48.15519)],
	[(Term(2, 1, 1), 21.11687), (Term(2, 0, 0), 47.99509), (Term(4, 1, 0), 48.29164)],
	[(Term(2, 1, 1), 21.17506), (Term(2, 0, 0), 48.05328), (Term(2, 1, 0), 48.95136)],
	[(Term(2, 1, 1), 21.14506), (Term(2, 0, 0), 48.02328), (Term(4, 1, 1), 51.65435)],
	[(Term(2, 1, 1), 21.30356), (Term(2, 0, 0), 48.18178), (Term(2, 2, 0), 51.82045)],
	[(Term(2, 1, 1), 21.13696), (Term(2, 0, 0), 48.01518), (Term(4, 2, 1), 52.02640)],
	[(Term(2, 1, 1), 21.17603), (Term(2, 0, 0), 48.05425), (Term(2, 2, 1), 52.29053)],
	[(Term(2, 1, 1), 21.24039), (Term(2, 0, 0), 48.11861), (Term(2, 0, 1), 52.55112)],
	[(Term(2, 1, 1), 21.21588), (Term(2, 0, 0), 48.09410), (Term(4, 0, 1), 52.54586)],
	[(Term(2, 1, 1), 21.22230), (Term(2, 0, 0), 48.10052), (Term(2, 1, 1), 52.70775)],
    ]
    # fmt: on

    def get_Es(i, *ts_given):
        Es = thresholds[i]

        ts_in_Es = tuple(t for t, _ in Es)
        assert ts_given == ts_in_Es, f"{ts_given} != {ts_in_Es}"

        return Es

    # fmt: off
    calculations = [
        # 1S
        [get_Es(0, Term(2, 1, 1), Term(2, 0, 0)),                Term(1, 0, 0), None         , 10   , Term(2, 0, 0), 10   , 1e-4],
        [get_Es(0, Term(2, 1, 1), Term(2, 0, 0)),                Term(1, 0, 0), Term(2, 0, 0), 10   , None         , 10   , 1e-4],
        # 1Po
        [get_Es(0, Term(2, 1, 1), Term(2, 0, 0)),                Term(1, 1, 1), None         , 10   , Term(2, 0, 0), 10   , 1e-4],
        [get_Es(0, Term(2, 1, 1), Term(2, 0, 0)),                Term(1, 1, 1), Term(2, 0, 0), 10   , None         , 10   , 1e-4],
        # 1D
        [get_Es(0, Term(2, 1, 1), Term(2, 0, 0)),                Term(1, 2, 0), None         , 10   , Term(2, 0, 0), 10   , 1e-4],
        [get_Es(0, Term(2, 1, 1), Term(2, 0, 0)),                Term(1, 2, 0), Term(2, 0, 0), 10   , None         , 10   , 1e-4],
        # 4P 1S
        [get_Es(1, Term(2, 1, 1), Term(2, 0, 0), Term(4, 1, 0)), Term(1, 0, 0), None         , 10   , Term(2, 0, 0), 10   , 1e-4],
        [get_Es(1, Term(2, 1, 1), Term(2, 0, 0), Term(4, 1, 0)), Term(1, 0, 0), Term(2, 0, 0), 10   , None         , 10   , 1e-4],
        # 4P 1Po
        [get_Es(1, Term(2, 1, 1), Term(2, 0, 0), Term(4, 1, 0)), Term(1, 1, 1), None         , 10   , Term(2, 0, 0), 10   , 1e-4],
        [get_Es(1, Term(2, 1, 1), Term(2, 0, 0), Term(4, 1, 0)), Term(1, 1, 1), Term(2, 0, 0), 10   , None         , 10   , 1e-4],
        # 4P 1D
        [get_Es(1, Term(2, 1, 1), Term(2, 0, 0), Term(4, 1, 0)), Term(1, 2, 0), None         , 10   , Term(2, 0, 0), 10   , 1e-4],
        [get_Es(1, Term(2, 1, 1), Term(2, 0, 0), Term(4, 1, 0)), Term(1, 2, 0), Term(2, 0, 0), 10   , None         , 10   , 1e-4],
        # 2P 1S
        [get_Es(2, Term(2, 1, 1), Term(2, 0, 0), Term(2, 1, 0)), Term(1, 0, 0), None         , 10   , Term(2, 0, 0), 10   , 1e-4],
        [get_Es(2, Term(2, 1, 1), Term(2, 0, 0), Term(2, 1, 0)), Term(1, 0, 0), Term(2, 0, 0), 10   , None         , 10   , 1e-4],
        # 2P 1Po
        [get_Es(2, Term(2, 1, 1), Term(2, 0, 0), Term(2, 1, 0)), Term(1, 1, 1), None         , 10   , Term(2, 0, 0), 1000 , 1e-4],
        [get_Es(2, Term(2, 1, 1), Term(2, 0, 0), Term(2, 1, 0)), Term(1, 1, 1), Term(2, 0, 0), 10   , Term(2, 1, 0), 10   , 1e-5],
        [get_Es(2, Term(2, 1, 1), Term(2, 0, 0), Term(2, 1, 0)), Term(1, 1, 1), Term(2, 1, 0), 10   , None         , 10   , 1e-4],
        # 2P 1D
        [get_Es(2, Term(2, 1, 1), Term(2, 0, 0), Term(2, 1, 0)), Term(1, 2, 0), None         , 10   , Term(2, 0, 0), 2500, 1e-5],
        [get_Es(2, Term(2, 1, 1), Term(2, 0, 0), Term(2, 1, 0)), Term(1, 2, 0), Term(2, 0, 0), 10   , Term(2, 1, 0), 10   , 1e-5],
        [get_Es(2, Term(2, 1, 1), Term(2, 0, 0), Term(2, 1, 0)), Term(1, 2, 0), Term(2, 1, 0), 10   , None         , 10   , 1e-4],
        # 4Po 1S
        [get_Es(3, Term(2, 1, 1), Term(2, 0, 0), Term(4, 1, 1)), Term(1, 0, 0), None         , 10   , Term(2, 0, 0), 10   , 1e-4],
        [get_Es(3, Term(2, 1, 1), Term(2, 0, 0), Term(4, 1, 1)), Term(1, 0, 0), Term(2, 0, 0), 10   , None         , 10   , 1e-4],
        # 4Po 1Po
        [get_Es(3, Term(2, 1, 1), Term(2, 0, 0), Term(4, 1, 1)), Term(1, 1, 1), None         , 10   , Term(2, 0, 0), 10   , 1e-4],
        [get_Es(3, Term(2, 1, 1), Term(2, 0, 0), Term(4, 1, 1)), Term(1, 1, 1), Term(2, 0, 0), 10   , None         , 10   , 1e-4],
        # 4Po 1D
        [get_Es(3, Term(2, 1, 1), Term(2, 0, 0), Term(4, 1, 1)), Term(1, 2, 0), None         , 10   , Term(2, 0, 0), 10   , 1e-5],
        [get_Es(3, Term(2, 1, 1), Term(2, 0, 0), Term(4, 1, 1)), Term(1, 2, 0), Term(2, 0, 0), 10   , None         , 10   , 1e-4],
        # 2D 1S
        [get_Es(4, Term(2, 1, 1), Term(2, 0, 0), Term(2, 2, 0)), Term(1, 0, 0), None         , 10   , Term(2, 0, 0), 1000 , 1e-4],
        [get_Es(4, Term(2, 1, 1), Term(2, 0, 0), Term(2, 2, 0)), Term(1, 0, 0), Term(2, 0, 0), 10   , Term(2, 2, 0), 10   , 1e-4],
        [get_Es(4, Term(2, 1, 1), Term(2, 0, 0), Term(2, 2, 0)), Term(1, 0, 0), Term(2, 2, 0), 10   , None         , 10   , 1e-4],
        # 2D 1Po
        [get_Es(4, Term(2, 1, 1), Term(2, 0, 0), Term(2, 2, 0)), Term(1, 1, 1), None         , 10   , Term(2, 0, 0), 1000 , 1e-4],
        [get_Es(4, Term(2, 1, 1), Term(2, 0, 0), Term(2, 2, 0)), Term(1, 1, 1), Term(2, 0, 0), 10   , Term(2, 2, 0), 10   , 1e-4],
        [get_Es(4, Term(2, 1, 1), Term(2, 0, 0), Term(2, 2, 0)), Term(1, 1, 1), Term(2, 2, 0), 10   , None         , 10   , 1e-4],
        # 2D 1D
        [get_Es(4, Term(2, 1, 1), Term(2, 0, 0), Term(2, 2, 0)), Term(1, 2, 0), None         , 10   , Term(2, 0, 0), 5000 , 1e-5],
        [get_Es(4, Term(2, 1, 1), Term(2, 0, 0), Term(2, 2, 0)), Term(1, 2, 0), Term(2, 0, 0), 10   , Term(2, 2, 0), 10   , 1e-5],
        [get_Es(4, Term(2, 1, 1), Term(2, 0, 0), Term(2, 2, 0)), Term(1, 2, 0), Term(2, 2, 0), 10   , None         , 10   , 1e-4],
        # 4Do 1S
        [get_Es(5, Term(2, 1, 1), Term(2, 0, 0), Term(4, 2, 1)), Term(1, 0, 0), None         , 10   , Term(2, 0, 0), 10   , 1e-4],
        [get_Es(5, Term(2, 1, 1), Term(2, 0, 0), Term(4, 2, 1)), Term(1, 0, 0), Term(2, 0, 0), 10   , None         , 10   , 1e-4],
        # 4Do 1Po
        [get_Es(5, Term(2, 1, 1), Term(2, 0, 0), Term(4, 2, 1)), Term(1, 1, 1), None         , 10   , Term(2, 0, 0), 10   , 1e-4],
        [get_Es(5, Term(2, 1, 1), Term(2, 0, 0), Term(4, 2, 1)), Term(1, 1, 1), Term(2, 0, 0), 10   , None         , 10   , 1e-4],
        # 4Do 1D
        [get_Es(5, Term(2, 1, 1), Term(2, 0, 0), Term(4, 2, 1)), Term(1, 2, 0), None         , 10   , Term(2, 0, 0), 10   , 1e-5],
        [get_Es(5, Term(2, 1, 1), Term(2, 0, 0), Term(4, 2, 1)), Term(1, 2, 0), Term(2, 0, 0), 10   , None         , 10   , 1e-4],
        # 2Do 1S
        [get_Es(6, Term(2, 1, 1), Term(2, 0, 0), Term(2, 2, 1)), Term(1, 0, 0), None         , 10   , Term(2, 0, 0), 10   , 1e-4],
        [get_Es(6, Term(2, 1, 1), Term(2, 0, 0), Term(2, 2, 1)), Term(1, 0, 0), Term(2, 0, 0), 10   , None         , 10   , 1e-4],
        # 2Do 1Po
        [get_Es(6, Term(2, 1, 1), Term(2, 0, 0), Term(2, 2, 1)), Term(1, 1, 1), None         , 10   , Term(2, 0, 0), 1000 , 1e-4],
        [get_Es(6, Term(2, 1, 1), Term(2, 0, 0), Term(2, 2, 1)), Term(1, 1, 1), Term(2, 0, 0), 10   , Term(2, 2, 1), 10   , 1e-5],
        [get_Es(6, Term(2, 1, 1), Term(2, 0, 0), Term(2, 2, 1)), Term(1, 1, 1), Term(2, 2, 1), 10   , None         , 10   , 1e-4],
        # 2Do 1D
        [get_Es(6, Term(2, 1, 1), Term(2, 0, 0), Term(2, 2, 1)), Term(1, 2, 0), None         , 10   , Term(2, 0, 0), 5000 , 1e-5],
        [get_Es(6, Term(2, 1, 1), Term(2, 0, 0), Term(2, 2, 1)), Term(1, 2, 0), Term(2, 0, 0), 10   , Term(2, 2, 1), 10   , 1e-4],
        [get_Es(6, Term(2, 1, 1), Term(2, 0, 0), Term(2, 2, 1)), Term(1, 2, 0), Term(2, 2, 1), 10   , None         , 10   , 1e-4],
        # 2So 1S
        [get_Es(7, Term(2, 1, 1), Term(2, 0, 0), Term(2, 0, 1)), Term(1, 0, 0), None         , 10   , Term(2, 0, 0), 10   , 1e-4],
        [get_Es(7, Term(2, 1, 1), Term(2, 0, 0), Term(2, 0, 1)), Term(1, 0, 0), Term(2, 0, 0), 10   , None         , 10   , 1e-4],
        # 2So 1Po
        [get_Es(7, Term(2, 1, 1), Term(2, 0, 0), Term(2, 0, 1)), Term(1, 1, 1), None         , 10   , Term(2, 0, 0), 10   , 1e-4],
        [get_Es(7, Term(2, 1, 1), Term(2, 0, 0), Term(2, 0, 1)), Term(1, 1, 1), Term(2, 0, 0), 10   , None         , 10   , 1e-4],
        # 2So 1D
        [get_Es(7, Term(2, 1, 1), Term(2, 0, 0), Term(2, 0, 1)), Term(1, 2, 0), None         , 10   , Term(2, 0, 0), 10   , 1e-5],
        [get_Es(7, Term(2, 1, 1), Term(2, 0, 0), Term(2, 0, 1)), Term(1, 2, 0), Term(2, 0, 0), 10   , None         , 10   , 1e-4],
        # 4So 1S
        [get_Es(8, Term(2, 1, 1), Term(2, 0, 0), Term(4, 0, 1)), Term(1, 0, 0), None         , 10   , Term(2, 0, 0), 10   , 1e-4],
        [get_Es(8, Term(2, 1, 1), Term(2, 0, 0), Term(4, 0, 1)), Term(1, 0, 0), Term(2, 0, 0), 10   , None         , 10   , 1e-4],
        # 4So 1Po
        [get_Es(8, Term(2, 1, 1), Term(2, 0, 0), Term(4, 0, 1)), Term(1, 1, 1), None         , 10   , Term(2, 0, 0), 10   , 1e-4],
        [get_Es(8, Term(2, 1, 1), Term(2, 0, 0), Term(4, 0, 1)), Term(1, 1, 1), Term(2, 0, 0), 10   , None         , 10   , 1e-4],
        # 4So 1D
        [get_Es(8, Term(2, 1, 1), Term(2, 0, 0), Term(4, 0, 1)), Term(1, 2, 0), None         , 10   , Term(2, 0, 0), 10   , 1e-5],
        [get_Es(8, Term(2, 1, 1), Term(2, 0, 0), Term(4, 0, 1)), Term(1, 2, 0), Term(2, 0, 0), 10   , None         , 10   , 1e-4],
        # 2Po 1S
        [get_Es(9, Term(2, 1, 1), Term(2, 0, 0), Term(2, 1, 1)), Term(1, 0, 0), None         , 10   , Term(2, 0, 0), 1000 , 1e-4],
        [get_Es(9, Term(2, 1, 1), Term(2, 0, 0), Term(2, 1, 1)), Term(1, 0, 0), Term(2, 0, 0), 10   , Term(2, 1, 1), 10   , 1e-4],
        [get_Es(9, Term(2, 1, 1), Term(2, 0, 0), Term(2, 1, 1)), Term(1, 0, 0), Term(2, 1, 1), 10   , None         , 10   , 1e-4],
        # 2Po 1Po
        [get_Es(9, Term(2, 1, 1), Term(2, 0, 0), Term(2, 1, 1)), Term(1, 1, 1), None         , 10   , Term(2, 0, 0), 1000 , 1e-4],
        [get_Es(9, Term(2, 1, 1), Term(2, 0, 0), Term(2, 1, 1)), Term(1, 1, 1), Term(2, 0, 0), 10   , Term(2, 1, 1), 10   , 1e-5],
        [get_Es(9, Term(2, 1, 1), Term(2, 0, 0), Term(2, 1, 1)), Term(1, 1, 1), Term(2, 1, 1), 10   , None         , 10   , 1e-4],
        # 2Po 1D
        [get_Es(9, Term(2, 1, 1), Term(2, 0, 0), Term(2, 1, 1)), Term(1, 2, 0), None         , 10   , Term(2, 0, 0), 5000, 1e-5],
        [get_Es(9, Term(2, 1, 1), Term(2, 0, 0), Term(2, 1, 1)), Term(1, 2, 0), Term(2, 0, 0), 10   , Term(2, 1, 1), 10   , 1e-5],
        [get_Es(9, Term(2, 1, 1), Term(2, 0, 0), Term(2, 1, 1)), Term(1, 2, 0), Term(2, 1, 1), 10   , None         , 10   , 1e-4],
    ]
    # fmt: on

    if args.check + args.submit + args.run != 1:
        raise Exception("only one option should be possible")
    elif args.check:
        for Es, s, l, lo, h, ho, p in calculations:
            ts = tuple(t for t, _ in Es)

            if l is None:
                l = ""
                lE = LOW_eV
            else:
                lE = get_energy(Es, l) + lo * p

            if h is None:
                h = ""
                hE = HIGH_eV
            else:
                hE = get_energy(Es, h) - ho * p

            print(
                f"{str(ts[0]):3s} {str(ts[1]):3s} {str(ts[2]):3s} | {str(s):3s} | {str(l):3s} ({lE:.5f}) - {str(h):3s} ({hE:.5f})"
            )

    elif args.submit:
        submit(len(calculations))
    elif args.run:
        multiprocessing.set_start_method("spawn")
        run(calculations)
    else:
        raise Exception("boolean logic is broken")
