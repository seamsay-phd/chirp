import argparse
import pathlib
import sys

import numpy as np

from utilities import dimensions as dim
from utilities import laser, logging
from utilities.rmt import runner

STAGE_DIR = pathlib.Path(__file__).absolute().parent


def run(threshold_dir, submit, overwrite):
    logging.info("Running RMT calculations for thresholds: %s", threshold_dir.name)

    data_dir = threshold_dir / "RMatrixII"

    nir = dim.PhotonEnergy.from_eV(0.9)
    xuv = dim.PhotonEnergy.from_eV(45.547)

    logging.info("Running XUV-only calculation: xuv=%.3feV", xuv.to_eV())
    runner.run(
        submit,
        overwrite,
        threshold_dir / "RMT" / f"xuv={xuv.to_eV():.3f}eV",
        data_dir,
        laser.EFieldDataXUVOnly.gaussian(xuv),
        ringing_factor=160,
        time_limit=runner.TimeLimit(hours=6),
    )

    delays = [-5, -1, 0, 1, 5]
    if threshold_dir.name == "2Po-2S":
        delays = [
            -99,
            -40,
            *np.linspace(-20, -4, 17),
            *np.linspace(-3, -1.5, 4),
            *np.linspace(-1, 1, 21),
            *np.linspace(1.5, 3, 4),
            *np.linspace(4, 20, 17),
            40,
            99,
        ]

    for delay_fs in delays:
        delay = dim.Time.from_fs(delay_fs)
        gaussian = laser.EFieldDataNIRAndXUV.gaussian(nir, xuv, delay)

        if sys.version_info.major > 3 or (
            sys.version_info.major == 3 and sys.version_info.minor >= 11
        ):
            delay_str = f"delay={delay_fs:+z07.3f}fs"
        else:
            if delay_fs == -0.0:
                delay_fs = 0.0

            delay_str = f"delay={delay_fs:+07.3f}fs"

        if abs(delay_fs) > 20:
            time_limit = runner.TimeLimit(hours=20)
        else:
            time_limit = runner.TimeLimit(hours=8)

        logging.info(
            "Running NIR calculation: xuv=%.3feV nir=%.3feV delay=%.3ffs",
            xuv.to_eV(),
            nir.to_eV(),
            delay.to_fs(),
        )
        runner.run(
            submit,
            overwrite,
            threshold_dir
            / "RMT"
            / f"xuv={xuv.to_eV():.3f}eV-nir={nir.to_eV():.3f}eV"
            / delay_str,
            data_dir,
            gaussian,
            time_limit=time_limit,
        )


def main(submit, overwrite):
    threshold_dirs = STAGE_DIR / "calculations"
    for threshold_dir in threshold_dirs.iterdir():
        run(threshold_dir, submit, overwrite)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("-s", "--submit", action="store_true", help="submit the job")
    parser.add_argument(
        "-o",
        "--overwrite",
        action="store_true",
        help="remove and regenerate old data",
    )

    args = parser.parse_args()
    main(args.submit, args.overwrite)
