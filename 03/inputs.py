import argparse
import os
import subprocess
import sys
import textwrap
import time
import traceback

import f90nml
import numpy as np

time_fs_per_au = 2.4188843265857e-2
energy_eV_per_au = 27.211386245988
h_eV_fs = 4.135667696
amplitude_au_per_root_100_TW_per_cm2 = 0.05336

cpus_per_node = 128
max_jobs_in_queue = 8


def dirname():
    return "xuv-only"


def jobname():
    return dirname()


def outer(inner, nodes):
    cpus = cpus_per_node * nodes
    return cpus - inner


def pulse_parameters(delta_t_au):
    ir_intensity_100_TW_per_cm2 = 1 / 100.0
    ir_amplitude_au = (
        np.sqrt(ir_intensity_100_TW_per_cm2) * amplitude_au_per_root_100_TW_per_cm2
    )
    xuv_amplitude_au = amplitude_au = ir_amplitude_au / 1000

    xuv_energy_eV = 45.547

    ir_energy_eV = xuv_energy_eV / 29
    ir_period_fs = h_eV_fs / ir_energy_eV
    fwhm_fs = ir_period_fs / 4

    fwhm_au = fwhm_fs / time_fs_per_au
    pulse_period_au = 4 * fwhm_au / 3

    xuv_period_au = h_eV_fs / xuv_energy_eV / time_fs_per_au
    cycles = int(np.ceil(pulse_period_au / xuv_period_au))
    pulse_period_au = cycles * xuv_period_au

    pulse_steps_f = pulse_period_au / delta_t_au
    pulse_steps = int(np.ceil(pulse_steps_f))

    return pulse_steps, xuv_energy_eV, amplitude_au


def pulse(pulse_ts, energy_eV, amplitude_au):
    frequency_per_fs = energy_eV / h_eV_fs
    frequency_per_au = frequency_per_fs * time_fs_per_au

    t0 = pulse_ts[len(pulse_ts) // 2]
    t = pulse_ts - t0
    f = frequency_per_au
    a = 1 / (t[-1] - t[0])
    fe = a / 2
    E0 = amplitude_au

    return E0 * np.cos(2 * np.pi * fe * t) ** 2 * np.cos(2 * np.pi * f * t)


def efield(final_t_au, delta_t_au):
    steps_per_run_approx = final_t_au / delta_t_au

    ts = np.arange(steps_per_run_approx) * delta_t_au

    xs = np.zeros_like(ts)
    ys = np.zeros_like(ts)

    zs = np.zeros_like(ts)

    pulse_start = 10
    pulse_steps, energy_eV, amplitude_au = pulse_parameters(delta_t_au)
    pulse_end = pulse_start + pulse_steps

    zs[:pulse_end] += 1e-90  # E Field must be non-zero at first time-step.
    zs[pulse_start:pulse_end] += pulse(
        ts[pulse_start:pulse_end], energy_eV, amplitude_au
    )

    return np.vstack([ts, xs, ys, zs]).T


def conf(inner, nodes, final_t_au, steps_per_run_approx, delta_r=0.08):
    x_last_others = 100
    master_prop = 0.8
    x_last_master = 4 * int(x_last_others * master_prop / 4)

    return {
        "InputData": {
            "version_root": jobname(),
            "no_of_pes_to_use_inner": inner,
            "no_of_pes_to_use_outer": outer(inner, nodes),
            "final_t": final_t_au,
            "steps_per_run_approx": steps_per_run_approx,
            "x_last_master": x_last_master,
            "x_last_others": x_last_others,
            "adjust_gs_energy": False,
            "ml_max": 0,
            "use_field_from_file": True,
            "use_2colour_field": False,
            "frequency": 0.0,
            "intensity": 0.0,
            "frequency_xuv": 0.0,
            "intensity_xuv": 0.0,
            "keep_checkpoints": False,
            "checkpts_per_run": 10,
            "dipole_output_desired": True,
            "dipole_velocity_output": True,
            "timings_desired": False,
            "absorb_desired": True,
        }
    }


def slurm(inner, nodes):
    rmt = os.path.join(
        os.path.dirname(os.path.dirname(__file__)), "RMT", "build", "bin", "rmt.x"
    )
    name = jobname()
    return textwrap.dedent(
        f"""\
        #!/bin/bash

        #SBATCH --job-name={name}
        #SBATCH --account=e813

        #SBATCH --qos=standard
        #SBATCH --partition=standard

        #SBATCH --nodes={nodes}
        #SBATCH --ntasks-per-node=128
        #SBATCH --cpus-per-task=1
        #SBATCH --time=01:30:00


        export OMP_NUM_THREADS=1
        export SRUN_CPUS_PER_TASK="$SLURM_CPUS_PER_TASK"
        srun --distribution=block:block --hint=nomultithread {rmt}
    """
    )


def run(submit):
    outdir = os.path.join(os.path.dirname(__file__), dirname())
    os.makedirs(outdir, exist_ok=True)

    currentdir = os.getcwd()
    try:
        os.chdir(outdir)

        delta_t_au = 0.01
        pulse_steps, _, _ = pulse_parameters(delta_t_au)
        steps_per_run_approx = 40 * pulse_steps
        final_t_au = steps_per_run_approx * delta_t_au

        e = efield(final_t_au, delta_t_au)
        np.savetxt("EField.inp", e)

        inner = 128
        box_size_approx_au = 4000
        deltar_au = 0.08
        x_last_others = 100
        outer_approx = box_size_approx_au / deltar_au / x_last_others
        nodes = int(np.ceil((inner + outer_approx) / cpus_per_node))

        configuration = conf(inner, nodes, final_t_au, steps_per_run_approx)
        with open("input.conf", "w") as f:
            f90nml.write(configuration, f)

        submission = slurm(inner, nodes)
        with open("submit.slurm", "w") as f:
            f.write(submission)

        if not os.path.exists("d"):
            os.symlink("../../Basis-Fix/Re-Optimised-6T/d", "d")
        if not os.path.exists("d00"):
            os.symlink("../../Basis-Fix/Re-Optimised-6T/d00", "d00")
        if not os.path.exists("H"):
            os.symlink("../../Basis-Fix/Re-Optimised-6T/H", "H")
        if not os.path.exists("Splinewaves"):
            os.symlink("../../Basis-Fix/Re-Optimised-6T/Splinewaves", "Splinewaves")
        if not os.path.exists("Splinedata"):
            os.symlink("../../Basis-Fix/Re-Optimised-6T/Splinedata", "Splinedata")

        if submit:
            subprocess.run(["sbatch", "submit.slurm"], check=True)
    finally:
        os.chdir(currentdir)


def main(submit):
    run(submit)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="XUV-Only Job Generator")

    parser.add_argument("-s", "--submit", action="store_true")

    args = parser.parse_args()
    main(args.submit)
