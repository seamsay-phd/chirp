import argparse
import os
import sys

import numpy as np

from utilities import dimensions as dim
from utilities import laser, logging
from utilities.rmt import runner

STAGE_DIR = os.path.dirname(__file__)
PROJ_DIR = os.path.dirname(STAGE_DIR)


def main(submit=False, overwrite=False):
    datadir = os.path.join(PROJ_DIR, "01")

    xuv = dim.PhotonEnergy.from_eV(45.547)

    for nir_eV in (0.9, 2.1):
        delays_fs = np.array([-99, -40, -20, 20, 40, 99])
        delays_fs = np.union1d(delays_fs, np.linspace(-10, 10, 21))
        delays_fs.sort()

        if nir_eV == 0.9:
            delays_fs = np.union1d(
                delays_fs, np.linspace(-10, 10, 81)
            )  # Quarter fs precision.
            delays_fs = np.union1d(
                delays_fs, np.linspace(-20, 20, 81)
            )  # Half fs precision.
            delays_fs.sort()

        nir = dim.PhotonEnergy.from_eV(nir_eV)

        for delay_fs in delays_fs:
            delay = dim.Time.from_fs(delay_fs)
            e_field = laser.EFieldDataNIRAndXUV.gaussian(nir, xuv, delay)

            if sys.version_info.major > 3 or (
                sys.version_info.major == 3 and sys.version_info.minor >= 11
            ):
                delay_str = f"delay={delay_fs:+z07.3f}fs"
            else:
                if delay_fs == -0.0:
                    delay_fs = 0.0

                delay_str = f"delay={delay_fs:+07.3f}fs"

            if abs(delay_fs) > 20:
                time_limit = runner.TimeLimit(hours=20)
            else:
                time_limit = runner.TimeLimit(hours=10)

            logging.info(
                "Running xuv=%.3feV nir=%.3feV delay=%.3fs",
                xuv.to_eV(),
                nir.to_eV(),
                delay.to_fs(),
            )
            runner.run(
                submit,
                overwrite,
                os.path.join(
                    STAGE_DIR,
                    f"xuv={xuv.to_eV():6.3f}eV",
                    f"nir={nir.to_eV():5.3f}eV",
                    delay_str,
                ),
                datadir,
                e_field,
                time_limit=time_limit,
            )


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("-s", "--submit", action="store_true", help="submit the job")
    parser.add_argument(
        "-o",
        "--overwrite",
        action="store_true",
        help="remove and regenerate old data",
    )

    args = parser.parse_args()
    main(args.submit, args.overwrite)
