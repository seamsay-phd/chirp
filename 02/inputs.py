import os
import subprocess
import sys
import textwrap
import time
import traceback

import f90nml
import numpy as np

time_fs_per_au = 2.4188843265857e-2
energy_eV_per_au = 27.211386245988
h_eV_fs = 4.135667696
amplitude_au_per_root_100_TW_per_cm2 = 0.05336

cpus_per_node = 128
max_jobs_in_queue = 8


def dirname(inner, nodes, x_last, master_prop):
    return f"i{inner:03}-n{nodes:01}-x{x_last:04}-p{master_prop:.1}"


def jobname(inner, nodes, x_last, master_prop):
    name = dirname(inner, nodes, x_last, master_prop)
    return f"ne-lb-{name}"


def outer(inner, nodes):
    cpus = cpus_per_node * nodes
    return cpus - inner


def pulse(pulse_ts, energy_eV, amplitude_au):
    frequency_per_fs = energy_eV / h_eV_fs
    frequency_per_au = frequency_per_fs * time_fs_per_au

    t0 = pulse_ts[0]
    t = pulse_ts - t0
    f = frequency_per_au
    a = 1 / t[-1]
    fe = a / 2
    E0 = amplitude_au

    return E0 * np.sin(2 * np.pi * fe * t) ** 2 * np.sin(2 * np.pi * f * t)


def efield(final_t_au, delta_t_au):
    steps_per_run_approx = final_t_au / delta_t_au

    intensity_TW_per_cm2 = 100.0
    amplitude_au = np.sqrt(intensity_TW_per_cm2) * amplitude_au_per_root_100_TW_per_cm2
    energy_eV = 45.547

    ir_energy_eV = 1.6
    ir_period_fs = h_eV_fs / ir_energy_eV
    fwhm_fs = ir_period_fs / 4

    ts = np.arange(steps_per_run_approx) * delta_t_au

    fwhm_au = fwhm_fs / time_fs_per_au
    pulse_period_au = 4 * fwhm_au / 3

    pulse_steps_f = pulse_period_au / delta_t_au
    pulse_steps = int(pulse_steps_f)

    pulse_start = 10
    pulse_end = pulse_start + pulse_steps

    xs = np.zeros_like(ts)
    ys = np.zeros_like(ts)

    zs = np.zeros_like(ts)
    zs[:pulse_end] += 1e-90  # E Field must be non-zero at first time-step.
    zs[pulse_start:pulse_end] += pulse(
        ts[pulse_start:pulse_end], energy_eV, amplitude_au
    )

    return np.vstack([ts, xs, ys, zs]).T


def conf(
    inner,
    nodes,
    x_last_others,
    master_prop,
    final_t_au,
    steps_per_run_approx,
    delta_r=0.08,
):
    x_last_master = 4 * int(x_last_others * master_prop / 4)

    return {
        "InputData": {
            "version_root": jobname(inner, nodes, x_last_others, master_prop),
            "no_of_pes_to_use_inner": inner,
            "no_of_pes_to_use_outer": outer(inner, nodes),
            "final_t": final_t_au,
            "steps_per_run_approx": steps_per_run_approx,
            "x_last_master": x_last_master,
            "x_last_others": x_last_others,
            "adjust_gs_energy": False,
            "ml_max": 0,
            "use_field_from_file": True,
            "use_2colour_field": False,
            "frequency": 0.0,
            "intensity": 0.0,
            "frequency_xuv": 0.0,
            "intensity_xuv": 0.0,
            "keep_checkpoints": False,
            "checkpts_per_run": 1,
            "dipole_output_desired": True,
            "dipole_velocity_output": True,
            "timings_desired": True,
            "absorb_desired": True,
        }
    }


def submit(inner, nodes, x_last, master_prop, rmt):
    name = jobname(inner, nodes, x_last, master_prop)
    return textwrap.dedent(
        f"""\
            #!/bin/bash

            #SBATCH --job-name={name}
            #SBATCH --account=e813

            #SBATCH --qos=short
            #SBATCH --partition=standard

        #SBATCH --nodes={nodes}
        #SBATCH --ntasks-per-node=128
        #SBATCH --cpus-per-task=1
        #SBATCH --time=00:20:00


            export OMP_NUM_THREADS=1
            export SRUN_CPUS_PER_TASK="$SLURM_CPUS_PER_TASK"
            srun --distribution=block:block --hint=nomultithread {rmt}
        """
    )


def run(inner, nodes, x_last, master_prop, rmt):
    outdir = os.path.join(
        os.path.dirname(__file__), dirname(inner, nodes, x_last, master_prop)
    )
    if os.path.isdir(outdir):
        print(
            f"inner={inner:03} nodes={nodes:01} x_last={x_last:04} master_prop={master_prop:.1} Skipping already run job.",
            file=sys.stderr,
        )
        return

    os.makedirs(outdir, exist_ok=True)

    currentdir = os.getcwd()
    try:
        os.chdir(outdir)

        steps_per_run_approx = 2000
        delta_t_au = 0.01
        final_t_au = steps_per_run_approx * delta_t_au

        e = efield(final_t_au, delta_t_au)
        np.savetxt("EField.inp", e)

        configuration = conf(
            inner, nodes, x_last, master_prop, final_t_au, steps_per_run_approx
        )
        with open("input.conf", "w") as f:
            f90nml.write(configuration, f)

        submission = submit(inner, nodes, x_last, master_prop, rmt)
        with open("submit.slurm", "w") as f:
            f.write(submission)

        os.symlink("../../01/d", "d")
        os.symlink("../../01/d00", "d00")
        os.symlink("../../01/H", "H")
        os.symlink("../../01/Splinewaves", "Splinewaves")
        os.symlink("../../01/Splinedata", "Splinedata")

        print(
            f"inner={inner:03} nodes={nodes:01} x_last={x_last:04} master_prop={master_prop:.1} Submitting job.",
            file=sys.stderr,
        )
        subprocess.run(["sbatch", "submit.slurm"], check=True)
    finally:
        os.chdir(currentdir)


def main(rmt):
    inners = [32, 64, 128, 256]
    nodes = [1, 2, 3, 4]
    x_lasts = [100, 1000]
    master_props = [0.5, 0.8]

    for i in inners:
        for n in nodes:
            for x in x_lasts:
                for p in master_props:
                    if i >= n * cpus_per_node:
                        continue

                    while True:
                        t = subprocess.run(
                            ["squeue", "--me", "--format=%i"],
                            check=True,
                            encoding="utf8",
                            capture_output=True,
                        ).stdout
                        # If there is no final newline then the number of newlines is one less than the number of lines.
                        # Also need to unconditionally remove one to account for the header line.
                        jobs = t.count("\n") + (not t.endswith("\n")) - 1
                        if jobs < max_jobs_in_queue:
                            break

                        print(
                            f"inner={i:03} nodes={n:01} x_last={x:04} master_prop={p:.1} There are already {jobs} jobs in the queue, waiting one minute.",
                            file=sys.stderr,
                        )
                        time.sleep(60)

                    try:
                        run(i, n, x, p, rmt)
                    except Exception:
                        print(traceback.format_exc())


if __name__ == "__main__":
    main(sys.argv[1])
