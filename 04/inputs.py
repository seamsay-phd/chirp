import argparse
import json
import logging
import os
import subprocess
import sys
import textwrap
import time
import traceback

import f90nml
import numpy as np

LOG_LEVEL_MAP = {
    "DEBUG": logging.DEBUG,
    "INFO": logging.INFO,
    "WARN": logging.WARN,
    "ERROR": logging.ERROR,
}

logging.basicConfig(
    level=LOG_LEVEL_MAP[os.environ.get("LOG_LEVEL", "INFO").upper()],
    format="{asctime} {levelname:>8s} {message}",
    style="{",
    stream=sys.stderr,
)

time_fs_per_au = 2.4188843265857e-2
energy_eV_per_au = 27.211386245988
h_eV_fs = 4.135667696
amplitude_au_per_root_100_TW_per_cm2 = 0.05336

cpus_per_node = 128
max_jobs_in_queue = 8


def dirname():
    return "xuv7+ir6"


def jobname():
    dname = dirname()
    name = f"ne-chirp-{dname}"
    assert len(name) <= 18
    return name


def outer(inner, nodes):
    cpus = cpus_per_node * nodes
    return cpus - inner


def pulse_parameters(delta_t_au):
    def amplitude(intensity_100_TW_per_cm2):
        return np.sqrt(intensity_100_TW_per_cm2) * amplitude_au_per_root_100_TW_per_cm2

    delta_t_fs = delta_t_au * time_fs_per_au

    ir_intensity_100_TW_per_cm2 = 1.0 / 100
    xuv_intensity_100_TW_per_cm2 = ir_intensity_100_TW_per_cm2 / 1000
    ir_amplitude_au = amplitude(ir_intensity_100_TW_per_cm2)
    xuv_amplitude_au = amplitude(xuv_intensity_100_TW_per_cm2)

    # Energy of the autoionising state.
    xuv_energy_eV = 45.547

    ir_energy_eV = xuv_energy_eV / 29
    ir_period_fs = h_eV_fs / ir_energy_eV

    # As a rule of thumb XUV pulse is at maximum a quarter of the IR period.
    max_xuv_pulse_length_fs = ir_period_fs / 4

    xuv_period_fs = h_eV_fs / xuv_energy_eV
    xuv_cycles = int(np.floor(max_xuv_pulse_length_fs / xuv_period_fs))
    xuv_pulse_length_fs = xuv_cycles * xuv_period_fs
    xuv_pulse_fwhm_fs = xuv_pulse_length_fs * 3 / 4
    xuv_pulse_steps = int(np.round(xuv_cycles * xuv_period_fs / delta_t_fs))

    # Ding, T. et al. Time-resolved four-wave-mixing spectroscopy for inner-valence transitions. Opt. Lett., OL 41, 709–712 (2016).
    # From that paper the FWHM was sub-7fs, but that only gives 3 cycles which seems to be too short, so use 6 cycles instead (better from experimentation).
    ir_cycles = 6
    ir_pulse_length_fs = ir_period_fs * ir_cycles
    ir_pulse_fwhm_fs = 3 * ir_pulse_length_fs / 4
    ir_pulse_steps = int(np.round(ir_cycles * ir_period_fs / delta_t_fs))

    # Only chirping XUV pulse (which starts well into the IR pulse), so no need to adjust too much.
    ir_pulse_start_steps = 10
    xuv_pulse_start_steps = ir_pulse_start_steps + int(
        np.floor((ir_pulse_steps - xuv_pulse_steps) / 2)
    )

    return {
        "IR": {
            "amplitude_au": ir_amplitude_au,
            "intensity_100_TW_per_cm2": ir_intensity_100_TW_per_cm2,
            "energy_eV": ir_energy_eV,
            "length": {
                "fs": ir_pulse_length_fs,
                "au": ir_pulse_length_fs / time_fs_per_au,
                "cycles": ir_cycles,
                "steps": ir_pulse_steps,
            },
            "fwhm": {
                "fs": ir_pulse_fwhm_fs,
                "au": ir_pulse_fwhm_fs / time_fs_per_au,
            },
            "start": {"steps": ir_pulse_start_steps},
        },
        "XUV": {
            "amplitude_au": xuv_amplitude_au,
            "intensity_100_TW_per_cm2": xuv_intensity_100_TW_per_cm2,
            "energy_eV": xuv_energy_eV,
            "length": {
                "fs": xuv_pulse_length_fs,
                "au": xuv_pulse_length_fs / time_fs_per_au,
                "cycles": xuv_cycles,
                "steps": xuv_pulse_steps,
            },
            "fwhm": {
                "fs": xuv_pulse_fwhm_fs,
                "au": xuv_pulse_fwhm_fs / time_fs_per_au,
            },
            "start": {
                "steps": xuv_pulse_start_steps,
            },
        },
    }


def pulse(t, start, end, energy_eV, amplitude_au):
    frequency_per_fs = energy_eV / h_eV_fs
    frequency_per_au = frequency_per_fs * time_fs_per_au

    tm = t[(start + end) // 2]
    f = frequency_per_au

    # Time period of envelope needs to be twice the time period of pulse so you only get the first peak.
    Te = 2 * (t[end] - t[start])
    fe = 1 / Te
    E0 = amplitude_au

    envelope = np.zeros_like(t)
    envelope[start:end] = np.cos(2 * np.pi * fe * (t[start:end] - tm)) ** 2

    plane = np.zeros_like(t)
    plane[start:end] = np.cos(2 * np.pi * f * (t[start:end] - tm))

    return E0 * envelope * plane


def efield(final_t_au, delta_t_au):
    steps_per_run_approx = final_t_au / delta_t_au

    ts = np.arange(steps_per_run_approx) * delta_t_au

    xs = np.zeros_like(ts)
    ys = np.zeros_like(ts)
    zs = np.zeros_like(ts)

    parameters = pulse_parameters(delta_t_au)
    xuv_pulse_start_steps = parameters["XUV"]["start"]["steps"]
    xuv_pulse_steps = parameters["XUV"]["length"]["steps"]
    xuv_energy_eV = parameters["XUV"]["energy_eV"]
    xuv_amplitude_au = parameters["XUV"]["amplitude_au"]
    ir_pulse_start_steps = parameters["IR"]["start"]["steps"]
    ir_pulse_steps = parameters["IR"]["length"]["steps"]
    ir_energy_eV = parameters["IR"]["energy_eV"]
    ir_amplitude_au = parameters["IR"]["amplitude_au"]

    ir_pulse_end_steps = ir_pulse_start_steps + ir_pulse_steps

    xuv_pulse_end_steps = xuv_pulse_start_steps + xuv_pulse_steps

    assert (
        ir_pulse_start_steps < xuv_pulse_start_steps
        and xuv_pulse_end_steps < ir_pulse_end_steps
    )

    zs[
        : ir_pulse_start_steps + 1
    ] += 1e-90  # E Field must be non-zero at first time-step.
    zs += pulse(
        ts, ir_pulse_start_steps, ir_pulse_end_steps, ir_energy_eV, ir_amplitude_au
    )
    zs += pulse(
        ts, xuv_pulse_start_steps, xuv_pulse_end_steps, xuv_energy_eV, xuv_amplitude_au
    )

    return np.vstack([ts, xs, ys, zs]).T


def conf(inner, nodes, final_t_au, steps_per_run_approx, delta_r=0.08):
    x_last_others = 100
    master_prop = 0.8
    # Must be a multiple of 4.
    x_last_master = 4 * int(x_last_others * master_prop / 4)

    return {
        "InputData": {
            "version_root": jobname(),
            "no_of_pes_to_use_inner": inner,
            "no_of_pes_to_use_outer": outer(inner, nodes),
            "final_t": final_t_au,
            "steps_per_run_approx": steps_per_run_approx,
            "x_last_master": x_last_master,
            "x_last_others": x_last_others,
            "adjust_gs_energy": False,
            "ml_max": 0,
            "use_field_from_file": True,
            "use_2colour_field": False,
            "frequency": 0.0,
            "intensity": 0.0,
            "frequency_xuv": 0.0,
            "intensity_xuv": 0.0,
            "keep_checkpoints": False,
            "checkpts_per_run": 10,
            "dipole_output_desired": True,
            "dipole_velocity_output": True,
            "timings_desired": False,
            "absorb_desired": True,
        }
    }


def slurm(inner, nodes):
    rmt = os.path.join(
        os.path.dirname(os.path.dirname(__file__)), "RMT", "build", "bin", "rmt.x"
    )
    name = jobname()
    return textwrap.dedent(
        f"""\
        #!/bin/bash

        #SBATCH --job-name={name}
        #SBATCH --account=e813

        #SBATCH --qos=standard
        #SBATCH --partition=standard

        #SBATCH --nodes={nodes}
        #SBATCH --ntasks-per-node=128
        #SBATCH --cpus-per-task=1
        #SBATCH --time=03:00:00


        export OMP_NUM_THREADS=1
        export SRUN_CPUS_PER_TASK="$SLURM_CPUS_PER_TASK"
        srun --distribution=block:block --hint=nomultithread {rmt}
    """
    )


def run(submit):
    outdir = os.path.join(os.path.dirname(__file__), dirname())
    os.makedirs(outdir, exist_ok=True)

    currentdir = os.getcwd()
    try:
        os.chdir(outdir)

        delta_t_au = 0.01
        parameters = pulse_parameters(delta_t_au)
        with open("pulse.json", "w") as f:
            json.dump(parameters, f, indent=4)
            print(file=f)

        ir_start_steps = parameters["IR"]["start"]["steps"]
        ir_steps = parameters["IR"]["length"]["steps"]

        ringing_time = 8 * ir_steps
        steps_per_run_approx = ir_start_steps + ir_steps + ringing_time
        final_t_au = steps_per_run_approx * delta_t_au

        e = efield(final_t_au, delta_t_au)
        np.savetxt("EField.inp", e)

        inner = 128
        box_size_approx_au = 4000
        deltar_au = 0.08
        x_last_others = 100
        outer_approx = box_size_approx_au / deltar_au / x_last_others
        nodes = int(np.ceil((inner + outer_approx) / cpus_per_node))

        configuration = conf(inner, nodes, final_t_au, steps_per_run_approx)
        with open("input.conf", "w") as f:
            f90nml.write(configuration, f)

        submission = slurm(inner, nodes)
        with open("submit.slurm", "w") as f:
            f.write(submission)

        if not os.path.exists("d"):
            os.symlink("../../01/d", "d")
        if not os.path.exists("d00"):
            os.symlink("../../01/d00", "d00")
        if not os.path.exists("H"):
            os.symlink("../../01/H", "H")
        if not os.path.exists("Splinewaves"):
            os.symlink("../../01/Splinewaves", "Splinewaves")
        if not os.path.exists("Splinedata"):
            os.symlink("../../01/Splinedata", "Splinedata")

        if submit:
            subprocess.run(["sbatch", "submit.slurm"], check=True)
    finally:
        os.chdir(currentdir)


def main(submit):
    run(submit)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Chirp Job Generator - XUV+IR")

    parser.add_argument("-s", "--submit", action="store_true")

    args = parser.parse_args()
    main(args.submit)
