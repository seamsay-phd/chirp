import argparse

import numpy as np
from rmt_utilities import rmtutil, windowing

from utilities import logging


class SuperGaussian(windowing.Window):
    def window(self, length):
        t_dash = np.linspace(0, 2.5, length)
        mask = np.exp(-(t_dash**4))
        return mask


def main(directory):
    logging.info("Calculating dipole responses: %s", directory)
    calc = rmtutil.RMTCalc(directory)

    logging.info("Calculating absorption spectrum.")
    tas = calc.ATAS(window=SuperGaussian())
    logging.info("Writing absorption spectrum.")
    tas.write(fname="TAS_", units="eV")

    logging.info("Calculating absorption spectrum.")
    response = calc.dipole_response(window=SuperGaussian())
    logging.info("Writing absorption spectrum.")
    response.write(fname="response_", units="eV")

    logging.info("Finished!")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("directory")
    args = parser.parse_args()
    main(args.directory)
