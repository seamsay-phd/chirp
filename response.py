import argparse

from utilities.response import calculate

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("directory")
    args = parser.parse_args()
    calculate(args.directory)
