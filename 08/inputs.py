import argparse
import logging
import os
import re
import shutil
import subprocess
import sys
import textwrap
import time

import f90nml
import numpy as np

from utilities import dimensions as dim
from utilities import laser

LOG_LEVEL_MAP = {
    "DEBUG": logging.DEBUG,
    "INFO": logging.INFO,
    "WARN": logging.WARN,
    "ERROR": logging.ERROR,
}

logging.basicConfig(
    level=LOG_LEVEL_MAP[os.environ.get("LOG_LEVEL", "INFO").upper()],
    format="{asctime} {levelname:>8s} {message}",
    style="{",
    stream=sys.stderr,
)

STAGEDIR = os.path.dirname(__file__)
PROJDIR = os.path.dirname(STAGEDIR)


SPEED_OF_LIGHT_au = 137.03599908330932

AMPLITUDE_au_per_sqrt_INTENSITY_100_TW_per_cm2 = 0.05336
PERIOD_au_ENERGY_au = 2 * np.pi
WAVELENGTH_au_ENERGY_au = PERIOD_au_ENERGY_au * SPEED_OF_LIGHT_au


CPUS_PER_NODE = 128
MAX_JOBS_IN_QUEUE = 64

DELTA_T_au = 0.01
DELTAR_au = 0.08

BOX_SIZE_APPROX_au = 4000
X_LAST_OTHERS = 100
MASTER_PROP = 0.8

NIR_INTENSITY_100_TW_per_cm2 = 1 / 100
XUV_INTENSITY_FACTOR = 1 / 1000

INNER_CORES = 128


def not_finished_directories():
    return set(
        filter(
            lambda l: l != "WORK_DIR" and l != "",
            subprocess.run(
                ["squeue", "--me", "--format=%Z"],
                check=True,
                encoding="utf8",
                capture_output=True,
            ).stdout.split("\n"),
        )
    )


def conf(
    inner_cores,
    nodes,
    final_t_au,
    steps_per_run_approx,
    jobname,
):
    # Must be a multiple of 4.
    x_last_master = 4 * int(X_LAST_OTHERS * MASTER_PROP / 4)

    return {
        "InputData": {
            "version_root": jobname,
            "no_of_pes_to_use_inner": inner_cores,
            "no_of_pes_to_use_outer": CPUS_PER_NODE * nodes - inner_cores,
            "final_t": final_t_au,
            "steps_per_run_approx": steps_per_run_approx,
            "x_last_master": x_last_master,
            "x_last_others": X_LAST_OTHERS,
            "adjust_gs_energy": False,
            "ml_max": 0,
            "use_field_from_file": True,
            "use_2colour_field": False,
            "frequency": 0.0,
            "intensity": 0.0,
            "frequency_xuv": 0.0,
            "intensity_xuv": 0.0,
            "keep_checkpoints": False,
            "checkpts_per_run": 10,
            "dipole_output_desired": True,
            "dipole_velocity_output": True,
            "timings_desired": False,
            "absorb_desired": True,
        }
    }


def slurm(nodes, jobname):
    rmt = os.path.join(PROJDIR, "RMT", "build", "bin", "rmt.x")
    return textwrap.dedent(
        f"""\
        #!/bin/bash

        #SBATCH --job-name={jobname}
        #SBATCH --account=e813

        #SBATCH --qos=standard
        #SBATCH --partition=standard

        #SBATCH --nodes={nodes}
        #SBATCH --ntasks-per-node=128
        #SBATCH --cpus-per-task=1
        #SBATCH --time=03:00:00


        export OMP_NUM_THREADS=1
        export SRUN_CPUS_PER_TASK="$SLURM_CPUS_PER_TASK"
        srun --distribution=block:block --hint=nomultithread {rmt}
    """
    )


def spaces_in_queue():
    t = subprocess.run(
        ["squeue", "--me", "--format=%i"],
        check=True,
        encoding="utf8",
        capture_output=True,
    ).stdout

    # If there is no final newline then the number of newlines is one less than the number of lines.
    # Also need to unconditionally remove one to account for the header line.
    jobs = t.count("\n") + (not t.endswith("\n")) - 1

    return MAX_JOBS_IN_QUEUE - jobs


def run(submit: bool, overwrite: bool, e_field_data: laser.EFieldData):
    outdir = os.path.join(STAGEDIR, *e_field_data.dirname())

    # CurrentPosition should have been created if a successful calculation was run.
    if (
        os.path.exists(outdir)
        and (
            os.path.exists(os.path.join(outdir, "CurrentPosition"))
            or outdir in not_finished_directories()
        )
        and not overwrite
    ):
        logging.info(
            "Skipping calculation as RMT has already been run on directory: %s", outdir
        )
        return

    if os.path.exists(outdir):
        logging.info("Removing directory to calculate afresh: %s", outdir)
        shutil.rmtree(outdir)

    logging.info(
        "Running job with: NIR = %.3feV (Int: %.3f) | XUV = %.3feV | Delay = %.3ffs",
        e_field_data.nir.energy.to_eV(),
        e_field_data.nir.intensity.to_100_TW_per_cm2(),
        e_field_data.xuv.energy.to_eV(),
        e_field_data.delay().to_fs(),
    )

    os.makedirs(outdir)

    datadir = os.path.relpath(
        os.path.join(PROJDIR, "Basis-Fix", "Re-Optimised-6T"), outdir
    )
    currentdir = os.getcwd()
    try:
        os.chdir(outdir)
        laser.jsonify("EField.json", e_field_data)

        nir_start_steps = round(e_field_data.nir.start.to_steps(e_field_data.delta_t))
        nir_steps = round(e_field_data.nir.duration.to_steps(e_field_data.delta_t))

        ringing_time = 4 * nir_steps
        assert ringing_time > 2 * (nir_start_steps + nir_steps)
        steps_per_run_approx = ringing_time
        final_t = dim.Time.from_au(steps_per_run_approx * e_field_data.delta_t.to_au())

        e_field_data.write_inp("EField.inp", final_t)

        outer_approx = BOX_SIZE_APPROX_au / DELTAR_au / X_LAST_OTHERS
        nodes = int(np.ceil((INNER_CORES + outer_approx) / CPUS_PER_NODE))

        configuration = conf(
            INNER_CORES,
            nodes,
            final_t.to_au(),
            steps_per_run_approx,
            e_field_data.jobname(),
        )
        with open("input.conf", "w") as f:
            f90nml.write(configuration, f)

        submission = slurm(nodes, e_field_data.jobname())
        with open("submit.slurm", "w") as f:
            f.write(submission)

        os.symlink(os.path.join(datadir, "d"), "d")
        os.symlink(os.path.join(datadir, "d00"), "d00")
        os.symlink(os.path.join(datadir, "H"), "H")
        os.symlink(os.path.join(datadir, "Splinewaves"), "Splinewaves")
        os.symlink(os.path.join(datadir, "Splinedata"), "Splinedata")

        if submit:
            while (spaces := spaces_in_queue()) <= 2:
                logging.info("Sleeping for 60s as spaces left in queue is: %d", spaces)
                time.sleep(60)

            subprocess.run(["sbatch", "submit.slurm"], check=True)
    finally:
        os.chdir(currentdir)


def time_delays(min_delay, max_delay, delay_delta):
    return (
        dim.Time.from_fs(min_delay + i * delay_delta)
        for i in range(round((max_delay - min_delay) / delay_delta) + 1)
    )


def main(submit=False, overwrite=False):
    min_delay_fs = -5.0
    max_delay_fs = 5.0
    delay_delta_fs = 0.5

    for delay in time_delays(min_delay_fs, max_delay_fs, delay_delta_fs):
        # Control run, same as previous stages.
        run(
            submit,
            overwrite,
            laser.EFieldData29thHarmonic.sine_squ_29_harm(
                dim.PhotonEnergy.from_eV(45.547), delay
            ),
        )

        # Targets 1De 46.651eV state.
        run(
            submit,
            overwrite,
            laser.EFieldDataExactNIR.sine_squ_exact_nir(
                dim.PhotonEnergy.from_eV(1.423), dim.PhotonEnergy.from_eV(45.547), delay
            ),
        )

    min_delay_fs = 0.0
    max_delay_fs = 3.0
    delay_delta_fs = 0.2
    # High resolution scan for 1De 46.651eV.
    for delay in time_delays(min_delay_fs, max_delay_fs, delay_delta_fs):
        run(
            submit,
            overwrite,
            laser.EFieldDataExactNIR.sine_squ_exact_nir(
                dim.PhotonEnergy.from_eV(1.423), dim.PhotonEnergy.from_eV(45.547), delay
            ),
        )

    # Longer scan for 2s2p(6)3p and 1De 46.651eV states, to examine what happens at large time delays.
    min_delay_fs = 5.0
    max_delay_fs = 15.0
    delay_delta_fs = 0.5

    for delay in time_delays(min_delay_fs, max_delay_fs, delay_delta_fs):
        run(
            submit,
            overwrite,
            laser.EFieldData29thHarmonic.sine_squ_29_harm(
                dim.PhotonEnergy.from_eV(45.547), delay
            ),
        )

        run(
            submit,
            overwrite,
            laser.EFieldDataExactNIR.sine_squ_exact_nir(
                dim.PhotonEnergy.from_eV(1.423), dim.PhotonEnergy.from_eV(45.547), delay
            ),
        )

    # NIR energy scan to see energy dependencies.
    for nir_eV in np.linspace(1.3, 1.8, 26):
        run(
            submit,
            overwrite,
            laser.EFieldDataExactNIR.sine_squ_exact_nir(
                dim.PhotonEnergy.from_eV(nir_eV),
                dim.PhotonEnergy.from_eV(45.547),
                dim.Time.from_fs(5.0),
            ),
        )

    for intensity_factor in np.linspace(0.3, 3, 10) ** 2:
        run(
            submit,
            overwrite,
            laser.EFieldDataExactNIR.sine_squ_exact_nir(
                dim.PhotonEnergy.from_eV(1.423),
                dim.PhotonEnergy.from_eV(45.547),
                dim.Time.from_fs(5.0),
                nir_intensity=dim.EFieldIntensity.from_100_TW_per_cm2(
                    np.round(NIR_INTENSITY_100_TW_per_cm2 * intensity_factor, 3)
                ),
            ),
        )

    logging.info("All calculations started.")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("-s", "--submit", action="store_true", help="submit the job")
    parser.add_argument(
        "-o",
        "--overwrite",
        action="store_true",
        help="remove and regenerate old data",
    )

    args = parser.parse_args()
    main(args.submit, args.overwrite)
