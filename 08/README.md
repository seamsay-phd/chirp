Unchirped pulses at different NIR and XUV energies, to help disambiguate which resonances are interfering.

| XUV    | NIR     |
| ------ | ------- |
| 45.7   | 45.7/29 |
| 45.547 | 1.376   |
| 45.547 | 1.856   |
| 45.547 | 1.908   |
