import os
import subprocess

from farm_utilities import phase
from utilities import farm as ufarm

STAGE_DIR = os.path.abspath(os.path.dirname(__file__))
PROJ_DIR = os.path.dirname(os.path.dirname(STAGE_DIR))

IONISATION_ENERGY_eV = {
    "Re-Optimised": 21.285589874664446,
}


def rmatrixii():
    # RMatrixII breaks if the directory is dirty.
    for entry in os.listdir(STAGE_DIR):
        path = os.path.join(STAGE_DIR, entry)
        if entry not in ("neon.inp", "orig-2RI.inp", "run.py") and os.path.isfile(path):
            os.remove(path)

    subprocess.run(
        [
            os.path.join(PROJ_DIR, "rmatrixii.bash"),
            os.path.join(PROJ_DIR, "RMatrixII", "build", "bin"),
            os.path.join(STAGE_DIR, "neon.inp"),
            STAGE_DIR,
            os.path.join(STAGE_DIR, "rad.out"),
            os.path.join(STAGE_DIR, "ang.out"),
            os.path.join(STAGE_DIR, "ham.out"),
        ],
        check=True,
    )


def rmt():
    old_calc_dir = os.path.join(
        PROJ_DIR, "08", "xuv=45.547eV-nir=1.571eV", "delay=+05.000fs"
    )

    file = os.path.join(STAGE_DIR, "EField.inp")
    if not os.path.exists(file):
        os.symlink(
            os.path.relpath(os.path.join(old_calc_dir, "EField.inp"), STAGE_DIR),
            file,
        )

    file = os.path.join(STAGE_DIR, "EField.json")
    if not os.path.exists(file):
        os.symlink(
            os.path.relpath(os.path.join(old_calc_dir, "EField.json"), STAGE_DIR),
            file,
        )

    file = os.path.join(STAGE_DIR, "input.conf")
    if not os.path.exists(file):
        os.symlink(
            os.path.relpath(os.path.join(old_calc_dir, "input.conf"), STAGE_DIR),
            file,
        )

    file = os.path.join(STAGE_DIR, "submit.slurm")
    if not os.path.exists(file):
        os.symlink(
            os.path.relpath(os.path.join(old_calc_dir, "submit.slurm"), STAGE_DIR),
            file,
        )

    current_dir = os.getcwd()
    try:
        os.chdir(STAGE_DIR)
        subprocess.run(["sbatch", "submit.slurm"], check=True)
    finally:
        os.chdir(current_dir)


def farm():
    resonance_eV = 45.547
    ir_max_eV = 3
    # Rough ionisation energy of 2s electron, FARM craps out at this point.
    threshold_eV = 48.0

    energy_low_eV = resonance_eV - ir_max_eV
    energy_high_eV = min(resonance_eV + ir_max_eV, threshold_eV)
    energy_step_eV = 0.0001

    current_dir = os.getcwd()
    folder = os.path.basename(current_dir)
    try:
        os.chdir(STAGE_DIR)

        conf = ufarm.run(
            ufarm.configuration(
                energy_low_eV,
                energy_high_eV,
                energy_step_eV,
                1,
                1,
                1,
                ionisation_energy_eV=IONISATION_ENERGY_eV[folder],
            ),
            overwrite=True,
            executable=os.path.join(PROJ_DIR, "FARM", "build", "bin", "farm"),
        )

        with open("cpc.d", "w") as io:
            print(conf, file=io)

        energies_eV, phases = phase.read_phase_data(
            ufarm.FILES["phase"], IONISATION_ENERGY_eV[folder]
        )
        peaks = [peak["fit"] for peak in phase.fit_peaks(energies_eV, phases)]

        with open("peaks.txt", "w") as io:
            print(
                "{:21s} {:13s} {:10s}".format(
                    "Resonance Energy (eV)", "Strength", "Width (eV)"
                ),
                file=io,
            )
            for peak in peaks:
                print(
                    f"{peak.centre:21.6f} {peak.strength:13.6f} {peak.width:10.6f}",
                    file=io,
                )
    finally:
        os.chdir(current_dir)
