# Chirp

## 00

* Builds RMatrixII and RMT.
* Stores input RMatrixII input data.

## 01

* Runs RMatrixII.
* Stores RMT input data (which are outputs from RMatrixII).

## 02

* Load balancing for RMT, no data stored. 
