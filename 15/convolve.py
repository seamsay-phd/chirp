import os

import h5py
import numpy as np
import scipy as sp
from matplotlib import pyplot as plt

from utilities import dimensions as dim
from utilities import fano, laser
from utilities.rmt import packaging

STAGE_DIR = os.path.abspath(os.path.dirname(__file__))
PROJ_DIR = os.path.dirname(STAGE_DIR)

DEFAULT_FIT_KWARGS = {"maxfev": 999999}


def fit_uv_only(energy, spectrum):
    def f(
        E,
        o_m,
        o_c,
        r_2s3p,
        s_2s3p,
        p_2s3p,
        w_2s3p,
        r_2p43s3p,
        s_2p43s3p,
        p_2p43s3p,
        w_2p43s3p,
        r_2s4p,
        s_2s4p,
        p_2s4p,
        w_2s4p,
    ):
        fano_2s3p = fano.Fano(r_2s3p, s_2s3p, p_2s3p, w_2s3p)
        fano_2p43s3p = fano.Fano(r_2p43s3p, s_2p43s3p, p_2p43s3p, w_2p43s3p)
        fano_2s4p = fano.Fano(r_2s4p, s_2s4p, p_2s4p, w_2s4p)
        return fano.lineshape(E, o_m, o_c, fano_2s3p, fano_2p43s3p, fano_2s4p)

    p0 = [
        -1.296e-3,  # Offset Gradient
        -0.231,  # Offset Constant
        # 2s2p(6)3p
        45.160,  # Centre
        1.749e-3,  # Strength / Width
        0.316 * np.pi,  # Phase
        8.54e-2,  # Width
        # 2p(4)3s3p
        44.804,  # Centre
        5.978e-4,  # Strength / Width
        0.322 * np.pi,  # Phase
        8.287e-2,  # Width
        # 2s2p(6)4p
        46.705,  # Centre
        6.502e-4,  # Strength / Width
        0.336 * np.pi,  # Phase
        7.301e-2,  # Width
    ]
    lbounds = [
        -1,  # Offset Gradient
        -1,  # Offset Constant
        # 2s2p(6)3p
        45.1,  # Centre
        0,  # Strength / Width
        -np.pi,  # Phase
        0,  # Width
        # 2p(4)3s3p
        44.75,  # Centre
        0,  # Strength / Width
        -np.pi,  # Phase
        0,  # Width
        # 2s2p(6)4p
        46.65,  # Centre
        0,  # Strength / Width
        -np.pi,  # Phase
        0,  # Width
    ]
    ubounds = [
        1,  # Offset Gradient
        1,  # Offset Constant
        # 2s2p(6)3p
        45.2,  # Centre
        np.inf,  # Strength / Width
        np.pi,  # Phase
        0.1,  # Width
        # 2p(4)3s3p
        44.85,  # Centre
        np.inf,  # Strength / Width
        np.pi,  # Phase
        0.1,  # Width
        # 2s2p(6)4p
        46.75,  # Centre
        np.inf,  # Strength / Width
        np.pi,  # Phase
        0.1,  # Width
    ]

    parameters, covariance = sp.optimize.curve_fit(
        f, energy, spectrum, p0, bounds=(lbounds, ubounds), **DEFAULT_FIT_KWARGS
    )
    errors = np.sqrt(np.diag(covariance))

    (
        o_m,
        o_c,
        r_2s3p,
        s_2s3p,
        p_2s3p,
        w_2s3p,
        r_2p43s3p,
        s_2p43s3p,
        p_2p43s3p,
        w_2p43s3p,
        r_2s4p,
        s_2s4p,
        p_2s4p,
        w_2s4p,
    ) = parameters
    (
        e_om,
        e_oc,
        e_r_2s3p,
        e_s_2s3p,
        e_p_2s3p,
        e_w_2s3p,
        e_r_2p43s3p,
        e_s_2p43s3p,
        e_p_2p43s3p,
        e_w_2p43s3p,
        e_r_2s4p,
        e_s_2s4p,
        e_p_2s4p,
        e_w_2s4p,
    ) = errors

    return (
        (o_m, e_om),
        (o_c, e_oc),
        (
            fano.Fano(r_2s3p, s_2s3p, p_2s3p, w_2s3p),
            fano.FanoParameters(e_r_2s3p, e_s_2s3p, e_p_2s3p, e_w_2s3p),
        ),
        (
            fano.Fano(r_2p43s3p, s_2p43s3p, p_2p43s3p, w_2p43s3p),
            fano.FanoParameters(e_r_2p43s3p, e_s_2p43s3p, e_p_2p43s3p, e_w_2p43s3p),
        ),
        (
            fano.Fano(r_2s4p, s_2s4p, p_2s4p, w_2s4p),
            fano.FanoParameters(e_r_2s4p, e_s_2s4p, e_p_2s4p, e_w_2s4p),
        ),
    )


def fit_with_background(
    energy,
    spectrum,
    photon_spectrum,
    cross_section,
    *uv_fanos,
):
    def f(
        E,
        o_m,
        o_c,
        r_2s3p,
        s_2s3p,
        p_2s3p,
        w_2s3p,
        r_2p43s3p,
        s_2p43s3p,
        p_2p43s3p,
        w_2p43s3p,
        r_2s4p,
        s_2s4p,
        p_2s4p,
        w_2s4p,
    ):
        fano_2s3p = fano.Fano(r_2s3p, s_2s3p, p_2s3p, w_2s3p)
        fano_2p43s3p = fano.Fano(r_2p43s3p, s_2p43s3p, p_2p43s3p, w_2p43s3p)
        fano_2s4p = fano.Fano(r_2s4p, s_2s4p, p_2s4p, w_2s4p)
        return fano.background_spectrum(
            E,
            photon_spectrum,
            cross_section,
            *zip(uv_fanos, [fano_2s3p, fano_2s4p, fano_2p43s3p]),
        ) + fano.lineshape(
            E,
            o_m,
            o_c,
            fano_2s3p,
            fano_2p43s3p,
            fano_2s4p,
        )

    p0 = [
        # Offset
        -3.831e-3,  # Gradient
        -0.118,  # Constant
        # 2s2p(6)3p
        45.160,  # Centre
        1.740e-3,  # Strength / Width
        0,  # Phase
        8.344e-2,  # Width
        # 2p(4)3s3p
        44.803,  # Centre
        0.001,  # Strength / Width
        0.344 * np.pi,  # Phase
        0.1,  # Width
        # 2s2p(6)4p
        46.700,  # Centre
        6.508e-4,  # Strength / Width
        0.303 * np.pi,  # Phase
        7.301e-2,  # Width
    ]
    lbounds = [
        # Offset
        -1,  # Gradient
        -1,  # Constant
        # 2s2p(6)3p
        45.15,  # Centre
        0,  # Strength / Width
        -np.pi,  # Phase
        0,  # Width
        # 2p(4)3s3p
        44.7,  # Centre
        0,  # Strength / Width
        -np.pi,  # Phase
        0,  # Width
        # 2s2p(6)4p
        46.6,  # Centre
        0,  # Strength / Width
        -np.pi,  # Phase
        0,  # Width
    ]
    ubounds = [
        # Offset
        1,  # Gradient
        1,  # Constant
        # 2s2p(6)3p
        45.3,  # Centre
        np.inf,  # Strength / Width
        np.pi,  # Phase
        0.2,  # Width
        # 2p(4)3s3p
        44.9,  # Centre
        np.inf,  # Strength / Width
        np.pi,  # Phase
        0.2,  # Width
        # 2s2p(6)4p
        46.8,  # Centre
        np.inf,  # Strength / Width
        np.pi,  # Phase
        0.2,  # Width
    ]

    parameters, covariance = sp.optimize.curve_fit(
        f, energy, spectrum, p0, bounds=(lbounds, ubounds), **DEFAULT_FIT_KWARGS
    )
    errors = np.sqrt(np.diag(covariance))

    (
        o_m,
        o_c,
        r_2s3p,
        s_2s3p,
        p_2s3p,
        w_2s3p,
        r_2p43s3p,
        s_2p43s3p,
        p_2p43s3p,
        w_2p43s3p,
        r_2s4p,
        s_2s4p,
        p_2s4p,
        w_2s4p,
    ) = parameters
    (
        e_om,
        e_oc,
        e_r_2s3p,
        e_s_2s3p,
        e_p_2s3p,
        e_w_2s3p,
        e_r_2p43s3p,
        e_s_2p43s3p,
        e_p_2p43s3p,
        e_w_2p43s3p,
        e_r_2s4p,
        e_s_2s4p,
        e_p_2s4p,
        e_w_2s4p,
    ) = errors

    return (
        (o_m, e_om),
        (o_c, e_oc),
        (
            fano.Fano(r_2s3p, s_2s3p, p_2s3p, w_2s3p),
            fano.FanoParameters(e_r_2s3p, e_s_2s3p, e_p_2s3p, e_w_2s3p),
        ),
        (
            fano.Fano(r_2p43s3p, s_2p43s3p, p_2p43s3p, w_2p43s3p),
            fano.FanoParameters(e_r_2p43s3p, e_s_2p43s3p, e_p_2p43s3p, e_w_2p43s3p),
        ),
        (
            fano.Fano(r_2s4p, s_2s4p, p_2s4p, w_2s4p),
            fano.FanoParameters(e_r_2s4p, e_s_2s4p, e_p_2s4p, e_w_2s4p),
        ),
    )


def fit_without_background(energy, spectrum):
    def f(
        E,
        o_m,
        o_c,
        r_2s3p,
        s_2s3p,
        p_2s3p,
        w_2s3p,
        r_2p43s3p,
        s_2p43s3p,
        p_2p43s3p,
        w_2p43s3p,
        r_2s4p,
        s_2s4p,
        p_2s4p,
        w_2s4p,
    ):
        fano_2s3p = fano.Fano(r_2s3p, s_2s3p, p_2s3p, w_2s3p)
        fano_2p43s3p = fano.Fano(r_2p43s3p, s_2p43s3p, p_2p43s3p, w_2p43s3p)
        fano_2s4p = fano.Fano(r_2s4p, s_2s4p, p_2s4p, w_2s4p)
        return fano.lineshape(
            E,
            o_m,
            o_c,
            fano_2s3p,
            fano_2p43s3p,
            fano_2s4p,
        )

    p0 = [
        # Offset
        -3.831e-3,  # Gradient
        -0.118,  # Constant
        # 2s2p(6)3p
        45.160,  # Centre
        1.740e-3,  # Strength / Width
        0,  # Phase
        8.344e-2,  # Width
        # 2p(4)3s3p
        44.803,  # Centre
        0.001,  # Strength / Width
        0.344 * np.pi,  # Phase
        0.1,  # Width
        # 2s2p(6)4p
        46.700,  # Centre
        6.508e-4,  # Strength / Width
        0.303 * np.pi,  # Phase
        7.301e-2,  # Width
    ]
    lbounds = [
        # Offset
        -1,  # Gradient
        -1,  # Constant
        # 2s2p(6)3p
        45.15,  # Centre
        0,  # Strength / Width
        -np.pi,  # Phase
        0,  # Width
        # 2p(4)3s3p
        44.7,  # Centre
        0,  # Strength / Width
        0,  # Phase
        0,  # Width
        # 2s2p(6)4p
        46.6,  # Centre
        0,  # Strength / Width
        0,  # Phase
        0,  # Width
    ]
    ubounds = [
        # Offset
        1,  # Gradient
        1,  # Constant
        # 2s2p(6)3p
        45.3,  # Centre
        np.inf,  # Strength / Width
        np.pi,  # Phase
        0.2,  # Width
        # 2p(4)3s3p
        44.9,  # Centre
        np.inf,  # Strength / Width
        0.5 * np.pi,  # Phase
        0.2,  # Width
        # 2s2p(6)4p
        46.8,  # Centre
        np.inf,  # Strength / Width
        0.5 * np.pi,  # Phase
        0.2,  # Width
    ]

    parameters, covariance = sp.optimize.curve_fit(
        f, energy, spectrum, p0, bounds=(lbounds, ubounds), **DEFAULT_FIT_KWARGS
    )
    errors = np.sqrt(np.diag(covariance))

    (
        o_m,
        o_c,
        r_2s3p,
        s_2s3p,
        p_2s3p,
        w_2s3p,
        r_2p43s3p,
        s_2p43s3p,
        p_2p43s3p,
        w_2p43s3p,
        r_2s4p,
        s_2s4p,
        p_2s4p,
        w_2s4p,
    ) = parameters
    (
        e_om,
        e_oc,
        e_r_2s3p,
        e_s_2s3p,
        e_p_2s3p,
        e_w_2s3p,
        e_r_2p43s3p,
        e_s_2p43s3p,
        e_p_2p43s3p,
        e_w_2p43s3p,
        e_r_2s4p,
        e_s_2s4p,
        e_p_2s4p,
        e_w_2s4p,
    ) = errors

    return (
        (o_m, e_om),
        (o_c, e_oc),
        (
            fano.Fano(r_2s3p, s_2s3p, p_2s3p, w_2s3p),
            fano.FanoParameters(e_r_2s3p, e_s_2s3p, e_p_2s3p, e_w_2s3p),
        ),
        (
            fano.Fano(r_2p43s3p, s_2p43s3p, p_2p43s3p, w_2p43s3p),
            fano.FanoParameters(e_r_2p43s3p, e_s_2p43s3p, e_p_2p43s3p, e_w_2p43s3p),
        ),
        (
            fano.Fano(r_2s4p, s_2s4p, p_2s4p, w_2s4p),
            fano.FanoParameters(e_r_2s4p, e_s_2s4p, e_p_2s4p, e_w_2s4p),
        ),
    )


def main():
    hdf = h5py.File(os.path.join(PROJ_DIR, "13", "absorption.hdf5"))

    nir_eV = 1.423
    xuv_eV = 45.547

    e_field = laser.EFieldDataExactNIR.sine_squ_exact_nir(
        dim.PhotonEnergy.from_eV(nir_eV),
        dim.PhotonEnergy.from_eV(100),
        dim.Time.from_fs(0),
    )
    t_au = e_field.e_field(dim.Time.from_au(e_field.nir.end.to_au() * 10))[:, 0]
    nir_field = e_field.nir.pulse(t_au, e_field.delta_t)

    t_centred_fs = dim.Time.from_au(t_au).to_fs() - e_field.nir.peak.to_fs()

    cross_section_factor = 3
    cross_section = sp.interpolate.make_interp_spline(
        (t_centred_fs[1:] + t_centred_fs[:-1]) / 2,
        cross_section_factor
        * np.sqrt(sp.integrate.cumulative_trapezoid(nir_field**2, t_centred_fs)),
    )

    E_nir, s_nir = e_field.nir.spectrum()
    s_nir = s_nir[E_nir <= fano.E_NIR_MAX]
    E_nir = E_nir[E_nir <= fano.E_NIR_MAX]
    nir_spectrum = sp.interpolate.make_interp_spline(E_nir, s_nir)

    E_xuv, s_xuv = packaging.absorption(hdf, None, None, None, None)
    (
        (muv, _),
        (cuv, _),
        (fuv_2s3p, _),
        (fuv_2p43s3p, _),
        (fuv_2s4p, _),
    ) = fit_uv_only(E_xuv, s_xuv)

    d = 7
    E, s = packaging.absorption(hdf, 45.547, 1.423, d)
    (
        (m, _),
        (c, _),
        (f_2s3p, _),
        (f_2p43s3p, _),
        (f_2s4p, _),
    ) = fit_with_background(
        E,
        s,
        nir_spectrum,
        1.1 * cross_section(d),
        fuv_2s3p,
        fuv_2s4p,
        fuv_2p43s3p,
    )

    big_grid = np.linspace(40, 50, 1000)
    fig, ax = plt.subplots(1, 1)

    ax.plot(E, s, label="Data")
    ax.plot(
        E,
        fano.background_spectrum(
            E,
            nir_spectrum,
            1.1 * cross_section(d),
            (fuv_2s3p, f_2s3p),
            (fuv_2s4p, f_2s4p),
            (fuv_2p43s3p, f_2p43s3p),
        )
        - m * E
        - c,
        label="One Emission, One Absorption",
        linestyle="dotted",
    )
    ax.plot(
        E,
        fano.background_spectrum(
            E,
            nir_spectrum,
            1.1 * cross_section(d),
            (fuv_2s3p, f_2s3p),
            (fuv_2s4p, f_2s4p),
            (fuv_2p43s3p, f_2p43s3p),
        )
        + fano.background_spectrum(
            E,
            nir_spectrum,
            0.7 * cross_section(d),
            (fuv_2s3p, f_2s3p),
            (fuv_2s4p, f_2s4p),
            (fuv_2p43s3p, f_2p43s3p),
            absorptions=2,
        )
        - m * E
        - c,
        label="One Emission, One Absorption",
        linestyle="dotted",
    )
    ax.legend()

    plt.show()

    return

    delay = np.array(
        [
            float(key[:-2])
            for key in packaging.absorption(hdf, xuv_eV, nir_eV, None)[
                "by-delay"
            ].keys()
        ]
    )
    delay.sort()
    delay = delay[(0 <= delay) & (delay <= 5) & np.isclose(delay * 10 % 5, 0.0)]

    err_with_bg = np.zeros_like(delay)
    err_without_bg = np.zeros_like(delay)

    for i, d in enumerate(delay):
        E, s = packaging.absorption(hdf, xuv_eV, nir_eV, d)

        (
            (m, _),
            (c, _),
            (f_2s3p, _),
            (f_2p43s3p, _),
            (f_2s4p, _),
        ) = fit_with_background(
            E, s, nir_spectrum, cross_section(d), fuv_2s3p, fuv_2s4p
        )

        (
            (m, _),
            (c, _),
            (f_2s3p_med, _),
            (f_2p43s3p_med, _),
            (f_2s4p_med, _),
        ) = fit_with_background(
            E,
            s,
            nir_spectrum,
            cross_section(d),
            fuv_2s3p,
            fuv_2s4p,
            fuv_2p43s3p,
        )

        err_with_bg[i] = np.abs(
            fano.lineshape(E, m, c, f_2s3p, f_2p43s3p, f_2s4p)
            + fano.background_spectrum(
                E,
                nir_spectrum,
                cross_section(d),
                (fuv_2s3p, f_2s3p),
                (fuv_2s4p, f_2s4p),
            )
            - s
        ).sum()

        (
            (m_bad, _),
            (c_bad, _),
            (f_2s3p_bad, _),
            (f_2p43s3p_bad, _),
            (f_2s4p_bad, _),
        ) = fit_without_background(E, s)

        err_without_bg[i] = np.abs(
            fano.lineshape(E, m_bad, c_bad, f_2s3p_bad, f_2p43s3p_bad, f_2s4p_bad) - s
        ).sum()

        if False:
            _, ax = plt.subplots(1, 1)
            ax.plot(E, s, label="Data")
            ax.plot(
                E,
                fano.background_spectrum(
                    E,
                    nir_spectrum,
                    cross_section(d),
                    (fuv_2s3p, f_2s3p),
                    (fuv_2s4p, f_2s4p),
                )
                + fano.lineshape(
                    E,
                    m,
                    c,
                    f_2s3p,
                    f_2p43s3p,
                    f_2s4p,
                ),
                label="Fit With Background",
                linestyle="dashed",
            )
            ax.plot(
                E,
                fano.lineshape(
                    E,
                    m_bad,
                    c_bad,
                    f_2s3p_bad,
                    f_2p43s3p_bad,
                    f_2s4p_bad,
                ),
                label="Fit Without Background",
                linestyle="dashed",
            )
            ax.set_title(f"{d:.1f}fs")
            ax.legend()
        else:
            _, ax = plt.subplots(1, 1)
            ax.plot(E, s, label="Data")
            ax.plot(
                E,
                fano.background_spectrum(
                    E,
                    nir_spectrum,
                    cross_section(d),
                    (fuv_2s3p, f_2s3p),
                    (fuv_2s4p, f_2s4p),
                )
                + fano.lineshape(
                    E,
                    m,
                    c,
                    f_2s3p,
                    f_2p43s3p,
                    f_2s4p,
                ),
                label="Fit Without $2p^4 3s 3p$",
                linestyle="dashed",
            )
            ax.plot(
                E,
                fano.background_spectrum(
                    E,
                    nir_spectrum,
                    cross_section(d),
                    (fuv_2s3p, f_2s3p_med),
                    (fuv_2p43s3p, f_2p43s3p_med),
                    (fuv_2s4p, f_2s4p_med),
                )
                + fano.lineshape(
                    E,
                    m,
                    c,
                    f_2s3p_med,
                    f_2p43s3p_med,
                    f_2s4p_med,
                ),
                label="Fit With $2p^4 3s 3p$",
                linestyle="dashed",
            )
            ax.set_title(f"{d:.1f}fs")
            ax.legend()

        plt.show()

    _, ax = plt.subplots(1, 1)
    ax.plot(delay, err_without_bg, label="Error Without Background")
    ax.plot(delay, err_with_bg, label="Error With Background")
    ax.legend()
    plt.show()


if __name__ == "__main__":
    main()
