import argparse
import os

import numpy as np
import scipy as sp
from utilities import constants
from utilities import dimensions as dim
from utilities import laser
from utilities.rmt import runner

E_MAX_PLOT_eV = 10

STAGE_DIR = os.path.dirname(os.path.abspath(__file__))
PROJ_DIR = os.path.dirname(STAGE_DIR)


def nir_field(
    dt=dim.Time.from_au(0.01),
):
    E_max_ft = dim.Energy.from_au(np.pi / dt.to_au())

    N_E = round(E_max_ft.to_eV()) * 1000
    N_t = 2 * (N_E - 1)

    energy = dim.PhotonEnergy.from_eV(np.linspace(0, E_max_ft.to_eV(), N_E))
    spectrum = sp.stats.lognorm.pdf(energy.to_eV(), 0.5, loc=0.5)

    t_max = dim.Time.from_au((N_t - 1) * dt.to_au())
    full_t = dim.Time.from_au(np.linspace(0, t_max.to_au(), N_t))

    full_signal = sp.fft.ifftshift(sp.fft.irfft(spectrum, N_t))
    i0 = np.argmax(np.abs(full_signal) > 1e-9)

    signal = full_signal[i0:-i0]
    t = dim.Time.from_au(full_t.to_au()[i0:-i0] - full_t.to_au()[i0])
    # Window first and last 5%.
    i1 = len(signal) // 20
    window = np.blackman(2 * i1)

    padding = i1 // 100
    assert padding > 100

    # Offset the window to ensure field isn't zero at t=0.
    signal[: i1 - 1] *= window[1:i1]
    # Add padding to ensure the splines don't go cray.
    signal[-i1 - padding : -padding] *= window[-i1:]
    signal[-padding:] = 0

    signal /= np.max(signal)

    return t, signal


def main(submit=False, overwrite=False):
    datadir = os.path.join(PROJ_DIR, "01")

    nir_t, nir_E = nir_field()
    xuv_energy = dim.PhotonEnergy.from_eV(45.547)

    for delay in [-3, 0, 3, 5]:
        e_field = laser.EFieldDataNIRAndXUV(
            laser.PulseDataArbitrary(
                nir_t,
                nir_E,
                dim.EFieldIntensity.from_100_TW_per_cm2(
                    constants.NIR_INTENSITY_100_TW_per_cm2
                ).amplitude(),
                start=dim.Time.from_au(0.0),
                duration=dim.Time.from_au(nir_t.to_au()[-1]),
                end=dim.Time.from_au(nir_t.to_au()[-1]),
                period=dim.PhotonEnergy.from_eV(1.5).period(),
            ),
            laser.PulseDataGaussian(
                amplitude=dim.EFieldIntensity.from_100_TW_per_cm2(
                    constants.NIR_INTENSITY_100_TW_per_cm2
                    * constants.XUV_INTENSITY_FACTOR
                ).amplitude(),
                energy=xuv_energy,
                chirp=dim.Chirp.from_au(0.0),
                start=dim.Time.from_au(0.0),
                peak=dim.Time.from_fs(nir_t.to_fs()[-1] / 2 - delay),
                end=dim.Time.from_au(nir_t.to_au()[-1]),
                fwhm=dim.Time.from_cycles(
                    constants.XUV_CYCLES / 2, dim.PhotonEnergy.from_eV(45.547).period()
                ),
            ),
        )

        runner.run(
            submit,
            overwrite,
            os.path.join(STAGE_DIR, "xuv+nir", f"delay={delay:+z07.3f}fs"),
            datadir,
            e_field,
            time_limit=runner.TimeLimit(hours=15),
        )

    runner.run(
        submit,
        overwrite,
        os.path.join(STAGE_DIR, "xuv"),
        datadir,
        laser.EFieldDataXUVOnly.gaussian(xuv_energy),
        time_limit=runner.TimeLimit(hours=15),
        ringing_factor=30,
    )


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("-s", "--submit", action="store_true", help="submit the job")
    parser.add_argument(
        "-o",
        "--overwrite",
        action="store_true",
        help="remove and regenerate old data",
    )

    args = parser.parse_args()
    main(args.submit, args.overwrite)
