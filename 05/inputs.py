import argparse
import os

import numpy as np

from utilities import constants
from utilities import dimensions as dim
from utilities import laser, logging
from utilities.rmt import runner

STAGE_DIR = os.path.dirname(__file__)
PROJ_DIR = os.path.dirname(STAGE_DIR)

REFERENCE_NIR_eV = 1.423

import argparse
import json
import logging
import os
import shutil
import subprocess
import sys
import textwrap
import time

import f90nml
import numpy as np

LOG_LEVEL_MAP = {
    "DEBUG": logging.DEBUG,
    "INFO": logging.INFO,
    "WARN": logging.WARN,
    "ERROR": logging.ERROR,
}

logging.basicConfig(
    level=LOG_LEVEL_MAP[os.environ.get("LOG_LEVEL", "INFO").upper()],
    format="{asctime} {levelname:>8s} {message}",
    style="{",
    stream=sys.stderr,
)

time_fs_per_au = 2.4188843265857e-2
energy_eV_per_au = 27.211386245988
h_eV_fs = 4.135667696
c_nm_per_fs = 299.792458
amplitude_au_per_root_100_TW_per_cm2 = 0.05336

cpus_per_node = 128
max_jobs_in_queue = 64

delta_t_au = 0.01
inner_cores = 128
box_size_approx_au = 4000
deltar_au = 0.08
x_last_others = 100


def dirname(nir_chirp_au, xuv_chirp_au, delay_fs):
    return [
        f"ir_chirp_{nir_chirp_au:+09.6f}au",
        f"uv_chirp_{xuv_chirp_au:+09.6f}au",
        f"delay_{delay_fs:+010.6f}fs",
    ]


def jobname(nir_chirp_au, xuv_chirp_au, delay_fs):
    name = f"{nir_chirp_au:+06.3f}{xuv_chirp_au:+06.3f}{delay_fs:+06.3f}"

    assert len(name) <= 18, name
    return name


def outer(inner_cores, nodes):
    cpus = cpus_per_node * nodes
    return cpus - inner_cores


def pulse_parameters(delta_t_au, delay_fs, nir_chirp_au, xuv_chirp_au):
    def amplitude(intensity_100_TW_per_cm2):
        return np.sqrt(intensity_100_TW_per_cm2) * amplitude_au_per_root_100_TW_per_cm2

    delta_t_fs = delta_t_au * time_fs_per_au

    nir_intensity_100_TW_per_cm2 = 1.0 / 100
    xuv_intensity_100_TW_per_cm2 = nir_intensity_100_TW_per_cm2 / 1000
    nir_amplitude_au = amplitude(nir_intensity_100_TW_per_cm2)
    xuv_amplitude_au = amplitude(xuv_intensity_100_TW_per_cm2)

    # Energy of the auto-ionising state.
    xuv_energy_eV = 45.547

    nir_energy_eV = xuv_energy_eV / 29
    nir_period_fs = h_eV_fs / nir_energy_eV

    # As a rule of thumb XUV pulse is at maximum a quarter of the NIR period.
    max_xuv_pulse_length_fs = nir_period_fs / 4

    xuv_period_fs = h_eV_fs / xuv_energy_eV
    xuv_cycles = int(np.floor(max_xuv_pulse_length_fs / xuv_period_fs))
    xuv_pulse_length_fs = xuv_cycles * xuv_period_fs
    xuv_pulse_fwhm_fs = xuv_pulse_length_fs * 3 / 4
    xuv_pulse_steps = int(np.round(xuv_cycles * xuv_period_fs / delta_t_fs))

    # Ding, T. et al. Time-resolved four-wave-mixing spectroscopy for inner_cores-valence transitions. Opt. Lett., OL 41, 709–712 (2016).
    # From that paper the FWHM was sub-7fs, but that only gives 3 cycles which seems to be too short, so use 6 cycles instead (better from experimentation).
    nir_cycles = 6
    nir_pulse_length_fs = nir_period_fs * nir_cycles
    nir_pulse_fwhm_fs = 3 * nir_pulse_length_fs / 4
    nir_pulse_steps = int(np.round(nir_cycles * nir_period_fs / delta_t_fs))

    nir_pulse_start_steps = 20000
    nir_pulse_start_fs = nir_pulse_start_steps * delta_t_fs
    nir_pulse_middle_steps = nir_pulse_start_steps + nir_pulse_steps / 2
    nir_pulse_middle_fs = nir_pulse_middle_steps * delta_t_fs
    nir_pulse_middle_au = nir_pulse_middle_fs / time_fs_per_au

    xuv_pulse_middle_fs = nir_pulse_middle_fs - delay_fs
    xuv_pulse_middle_au = xuv_pulse_middle_fs / time_fs_per_au
    xuv_pulse_start_fs = xuv_pulse_middle_fs - xuv_pulse_length_fs / 2
    xuv_pulse_start_steps = int(np.round(xuv_pulse_start_fs / delta_t_fs))

    if xuv_chirp_au != 0.0:
        xuv_envelope_period_fs = 2 * xuv_pulse_length_fs
        T = xuv_envelope_period_fs / time_fs_per_au
        c = xuv_chirp_au
        xuv_pulse_first_root_au = xuv_pulse_middle_au + (-1 + np.sqrt(1 - c * T)) / (
            2 * c
        )
        xuv_pulse_last_root_au = xuv_pulse_middle_au + (-1 + np.sqrt(1 + c * T)) / (
            2 * c
        )
        xuv_pulse_first_root_fs = xuv_pulse_first_root_au * time_fs_per_au
        xuv_pulse_last_root_fs = xuv_pulse_last_root_au * time_fs_per_au
    else:
        xuv_pulse_first_root_fs = xuv_pulse_start_fs
        xuv_pulse_last_root_fs = xuv_pulse_start_fs + xuv_pulse_length_fs
        xuv_pulse_first_root_au = xuv_pulse_first_root_fs / time_fs_per_au
        xuv_pulse_last_root_au = xuv_pulse_last_root_fs / time_fs_per_au

    if nir_chirp_au != 0.0:
        nir_envelope_period_fs = 2 * nir_pulse_length_fs
        T = nir_envelope_period_fs / time_fs_per_au
        c = nir_chirp_au
        nir_pulse_first_root_au = nir_pulse_middle_au + (-1 + np.sqrt(1 - c * T)) / (
            2 * c
        )
        nir_pulse_last_root_au = nir_pulse_middle_au + (-1 + np.sqrt(1 + c * T)) / (
            2 * c
        )
        nir_pulse_first_root_fs = nir_pulse_first_root_au * time_fs_per_au
        nir_pulse_last_root_fs = nir_pulse_last_root_au * time_fs_per_au
    else:
        nir_pulse_first_root_fs = nir_pulse_start_fs
        nir_pulse_last_root_fs = nir_pulse_start_fs + nir_pulse_length_fs
        nir_pulse_first_root_au = nir_pulse_first_root_fs / time_fs_per_au
        nir_pulse_last_root_au = nir_pulse_last_root_fs / time_fs_per_au

    return {
        "NIR": {
            "amplitude": {"au": nir_amplitude_au},
            "intensity": {"100_TW_per_cm2": nir_intensity_100_TW_per_cm2},
            "energy": {
                "eV": nir_energy_eV,
                "au": nir_energy_eV / energy_eV_per_au,
                "fs": nir_period_fs,
                "nm": h_eV_fs * c_nm_per_fs / nir_energy_eV,
            },
            "chirp": {"au": nir_chirp_au},
            "length": {
                "fs": nir_pulse_length_fs,
                "au": nir_pulse_length_fs / time_fs_per_au,
                "cycles": nir_cycles,
                "steps": nir_pulse_steps,
            },
            "fwhm": {
                "fs": nir_pulse_fwhm_fs,
                "au": nir_pulse_fwhm_fs / time_fs_per_au,
            },
            "start": {
                "fs": nir_pulse_start_fs,
                "au": nir_pulse_start_fs / time_fs_per_au,
                "steps": nir_pulse_start_steps,
            },
            "middle": {
                "fs": nir_pulse_middle_fs,
                "au": nir_pulse_middle_au,
                "steps": nir_pulse_middle_steps,
            },
            "roots": {
                "first": {
                    "fs": nir_pulse_first_root_fs,
                    "au": nir_pulse_first_root_au,
                    "steps": nir_pulse_first_root_fs / delta_t_fs,
                },
                "last": {
                    "fs": nir_pulse_last_root_fs,
                    "au": nir_pulse_last_root_au,
                    "steps": nir_pulse_last_root_fs / delta_t_fs,
                },
            },
        },
        "XUV": {
            "amplitude": {"au": xuv_amplitude_au},
            "intensity": {"100_TW_per_cm2": xuv_intensity_100_TW_per_cm2},
            "energy": {
                "eV": xuv_energy_eV,
                "au": xuv_energy_eV / energy_eV_per_au,
                "fs": xuv_period_fs,
                "nm": h_eV_fs * c_nm_per_fs / xuv_energy_eV,
            },
            "chirp": {"au": xuv_chirp_au},
            "length": {
                "fs": xuv_pulse_length_fs,
                "au": xuv_pulse_length_fs / time_fs_per_au,
                "cycles": xuv_cycles,
                "steps": xuv_pulse_steps,
            },
            "fwhm": {
                "fs": xuv_pulse_fwhm_fs,
                "au": xuv_pulse_fwhm_fs / time_fs_per_au,
            },
            "start": {
                "fs": xuv_pulse_start_steps * delta_t_fs,
                "au": xuv_pulse_start_steps * delta_t_fs / time_fs_per_au,
                "steps": xuv_pulse_start_steps,
            },
            "middle": {
                "fs": xuv_pulse_middle_fs,
                "au": xuv_pulse_middle_au,
                "steps": xuv_pulse_middle_fs / delta_t_fs,
            },
            "roots": {
                "first": {
                    "fs": xuv_pulse_first_root_fs,
                    "au": xuv_pulse_first_root_au,
                    "steps": xuv_pulse_first_root_fs / delta_t_fs,
                },
                "last": {
                    "fs": xuv_pulse_last_root_fs,
                    "au": xuv_pulse_last_root_au,
                    "steps": xuv_pulse_last_root_fs / delta_t_fs,
                },
            },
        },
    }


def pulse(t, start, end, energy_eV, amplitude_au, chirp_au):
    frequency_per_fs = energy_eV / h_eV_fs
    frequency_per_au = frequency_per_fs * time_fs_per_au

    c = chirp_au

    tm = t[(start + end) // 2]
    t0 = t - tm

    f = frequency_per_au * (1 + c * t0)

    # Time period of envelope needs to be twice the time period of pulse so you only get the first peak.
    Te = 2 * (t[end] - t[start])
    envelope_frequency_per_au = 1 / Te
    fe = envelope_frequency_per_au * (1 + c * t0)

    assert (
        abs(c) <= envelope_frequency_per_au
    ), f"Chirp: {c} | Envelope Freq: {envelope_frequency_per_au}"

    E0 = amplitude_au

    # envelope = cos(2pi fe t0)^2 .
    # envelope = 0 when 2pi fe t0 = pi (n + 1/2) .

    # First 0 at +ve t0 occurs when n = 0 , so fe t0 = 1/4 .
    # fe = (1 + c t0) / Te .
    # c t0^2 + t0 - Te/4 = 0 .
    # t0 = (-1 ± sqrt(1 + c Te)) / 2c .
    # Specified +ve t0, so t0 = (-1 + sqrt(1 + c Te)) / 2c .

    # First 0 at -ve t0 occurs when n = -1, so fe t0 = -1/4 .
    # c t0^2 + t0 + Te/4 = 0 .
    # t0 = (-1 ± sqrt(1 - c Te)) / 2c .
    # For all valid choices of c, sqrt(1 - c Te) will be +ve.
    # We want the _first_ negative t0, so choose the +ve choice.
    # Therefore t0 = (-1 + sqrt(1 - c Te)) / 2c .

    # NOTE: If chirp is expanded to arbitrary functions, we can instead to an optimisation looking for the first minimum either direction.

    if c != 0.0:
        t0n = (-1 + np.sqrt(1 - c * Te)) / (2 * c)
        t0p = (-1 + np.sqrt(1 + c * Te)) / (2 * c)

        estart = np.argmin((t0 - t0n) ** 2)
        eend = np.argmin((t0 - t0p) ** 2)
    else:
        estart = start
        eend = end

    assert np.all(fe[estart:eend] > 0)
    assert np.all(f[estart:eend] > 0)

    envelope = np.zeros_like(t)
    envelope[estart:eend] = np.cos(2 * np.pi * fe[estart:eend] * t0[estart:eend]) ** 2

    plane = np.zeros_like(t)
    plane[estart:eend] = np.cos(2 * np.pi * f[estart:eend] * t0[estart:eend])

    return E0 * envelope * plane


def efield(final_t_au, delta_t_au, nir_chirp_au, xuv_chirp_au, delay_fs):
    steps_per_run_approx = final_t_au / delta_t_au

    ts = np.arange(steps_per_run_approx) * delta_t_au

    xs = np.zeros_like(ts)
    ys = np.zeros_like(ts)
    zs = np.zeros_like(ts)

    parameters = pulse_parameters(delta_t_au, delay_fs, nir_chirp_au, xuv_chirp_au)
    xuv_pulse_start_steps = parameters["XUV"]["start"]["steps"]
    xuv_pulse_steps = parameters["XUV"]["length"]["steps"]
    xuv_energy_eV = parameters["XUV"]["energy"]["eV"]
    xuv_amplitude_au = parameters["XUV"]["amplitude"]["au"]
    nir_pulse_start_steps = parameters["NIR"]["start"]["steps"]
    nir_pulse_steps = parameters["NIR"]["length"]["steps"]
    nir_energy_eV = parameters["NIR"]["energy"]["eV"]
    nir_amplitude_au = parameters["NIR"]["amplitude"]["au"]

    nir_pulse_end_steps = nir_pulse_start_steps + nir_pulse_steps

    xuv_pulse_end_steps = xuv_pulse_start_steps + xuv_pulse_steps

    assert (
        nir_pulse_start_steps <= xuv_pulse_start_steps
        and xuv_pulse_end_steps <= nir_pulse_end_steps
    )

    zs[
        : nir_pulse_start_steps + 1
    ] += 1e-90  # E Field must be non-zero at first time-step.
    zs += pulse(
        ts,
        nir_pulse_start_steps,
        nir_pulse_end_steps,
        nir_energy_eV,
        nir_amplitude_au,
        nir_chirp_au,
    )
    zs += pulse(
        ts,
        xuv_pulse_start_steps,
        xuv_pulse_end_steps,
        xuv_energy_eV,
        xuv_amplitude_au,
        xuv_chirp_au,
    )

    return np.vstack([ts, xs, ys, zs]).T


def conf(
    inner_cores,
    nodes,
    final_t_au,
    steps_per_run_approx,
    nir_chirp_au,
    xuv_chirp_au,
    delay_fs,
):
    x_last_others = 100
    master_prop = 0.8
    # Must be a multiple of 4.
    x_last_master = 4 * int(x_last_others * master_prop / 4)

    return {
        "InputData": {
            "version_root": jobname(nir_chirp_au, xuv_chirp_au, delay_fs),
            "no_of_pes_to_use_inner": inner_cores,
            "no_of_pes_to_use_outer": outer(inner_cores, nodes),
            "final_t": final_t_au,
            "steps_per_run_approx": steps_per_run_approx,
            "x_last_master": x_last_master,
            "x_last_others": x_last_others,
            "adjust_gs_energy": False,
            "ml_max": 0,
            "use_field_from_file": True,
            "use_2colour_field": False,
            "frequency": 0.0,
            "intensity": 0.0,
            "frequency_xuv": 0.0,
            "intensity_xuv": 0.0,
            "keep_checkpoints": False,
            "checkpts_per_run": 10,
            "dipole_output_desired": True,
            "dipole_velocity_output": True,
            "timings_desired": False,
            "absorb_desired": True,
        }
    }


def slurm(nodes, nir_chirp_au, xuv_chirp_au, delay_fs, projdir):
    rmt = os.path.join(projdir, "RMT", "build", "bin", "rmt.x")
    name = jobname(nir_chirp_au, xuv_chirp_au, delay_fs)
    return textwrap.dedent(
        f"""\
        #!/bin/bash

        #SBATCH --job-name={name}
        #SBATCH --account=e813

        #SBATCH --qos=standard
        #SBATCH --partition=standard

        #SBATCH --nodes={nodes}
        #SBATCH --ntasks-per-node=128
        #SBATCH --cpus-per-task=1
        #SBATCH --time=03:00:00


        export OMP_NUM_THREADS=1
        export SRUN_CPUS_PER_TASK="$SLURM_CPUS_PER_TASK"
        srun --distribution=block:block --hint=nomultithread {rmt}
    """
    )


def space_in_queue():
    t = subprocess.run(
        ["squeue", "--me", "--format=%i"],
        check=True,
        encoding="utf8",
        capture_output=True,
    ).stdout

    # If there is no final newline then the number of newlines is one less than the number of lines.
    # Also need to unconditionally remove one to account for the header line.
    jobs = t.count("\n") + (not t.endswith("\n")) - 1

    return jobs <= max_jobs_in_queue


def run(submit, overwrite, nir_chirp_au, xuv_chirp_au, delay_fs):
    stagedir = os.path.dirname(__file__)
    projdir = os.path.dirname(stagedir)
    outdir = os.path.join(stagedir, *dirname(nir_chirp_au, xuv_chirp_au, delay_fs))

    if os.path.exists(outdir) and not overwrite:
        logging.info("Directory `%s` already exists, skipping.", outdir)
        return

    if os.path.exists(outdir):
        logging.info("Directory `%s` exists, overwriting.", outdir)
        shutil.rmtree(outdir)

    logging.info(
        "Running job with: NIR Chirp = %.6fau | XUV Chirp = %.6fau | Delay = %.6ffs",
        nir_chirp_au,
        xuv_chirp_au,
        delay_fs,
    )

    os.makedirs(outdir)

    datadir = os.path.relpath(os.path.join(projdir, "01"), outdir)
    currentdir = os.getcwd()
    try:
        os.chdir(outdir)

        parameters = pulse_parameters(delta_t_au, delay_fs, nir_chirp_au, xuv_chirp_au)
        with open("pulse.json", "w") as f:
            json.dump(parameters, f, sort_keys=True, indent=4)
            print(file=f)

        nir_start_steps = parameters["NIR"]["start"]["steps"]
        nir_steps = parameters["NIR"]["length"]["steps"]

        ringing_time = 8 * nir_steps
        steps_per_run_approx = nir_start_steps + nir_steps + ringing_time
        final_t_au = steps_per_run_approx * delta_t_au

        e = efield(final_t_au, delta_t_au, nir_chirp_au, xuv_chirp_au, delay_fs)
        np.savetxt("EField.inp", e)

        outer_approx = box_size_approx_au / deltar_au / x_last_others
        nodes = int(np.ceil((inner_cores + outer_approx) / cpus_per_node))

        configuration = conf(
            inner_cores,
            nodes,
            final_t_au,
            steps_per_run_approx,
            nir_chirp_au,
            xuv_chirp_au,
            delay_fs,
        )
        with open("input.conf", "w") as f:
            f90nml.write(configuration, f)

        submission = slurm(nodes, nir_chirp_au, xuv_chirp_au, delay_fs, projdir)
        with open("submit.slurm", "w") as f:
            f.write(submission)

        os.symlink(os.path.join(datadir, "d"), "d")
        os.symlink(os.path.join(datadir, "d00"), "d00")
        os.symlink(os.path.join(datadir, "H"), "H")
        os.symlink(os.path.join(datadir, "Splinewaves"), "Splinewaves")
        os.symlink(os.path.join(datadir, "Splinedata"), "Splinedata")

        if submit:
            while not space_in_queue():
                time.sleep(60)

            subprocess.run(["sbatch", "submit.slurm"], check=True)
    finally:
        os.chdir(currentdir)


def scan(
    submit,
    overwrite,
    nir_chirp_au,
    xuv_chirp_au,
    delay_delta_fs,
    num_delays,
    min_delay_fs,
    max_delay_fs,
):
    pp = pulse_parameters(delta_t_au, 0.0, 0.0, 0.0)

    nir_pulse_length_fs = pp["NIR"]["length"]["fs"]
    xuv_pulse_length_fs = pp["XUV"]["length"]["fs"]

    max_valid_time_delay_fs = nir_pulse_length_fs / 2 - 1.01 * xuv_pulse_length_fs / 2

    argument_exception = ValueError(
        "Valid combos are: [delay_delta_fs, num_delays] [delay_delta_fs, num_delays, min_delay_fs] [delay_delta_fs, num_delays, max_delay_fs] [delay_delta_fs, min_delay_fs, max_delay_fs] [num_delays min_delay_fs max_delay_fs]"
    )
    if (
        delay_delta_fs is not None
        and num_delays is not None
        and min_delay_fs is not None
        and max_delay_fs is not None
    ):
        raise argument_exception
    elif (
        delay_delta_fs is not None
        and num_delays is not None
        and min_delay_fs is not None
    ):
        pass
    elif (
        delay_delta_fs is not None
        and num_delays is not None
        and max_delay_fs is not None
    ):
        min_delay_fs = max_delay_fs - delay_delta_fs * (num_delays - 1)
    elif (
        delay_delta_fs is not None
        and min_delay_fs is not None
        and max_delay_fs is not None
    ):
        num_delays = int(np.floor((max_delay_fs - min_delay_fs) / delay_delta_fs)) + 1
    elif (
        num_delays is not None and min_delay_fs is not None and max_delay_fs is not None
    ):
        if num_delays <= 1:
            delay_delta_fs = float("inf")
        else:
            delay_delta_fs = (max_delay_fs - min_delay_fs) / (num_delays - 1)
    elif delay_delta_fs is not None and num_delays is not None:
        min_delay_fs = -delay_delta_fs * (num_delays // 2)
        if num_delays % 2 == 0:
            min_delay_fs += delay_delta_fs / 2
    else:
        raise argument_exception

    for i in range(num_delays):
        delay_fs = min_delay_fs + i * delay_delta_fs
        assert abs(delay_fs) <= max_valid_time_delay_fs, delay_fs
        run(submit, overwrite, nir_chirp_au, xuv_chirp_au, delay_fs)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Chirp Job Generator - Time Delay Scan"
    )

    parser.add_argument("-s", "--submit", action="store_true", help="submit the job")
    parser.add_argument(
        "-o", "--overwrite", action="store_true", help="delete old data and re-run job"
    )
    parser.add_argument(
        "-u",
        "--xuv-chirp-au",
        type=float,
        default=0.0,
        help="value of chirp (in a.u.) to use for the XUV pulse",
    )
    parser.add_argument(
        "-i",
        "--nir-chirp-au",
        type=float,
        default=0.0,
        help="value of chirp (in a.u.) to use for the NIR pulse",
    )

    subparsers = parser.add_subparsers()

    delay_parser = subparsers.add_parser("delay", help="run a single time delay")
    delay_parser.add_argument(
        "delay_fs", type=float, help="the time delay (in fs) to use"
    )

    scan_parser = subparsers.add_parser("scan", help="run a time delay scan")
    scan_parser.add_argument(
        "-d",
        "--delay-delta-fs",
        type=float,
        help="the step size (in fs) between time delays",
    )
    scan_parser.add_argument(
        "-n", "--num-delays", type=int, help="the number of time delays to run"
    )
    scan_parser.add_argument(
        "-s", "--min-delay-fs", type=float, help="minimum time delay to use, in fs"
    )
    scan_parser.add_argument(
        "-e", "--max-delay-fs", type=float, help="maximum time delay to use, in fs"
    )

    args = parser.parse_args()

    if hasattr(args, "delay_fs"):
        run(
            args.submit,
            args.overwrite,
            args.nir_chirp_au,
            args.xuv_chirp_au,
            args.delay_fs,
        )
    elif hasattr(args, "delay_delta_fs"):
        scan(
            args.submit,
            args.overwrite,
            args.nir_chirp_au,
            args.xuv_chirp_au,
            args.delay_delta_fs,
            args.num_delays,
            args.min_delay_fs,
            args.max_delay_fs,
        )
    else:
        parser.print_help()
