def main [...paths: path] {
    let paths = if ($paths | length) == 0 {
        [($env.FILE_PWD | path dirname)]
    } else {
        $paths
    }

    isort --extend-skip RMT --profile black ...$paths
    black --extend-exclude 'RMT' ...$paths
}